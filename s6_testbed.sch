<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.4">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="6" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="5" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="12" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="25" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="13" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="12" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="12" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="6" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="no" active="no"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="no" active="no"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="no" active="no"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="tGTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="bGTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="prix" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="test" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="Ports" color="7" fill="1" visible="yes" active="yes"/>
<layer number="134" name="Port2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="135" name="Port3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="4" fill="9" visible="no" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="supply1">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MondoCPLDCore-XC95_Rev1.5">
<packages>
<package name="OSC1">
<description>5BY7OSCILLATORSMD</description>
<wire x1="-1.1" y1="5.3" x2="-1.1" y2="-1.1" width="0.1" layer="21"/>
<wire x1="-1.1" y1="-1.1" x2="6.1" y2="-1.1" width="0.1" layer="21"/>
<wire x1="6.1" y1="5.3" x2="6.1" y2="-1.1" width="0.1" layer="21"/>
<wire x1="-1.1" y1="5.3" x2="6.1" y2="5.3" width="0.1" layer="21"/>
<circle x="1.212" y="-0.787" radius="0.142" width="0" layer="21"/>
<smd name="1@1" x="0" y="0" dx="1.8" dy="2" layer="1"/>
<smd name="2@2" x="5.08" y="0" dx="1.8" dy="2" layer="1"/>
<smd name="3@3" x="5.08" y="4.2" dx="1.8" dy="2" layer="1"/>
<smd name="4@4" x="0" y="4.2" dx="1.8" dy="2" layer="1"/>
<text x="-1.14" y="5.65" size="2" layer="21" font="vector" ratio="13">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="OSC1">
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-5.08" width="0.3" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.3" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.3" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.3" layer="94"/>
<text x="-6.485" y="-3.29" size="1.5" layer="94" font="vector" ratio="10">OE</text>
<text x="1.73" y="-3.29" size="1.5" layer="94" font="vector" ratio="10">GND</text>
<text x="-6.485" y="1.79" size="1.5" layer="94" font="vector" ratio="10">VCC</text>
<text x="1.73" y="1.79" size="1.5" layer="94" font="vector" ratio="10">Out</text>
<text x="-6.985" y="5.715" size="2" layer="95" font="vector" ratio="13">&gt;NAME</text>
<text x="-6.985" y="-7.62" size="2" layer="96" font="vector" ratio="13">&gt;VALUE</text>
<pin name="OE" x="-10.16" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="GND" x="10.16" y="-2.54" visible="off" length="short" direction="pwr" rot="R180"/>
<pin name="VCC" x="-10.16" y="2.54" visible="off" length="short" direction="out"/>
<pin name="OUT" x="10.16" y="2.54" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="OSC1" prefix="OSC" uservalue="yes">
<gates>
<gate name="G$1" symbol="OSC1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="OSC1">
<connects>
<connect gate="G$1" pin="GND" pad="2@2"/>
<connect gate="G$1" pin="OE" pad="1@1"/>
<connect gate="G$1" pin="OUT" pad="3@3"/>
<connect gate="G$1" pin="VCC" pad="4@4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="GadgetFactory">
<packages>
<package name="0201">
<description>Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<smd name="1" x="-0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<smd name="2" x="0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<text x="-0.4" y="0.6" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-0.4" y="-1.4" size="0.8128" layer="27" font="vector" ratio="16">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="0.15" layer="21"/>
</package>
<package name="0402">
<description>IPC 1005 chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.05" y1="0.4" x2="-1" y2="0.45" width="0" layer="39"/>
<wire x1="-1" y1="0.45" x2="1" y2="0.45" width="0" layer="39"/>
<wire x1="1" y1="0.45" x2="1.05" y2="0.4" width="0" layer="39"/>
<wire x1="1.05" y1="0.4" x2="1.05" y2="-0.4" width="0" layer="39"/>
<wire x1="1.05" y1="-0.4" x2="1" y2="-0.45" width="0" layer="39"/>
<wire x1="1" y1="-0.45" x2="-1" y2="-0.45" width="0" layer="39"/>
<wire x1="-1" y1="-0.45" x2="-1.05" y2="-0.4" width="0" layer="39"/>
<wire x1="-1.05" y1="-0.4" x2="-1.05" y2="0.4" width="0" layer="39"/>
<wire x1="-1.1" y1="0.5" x2="-0.3" y2="0.5" width="0.127" layer="21"/>
<wire x1="0.3" y1="0.5" x2="1.1" y2="0.5" width="0.127" layer="21"/>
<wire x1="1.1" y1="0.5" x2="1.1" y2="-0.5" width="0.127" layer="21"/>
<wire x1="1.1" y1="-0.5" x2="0.3" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-0.3" y1="-0.5" x2="-1.1" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-1.1" y1="-0.5" x2="-1.1" y2="0.5" width="0.127" layer="21"/>
<wire x1="-0.7" y1="0.1" x2="-0.5" y2="-0.1" width="0" layer="49"/>
<wire x1="-0.7" y1="-0.1" x2="-0.5" y2="0.1" width="0" layer="49"/>
<wire x1="0.5" y1="0.1" x2="0.7" y2="-0.1" width="0" layer="49"/>
<wire x1="0.5" y1="-0.1" x2="0.7" y2="0.1" width="0" layer="49"/>
<wire x1="-0.2" y1="0.3" x2="0.2" y2="0.3" width="0" layer="41"/>
<wire x1="0.2" y1="0.3" x2="0.2" y2="-0.3" width="0" layer="41"/>
<wire x1="0.2" y1="-0.3" x2="-0.2" y2="-0.3" width="0" layer="41"/>
<wire x1="-0.2" y1="-0.3" x2="-0.2" y2="0.3" width="0" layer="41"/>
<smd name="1" x="-0.6" y="0" dx="0.6" dy="0.6" layer="1" roundness="20"/>
<smd name="2" x="0.6" y="0" dx="0.6" dy="0.6" layer="1" roundness="20"/>
<text x="-1.1" y="0.8" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-1" y="0.35" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-1" y="-0.4" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="1" y="0.4" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="1" y="-0.35" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="0402S">
<wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0.1" layer="51"/>
<wire x1="-0.5" y1="0.25" x2="0.5" y2="0.25" width="0.1" layer="51"/>
<wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0.1" layer="51"/>
<wire x1="0.5" y1="-0.25" x2="-0.5" y2="-0.25" width="0.1" layer="51"/>
<wire x1="0" y1="-0.2" x2="0" y2="0.2" width="0.127" layer="21"/>
<wire x1="-0.8" y1="0.45" x2="0.8" y2="0.45" width="0" layer="39"/>
<wire x1="0.8" y1="0.45" x2="0.8" y2="-0.45" width="0" layer="39"/>
<wire x1="0.8" y1="-0.45" x2="-0.8" y2="-0.45" width="0" layer="39"/>
<wire x1="-0.8" y1="-0.45" x2="-0.8" y2="0.45" width="0" layer="39"/>
<wire x1="-0.85" y1="0.5" x2="-0.2" y2="0.5" width="0.127" layer="21"/>
<wire x1="0.2" y1="0.5" x2="0.85" y2="0.5" width="0.127" layer="21"/>
<wire x1="0.85" y1="0.5" x2="0.85" y2="-0.5" width="0.127" layer="21"/>
<wire x1="0.85" y1="-0.5" x2="0.2" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-0.2" y1="-0.5" x2="-0.85" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-0.85" y1="-0.5" x2="-0.85" y2="0.5" width="0.127" layer="21"/>
<smd name="1" x="-0.4" y="0" dx="0.6" dy="0.5" layer="1" roundness="20" rot="R90"/>
<smd name="2" x="0.4" y="0" dx="0.6" dy="0.5" layer="1" roundness="20" rot="R90"/>
<text x="-0.85" y="0.7" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-0.75" y="0.35" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-0.75" y="-0.4" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="0.75" y="0.4" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="0.75" y="-0.35" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.2" x2="-0.2" y2="0.2" layer="51"/>
<rectangle x1="0.2" y1="-0.2" x2="0.5" y2="0.2" layer="51"/>
</package>
<package name="0504">
<description>IPC 1310 chip</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-1.2" y="1.1" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-1.3" y="-1.9" size="0.8128" layer="27" font="vector" ratio="16">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="0603">
<description>IPC 1608 chip</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.45" y1="0.65" x2="1.45" y2="0.65" width="0" layer="39"/>
<wire x1="1.45" y1="0.65" x2="1.45" y2="-0.65" width="0" layer="39"/>
<wire x1="1.45" y1="-0.65" x2="-1.45" y2="-0.65" width="0" layer="39"/>
<wire x1="-1.45" y1="-0.65" x2="-1.45" y2="0.65" width="0" layer="39"/>
<wire x1="-1.5" y1="0.7" x2="-0.3" y2="0.7" width="0.127" layer="21"/>
<wire x1="0.3" y1="0.7" x2="1.5" y2="0.7" width="0.127" layer="21"/>
<wire x1="1.5" y1="0.7" x2="1.5" y2="-0.7" width="0.127" layer="21"/>
<wire x1="1.5" y1="-0.7" x2="0.3" y2="-0.7" width="0.127" layer="21"/>
<wire x1="-0.3" y1="-0.7" x2="-1.5" y2="-0.7" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-0.7" x2="-1.5" y2="0.7" width="0.127" layer="21"/>
<wire x1="-0.9" y1="0.1" x2="-0.7" y2="-0.1" width="0" layer="49"/>
<wire x1="-0.9" y1="-0.1" x2="-0.7" y2="0.1" width="0" layer="49"/>
<wire x1="0.7" y1="0.1" x2="0.9" y2="-0.1" width="0" layer="49"/>
<wire x1="0.7" y1="-0.1" x2="0.9" y2="0.1" width="0" layer="49"/>
<wire x1="-0.2" y1="0.4" x2="0.2" y2="0.4" width="0" layer="41"/>
<wire x1="0.2" y1="0.4" x2="0.2" y2="-0.4" width="0" layer="41"/>
<wire x1="0.2" y1="-0.4" x2="-0.2" y2="-0.4" width="0" layer="41"/>
<wire x1="-0.2" y1="-0.4" x2="-0.2" y2="0.4" width="0" layer="41"/>
<smd name="1" x="-0.8" y="0" dx="1" dy="1" layer="1" roundness="20"/>
<smd name="2" x="0.8" y="0" dx="1" dy="1" layer="1" roundness="20"/>
<text x="-1.5" y="0.9" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-1.4" y="0.55" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-1.4" y="-0.6" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="1.4" y="0.6" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="1.4" y="-0.55" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="0805">
<description>IPC 2012 chip</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.75" y1="0.85" x2="1.75" y2="0.85" width="0" layer="39"/>
<wire x1="1.75" y1="0.85" x2="1.75" y2="-0.85" width="0" layer="39"/>
<wire x1="1.75" y1="-0.85" x2="-1.75" y2="-0.85" width="0" layer="39"/>
<wire x1="-1.75" y1="-0.85" x2="-1.75" y2="0.85" width="0" layer="39"/>
<wire x1="-1.8" y1="0.9" x2="-0.4" y2="0.9" width="0.127" layer="21"/>
<wire x1="0.4" y1="0.9" x2="1.8" y2="0.9" width="0.127" layer="21"/>
<wire x1="1.8" y1="0.9" x2="1.8" y2="-0.9" width="0.127" layer="21"/>
<wire x1="1.8" y1="-0.9" x2="0.4" y2="-0.9" width="0.127" layer="21"/>
<wire x1="-0.4" y1="-0.9" x2="-1.8" y2="-0.9" width="0.127" layer="21"/>
<wire x1="-1.8" y1="-0.9" x2="-1.8" y2="0.9" width="0.127" layer="21"/>
<wire x1="-1.1" y1="0.1" x2="-0.9" y2="-0.1" width="0" layer="49"/>
<wire x1="-1.1" y1="-0.1" x2="-0.9" y2="0.1" width="0" layer="49"/>
<wire x1="0.9" y1="0.1" x2="1.1" y2="-0.1" width="0" layer="49"/>
<wire x1="0.9" y1="-0.1" x2="1.1" y2="0.1" width="0" layer="49"/>
<wire x1="-0.3" y1="0.7" x2="0.3" y2="0.7" width="0" layer="41"/>
<wire x1="0.3" y1="0.7" x2="0.3" y2="-0.7" width="0" layer="41"/>
<wire x1="0.3" y1="-0.7" x2="-0.3" y2="-0.7" width="0" layer="41"/>
<wire x1="-0.3" y1="-0.7" x2="-0.3" y2="0.7" width="0" layer="41"/>
<smd name="1" x="-1" y="0" dx="1.2" dy="1.4" layer="1" roundness="20"/>
<smd name="2" x="1" y="0" dx="1.2" dy="1.4" layer="1" roundness="20"/>
<text x="-1.8" y="1.2" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-1.7" y="0.75" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-1.7" y="-0.8" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="1.7" y="0.8" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="1.7" y="-0.75" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="01005">
<smd name="1" x="-1.27" y="0" dx="1.27" dy="1.27" layer="1"/>
<smd name="2" x="1.27" y="0" dx="1.27" dy="1.27" layer="1"/>
</package>
<package name="1206">
<description>IPC 3216 chip</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.35" y1="1.05" x2="2.35" y2="1.05" width="0" layer="39"/>
<wire x1="2.35" y1="1.05" x2="2.35" y2="-1.05" width="0" layer="39"/>
<wire x1="2.35" y1="-1.05" x2="-2.35" y2="-1.05" width="0" layer="39"/>
<wire x1="-2.35" y1="-1.05" x2="-2.35" y2="1.05" width="0" layer="39"/>
<wire x1="-2.4" y1="1.1" x2="-0.6" y2="1.1" width="0.127" layer="21"/>
<wire x1="0.6" y1="1.1" x2="2.4" y2="1.1" width="0.127" layer="21"/>
<wire x1="2.4" y1="1.1" x2="2.4" y2="-1.1" width="0.127" layer="21"/>
<wire x1="2.4" y1="-1.1" x2="0.6" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-0.6" y1="-1.1" x2="-2.4" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-2.4" y1="-1.1" x2="-2.4" y2="1.1" width="0.127" layer="21"/>
<wire x1="-1.5" y1="0.1" x2="-1.3" y2="-0.1" width="0" layer="49"/>
<wire x1="-1.5" y1="-0.1" x2="-1.3" y2="0.1" width="0" layer="49"/>
<wire x1="1.3" y1="0.1" x2="1.5" y2="-0.1" width="0" layer="49"/>
<wire x1="1.3" y1="-0.1" x2="1.5" y2="0.1" width="0" layer="49"/>
<wire x1="-0.5" y1="0.9" x2="0.5" y2="0.9" width="0" layer="41"/>
<wire x1="0.5" y1="0.9" x2="0.5" y2="-0.9" width="0" layer="41"/>
<wire x1="0.5" y1="-0.9" x2="-0.5" y2="-0.9" width="0" layer="41"/>
<wire x1="-0.5" y1="-0.9" x2="-0.5" y2="0.9" width="0" layer="41"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1" roundness="20"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1" roundness="20"/>
<text x="-2.4" y="1.4" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-2.3" y="0.95" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-2.3" y="-1" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="2.3" y="1" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="2.3" y="-0.95" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="1210">
<description>IPC 3225 chip</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.35" y1="1.45" x2="2.35" y2="1.45" width="0" layer="39"/>
<wire x1="2.35" y1="1.45" x2="2.35" y2="-1.45" width="0" layer="39"/>
<wire x1="2.35" y1="-1.45" x2="-2.35" y2="-1.45" width="0" layer="39"/>
<wire x1="-2.35" y1="-1.45" x2="-2.35" y2="1.45" width="0" layer="39"/>
<wire x1="-2.4" y1="1.5" x2="2.4" y2="1.5" width="0.127" layer="21"/>
<wire x1="2.4" y1="1.5" x2="2.4" y2="-1.5" width="0.127" layer="21"/>
<wire x1="2.4" y1="-1.5" x2="-2.4" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-2.4" y1="-1.5" x2="-2.4" y2="1.5" width="0.127" layer="21"/>
<wire x1="-0.5" y1="1.3" x2="0.5" y2="1.3" width="0" layer="41"/>
<wire x1="0.5" y1="1.3" x2="0.5" y2="-1.3" width="0" layer="41"/>
<wire x1="0.5" y1="-1.3" x2="-0.5" y2="-1.3" width="0" layer="41"/>
<wire x1="-0.5" y1="-1.3" x2="-0.5" y2="1.3" width="0" layer="41"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.6" layer="1" roundness="20"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.6" layer="1" roundness="20"/>
<text x="-2.4" y="1.8" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-2.3" y="1.35" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-2.3" y="-1.4" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="2.3" y="1.4" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="2.3" y="-1.35" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="1806">
<description>IPC 4516 chip</description>
<smd name="1" x="-1.27" y="0" dx="1.27" dy="1.27" layer="1"/>
<smd name="2" x="1.27" y="0" dx="1.27" dy="1.27" layer="1"/>
</package>
<package name="1812">
<description>IPC 4532 chip</description>
<wire x1="-3.05" y1="1.85" x2="3.05" y2="1.85" width="0" layer="39"/>
<wire x1="3.05" y1="-1.85" x2="-3.05" y2="-1.85" width="0" layer="39"/>
<wire x1="-3.05" y1="-1.85" x2="-3.05" y2="1.85" width="0" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="3.05" y1="1.85" x2="3.05" y2="-1.85" width="0" layer="39"/>
<wire x1="-3.1" y1="1.9" x2="3.1" y2="1.9" width="0.127" layer="21"/>
<wire x1="3.1" y1="1.9" x2="3.1" y2="-1.9" width="0.127" layer="21"/>
<wire x1="3.1" y1="-1.9" x2="-3.1" y2="-1.9" width="0.127" layer="21"/>
<wire x1="-3.1" y1="-1.9" x2="-3.1" y2="1.9" width="0.127" layer="21"/>
<wire x1="-1" y1="1.7" x2="1" y2="1.7" width="0" layer="41"/>
<wire x1="1" y1="1.7" x2="1" y2="-1.7" width="0" layer="41"/>
<wire x1="1" y1="-1.7" x2="-1" y2="-1.7" width="0" layer="41"/>
<wire x1="-1" y1="-1.7" x2="-1" y2="1.7" width="0" layer="41"/>
<smd name="1" x="-2" y="0" dx="1.8" dy="3.4" layer="1" roundness="20"/>
<smd name="2" x="2" y="0" dx="1.8" dy="3.4" layer="1" roundness="20"/>
<text x="-3.1" y="2.2" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-3" y="1.75" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-3" y="-1.8" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="3" y="1.8" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="3" y="-1.75" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="1825">
<description>IPC 4564 chip</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-2.9" y="3.7" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-2.9" y="-4.6" size="0.8128" layer="27" font="vector" ratio="16">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="2010">
<description>IPC 5025 chip</description>
<wire x1="-3.25" y1="1.45" x2="3.25" y2="1.45" width="0" layer="39"/>
<wire x1="3.25" y1="1.45" x2="3.25" y2="-1.45" width="0" layer="39"/>
<wire x1="3.25" y1="-1.45" x2="-3.25" y2="-1.45" width="0" layer="39"/>
<wire x1="-3.25" y1="-1.45" x2="-3.25" y2="1.45" width="0" layer="39"/>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.3" y1="1.5" x2="3.3" y2="1.5" width="0.127" layer="21"/>
<wire x1="3.3" y1="1.5" x2="3.3" y2="-1.5" width="0.127" layer="21"/>
<wire x1="3.3" y1="-1.5" x2="-3.3" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-3.3" y1="-1.5" x2="-3.3" y2="1.5" width="0.127" layer="21"/>
<wire x1="-1.2" y1="1.3" x2="1.2" y2="1.3" width="0" layer="41"/>
<wire x1="1.2" y1="1.3" x2="1.2" y2="-1.3" width="0" layer="41"/>
<wire x1="1.2" y1="-1.3" x2="-1.2" y2="-1.3" width="0" layer="41"/>
<wire x1="-1.2" y1="-1.3" x2="-1.2" y2="1.3" width="0" layer="41"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.6" layer="1" roundness="20"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.6" layer="1" roundness="20"/>
<text x="-3.3" y="1.8" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-3.2" y="1.35" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-3.2" y="-1.4" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="3.2" y="1.4" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="3.2" y="-1.35" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="2512">
<description>IPC 6332 chip</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="3.9" y1="1.8" x2="3.9" y2="-1.8" width="0.127" layer="21"/>
<wire x1="3.9" y1="-1.8" x2="-3.9" y2="-1.8" width="0.127" layer="21"/>
<wire x1="-3.9" y1="-1.8" x2="-3.9" y2="1.8" width="0.127" layer="21"/>
<wire x1="-3.9" y1="1.8" x2="3.9" y2="1.8" width="0.127" layer="21"/>
<wire x1="-1.7" y1="1.3" x2="-1.5" y2="1.5" width="0" layer="41"/>
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0" layer="41"/>
<wire x1="1.5" y1="1.5" x2="1.7" y2="1.3" width="0" layer="41"/>
<wire x1="1.7" y1="1.3" x2="1.7" y2="-1.3" width="0" layer="41"/>
<wire x1="1.7" y1="-1.3" x2="1.5" y2="-1.5" width="0" layer="41"/>
<wire x1="1.5" y1="-1.5" x2="-1.5" y2="-1.5" width="0" layer="41"/>
<wire x1="-1.5" y1="-1.5" x2="-1.7" y2="-1.3" width="0" layer="41"/>
<wire x1="-1.7" y1="-1.3" x2="-1.7" y2="1.3" width="0" layer="41"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1" roundness="20"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1" roundness="20"/>
<text x="-3.9" y="2.1" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-3.8" y="1.65" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-3.8" y="-1.7" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="3.8" y="1.7" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="3.8" y="-1.65" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="3616">
<smd name="1" x="-1.27" y="0" dx="1.27" dy="1.27" layer="1"/>
<smd name="2" x="1.27" y="0" dx="1.27" dy="1.27" layer="1"/>
</package>
<package name="4022">
<smd name="1" x="-1.27" y="0" dx="1.27" dy="1.27" layer="1"/>
<smd name="2" x="1.27" y="0" dx="1.27" dy="1.27" layer="1"/>
</package>
<package name="TANT_A">
<wire x1="-1.6" y1="1.2" x2="1.6" y2="1.2" width="0.1016" layer="51"/>
<wire x1="1.6" y1="1.2" x2="1.6" y2="-1.2" width="0.1016" layer="51"/>
<wire x1="1.6" y1="-1.2" x2="-1.6" y2="-1.2" width="0.1016" layer="51"/>
<wire x1="-1.6" y1="-1.2" x2="-1.6" y2="1.2" width="0.1016" layer="51"/>
<wire x1="2.5" y1="0" x2="2.8" y2="0" width="0.127" layer="21"/>
<wire x1="2.8" y1="0" x2="3.1" y2="0" width="0.127" layer="21"/>
<wire x1="2.8" y1="0.3" x2="2.8" y2="0" width="0.127" layer="21"/>
<wire x1="2.8" y1="0" x2="2.8" y2="-0.3" width="0.127" layer="21"/>
<smd name="+" x="1.4" y="0" dx="1.8" dy="2.2" layer="1" roundness="20"/>
<smd name="-" x="-1.4" y="0" dx="1.8" dy="2.2" layer="1" roundness="20"/>
<text x="-1.6" y="1.5" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-1.6" y="-2.3" size="0.8128" layer="27" font="vector" ratio="16">&gt;VALUE</text>
<rectangle x1="-1.75" y1="-0.6" x2="-1.625" y2="0.6" layer="51"/>
<rectangle x1="1.625" y1="-0.6" x2="1.75" y2="0.6" layer="51"/>
<rectangle x1="0.95" y1="-1.225" x2="1.25" y2="1.225" layer="51"/>
</package>
<package name="SO08">
<description>&lt;b&gt;Small Outline Package&lt;/b&gt; Fits JEDEC packages (narrow SOIC-8)</description>
<wire x1="-2.362" y1="-1.803" x2="2.362" y2="-1.803" width="0.1524" layer="51"/>
<wire x1="2.362" y1="-1.803" x2="2.362" y2="1.803" width="0.1524" layer="21"/>
<wire x1="2.362" y1="1.803" x2="-2.362" y2="1.803" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="1.803" x2="-2.362" y2="-1.803" width="0.1524" layer="21"/>
<circle x="-1.8034" y="-0.9906" radius="0.1436" width="0.2032" layer="21"/>
<smd name="1" x="-1.905" y="-2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="2" x="-0.635" y="-2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="3" x="0.635" y="-2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="4" x="1.905" y="-2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="5" x="1.905" y="2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="6" x="0.635" y="2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="7" x="-0.635" y="2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="8" x="-1.905" y="2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<text x="-1.27" y="-0.635" size="0.4064" layer="27">&gt;VALUE</text>
<text x="-1.27" y="0" size="0.4064" layer="25">&gt;NAME</text>
<rectangle x1="-2.0828" y1="-2.8702" x2="-1.7272" y2="-1.8542" layer="51"/>
<rectangle x1="-0.8128" y1="-2.8702" x2="-0.4572" y2="-1.8542" layer="51"/>
<rectangle x1="0.4572" y1="-2.8702" x2="0.8128" y2="-1.8542" layer="51"/>
<rectangle x1="1.7272" y1="-2.8702" x2="2.0828" y2="-1.8542" layer="51"/>
<rectangle x1="-2.0828" y1="1.8542" x2="-1.7272" y2="2.8702" layer="51"/>
<rectangle x1="-0.8128" y1="1.8542" x2="-0.4572" y2="2.8702" layer="51"/>
<rectangle x1="0.4572" y1="1.8542" x2="0.8128" y2="2.8702" layer="51"/>
<rectangle x1="1.7272" y1="1.8542" x2="2.0828" y2="2.8702" layer="51"/>
</package>
<package name="SO08-EIAJ">
<description>Fits EIAJ packages (wide version of the SOIC-8).</description>
<wire x1="-2.362" y1="-2.565" x2="2.362" y2="-2.565" width="0.1524" layer="51"/>
<wire x1="2.362" y1="-2.565" x2="2.362" y2="2.5396" width="0.1524" layer="21"/>
<wire x1="2.362" y1="2.5396" x2="-2.362" y2="2.5396" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="2.5396" x2="-2.362" y2="-2.565" width="0.1524" layer="21"/>
<circle x="-1.8034" y="-1.7526" radius="0.1436" width="0.2032" layer="21"/>
<smd name="1" x="-1.905" y="-3.3782" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="2" x="-0.635" y="-3.3782" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="3" x="0.635" y="-3.3782" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="4" x="1.905" y="-3.3782" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="5" x="1.905" y="3.3528" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="6" x="0.635" y="3.3528" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="7" x="-0.635" y="3.3528" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="8" x="-1.905" y="3.3528" dx="0.6096" dy="2.2098" layer="1"/>
<text x="-1.27" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-0.762" size="0.4064" layer="25">&gt;NAME</text>
<rectangle x1="-2.0828" y1="-3.6322" x2="-1.7272" y2="-2.6162" layer="51"/>
<rectangle x1="-0.8128" y1="-3.6322" x2="-0.4572" y2="-2.6162" layer="51"/>
<rectangle x1="0.4572" y1="-3.6322" x2="0.8128" y2="-2.6162" layer="51"/>
<rectangle x1="1.7272" y1="-3.6322" x2="2.0828" y2="-2.6162" layer="51"/>
<rectangle x1="-2.0828" y1="2.5908" x2="-1.7272" y2="3.6068" layer="51"/>
<rectangle x1="-0.8128" y1="2.5908" x2="-0.4572" y2="3.6068" layer="51"/>
<rectangle x1="0.4572" y1="2.5908" x2="0.8128" y2="3.6068" layer="51"/>
<rectangle x1="1.7272" y1="2.5908" x2="2.0828" y2="3.6068" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="R-EU">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<circle x="-3.2385" y="-0.4445" radius="0.0635" width="0" layer="94"/>
<text x="-2.54" y="1.524" size="1.27" layer="95" font="vector">&gt;NAME</text>
<text x="-2.54" y="-2.794" size="1.27" layer="96" font="vector">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="POWER-0.030=1/32W">
<text x="-1.9685" y="-0.4445" size="0.889" layer="94" font="vector">1/32W</text>
</symbol>
<symbol name="POWER-0.050=1/20W">
<text x="-1.9685" y="-0.4445" size="0.889" layer="94" font="vector">1/20W</text>
</symbol>
<symbol name="POWER-0.063=1/16W">
<text x="-1.9685" y="-0.4445" size="0.889" layer="94" font="vector">1/16W</text>
</symbol>
<symbol name="POWER-0.100=1/10W">
<text x="-1.9685" y="-0.4445" size="0.889" layer="94" font="vector">1/10W</text>
</symbol>
<symbol name="POWER-0.125=1/8W">
<text x="-1.524" y="-0.4445" size="0.889" layer="94" font="vector">1/8W</text>
</symbol>
<symbol name="POWER-0.167=1/6W">
<text x="-1.524" y="-0.4445" size="0.889" layer="94" font="vector">1/6W</text>
</symbol>
<symbol name="POWER-0.200=1/5W">
<text x="-1.524" y="-0.4445" size="0.889" layer="94" font="vector">1/5W</text>
</symbol>
<symbol name="POWER-0.250=1/4W">
<text x="-1.524" y="-0.4445" size="0.889" layer="94" font="vector">1/4W</text>
</symbol>
<symbol name="POWER-0.333=1/3W">
<text x="-1.524" y="-0.4445" size="0.889" layer="94" font="vector">1/3W</text>
</symbol>
<symbol name="POWER-0.500=1/2W">
<text x="-1.524" y="-0.4445" size="0.889" layer="94" font="vector">1/2W</text>
</symbol>
<symbol name="POWER-0.750=3/4W">
<text x="-1.524" y="-0.4445" size="0.889" layer="94" font="vector">3/4W</text>
</symbol>
<symbol name="POWER-1W">
<text x="-0.6985" y="-0.4445" size="0.889" layer="94" font="vector">1W</text>
</symbol>
<symbol name="POWER-2W">
<text x="-0.6985" y="-0.4445" size="0.889" layer="94" font="vector">2W</text>
</symbol>
<symbol name="POWER-3W">
<text x="-0.6985" y="-0.4445" size="0.889" layer="94" font="vector">3W</text>
</symbol>
<symbol name="POWER-4W">
<text x="-0.6985" y="-0.4445" size="0.889" layer="94" font="vector">4W</text>
</symbol>
<symbol name="POWER-5W">
<text x="-0.6985" y="-0.4445" size="0.889" layer="94" font="vector">5W</text>
</symbol>
<symbol name="POWER-10W">
<text x="-1.1429" y="-0.4445" size="0.889" layer="94" font="vector">10W</text>
</symbol>
<symbol name="POWER-15W">
<text x="-1.1429" y="-0.4445" size="0.889" layer="94" font="vector">15W</text>
</symbol>
<symbol name="POWER-20W">
<text x="-1.1429" y="-0.4445" size="0.889" layer="94" font="vector">20W</text>
</symbol>
<symbol name="POWER-25W">
<text x="-1.1429" y="-0.4445" size="0.889" layer="94" font="vector">25W</text>
</symbol>
<symbol name="POWER-35W">
<text x="-1.1429" y="-0.4445" size="0.889" layer="94" font="vector">35W</text>
</symbol>
<symbol name="POWER-40W">
<text x="-1.1429" y="-0.4445" size="0.889" layer="94" font="vector">40W</text>
</symbol>
<symbol name="POWER-100W">
<text x="-1.524" y="-0.4445" size="0.889" layer="94" font="vector">100W</text>
</symbol>
<symbol name="POWER-50W">
<text x="-1.1429" y="-0.4445" size="0.889" layer="94" font="vector">50W</text>
</symbol>
<symbol name="C-EU">
<wire x1="-2.54" y1="0" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="0.762" y2="0" width="0.1524" layer="94"/>
<circle x="-1.4605" y="-0.4445" radius="0.0635" width="0" layer="94"/>
<text x="-2.54" y="2.032" size="1.27" layer="95" font="vector">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.27" layer="96" font="vector">&gt;VALUE</text>
<rectangle x1="-0.762" y1="-1.524" x2="-0.254" y2="1.524" layer="94"/>
<rectangle x1="0.254" y1="-1.524" x2="0.762" y2="1.524" layer="94"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="SPI_FLASH">
<wire x1="-10.16" y1="5.08" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="5.08" x2="-10.16" y2="5.08" width="0.254" layer="94"/>
<text x="-5.08" y="7.62" size="1.27" layer="95" rot="R180">&gt;NAME</text>
<text x="-10.16" y="-10.16" size="1.27" layer="96">&gt;VALUE</text>
<pin name="CS" x="-15.24" y="2.54" length="middle"/>
<pin name="MISO" x="-15.24" y="0" length="middle"/>
<pin name="WP" x="-15.24" y="-2.54" length="middle"/>
<pin name="GND" x="-15.24" y="-5.08" length="middle"/>
<pin name="VCC" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="HOLD" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="SCK" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="MOSI" x="15.24" y="-5.08" length="middle" rot="R180"/>
</symbol>
<symbol name="1V2">
<wire x1="0" y1="1.905" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.778" y="3.175" size="1.397" layer="96">&gt;VALUE</text>
<pin name="1V2" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="A" symbol="R-EU" x="0" y="0"/>
<gate name="G$1" symbol="POWER-0.030=1/32W" x="-10.16" y="22.86" addlevel="request"/>
<gate name="G$2" symbol="POWER-0.050=1/20W" x="-5.08" y="22.86" addlevel="request"/>
<gate name="G$3" symbol="POWER-0.063=1/16W" x="0" y="22.86" addlevel="request"/>
<gate name="G$4" symbol="POWER-0.100=1/10W" x="5.08" y="22.86" addlevel="request"/>
<gate name="G$5" symbol="POWER-0.125=1/8W" x="10.16" y="22.86" addlevel="request"/>
<gate name="G$6" symbol="POWER-0.167=1/6W" x="15.24" y="22.86" addlevel="request"/>
<gate name="G$7" symbol="POWER-0.200=1/5W" x="-10.16" y="17.78" addlevel="request"/>
<gate name="G$8" symbol="POWER-0.250=1/4W" x="-5.08" y="17.78" addlevel="request"/>
<gate name="G$9" symbol="POWER-0.333=1/3W" x="0" y="17.78" addlevel="request"/>
<gate name="G$10" symbol="POWER-0.500=1/2W" x="5.08" y="17.78" addlevel="request"/>
<gate name="G$11" symbol="POWER-0.750=3/4W" x="10.16" y="17.78" addlevel="request"/>
<gate name="G$12" symbol="POWER-1W" x="15.24" y="17.78" addlevel="request"/>
<gate name="G$13" symbol="POWER-2W" x="-10.16" y="12.7" addlevel="request"/>
<gate name="G$14" symbol="POWER-3W" x="-5.08" y="12.7" addlevel="request"/>
<gate name="G$15" symbol="POWER-4W" x="0" y="12.7" addlevel="request"/>
<gate name="G$16" symbol="POWER-5W" x="5.08" y="12.7" addlevel="request"/>
<gate name="G$17" symbol="POWER-10W" x="10.16" y="12.7" addlevel="request"/>
<gate name="G$18" symbol="POWER-15W" x="15.24" y="12.7" addlevel="request"/>
<gate name="G$19" symbol="POWER-20W" x="-10.16" y="7.62" addlevel="request"/>
<gate name="G$20" symbol="POWER-25W" x="-5.08" y="7.62" addlevel="request"/>
<gate name="G$21" symbol="POWER-35W" x="0" y="7.62" addlevel="request"/>
<gate name="G$22" symbol="POWER-40W" x="5.08" y="7.62" addlevel="request"/>
<gate name="G$23" symbol="POWER-100W" x="15.24" y="7.62" addlevel="request"/>
<gate name="G$24" symbol="POWER-50W" x="10.16" y="7.62" addlevel="request"/>
</gates>
<devices>
<device name="0201" package="0201">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402S" package="0402S">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0504" package="0504">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="01005" package="01005">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1806" package="1806">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="1812">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1825" package="1825">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="2010">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="2512">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3616" package="3616">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4022" package="4022">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C" prefix="C" uservalue="yes">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="A" symbol="C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="0201" package="0201">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402S" package="0402S">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0504" package="0504">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="01005" package="01005">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1806" package="1806">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="1812">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1825" package="1825">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="2010">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="2512">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3616" package="3616">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4022" package="4022">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="A" package="TANT_A">
<connects>
<connect gate="A" pin="1" pad="-"/>
<connect gate="A" pin="2" pad="+"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FLASH-SPI-25XX">
<description>SPI Flash</description>
<gates>
<gate name="G$1" symbol="SPI_FLASH" x="0" y="0"/>
</gates>
<devices>
<device name="SMD" package="SO08">
<connects>
<connect gate="G$1" pin="CS" pad="1"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="HOLD" pad="7"/>
<connect gate="G$1" pin="MISO" pad="2"/>
<connect gate="G$1" pin="MOSI" pad="5"/>
<connect gate="G$1" pin="SCK" pad="6"/>
<connect gate="G$1" pin="VCC" pad="8"/>
<connect gate="G$1" pin="WP" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD1" package="SO08-EIAJ">
<connects>
<connect gate="G$1" pin="CS" pad="1"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="HOLD" pad="7"/>
<connect gate="G$1" pin="MISO" pad="2"/>
<connect gate="G$1" pin="MOSI" pad="5"/>
<connect gate="G$1" pin="SCK" pad="6"/>
<connect gate="G$1" pin="VCC" pad="8"/>
<connect gate="G$1" pin="WP" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1V2" prefix="1V2_PWR">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="1V2" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Bogdan_Commons">
<packages>
<package name="0201@1">
<description>Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<smd name="1" x="-0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<smd name="2" x="0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<text x="-0.4" y="0.6" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-0.4" y="-1.4" size="0.8128" layer="27" font="vector" ratio="16">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="0.15" layer="21"/>
</package>
<package name="0402@1">
<description>IPC 1005 chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.05" y1="0.4" x2="-1" y2="0.45" width="0" layer="39"/>
<wire x1="-1" y1="0.45" x2="1" y2="0.45" width="0" layer="39"/>
<wire x1="1" y1="0.45" x2="1.05" y2="0.4" width="0" layer="39"/>
<wire x1="1.05" y1="0.4" x2="1.05" y2="-0.4" width="0" layer="39"/>
<wire x1="1.05" y1="-0.4" x2="1" y2="-0.45" width="0" layer="39"/>
<wire x1="1" y1="-0.45" x2="-1" y2="-0.45" width="0" layer="39"/>
<wire x1="-1" y1="-0.45" x2="-1.05" y2="-0.4" width="0" layer="39"/>
<wire x1="-1.05" y1="-0.4" x2="-1.05" y2="0.4" width="0" layer="39"/>
<wire x1="-1.1" y1="0.5" x2="-0.3" y2="0.5" width="0.127" layer="21"/>
<wire x1="0.3" y1="0.5" x2="1.1" y2="0.5" width="0.127" layer="21"/>
<wire x1="1.1" y1="0.5" x2="1.1" y2="-0.5" width="0.127" layer="21"/>
<wire x1="1.1" y1="-0.5" x2="0.3" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-0.3" y1="-0.5" x2="-1.1" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-1.1" y1="-0.5" x2="-1.1" y2="0.5" width="0.127" layer="21"/>
<wire x1="-0.7" y1="0.1" x2="-0.5" y2="-0.1" width="0" layer="49"/>
<wire x1="-0.7" y1="-0.1" x2="-0.5" y2="0.1" width="0" layer="49"/>
<wire x1="0.5" y1="0.1" x2="0.7" y2="-0.1" width="0" layer="49"/>
<wire x1="0.5" y1="-0.1" x2="0.7" y2="0.1" width="0" layer="49"/>
<wire x1="-0.2" y1="0.3" x2="0.2" y2="0.3" width="0" layer="41"/>
<wire x1="0.2" y1="0.3" x2="0.2" y2="-0.3" width="0" layer="41"/>
<wire x1="0.2" y1="-0.3" x2="-0.2" y2="-0.3" width="0" layer="41"/>
<wire x1="-0.2" y1="-0.3" x2="-0.2" y2="0.3" width="0" layer="41"/>
<smd name="1" x="-0.6" y="0" dx="0.6" dy="0.6" layer="1" roundness="20"/>
<smd name="2" x="0.6" y="0" dx="0.6" dy="0.6" layer="1" roundness="20"/>
<text x="-1.1" y="0.8" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-1" y="0.35" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-1" y="-0.4" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="1" y="0.4" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="1" y="-0.35" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="0504">
<description>IPC 1310 chip</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-1.2" y="1.1" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-1.3" y="-1.9" size="0.8128" layer="27" font="vector" ratio="16">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="0603@1">
<description>IPC 1608 chip</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.45" y1="0.65" x2="1.45" y2="0.65" width="0" layer="39"/>
<wire x1="1.45" y1="0.65" x2="1.45" y2="-0.65" width="0" layer="39"/>
<wire x1="1.45" y1="-0.65" x2="-1.45" y2="-0.65" width="0" layer="39"/>
<wire x1="-1.45" y1="-0.65" x2="-1.45" y2="0.65" width="0" layer="39"/>
<wire x1="-1.5" y1="0.7" x2="-0.3" y2="0.7" width="0.127" layer="21"/>
<wire x1="0.3" y1="0.7" x2="1.5" y2="0.7" width="0.127" layer="21"/>
<wire x1="1.5" y1="0.7" x2="1.5" y2="-0.7" width="0.127" layer="21"/>
<wire x1="1.5" y1="-0.7" x2="0.3" y2="-0.7" width="0.127" layer="21"/>
<wire x1="-0.3" y1="-0.7" x2="-1.5" y2="-0.7" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-0.7" x2="-1.5" y2="0.7" width="0.127" layer="21"/>
<wire x1="-0.9" y1="0.1" x2="-0.7" y2="-0.1" width="0" layer="49"/>
<wire x1="-0.9" y1="-0.1" x2="-0.7" y2="0.1" width="0" layer="49"/>
<wire x1="0.7" y1="0.1" x2="0.9" y2="-0.1" width="0" layer="49"/>
<wire x1="0.7" y1="-0.1" x2="0.9" y2="0.1" width="0" layer="49"/>
<wire x1="-0.2" y1="0.4" x2="0.2" y2="0.4" width="0" layer="41"/>
<wire x1="0.2" y1="0.4" x2="0.2" y2="-0.4" width="0" layer="41"/>
<wire x1="0.2" y1="-0.4" x2="-0.2" y2="-0.4" width="0" layer="41"/>
<wire x1="-0.2" y1="-0.4" x2="-0.2" y2="0.4" width="0" layer="41"/>
<smd name="1" x="-0.8" y="0" dx="1" dy="1" layer="1" roundness="20"/>
<smd name="2" x="0.8" y="0" dx="1" dy="1" layer="1" roundness="20"/>
<text x="-1.5" y="0.9" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-1.4" y="0.55" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-1.4" y="-0.6" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="1.4" y="0.6" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="1.4" y="-0.55" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="0805@1">
<description>IPC 2012 chip</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.75" y1="0.85" x2="1.75" y2="0.85" width="0" layer="39"/>
<wire x1="1.75" y1="0.85" x2="1.75" y2="-0.85" width="0" layer="39"/>
<wire x1="1.75" y1="-0.85" x2="-1.75" y2="-0.85" width="0" layer="39"/>
<wire x1="-1.75" y1="-0.85" x2="-1.75" y2="0.85" width="0" layer="39"/>
<wire x1="-1.8" y1="0.9" x2="-0.4" y2="0.9" width="0.127" layer="21"/>
<wire x1="0.4" y1="0.9" x2="1.8" y2="0.9" width="0.127" layer="21"/>
<wire x1="1.8" y1="0.9" x2="1.8" y2="-0.9" width="0.127" layer="21"/>
<wire x1="1.8" y1="-0.9" x2="0.4" y2="-0.9" width="0.127" layer="21"/>
<wire x1="-0.4" y1="-0.9" x2="-1.8" y2="-0.9" width="0.127" layer="21"/>
<wire x1="-1.8" y1="-0.9" x2="-1.8" y2="0.9" width="0.127" layer="21"/>
<wire x1="-1.1" y1="0.1" x2="-0.9" y2="-0.1" width="0" layer="49"/>
<wire x1="-1.1" y1="-0.1" x2="-0.9" y2="0.1" width="0" layer="49"/>
<wire x1="0.9" y1="0.1" x2="1.1" y2="-0.1" width="0" layer="49"/>
<wire x1="0.9" y1="-0.1" x2="1.1" y2="0.1" width="0" layer="49"/>
<wire x1="-0.3" y1="0.7" x2="0.3" y2="0.7" width="0" layer="41"/>
<wire x1="0.3" y1="0.7" x2="0.3" y2="-0.7" width="0" layer="41"/>
<wire x1="0.3" y1="-0.7" x2="-0.3" y2="-0.7" width="0" layer="41"/>
<wire x1="-0.3" y1="-0.7" x2="-0.3" y2="0.7" width="0" layer="41"/>
<smd name="1" x="-1" y="0" dx="1.2" dy="1.4" layer="1" roundness="20"/>
<smd name="2" x="1" y="0" dx="1.2" dy="1.4" layer="1" roundness="20"/>
<text x="-1.8" y="1.2" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-1.7" y="0.75" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-1.7" y="-0.8" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="1.7" y="0.8" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="1.7" y="-0.75" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="01005">
<smd name="1" x="-1.27" y="0" dx="1.27" dy="1.27" layer="1"/>
<smd name="2" x="1.27" y="0" dx="1.27" dy="1.27" layer="1"/>
</package>
<package name="1206@1">
<description>IPC 3216 chip</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.35" y1="1.05" x2="2.35" y2="1.05" width="0" layer="39"/>
<wire x1="2.35" y1="1.05" x2="2.35" y2="-1.05" width="0" layer="39"/>
<wire x1="2.35" y1="-1.05" x2="-2.35" y2="-1.05" width="0" layer="39"/>
<wire x1="-2.35" y1="-1.05" x2="-2.35" y2="1.05" width="0" layer="39"/>
<wire x1="-2.4" y1="1.1" x2="-0.6" y2="1.1" width="0.127" layer="21"/>
<wire x1="0.6" y1="1.1" x2="2.4" y2="1.1" width="0.127" layer="21"/>
<wire x1="2.4" y1="1.1" x2="2.4" y2="-1.1" width="0.127" layer="21"/>
<wire x1="2.4" y1="-1.1" x2="0.6" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-0.6" y1="-1.1" x2="-2.4" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-2.4" y1="-1.1" x2="-2.4" y2="1.1" width="0.127" layer="21"/>
<wire x1="-1.5" y1="0.1" x2="-1.3" y2="-0.1" width="0" layer="49"/>
<wire x1="-1.5" y1="-0.1" x2="-1.3" y2="0.1" width="0" layer="49"/>
<wire x1="1.3" y1="0.1" x2="1.5" y2="-0.1" width="0" layer="49"/>
<wire x1="1.3" y1="-0.1" x2="1.5" y2="0.1" width="0" layer="49"/>
<wire x1="-0.5" y1="0.9" x2="0.5" y2="0.9" width="0" layer="41"/>
<wire x1="0.5" y1="0.9" x2="0.5" y2="-0.9" width="0" layer="41"/>
<wire x1="0.5" y1="-0.9" x2="-0.5" y2="-0.9" width="0" layer="41"/>
<wire x1="-0.5" y1="-0.9" x2="-0.5" y2="0.9" width="0" layer="41"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1" roundness="20"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1" roundness="20"/>
<text x="-2.4" y="1.4" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-2.3" y="0.95" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-2.3" y="-1" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="2.3" y="1" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="2.3" y="-0.95" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="1210@1">
<description>IPC 3225 chip</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.35" y1="1.45" x2="2.35" y2="1.45" width="0" layer="39"/>
<wire x1="2.35" y1="1.45" x2="2.35" y2="-1.45" width="0" layer="39"/>
<wire x1="2.35" y1="-1.45" x2="-2.35" y2="-1.45" width="0" layer="39"/>
<wire x1="-2.35" y1="-1.45" x2="-2.35" y2="1.45" width="0" layer="39"/>
<wire x1="-2.4" y1="1.5" x2="2.4" y2="1.5" width="0.127" layer="21"/>
<wire x1="2.4" y1="1.5" x2="2.4" y2="-1.5" width="0.127" layer="21"/>
<wire x1="2.4" y1="-1.5" x2="-2.4" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-2.4" y1="-1.5" x2="-2.4" y2="1.5" width="0.127" layer="21"/>
<wire x1="-0.5" y1="1.3" x2="0.5" y2="1.3" width="0" layer="41"/>
<wire x1="0.5" y1="1.3" x2="0.5" y2="-1.3" width="0" layer="41"/>
<wire x1="0.5" y1="-1.3" x2="-0.5" y2="-1.3" width="0" layer="41"/>
<wire x1="-0.5" y1="-1.3" x2="-0.5" y2="1.3" width="0" layer="41"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.6" layer="1" roundness="20"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.6" layer="1" roundness="20"/>
<text x="-2.4" y="1.8" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-2.3" y="1.35" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-2.3" y="-1.4" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="2.3" y="1.4" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="2.3" y="-1.35" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="1806@1">
<description>IPC 4516 chip</description>
<smd name="1" x="-1.27" y="0" dx="1.27" dy="1.27" layer="1"/>
<smd name="2" x="1.27" y="0" dx="1.27" dy="1.27" layer="1"/>
</package>
<package name="1812@1">
<description>IPC 4532 chip</description>
<wire x1="-3.05" y1="1.85" x2="3.05" y2="1.85" width="0" layer="39"/>
<wire x1="3.05" y1="-1.85" x2="-3.05" y2="-1.85" width="0" layer="39"/>
<wire x1="-3.05" y1="-1.85" x2="-3.05" y2="1.85" width="0" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="3.05" y1="1.85" x2="3.05" y2="-1.85" width="0" layer="39"/>
<wire x1="-3.1" y1="1.9" x2="3.1" y2="1.9" width="0.127" layer="21"/>
<wire x1="3.1" y1="1.9" x2="3.1" y2="-1.9" width="0.127" layer="21"/>
<wire x1="3.1" y1="-1.9" x2="-3.1" y2="-1.9" width="0.127" layer="21"/>
<wire x1="-3.1" y1="-1.9" x2="-3.1" y2="1.9" width="0.127" layer="21"/>
<wire x1="-1" y1="1.7" x2="1" y2="1.7" width="0" layer="41"/>
<wire x1="1" y1="1.7" x2="1" y2="-1.7" width="0" layer="41"/>
<wire x1="1" y1="-1.7" x2="-1" y2="-1.7" width="0" layer="41"/>
<wire x1="-1" y1="-1.7" x2="-1" y2="1.7" width="0" layer="41"/>
<smd name="1" x="-2" y="0" dx="1.8" dy="3.4" layer="1" roundness="20"/>
<smd name="2" x="2" y="0" dx="1.8" dy="3.4" layer="1" roundness="20"/>
<text x="-3.1" y="2.2" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-3" y="1.75" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-3" y="-1.8" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="3" y="1.8" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="3" y="-1.75" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="1825">
<description>IPC 4564 chip</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-2.9" y="3.7" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-2.9" y="-4.6" size="0.8128" layer="27" font="vector" ratio="16">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="2010">
<description>IPC 5025 chip</description>
<wire x1="-3.25" y1="1.45" x2="3.25" y2="1.45" width="0" layer="39"/>
<wire x1="3.25" y1="1.45" x2="3.25" y2="-1.45" width="0" layer="39"/>
<wire x1="3.25" y1="-1.45" x2="-3.25" y2="-1.45" width="0" layer="39"/>
<wire x1="-3.25" y1="-1.45" x2="-3.25" y2="1.45" width="0" layer="39"/>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.3" y1="1.5" x2="3.3" y2="1.5" width="0.127" layer="21"/>
<wire x1="3.3" y1="1.5" x2="3.3" y2="-1.5" width="0.127" layer="21"/>
<wire x1="3.3" y1="-1.5" x2="-3.3" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-3.3" y1="-1.5" x2="-3.3" y2="1.5" width="0.127" layer="21"/>
<wire x1="-1.2" y1="1.3" x2="1.2" y2="1.3" width="0" layer="41"/>
<wire x1="1.2" y1="1.3" x2="1.2" y2="-1.3" width="0" layer="41"/>
<wire x1="1.2" y1="-1.3" x2="-1.2" y2="-1.3" width="0" layer="41"/>
<wire x1="-1.2" y1="-1.3" x2="-1.2" y2="1.3" width="0" layer="41"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.6" layer="1" roundness="20"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.6" layer="1" roundness="20"/>
<text x="-3.3" y="1.8" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-3.2" y="1.35" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-3.2" y="-1.4" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="3.2" y="1.4" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="3.2" y="-1.35" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="2512@1">
<description>IPC 6332 chip</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="3.9" y1="1.8" x2="3.9" y2="-1.8" width="0.127" layer="21"/>
<wire x1="3.9" y1="-1.8" x2="-3.9" y2="-1.8" width="0.127" layer="21"/>
<wire x1="-3.9" y1="-1.8" x2="-3.9" y2="1.8" width="0.127" layer="21"/>
<wire x1="-3.9" y1="1.8" x2="3.9" y2="1.8" width="0.127" layer="21"/>
<wire x1="-1.7" y1="1.3" x2="-1.5" y2="1.5" width="0" layer="41"/>
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0" layer="41"/>
<wire x1="1.5" y1="1.5" x2="1.7" y2="1.3" width="0" layer="41"/>
<wire x1="1.7" y1="1.3" x2="1.7" y2="-1.3" width="0" layer="41"/>
<wire x1="1.7" y1="-1.3" x2="1.5" y2="-1.5" width="0" layer="41"/>
<wire x1="1.5" y1="-1.5" x2="-1.5" y2="-1.5" width="0" layer="41"/>
<wire x1="-1.5" y1="-1.5" x2="-1.7" y2="-1.3" width="0" layer="41"/>
<wire x1="-1.7" y1="-1.3" x2="-1.7" y2="1.3" width="0" layer="41"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1" roundness="20"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1" roundness="20"/>
<text x="-3.9" y="2.1" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-3.8" y="1.65" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-3.8" y="-1.7" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="3.8" y="1.7" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="3.8" y="-1.65" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="3616">
<smd name="1" x="-1.27" y="0" dx="1.27" dy="1.27" layer="1"/>
<smd name="2" x="1.27" y="0" dx="1.27" dy="1.27" layer="1"/>
</package>
<package name="4022">
<smd name="1" x="-1.27" y="0" dx="1.27" dy="1.27" layer="1"/>
<smd name="2" x="1.27" y="0" dx="1.27" dy="1.27" layer="1"/>
</package>
<package name="0402S@1">
<wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0.1" layer="51"/>
<wire x1="-0.5" y1="0.25" x2="0.5" y2="0.25" width="0.1" layer="51"/>
<wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0.1" layer="51"/>
<wire x1="0.5" y1="-0.25" x2="-0.5" y2="-0.25" width="0.1" layer="51"/>
<wire x1="0" y1="-0.2" x2="0" y2="0.2" width="0.127" layer="21"/>
<wire x1="-0.8" y1="0.45" x2="0.8" y2="0.45" width="0" layer="39"/>
<wire x1="0.8" y1="0.45" x2="0.8" y2="-0.45" width="0" layer="39"/>
<wire x1="0.8" y1="-0.45" x2="-0.8" y2="-0.45" width="0" layer="39"/>
<wire x1="-0.8" y1="-0.45" x2="-0.8" y2="0.45" width="0" layer="39"/>
<wire x1="-0.85" y1="0.5" x2="-0.2" y2="0.5" width="0.127" layer="21"/>
<wire x1="0.2" y1="0.5" x2="0.85" y2="0.5" width="0.127" layer="21"/>
<wire x1="0.85" y1="0.5" x2="0.85" y2="-0.5" width="0.127" layer="21"/>
<wire x1="0.85" y1="-0.5" x2="0.2" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-0.2" y1="-0.5" x2="-0.85" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-0.85" y1="-0.5" x2="-0.85" y2="0.5" width="0.127" layer="21"/>
<smd name="1" x="-0.4" y="0" dx="0.6" dy="0.5" layer="1" roundness="20" rot="R90"/>
<smd name="2" x="0.4" y="0" dx="0.6" dy="0.5" layer="1" roundness="20" rot="R90"/>
<text x="-0.85" y="0.7" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-0.75" y="0.35" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-0.75" y="-0.4" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="0.75" y="0.4" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="0.75" y="-0.35" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.2" x2="-0.2" y2="0.2" layer="51"/>
<rectangle x1="0.2" y1="-0.2" x2="0.5" y2="0.2" layer="51"/>
</package>
<package name="LQFP48">
<wire x1="-3.375" y1="3.1" x2="-3.1" y2="3.375" width="0.254" layer="21"/>
<wire x1="-3.1" y1="3.375" x2="3.1" y2="3.375" width="0.254" layer="21"/>
<wire x1="3.1" y1="3.375" x2="3.375" y2="3.1" width="0.254" layer="21"/>
<wire x1="3.375" y1="3.1" x2="3.375" y2="-3.1" width="0.254" layer="21"/>
<wire x1="3.375" y1="-3.1" x2="3.1" y2="-3.375" width="0.254" layer="21"/>
<wire x1="3.1" y1="-3.375" x2="-3.1" y2="-3.375" width="0.254" layer="21"/>
<wire x1="-3.1" y1="-3.375" x2="-3.375" y2="-3.1" width="0.254" layer="21"/>
<wire x1="-3.375" y1="-3.1" x2="-3.375" y2="3.1" width="0.254" layer="21"/>
<circle x="-2" y="-2" radius="0.6" width="0.254" layer="21"/>
<smd name="1" x="-2.75" y="-4.25" dx="0.25" dy="1" layer="1"/>
<smd name="2" x="-2.25" y="-4.25" dx="0.25" dy="1" layer="1"/>
<smd name="3" x="-1.75" y="-4.25" dx="0.25" dy="1" layer="1"/>
<smd name="4" x="-1.25" y="-4.25" dx="0.25" dy="1" layer="1"/>
<smd name="5" x="-0.75" y="-4.25" dx="0.25" dy="1" layer="1"/>
<smd name="6" x="-0.25" y="-4.25" dx="0.25" dy="1" layer="1"/>
<smd name="7" x="0.25" y="-4.25" dx="0.25" dy="1" layer="1"/>
<smd name="8" x="0.75" y="-4.25" dx="0.25" dy="1" layer="1"/>
<smd name="9" x="1.25" y="-4.25" dx="0.25" dy="1" layer="1"/>
<smd name="10" x="1.75" y="-4.25" dx="0.25" dy="1" layer="1"/>
<smd name="11" x="2.25" y="-4.25" dx="0.25" dy="1" layer="1"/>
<smd name="12" x="2.75" y="-4.25" dx="0.25" dy="1" layer="1"/>
<smd name="13" x="4.25" y="-2.75" dx="1" dy="0.25" layer="1"/>
<smd name="14" x="4.25" y="-2.25" dx="1" dy="0.25" layer="1"/>
<smd name="15" x="4.25" y="-1.75" dx="1" dy="0.25" layer="1"/>
<smd name="16" x="4.25" y="-1.25" dx="1" dy="0.25" layer="1"/>
<smd name="17" x="4.25" y="-0.75" dx="1" dy="0.25" layer="1"/>
<smd name="18" x="4.25" y="-0.25" dx="1" dy="0.25" layer="1"/>
<smd name="19" x="4.25" y="0.25" dx="1" dy="0.25" layer="1"/>
<smd name="20" x="4.25" y="0.75" dx="1" dy="0.25" layer="1"/>
<smd name="21" x="4.25" y="1.25" dx="1" dy="0.25" layer="1"/>
<smd name="22" x="4.25" y="1.75" dx="1" dy="0.25" layer="1"/>
<smd name="23" x="4.25" y="2.25" dx="1" dy="0.25" layer="1"/>
<smd name="24" x="4.25" y="2.75" dx="1" dy="0.25" layer="1"/>
<smd name="25" x="2.75" y="4.25" dx="0.25" dy="1" layer="1"/>
<smd name="26" x="2.25" y="4.25" dx="0.25" dy="1" layer="1"/>
<smd name="27" x="1.75" y="4.25" dx="0.25" dy="1" layer="1"/>
<smd name="28" x="1.25" y="4.25" dx="0.25" dy="1" layer="1"/>
<smd name="29" x="0.75" y="4.25" dx="0.25" dy="1" layer="1"/>
<smd name="30" x="0.25" y="4.25" dx="0.25" dy="1" layer="1"/>
<smd name="31" x="-0.25" y="4.25" dx="0.25" dy="1" layer="1"/>
<smd name="32" x="-0.75" y="4.25" dx="0.25" dy="1" layer="1"/>
<smd name="33" x="-1.25" y="4.25" dx="0.25" dy="1" layer="1"/>
<smd name="34" x="-1.75" y="4.25" dx="0.25" dy="1" layer="1"/>
<smd name="35" x="-2.25" y="4.25" dx="0.25" dy="1" layer="1"/>
<smd name="36" x="-2.75" y="4.25" dx="0.25" dy="1" layer="1"/>
<smd name="37" x="-4.25" y="2.75" dx="1" dy="0.25" layer="1"/>
<smd name="38" x="-4.25" y="2.25" dx="1" dy="0.25" layer="1"/>
<smd name="39" x="-4.25" y="1.75" dx="1" dy="0.25" layer="1"/>
<smd name="40" x="-4.25" y="1.25" dx="1" dy="0.25" layer="1"/>
<smd name="41" x="-4.25" y="0.75" dx="1" dy="0.25" layer="1"/>
<smd name="42" x="-4.25" y="0.25" dx="1" dy="0.25" layer="1"/>
<smd name="43" x="-4.25" y="-0.25" dx="1" dy="0.25" layer="1"/>
<smd name="44" x="-4.25" y="-0.75" dx="1" dy="0.25" layer="1"/>
<smd name="45" x="-4.25" y="-1.25" dx="1" dy="0.25" layer="1"/>
<smd name="46" x="-4.25" y="-1.75" dx="1" dy="0.25" layer="1"/>
<smd name="47" x="-4.25" y="-2.25" dx="1" dy="0.25" layer="1"/>
<smd name="48" x="-4.25" y="-2.75" dx="1" dy="0.25" layer="1"/>
<text x="-1.4" y="0.5" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<rectangle x1="-2.85" y1="-4.5" x2="-2.65" y2="-3.45" layer="51"/>
<rectangle x1="-2.35" y1="-4.5" x2="-2.15" y2="-3.45" layer="51"/>
<rectangle x1="-1.85" y1="-4.5" x2="-1.65" y2="-3.45" layer="51"/>
<rectangle x1="-1.35" y1="-4.5" x2="-1.15" y2="-3.45" layer="51"/>
<rectangle x1="-0.85" y1="-4.5" x2="-0.65" y2="-3.45" layer="51"/>
<rectangle x1="-0.35" y1="-4.5" x2="-0.15" y2="-3.45" layer="51"/>
<rectangle x1="0.15" y1="-4.5" x2="0.35" y2="-3.45" layer="51"/>
<rectangle x1="0.65" y1="-4.5" x2="0.85" y2="-3.45" layer="51"/>
<rectangle x1="1.15" y1="-4.5" x2="1.35" y2="-3.45" layer="51"/>
<rectangle x1="1.65" y1="-4.5" x2="1.85" y2="-3.45" layer="51"/>
<rectangle x1="2.15" y1="-4.5" x2="2.35" y2="-3.45" layer="51"/>
<rectangle x1="2.65" y1="-4.5" x2="2.85" y2="-3.45" layer="51"/>
<rectangle x1="3.45" y1="-2.85" x2="4.5" y2="-2.65" layer="51"/>
<rectangle x1="3.45" y1="-2.35" x2="4.5" y2="-2.15" layer="51"/>
<rectangle x1="3.45" y1="-1.85" x2="4.5" y2="-1.65" layer="51"/>
<rectangle x1="3.45" y1="-1.35" x2="4.5" y2="-1.15" layer="51"/>
<rectangle x1="3.45" y1="-0.85" x2="4.5" y2="-0.65" layer="51"/>
<rectangle x1="3.45" y1="-0.35" x2="4.5" y2="-0.15" layer="51"/>
<rectangle x1="3.45" y1="0.15" x2="4.5" y2="0.35" layer="51"/>
<rectangle x1="3.45" y1="0.65" x2="4.5" y2="0.85" layer="51"/>
<rectangle x1="3.45" y1="1.15" x2="4.5" y2="1.35" layer="51"/>
<rectangle x1="3.45" y1="1.65" x2="4.5" y2="1.85" layer="51"/>
<rectangle x1="3.45" y1="2.15" x2="4.5" y2="2.35" layer="51"/>
<rectangle x1="3.45" y1="2.65" x2="4.5" y2="2.85" layer="51"/>
<rectangle x1="2.65" y1="3.45" x2="2.85" y2="4.5" layer="51"/>
<rectangle x1="2.15" y1="3.45" x2="2.35" y2="4.5" layer="51"/>
<rectangle x1="1.65" y1="3.45" x2="1.85" y2="4.5" layer="51"/>
<rectangle x1="1.15" y1="3.45" x2="1.35" y2="4.5" layer="51"/>
<rectangle x1="0.65" y1="3.45" x2="0.85" y2="4.5" layer="51"/>
<rectangle x1="0.15" y1="3.45" x2="0.35" y2="4.5" layer="51"/>
<rectangle x1="-0.35" y1="3.45" x2="-0.15" y2="4.5" layer="51"/>
<rectangle x1="-0.85" y1="3.45" x2="-0.65" y2="4.5" layer="51"/>
<rectangle x1="-1.35" y1="3.45" x2="-1.15" y2="4.5" layer="51"/>
<rectangle x1="-1.85" y1="3.45" x2="-1.65" y2="4.5" layer="51"/>
<rectangle x1="-2.35" y1="3.45" x2="-2.15" y2="4.5" layer="51"/>
<rectangle x1="-2.85" y1="3.45" x2="-2.65" y2="4.5" layer="51"/>
<rectangle x1="-4.5" y1="2.65" x2="-3.45" y2="2.85" layer="51"/>
<rectangle x1="-4.5" y1="2.15" x2="-3.45" y2="2.35" layer="51"/>
<rectangle x1="-4.5" y1="1.65" x2="-3.45" y2="1.85" layer="51"/>
<rectangle x1="-4.5" y1="1.15" x2="-3.45" y2="1.35" layer="51"/>
<rectangle x1="-4.5" y1="0.65" x2="-3.45" y2="0.85" layer="51"/>
<rectangle x1="-4.5" y1="0.15" x2="-3.45" y2="0.35" layer="51"/>
<rectangle x1="-4.5" y1="-0.35" x2="-3.45" y2="-0.15" layer="51"/>
<rectangle x1="-4.5" y1="-0.85" x2="-3.45" y2="-0.65" layer="51"/>
<rectangle x1="-4.5" y1="-1.35" x2="-3.45" y2="-1.15" layer="51"/>
<rectangle x1="-4.5" y1="-1.85" x2="-3.45" y2="-1.65" layer="51"/>
<rectangle x1="-4.5" y1="-2.35" x2="-3.45" y2="-2.15" layer="51"/>
<rectangle x1="-4.5" y1="-2.85" x2="-3.45" y2="-2.65" layer="51"/>
</package>
<package name="SOT23-6">
<description>&lt;b&gt;SOT23-6&lt;/b&gt;&lt;br&gt;
also TSOT
also Texas Instruments DBV</description>
<wire x1="1.5" y1="0.8" x2="1.5" y2="-0.8" width="0.127" layer="21"/>
<wire x1="1.5" y1="-0.8" x2="-1.5" y2="-0.8" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-0.8" x2="-1.5" y2="0.8" width="0.127" layer="21"/>
<wire x1="-1.5" y1="0.8" x2="1.5" y2="0.8" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-0.8" x2="-1.5" y2="0.8" width="0.127" layer="51"/>
<wire x1="1.5" y1="-0.8" x2="1.5" y2="0.8" width="0.127" layer="51"/>
<wire x1="-0.45" y1="-0.8" x2="-0.5" y2="-0.8" width="0.127" layer="21"/>
<wire x1="0.5" y1="-0.8" x2="0.45" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-0.45" y1="0.8" x2="-0.5" y2="0.8" width="0.127" layer="21"/>
<wire x1="0.5" y1="0.8" x2="0.45" y2="0.8" width="0.127" layer="21"/>
<wire x1="1.5" y1="0.8" x2="1.4" y2="0.8" width="0.127" layer="21"/>
<wire x1="-1.4" y1="0.8" x2="-1.5" y2="0.8" width="0.127" layer="21"/>
<wire x1="-1.4" y1="-0.8" x2="-1.5" y2="-0.8" width="0.127" layer="21"/>
<wire x1="1.5" y1="-0.8" x2="1.4" y2="-0.8" width="0.127" layer="21"/>
<circle x="-0.95" y="-0.4" radius="0.15" width="0" layer="21"/>
<smd name="1" x="-0.95" y="-1.3" dx="0.55" dy="1.2" layer="1" roundness="20"/>
<smd name="2" x="0" y="-1.3" dx="0.55" dy="1.2" layer="1" roundness="20"/>
<smd name="3" x="0.95" y="-1.3" dx="0.55" dy="1.2" layer="1" roundness="20"/>
<smd name="4" x="0.95" y="1.3" dx="0.55" dy="1.2" layer="1" roundness="20"/>
<smd name="5" x="0" y="1.3" dx="0.55" dy="1.2" layer="1" roundness="20"/>
<smd name="6" x="-0.95" y="1.3" dx="0.55" dy="1.2" layer="1" roundness="20"/>
<text x="-1.5" y="2.2" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-1.4" y="0.65" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="1.4" y="0.7" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="-1.4" y="-0.7" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="1.4" y="-0.65" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-1.2" y1="-1.5" x2="-0.7" y2="-0.85" layer="51"/>
<rectangle x1="-0.25" y1="-1.5" x2="0.25" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="-1.5" x2="1.2" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="0.85" x2="1.2" y2="1.5" layer="51"/>
<rectangle x1="-0.25" y1="0.85" x2="0.25" y2="1.5" layer="51"/>
<rectangle x1="-1.2" y1="0.85" x2="-0.7" y2="1.5" layer="51"/>
</package>
<package name="0201">
<description>IPC 0603 chip</description>
<wire x1="-0.3" y1="0.15" x2="0.3" y2="0.15" width="0.127" layer="51"/>
<wire x1="0.3" y1="-0.15" x2="-0.3" y2="-0.15" width="0.127" layer="51"/>
<wire x1="-0.6" y1="0.35" x2="-0.2" y2="0.35" width="0.127" layer="21"/>
<wire x1="0.2" y1="0.35" x2="0.6" y2="0.35" width="0.127" layer="21"/>
<wire x1="0.6" y1="0.35" x2="0.6" y2="-0.35" width="0.127" layer="21"/>
<wire x1="0.6" y1="-0.35" x2="0.2" y2="-0.35" width="0.127" layer="21"/>
<wire x1="-0.2" y1="-0.35" x2="-0.6" y2="-0.35" width="0.127" layer="21"/>
<wire x1="-0.6" y1="-0.35" x2="-0.6" y2="0.35" width="0.127" layer="21"/>
<wire x1="0.3" y1="-0.15" x2="0.3" y2="0.15" width="0.127" layer="51"/>
<wire x1="-0.3" y1="-0.15" x2="-0.3" y2="0.15" width="0.127" layer="51"/>
<smd name="1" x="-0.3" y="0" dx="0.3" dy="0.4" layer="1" roundness="20"/>
<smd name="2" x="0.3" y="0" dx="0.3" dy="0.4" layer="1" roundness="20"/>
<text x="-0.6" y="0.5" size="0.6" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="0402">
<description>IPC 1005 chip - 0.1mm grid friendly</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.05" y1="0.4" x2="-1" y2="0.45" width="0" layer="39"/>
<wire x1="-1" y1="0.45" x2="1" y2="0.45" width="0" layer="39"/>
<wire x1="1" y1="0.45" x2="1.05" y2="0.4" width="0" layer="39"/>
<wire x1="1.05" y1="0.4" x2="1.05" y2="-0.4" width="0" layer="39"/>
<wire x1="1.05" y1="-0.4" x2="1" y2="-0.45" width="0" layer="39"/>
<wire x1="1" y1="-0.45" x2="-1" y2="-0.45" width="0" layer="39"/>
<wire x1="-1" y1="-0.45" x2="-1.05" y2="-0.4" width="0" layer="39"/>
<wire x1="-1.05" y1="-0.4" x2="-1.05" y2="0.4" width="0" layer="39"/>
<wire x1="-1.1" y1="0.5" x2="-0.3" y2="0.5" width="0.127" layer="21"/>
<wire x1="0.3" y1="0.5" x2="1.1" y2="0.5" width="0.127" layer="21"/>
<wire x1="1.1" y1="0.5" x2="1.1" y2="-0.5" width="0.127" layer="21"/>
<wire x1="1.1" y1="-0.5" x2="0.3" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-0.3" y1="-0.5" x2="-1.1" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-1.1" y1="-0.5" x2="-1.1" y2="0.5" width="0.127" layer="21"/>
<wire x1="-0.7" y1="0.1" x2="-0.5" y2="-0.1" width="0" layer="49"/>
<wire x1="-0.7" y1="-0.1" x2="-0.5" y2="0.1" width="0" layer="49"/>
<wire x1="0.5" y1="0.1" x2="0.7" y2="-0.1" width="0" layer="49"/>
<wire x1="0.5" y1="-0.1" x2="0.7" y2="0.1" width="0" layer="49"/>
<wire x1="-0.15" y1="0.3" x2="-0.125" y2="0.3" width="0" layer="41"/>
<wire x1="0.125" y1="0.3" x2="0.15" y2="0.3" width="0" layer="41"/>
<wire x1="0.15" y1="0.3" x2="0.2" y2="0.25" width="0" layer="41"/>
<wire x1="0.2" y1="0.25" x2="0.2" y2="-0.25" width="0" layer="41"/>
<wire x1="0.2" y1="-0.25" x2="0.15" y2="-0.3" width="0" layer="41"/>
<wire x1="0.15" y1="-0.3" x2="0.125" y2="-0.3" width="0" layer="41"/>
<wire x1="-0.125" y1="-0.3" x2="-0.15" y2="-0.3" width="0" layer="41"/>
<wire x1="-0.15" y1="-0.3" x2="-0.2" y2="-0.25" width="0" layer="41"/>
<wire x1="-0.2" y1="-0.25" x2="-0.2" y2="0.25" width="0" layer="41"/>
<wire x1="-0.2" y1="0.25" x2="-0.15" y2="0.3" width="0" layer="41"/>
<smd name="1" x="-0.6" y="0" dx="0.6" dy="0.6" layer="1" roundness="20"/>
<smd name="2" x="0.6" y="0" dx="0.6" dy="0.6" layer="1" roundness="20"/>
<text x="-1.1" y="0.8" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-1" y="0.35" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-1" y="-0.4" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="1" y="0.4" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="1" y="-0.35" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="0402I">
<description>IPC 1005 chip - 5mil grid friendly</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.05" y1="0.4" x2="-1" y2="0.45" width="0" layer="39"/>
<wire x1="-1" y1="0.45" x2="1" y2="0.45" width="0" layer="39"/>
<wire x1="1" y1="0.45" x2="1.05" y2="0.4" width="0" layer="39"/>
<wire x1="1.05" y1="0.4" x2="1.05" y2="-0.4" width="0" layer="39"/>
<wire x1="1.05" y1="-0.4" x2="1" y2="-0.45" width="0" layer="39"/>
<wire x1="1" y1="-0.45" x2="-1" y2="-0.45" width="0" layer="39"/>
<wire x1="-1" y1="-0.45" x2="-1.05" y2="-0.4" width="0" layer="39"/>
<wire x1="-1.05" y1="-0.4" x2="-1.05" y2="0.4" width="0" layer="39"/>
<wire x1="-1.143" y1="0.508" x2="-0.254" y2="0.508" width="0.127" layer="21"/>
<wire x1="0.254" y1="0.508" x2="1.143" y2="0.508" width="0.127" layer="21"/>
<wire x1="1.143" y1="0.508" x2="1.143" y2="-0.508" width="0.127" layer="21"/>
<wire x1="1.143" y1="-0.508" x2="0.254" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-0.508" x2="-1.143" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-1.143" y1="-0.508" x2="-1.143" y2="0.508" width="0.127" layer="21"/>
<wire x1="-0.762" y1="0.127" x2="-0.508" y2="-0.127" width="0" layer="49"/>
<wire x1="-0.762" y1="-0.127" x2="-0.508" y2="0.127" width="0" layer="49"/>
<wire x1="0.508" y1="0.127" x2="0.762" y2="-0.127" width="0" layer="49"/>
<wire x1="0.508" y1="-0.127" x2="0.762" y2="0.127" width="0" layer="49"/>
<smd name="1" x="-0.635" y="0" dx="0.635" dy="0.635" layer="1" roundness="20"/>
<smd name="2" x="0.635" y="0" dx="0.635" dy="0.635" layer="1" roundness="20"/>
<text x="-1.143" y="0.762" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-1" y="0.35" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-1" y="-0.4" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="1" y="0.4" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="1" y="-0.35" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="0402S">
<description>IPC 1005 chip - 0.1mm grid friendly - small size</description>
<wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0.1" layer="51"/>
<wire x1="-0.5" y1="0.25" x2="0.5" y2="0.25" width="0.1" layer="51"/>
<wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0.1" layer="51"/>
<wire x1="0.5" y1="-0.25" x2="-0.5" y2="-0.25" width="0.1" layer="51"/>
<wire x1="0" y1="-0.2" x2="0" y2="0.2" width="0.127" layer="21"/>
<wire x1="-0.8" y1="0.45" x2="0.8" y2="0.45" width="0" layer="39"/>
<wire x1="0.8" y1="0.45" x2="0.8" y2="-0.45" width="0" layer="39"/>
<wire x1="0.8" y1="-0.45" x2="-0.8" y2="-0.45" width="0" layer="39"/>
<wire x1="-0.8" y1="-0.45" x2="-0.8" y2="0.45" width="0" layer="39"/>
<wire x1="-0.85" y1="0.5" x2="-0.2" y2="0.5" width="0.127" layer="21"/>
<wire x1="0.2" y1="0.5" x2="0.85" y2="0.5" width="0.127" layer="21"/>
<wire x1="0.85" y1="0.5" x2="0.85" y2="-0.5" width="0.127" layer="21"/>
<wire x1="0.85" y1="-0.5" x2="0.2" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-0.2" y1="-0.5" x2="-0.85" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-0.85" y1="-0.5" x2="-0.85" y2="0.5" width="0.127" layer="21"/>
<smd name="1" x="-0.4" y="0" dx="0.5" dy="0.6" layer="1" roundness="20"/>
<smd name="2" x="0.4" y="0" dx="0.5" dy="0.6" layer="1" roundness="20"/>
<text x="-0.85" y="0.7" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-0.75" y="0.35" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-0.75" y="-0.4" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="0.75" y="0.4" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="0.75" y="-0.35" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.2" x2="-0.2" y2="0.2" layer="51"/>
<rectangle x1="0.2" y1="-0.2" x2="0.5" y2="0.2" layer="51"/>
</package>
<package name="0603">
<description>IPC 1608 chip - 0.1mm grid friendly</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.45" y1="0.65" x2="1.45" y2="0.65" width="0" layer="39"/>
<wire x1="1.45" y1="0.65" x2="1.45" y2="-0.65" width="0" layer="39"/>
<wire x1="1.45" y1="-0.65" x2="-1.45" y2="-0.65" width="0" layer="39"/>
<wire x1="-1.45" y1="-0.65" x2="-1.45" y2="0.65" width="0" layer="39"/>
<wire x1="-1.5" y1="0.7" x2="-0.3" y2="0.7" width="0.127" layer="21"/>
<wire x1="0.3" y1="0.7" x2="1.5" y2="0.7" width="0.127" layer="21"/>
<wire x1="1.5" y1="0.7" x2="1.5" y2="-0.7" width="0.127" layer="21"/>
<wire x1="1.5" y1="-0.7" x2="0.3" y2="-0.7" width="0.127" layer="21"/>
<wire x1="-0.3" y1="-0.7" x2="-1.5" y2="-0.7" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-0.7" x2="-1.5" y2="0.7" width="0.127" layer="21"/>
<wire x1="-0.9" y1="0.1" x2="-0.7" y2="-0.1" width="0" layer="49"/>
<wire x1="-0.9" y1="-0.1" x2="-0.7" y2="0.1" width="0" layer="49"/>
<wire x1="0.7" y1="0.1" x2="0.9" y2="-0.1" width="0" layer="49"/>
<wire x1="0.7" y1="-0.1" x2="0.9" y2="0.1" width="0" layer="49"/>
<wire x1="-0.2" y1="0.4" x2="-0.15" y2="0.4" width="0" layer="41"/>
<wire x1="0.15" y1="0.4" x2="0.2" y2="0.4" width="0" layer="41"/>
<wire x1="0.2" y1="0.4" x2="0.2" y2="-0.4" width="0" layer="41"/>
<wire x1="0.2" y1="-0.4" x2="0.15" y2="-0.4" width="0" layer="41"/>
<wire x1="-0.15" y1="-0.4" x2="-0.2" y2="-0.4" width="0" layer="41"/>
<wire x1="-0.2" y1="-0.4" x2="-0.2" y2="0.4" width="0" layer="41"/>
<smd name="1" x="-0.8" y="0" dx="1" dy="1" layer="1" roundness="20"/>
<smd name="2" x="0.8" y="0" dx="1" dy="1" layer="1" roundness="20"/>
<text x="-1.5" y="0.9" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-1.4" y="0.55" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-1.4" y="-0.6" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="1.4" y="0.6" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="1.4" y="-0.55" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="0603I">
<description>IPC 1608 chip - 5mil grid friendly</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.5135" y1="0.65" x2="1.5135" y2="0.65" width="0" layer="39"/>
<wire x1="1.5135" y1="0.65" x2="1.5135" y2="-0.65" width="0" layer="39"/>
<wire x1="1.5135" y1="-0.65" x2="-1.5135" y2="-0.65" width="0" layer="39"/>
<wire x1="-1.5135" y1="-0.65" x2="-1.5135" y2="0.65" width="0" layer="39"/>
<wire x1="-1.5875" y1="0.6985" x2="-0.381" y2="0.6985" width="0.127" layer="21"/>
<wire x1="0.381" y1="0.6985" x2="1.5875" y2="0.6985" width="0.127" layer="21"/>
<wire x1="1.5875" y1="0.6985" x2="1.5875" y2="-0.6985" width="0.127" layer="21"/>
<wire x1="1.5875" y1="-0.6985" x2="0.381" y2="-0.6985" width="0.127" layer="21"/>
<wire x1="-0.381" y1="-0.6985" x2="-1.5875" y2="-0.6985" width="0.127" layer="21"/>
<wire x1="-1.5875" y1="-0.6985" x2="-1.5875" y2="0.6985" width="0.127" layer="21"/>
<wire x1="-1.016" y1="0.127" x2="-0.762" y2="-0.127" width="0" layer="49"/>
<wire x1="-1.016" y1="-0.127" x2="-0.762" y2="0.127" width="0" layer="49"/>
<wire x1="0.762" y1="0.127" x2="1.016" y2="-0.127" width="0" layer="49"/>
<wire x1="0.762" y1="-0.127" x2="1.016" y2="0.127" width="0" layer="49"/>
<wire x1="0.254" y1="0.381" x2="0.254" y2="-0.381" width="0" layer="41"/>
<wire x1="-0.254" y1="-0.381" x2="-0.254" y2="0.381" width="0" layer="41"/>
<smd name="1" x="-0.889" y="0" dx="1.016" dy="1.016" layer="1" roundness="20"/>
<smd name="2" x="0.889" y="0" dx="1.016" dy="1.016" layer="1" roundness="20"/>
<text x="-1.5875" y="1.016" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-1.4" y="0.55" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-1.4" y="-0.6" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="1.4" y="0.6" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="1.4" y="-0.55" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="0805">
<description>IPC 2012 chip - 0.1mm grid friendly</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.75" y1="0.85" x2="1.75" y2="0.85" width="0" layer="39"/>
<wire x1="1.75" y1="0.85" x2="1.75" y2="-0.85" width="0" layer="39"/>
<wire x1="1.75" y1="-0.85" x2="-1.75" y2="-0.85" width="0" layer="39"/>
<wire x1="-1.75" y1="-0.85" x2="-1.75" y2="0.85" width="0" layer="39"/>
<wire x1="-1.8" y1="0.9" x2="-0.4" y2="0.9" width="0.127" layer="21"/>
<wire x1="0.4" y1="0.9" x2="1.8" y2="0.9" width="0.127" layer="21"/>
<wire x1="1.8" y1="0.9" x2="1.8" y2="-0.9" width="0.127" layer="21"/>
<wire x1="1.8" y1="-0.9" x2="0.4" y2="-0.9" width="0.127" layer="21"/>
<wire x1="-0.4" y1="-0.9" x2="-1.8" y2="-0.9" width="0.127" layer="21"/>
<wire x1="-1.8" y1="-0.9" x2="-1.8" y2="0.9" width="0.127" layer="21"/>
<wire x1="-1.1" y1="0.1" x2="-0.9" y2="-0.1" width="0" layer="49"/>
<wire x1="-1.1" y1="-0.1" x2="-0.9" y2="0.1" width="0" layer="49"/>
<wire x1="0.9" y1="0.1" x2="1.1" y2="-0.1" width="0" layer="49"/>
<wire x1="0.9" y1="-0.1" x2="1.1" y2="0.1" width="0" layer="49"/>
<smd name="1" x="-1" y="0" dx="1.2" dy="1.4" layer="1" roundness="20"/>
<smd name="2" x="1" y="0" dx="1.2" dy="1.4" layer="1" roundness="20"/>
<text x="-1.8" y="1.2" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-1.7" y="0.75" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-1.7" y="-0.8" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="1.7" y="0.8" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="1.7" y="-0.75" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4" x2="0.1999" y2="0.4" layer="35"/>
</package>
<package name="0805I">
<description>IPC 2012 chip - 5mil grid friendly</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-0.381" y1="0.889" x2="-1.778" y2="0.889" width="0.127" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.778" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="-0.889" x2="1.778" y2="-0.889" width="0.127" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.778" y2="0.889" width="0.127" layer="21"/>
<wire x1="1.778" y1="0.889" x2="0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="-1.75" y1="0.85" x2="1.75" y2="0.85" width="0" layer="39"/>
<wire x1="1.75" y1="0.85" x2="1.75" y2="-0.85" width="0" layer="39"/>
<wire x1="1.75" y1="-0.85" x2="-1.75" y2="-0.85" width="0" layer="39"/>
<wire x1="-1.75" y1="-0.85" x2="-1.75" y2="0.85" width="0" layer="39"/>
<wire x1="-1.143" y1="0.127" x2="-0.889" y2="-0.127" width="0" layer="49"/>
<wire x1="-1.143" y1="-0.127" x2="-0.889" y2="0.127" width="0" layer="49"/>
<wire x1="0.889" y1="0.127" x2="1.143" y2="-0.127" width="0" layer="49"/>
<wire x1="0.889" y1="-0.127" x2="1.143" y2="0.127" width="0" layer="49"/>
<wire x1="-0.3" y1="0.5" x2="-0.2" y2="0.5" width="0" layer="41"/>
<wire x1="0.2" y1="0.5" x2="0.3" y2="0.5" width="0" layer="41"/>
<wire x1="0.3" y1="0.5" x2="0.3" y2="-0.5" width="0" layer="41"/>
<wire x1="0.3" y1="-0.5" x2="0.2" y2="-0.5" width="0" layer="41"/>
<wire x1="-0.2" y1="-0.5" x2="-0.3" y2="-0.5" width="0" layer="41"/>
<wire x1="-0.3" y1="-0.5" x2="-0.3" y2="0.5" width="0" layer="41"/>
<smd name="1" x="-1.016" y="0" dx="1.143" dy="1.397" layer="1" roundness="20"/>
<smd name="2" x="1.016" y="0" dx="1.143" dy="1.397" layer="1" roundness="20"/>
<text x="-1.778" y="1.143" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-1.7" y="0.75" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-1.7" y="-0.8" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="1.7" y="0.8" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="1.7" y="-0.75" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="1206">
<description>IPC 3216 chip - 0.1mm grid friendly</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.35" y1="1.05" x2="2.35" y2="1.05" width="0" layer="39"/>
<wire x1="2.35" y1="1.05" x2="2.35" y2="-1.05" width="0" layer="39"/>
<wire x1="2.35" y1="-1.05" x2="-2.35" y2="-1.05" width="0" layer="39"/>
<wire x1="-2.35" y1="-1.05" x2="-2.35" y2="1.05" width="0" layer="39"/>
<wire x1="-2.4" y1="1.1" x2="-0.6" y2="1.1" width="0.127" layer="21"/>
<wire x1="0.6" y1="1.1" x2="2.4" y2="1.1" width="0.127" layer="21"/>
<wire x1="2.4" y1="1.1" x2="2.4" y2="-1.1" width="0.127" layer="21"/>
<wire x1="2.4" y1="-1.1" x2="0.6" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-0.6" y1="-1.1" x2="-2.4" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-2.4" y1="-1.1" x2="-2.4" y2="1.1" width="0.127" layer="21"/>
<wire x1="-1.5" y1="0.1" x2="-1.3" y2="-0.1" width="0" layer="49"/>
<wire x1="-1.5" y1="-0.1" x2="-1.3" y2="0.1" width="0" layer="49"/>
<wire x1="1.3" y1="0.1" x2="1.5" y2="-0.1" width="0" layer="49"/>
<wire x1="1.3" y1="-0.1" x2="1.5" y2="0.1" width="0" layer="49"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1" roundness="20"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1" roundness="20"/>
<text x="-2.4" y="1.4" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-2.3" y="0.95" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-2.3" y="-1" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="2.3" y="1" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="2.3" y="-0.95" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="1206I">
<description>IPC 3216 chip - 5mil grid friendly</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="1.0795" x2="-2.413" y2="1.0795" width="0.127" layer="21"/>
<wire x1="-2.413" y1="1.0795" x2="-2.413" y2="-1.0795" width="0.127" layer="21"/>
<wire x1="-2.413" y1="-1.0795" x2="-0.508" y2="-1.0795" width="0.127" layer="21"/>
<wire x1="0.508" y1="-1.0795" x2="2.413" y2="-1.0795" width="0.127" layer="21"/>
<wire x1="2.413" y1="-1.0795" x2="2.413" y2="1.0795" width="0.127" layer="21"/>
<wire x1="2.413" y1="1.0795" x2="0.508" y2="1.0795" width="0.127" layer="21"/>
<wire x1="-2.35" y1="1.05" x2="2.35" y2="1.05" width="0" layer="39"/>
<wire x1="2.35" y1="1.05" x2="2.35" y2="-1.05" width="0" layer="39"/>
<wire x1="2.35" y1="-1.05" x2="-2.35" y2="-1.05" width="0" layer="39"/>
<wire x1="-2.35" y1="-1.05" x2="-2.35" y2="1.05" width="0" layer="39"/>
<wire x1="-1.524" y1="0.127" x2="-1.27" y2="-0.127" width="0" layer="49"/>
<wire x1="-1.524" y1="-0.127" x2="-1.27" y2="0.127" width="0" layer="49"/>
<wire x1="1.27" y1="0.127" x2="1.524" y2="-0.127" width="0" layer="49"/>
<wire x1="1.27" y1="-0.127" x2="1.524" y2="0.127" width="0" layer="49"/>
<wire x1="-0.5" y1="0.9" x2="-0.3" y2="0.9" width="0" layer="41"/>
<wire x1="0.3" y1="0.9" x2="0.5" y2="0.9" width="0" layer="41"/>
<wire x1="0.5" y1="0.9" x2="0.5" y2="-0.9" width="0" layer="41"/>
<wire x1="0.5" y1="-0.9" x2="0.3" y2="-0.9" width="0" layer="41"/>
<wire x1="-0.3" y1="-0.9" x2="-0.5" y2="-0.9" width="0" layer="41"/>
<wire x1="-0.5" y1="-0.9" x2="-0.5" y2="0.9" width="0" layer="41"/>
<smd name="1" x="-1.397" y="0" dx="1.651" dy="1.778" layer="1" roundness="20"/>
<smd name="2" x="1.397" y="0" dx="1.651" dy="1.778" layer="1" roundness="20"/>
<text x="-2.4" y="1.4" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-2.3" y="0.95" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-2.3" y="-1" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="2.3" y="1" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="2.3" y="-0.95" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="1210">
<description>IPC 3225 chip</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.35" y1="1.45" x2="2.35" y2="1.45" width="0" layer="39"/>
<wire x1="2.35" y1="1.45" x2="2.35" y2="-1.45" width="0" layer="39"/>
<wire x1="2.35" y1="-1.45" x2="-2.35" y2="-1.45" width="0" layer="39"/>
<wire x1="-2.35" y1="-1.45" x2="-2.35" y2="1.45" width="0" layer="39"/>
<wire x1="-2.4" y1="1.5" x2="2.4" y2="1.5" width="0.127" layer="21"/>
<wire x1="2.4" y1="1.5" x2="2.4" y2="-1.5" width="0.127" layer="21"/>
<wire x1="2.4" y1="-1.5" x2="-2.4" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-2.4" y1="-1.5" x2="-2.4" y2="1.5" width="0.127" layer="21"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.6" layer="1" roundness="20"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.6" layer="1" roundness="20"/>
<text x="-2.4" y="1.8" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-2.3" y="1.35" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-2.3" y="-1.4" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="2.3" y="1.4" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="2.3" y="-1.35" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="1806">
<description>IPC 4516 chip</description>
<wire x1="-1.7" y1="0" x2="-1.7" y2="0.1" width="0" layer="47"/>
<wire x1="-1.7" y1="0.1" x2="1.7" y2="0.1" width="0" layer="47"/>
<wire x1="1.7" y1="0.1" x2="1.7" y2="0" width="0" layer="47"/>
<wire x1="1.6525" y1="-0.8128" x2="-1.6652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="1.6525" y1="0.8128" x2="-1.6652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-3.2" y1="1.1" x2="-1.3" y2="1.1" width="0.127" layer="21"/>
<wire x1="1.3" y1="1.1" x2="3.2" y2="1.1" width="0.127" layer="21"/>
<wire x1="3.2" y1="1.1" x2="3.2" y2="-1.1" width="0.127" layer="21"/>
<wire x1="3.2" y1="-1.1" x2="1.3" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-1.3" y1="-1.1" x2="-3.2" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-3.2" y1="-1.1" x2="-3.2" y2="1.1" width="0.127" layer="21"/>
<smd name="1" x="-2.1" y="0" dx="1.8" dy="1.8" layer="1" roundness="20"/>
<smd name="2" x="2.1" y="0" dx="1.8" dy="1.8" layer="1" roundness="20"/>
<text x="-3.2" y="1.4" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<rectangle x1="-2.3891" y1="-0.8763" x2="-1.6525" y2="0.8763" layer="51"/>
<rectangle x1="1.6525" y1="-0.8763" x2="2.3891" y2="0.8763" layer="51"/>
</package>
<package name="1812">
<description>IPC 4532 chip</description>
<wire x1="-3.05" y1="1.85" x2="3.05" y2="1.85" width="0" layer="39"/>
<wire x1="3.05" y1="-1.85" x2="-3.05" y2="-1.85" width="0" layer="39"/>
<wire x1="-3.05" y1="-1.85" x2="-3.05" y2="1.85" width="0" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="3.05" y1="1.85" x2="3.05" y2="-1.85" width="0" layer="39"/>
<wire x1="-3.1" y1="1.9" x2="3.1" y2="1.9" width="0.127" layer="21"/>
<wire x1="3.1" y1="1.9" x2="3.1" y2="-1.9" width="0.127" layer="21"/>
<wire x1="3.1" y1="-1.9" x2="-3.1" y2="-1.9" width="0.127" layer="21"/>
<wire x1="-3.1" y1="-1.9" x2="-3.1" y2="1.9" width="0.127" layer="21"/>
<smd name="1" x="-2" y="0" dx="1.8" dy="3.4" layer="1" roundness="20"/>
<smd name="2" x="2" y="0" dx="1.8" dy="3.4" layer="1" roundness="20"/>
<text x="-3.1" y="2.2" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-3" y="1.75" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-3" y="-1.8" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="3" y="1.8" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="3" y="-1.75" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="2512">
<description>IPC 6332 chip</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="3.9" y1="1.8" x2="3.9" y2="-1.8" width="0.127" layer="21"/>
<wire x1="3.9" y1="-1.8" x2="-3.9" y2="-1.8" width="0.127" layer="21"/>
<wire x1="-3.9" y1="-1.8" x2="-3.9" y2="1.8" width="0.127" layer="21"/>
<wire x1="-3.9" y1="1.8" x2="3.9" y2="1.8" width="0.127" layer="21"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1" roundness="20"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1" roundness="20"/>
<text x="-3.9" y="2.1" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-3.8" y="1.65" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-3.8" y="-1.7" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="3.8" y="1.7" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="3.8" y="-1.65" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="4527">
<description>4527 also WSR</description>
<wire x1="-5.842" y1="-3.556" x2="-5.842" y2="3.556" width="0.2032" layer="51"/>
<wire x1="-5.842" y1="3.556" x2="5.842" y2="3.556" width="0.2032" layer="21"/>
<wire x1="5.842" y1="3.556" x2="5.842" y2="-3.556" width="0.2032" layer="51"/>
<wire x1="5.842" y1="-3.556" x2="-5.842" y2="-3.556" width="0.2032" layer="21"/>
<smd name="1" x="4.572" y="0" dx="3.937" dy="5.842" layer="1"/>
<smd name="2" x="-4.572" y="0" dx="3.937" dy="5.842" layer="1"/>
<text x="-2.8448" y="0.3556" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.8448" y="-1.8288" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="CDEP134">
<wire x1="-5" y1="-5" x2="5" y2="-5" width="0" layer="47"/>
<wire x1="-1.9" y1="-7.5" x2="-1.9" y2="7.5" width="0" layer="47"/>
<wire x1="-2.2" y1="-5.4" x2="2.2" y2="-5.4" width="0" layer="47"/>
<wire x1="-1" y1="4.7" x2="-1" y2="-4.7" width="0" layer="47"/>
<wire x1="-6.75" y1="6.75" x2="-6.75" y2="-6.75" width="0.127" layer="21"/>
<wire x1="-6.75" y1="-6.75" x2="-5.25" y2="-6.75" width="0.127" layer="21"/>
<wire x1="-5.25" y1="-6.75" x2="-2" y2="-6.75" width="0.127" layer="51"/>
<wire x1="-2" y1="-6.75" x2="2" y2="-6.75" width="0.127" layer="21"/>
<wire x1="2" y1="-6.75" x2="5.25" y2="-6.75" width="0.127" layer="51"/>
<wire x1="5.25" y1="-6.75" x2="6.75" y2="-6.75" width="0.127" layer="21"/>
<wire x1="6.75" y1="-6.75" x2="6.75" y2="6.75" width="0.127" layer="21"/>
<wire x1="6.75" y1="6.75" x2="1.5" y2="6.75" width="0.127" layer="21"/>
<wire x1="1.5" y1="6.75" x2="-1.5" y2="6.75" width="0.127" layer="51"/>
<wire x1="-1.5" y1="6.75" x2="-6.75" y2="6.75" width="0.127" layer="21"/>
<smd name="1" x="-3.6" y="-6.1" dx="2.8" dy="2.8" layer="1" roundness="20"/>
<smd name="2" x="3.6" y="-6.1" dx="2.8" dy="2.8" layer="1" roundness="20"/>
<smd name="3" x="0" y="6.1" dx="2.6" dy="2.8" layer="1" roundness="20"/>
<text x="-6.7" y="7" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="CDRH5D">
<wire x1="-2.9" y1="2.8" x2="2.9" y2="2.8" width="0.127" layer="51"/>
<wire x1="3.4" y1="3.4" x2="3.4" y2="-3.4" width="0.127" layer="21"/>
<wire x1="2.9" y1="2.8" x2="2.9" y2="-2.8" width="0.127" layer="51"/>
<wire x1="2.9" y1="-2.8" x2="-2.9" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-3.4" y1="-3.4" x2="-3.4" y2="3.4" width="0.127" layer="21"/>
<wire x1="-2.9" y1="-2.8" x2="-2.9" y2="2.8" width="0.127" layer="51"/>
<wire x1="-1" y1="0.1" x2="1" y2="0.1" width="0" layer="47"/>
<wire x1="1" y1="0.1" x2="1" y2="0" width="0" layer="47"/>
<wire x1="-1" y1="0.1" x2="-1" y2="0" width="0" layer="47"/>
<wire x1="3.4" y1="3.4" x2="-3.4" y2="3.4" width="0.127" layer="21"/>
<wire x1="3.4" y1="-3.4" x2="-3.4" y2="-3.4" width="0.127" layer="21"/>
<smd name="1" x="-2.1" y="0" dx="2.2" dy="6.3" layer="1" roundness="20"/>
<smd name="2" x="2.1" y="0" dx="2.2" dy="6.3" layer="1" roundness="20"/>
<text x="-3.4" y="3.6" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="DR73/74">
<description>Coiltronics DR73/DR74 - 7.6x7.6 mm</description>
<wire x1="-3.8" y1="-3.8" x2="-3.8" y2="3.8" width="0.2032" layer="51"/>
<wire x1="-3.8" y1="3.8" x2="3.8" y2="3.8" width="0.2032" layer="51"/>
<wire x1="3.8" y1="3.8" x2="3.8" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="3.8" y1="-3.8" x2="-3.8" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="-2.6" y1="-3.3" x2="-3.3" y2="-2.6" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-2.6" x2="2.6" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="-3.3" x2="2.6" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-2.6" x2="3.3" y2="2.6" width="0.2032" layer="21"/>
<wire x1="3.3" y1="2.6" x2="2.6" y2="3.3" width="0.2032" layer="21"/>
<wire x1="2.6" y1="3.3" x2="-2.6" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="3.3" x2="-3.3" y2="2.6" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="-2.6" x2="-3.3" y2="2.6" width="0.2032" layer="21"/>
<smd name="1" x="-3" y="0" dx="3.25" dy="2.5" layer="1" rot="R90" thermals="no"/>
<smd name="2" x="3" y="0" dx="3.25" dy="2.5" layer="1" rot="R90" thermals="no"/>
<text x="-3.8" y="4.1" size="0.8128" layer="25" font="vector" ratio="20">&gt;NAME</text>
</package>
<package name="IHLP1616">
<description>Vishay IHLP-1616 - modified for 0.1 metric grid</description>
<wire x1="-2.2" y1="2" x2="2.2" y2="2" width="0.127" layer="21"/>
<wire x1="2.2" y1="2" x2="2.2" y2="1.5" width="0.127" layer="21"/>
<wire x1="2.2" y1="-1.5" x2="2.2" y2="-2" width="0.127" layer="21"/>
<wire x1="2.2" y1="-2" x2="-2.2" y2="-2" width="0.127" layer="21"/>
<wire x1="-2.2" y1="-2" x2="-2.2" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-2.2" y1="1.5" x2="-2.2" y2="2" width="0.127" layer="21"/>
<wire x1="-0.9" y1="-0.1" x2="0.9" y2="-0.1" width="0" layer="47"/>
<wire x1="0.9" y1="-0.1" x2="0.9" y2="-0.2" width="0" layer="47"/>
<wire x1="-0.9" y1="-0.1" x2="-0.9" y2="-0.2" width="0" layer="47"/>
<wire x1="-2.5" y1="0.1" x2="2.5" y2="0.1" width="0" layer="47"/>
<wire x1="2.5" y1="0.1" x2="2.5" y2="0" width="0" layer="47"/>
<wire x1="-2.5" y1="0.1" x2="-2.5" y2="0" width="0" layer="47"/>
<smd name="1" x="-1.7" y="0" dx="1.6" dy="2.4" layer="1" roundness="20"/>
<smd name="2" x="1.7" y="0" dx="1.6" dy="2.4" layer="1" roundness="20"/>
</package>
<package name="IHLP2020">
<description>Vishay IHLP-2020 - modified for 0.1 metric grid</description>
<wire x1="-2.7" y1="2.6" x2="2.7" y2="2.6" width="0.127" layer="21"/>
<wire x1="2.7" y1="2.6" x2="2.7" y2="1.6" width="0.127" layer="21"/>
<wire x1="2.7" y1="1.6" x2="2.7" y2="-1.6" width="0.127" layer="51"/>
<wire x1="2.7" y1="-1.6" x2="2.7" y2="-2.6" width="0.127" layer="21"/>
<wire x1="2.7" y1="-2.6" x2="-2.7" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-2.7" y1="-2.6" x2="-2.7" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-2.7" y1="-1.6" x2="-2.7" y2="1.6" width="0.127" layer="51"/>
<wire x1="-2.7" y1="1.6" x2="-2.7" y2="2.6" width="0.127" layer="21"/>
<wire x1="-1" y1="-0.1" x2="1" y2="-0.1" width="0" layer="47"/>
<wire x1="1" y1="-0.1" x2="1" y2="-0.2" width="0" layer="47"/>
<wire x1="-1" y1="-0.1" x2="-1" y2="-0.2" width="0" layer="47"/>
<wire x1="-3" y1="0.1" x2="3" y2="0.1" width="0" layer="47"/>
<wire x1="3" y1="0.1" x2="3" y2="0" width="0" layer="47"/>
<wire x1="-3" y1="0.1" x2="-3" y2="0" width="0" layer="47"/>
<smd name="1" x="-2" y="0" dx="2" dy="2.8" layer="1" roundness="20"/>
<smd name="2" x="2" y="0" dx="2" dy="2.8" layer="1" roundness="20"/>
<text x="-2.7" y="2.9" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="IHLP2525">
<description>Vishay IHLP-2525 - modified for 0.1 metric grid</description>
<wire x1="-3.2" y1="3.2" x2="3.2" y2="3.2" width="0.127" layer="21"/>
<wire x1="3.2" y1="3.2" x2="3.2" y2="2" width="0.127" layer="21"/>
<wire x1="3.2" y1="-2" x2="3.2" y2="-3.2" width="0.127" layer="21"/>
<wire x1="3.2" y1="-3.2" x2="-3.2" y2="-3.2" width="0.127" layer="21"/>
<wire x1="-3.2" y1="-3.2" x2="-3.2" y2="-2" width="0.127" layer="21"/>
<wire x1="-3.2" y1="2" x2="-3.2" y2="3.2" width="0.127" layer="21"/>
<wire x1="-1.9" y1="-0.1" x2="1.9" y2="-0.1" width="0" layer="47"/>
<wire x1="1.9" y1="-0.1" x2="1.9" y2="-0.2" width="0" layer="47"/>
<wire x1="-1.9" y1="-0.1" x2="-1.9" y2="-0.2" width="0" layer="47"/>
<wire x1="-3.7" y1="0.1" x2="3.7" y2="0.1" width="0" layer="47"/>
<wire x1="3.7" y1="0.1" x2="3.7" y2="0" width="0" layer="47"/>
<wire x1="-3.7" y1="0.1" x2="-3.7" y2="0" width="0" layer="47"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.4" layer="1" roundness="20"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.4" layer="1" roundness="20"/>
</package>
<package name="IHLP4040">
<description>Vishay IHLP-4040 - modified for 0.1 metric grid</description>
<wire x1="-3.05" y1="-0.1" x2="3.05" y2="-0.1" width="0" layer="47"/>
<wire x1="3.05" y1="-0.1" x2="3.05" y2="-0.2" width="0" layer="47"/>
<wire x1="-3.05" y1="-0.1" x2="-3.05" y2="-0.2" width="0" layer="47"/>
<wire x1="-5.75" y1="5.15" x2="5.75" y2="5.15" width="0.127" layer="21"/>
<wire x1="5.75" y1="5.15" x2="5.75" y2="2.7" width="0.127" layer="21"/>
<wire x1="5.75" y1="2.7" x2="5.75" y2="-2.7" width="0.127" layer="51"/>
<wire x1="5.75" y1="-2.7" x2="5.75" y2="-5.15" width="0.127" layer="21"/>
<wire x1="5.75" y1="-5.15" x2="-5.75" y2="-5.15" width="0.127" layer="21"/>
<wire x1="-5.75" y1="-5.15" x2="-5.75" y2="-2.7" width="0.127" layer="21"/>
<wire x1="-5.75" y1="-2.7" x2="-5.75" y2="2.7" width="0.127" layer="51"/>
<wire x1="-5.75" y1="2.7" x2="-5.75" y2="5.15" width="0.127" layer="21"/>
<wire x1="-6.55" y1="0.1" x2="6.55" y2="0.1" width="0" layer="47"/>
<wire x1="6.55" y1="0.1" x2="6.55" y2="0" width="0" layer="47"/>
<wire x1="-6.55" y1="0.1" x2="-6.55" y2="0" width="0" layer="47"/>
<wire x1="-1.6" y1="-5.15" x2="-1.6" y2="5.15" width="0" layer="47"/>
<wire x1="-1.6" y1="5.15" x2="-1.5" y2="5.15" width="0" layer="47"/>
<wire x1="-1.6" y1="-5.15" x2="-1.5" y2="-5.15" width="0" layer="47"/>
<smd name="1" x="-4.8" y="0" dx="3.5" dy="5" layer="1" roundness="20"/>
<smd name="2" x="4.8" y="0" dx="3.5" dy="5" layer="1" roundness="20"/>
<text x="-5.75" y="5.4" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="IHLP5050">
<description>Vishay IHLP-5050 - modified for 0.1 metric grid</description>
<wire x1="-4.1" y1="-0.1" x2="4.1" y2="-0.1" width="0" layer="47"/>
<wire x1="4.1" y1="-0.1" x2="4.1" y2="-0.2" width="0" layer="47"/>
<wire x1="-4.1" y1="-0.1" x2="-4.1" y2="-0.2" width="0" layer="47"/>
<wire x1="-6.6" y1="6.4" x2="6.6" y2="6.4" width="0.127" layer="21"/>
<wire x1="6.6" y1="6.4" x2="6.6" y2="2" width="0.127" layer="21"/>
<wire x1="6.6" y1="1.9" x2="6.6" y2="-1.9" width="0.127" layer="51"/>
<wire x1="6.6" y1="-2" x2="6.6" y2="-6.4" width="0.127" layer="21"/>
<wire x1="6.6" y1="-6.4" x2="-6.6" y2="-6.4" width="0.127" layer="21"/>
<wire x1="-6.6" y1="-6.4" x2="-6.6" y2="-2" width="0.127" layer="21"/>
<wire x1="-6.6" y1="-1.9" x2="-6.6" y2="1.9" width="0.127" layer="51"/>
<wire x1="-6.6" y1="2" x2="-6.6" y2="6.4" width="0.127" layer="21"/>
<wire x1="-6.9" y1="0.1" x2="6.9" y2="0.1" width="0" layer="47"/>
<wire x1="6.9" y1="0.1" x2="6.9" y2="0" width="0" layer="47"/>
<wire x1="-6.9" y1="0.1" x2="-6.9" y2="0" width="0" layer="47"/>
<smd name="1" x="-5.5" y="0" dx="2.8" dy="3.4" layer="1" roundness="20"/>
<smd name="2" x="5.5" y="0" dx="2.8" dy="3.4" layer="1" roundness="20"/>
<text x="-6.6" y="6.7" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="SD53">
<wire x1="-2.7" y1="1.2" x2="-2.7" y2="-1.2" width="0.127" layer="51"/>
<wire x1="-2.7" y1="-1.2" x2="-1.1" y2="-2.8" width="0.127" layer="21"/>
<wire x1="-1.1" y1="-2.8" x2="1.1" y2="-2.8" width="0.127" layer="21"/>
<wire x1="1.1" y1="-2.8" x2="2.7" y2="-1.2" width="0.127" layer="21"/>
<wire x1="2.7" y1="-1.2" x2="2.7" y2="1.2" width="0.127" layer="51"/>
<wire x1="2.7" y1="1.2" x2="1.1" y2="2.8" width="0.127" layer="21"/>
<wire x1="1.1" y1="2.8" x2="-1.1" y2="2.8" width="0.127" layer="21"/>
<wire x1="-1.1" y1="2.8" x2="-2.7" y2="1.2" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.6401" width="0.127" layer="21"/>
<circle x="-1.2" y="0" radius="0.1" width="0.127" layer="21"/>
<smd name="1" x="-2.5" y="0" dx="1" dy="2" layer="1"/>
<smd name="2" x="2.5" y="0" dx="1" dy="2" layer="1"/>
</package>
<package name="VLCF4020">
<description>TDK&lt;br&gt;
VLCF4018 - (4.00mm x 4.00mm x 1.80mm)&lt;br&gt;
VLCF4020 - (4.00mm x 4.00mm x 2.00mm)&lt;br&gt;</description>
<wire x1="-2.1" y1="2.4" x2="2.1" y2="2.4" width="0.127" layer="21"/>
<wire x1="2.1" y1="2.4" x2="2.1" y2="-2.4" width="0.127" layer="21"/>
<wire x1="2.1" y1="-2.4" x2="-2.1" y2="-2.4" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-2.4" x2="-2.1" y2="2.4" width="0.127" layer="21"/>
<wire x1="-1.9" y1="0.5" x2="1.9" y2="0.5" width="0" layer="41"/>
<wire x1="1.9" y1="0.5" x2="1.9" y2="-0.5" width="0" layer="41"/>
<wire x1="1.9" y1="-0.5" x2="-1.9" y2="-0.5" width="0" layer="41"/>
<wire x1="-1.9" y1="-0.5" x2="-1.9" y2="0.5" width="0" layer="41"/>
<wire x1="-0.6" y1="-0.05" x2="-0.2" y2="-0.05" width="0.127" layer="21" curve="-180"/>
<wire x1="-0.2" y1="-0.05" x2="0.2" y2="-0.05" width="0.127" layer="21" curve="-180"/>
<wire x1="0.2" y1="-0.05" x2="0.6" y2="-0.05" width="0.127" layer="21" curve="-180"/>
<wire x1="-0.6" y1="-0.05" x2="-0.9" y2="-0.05" width="0.127" layer="21"/>
<wire x1="0.6" y1="-0.05" x2="0.9" y2="-0.05" width="0.127" layer="21"/>
<wire x1="-0.9" y1="-0.05" x2="-0.9" y2="-0.35" width="0.127" layer="21"/>
<wire x1="0.9" y1="-0.05" x2="0.9" y2="0.35" width="0.127" layer="21"/>
<smd name="1" x="0" y="1.4" dx="3.8" dy="1.6" layer="1" roundness="20"/>
<smd name="2" x="0" y="-1.4" dx="3.8" dy="1.6" layer="1" roundness="20"/>
<text x="-2.1" y="2.6" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-1.8" y="0.35" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-1.8" y="-0.4" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="1.8" y="0.4" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="1.8" y="-0.35" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
</package>
<package name="VLCF5020">
<wire x1="-2.7" y1="2.8" x2="2.7" y2="2.8" width="0.127" layer="21"/>
<wire x1="2.7" y1="2.8" x2="2.7" y2="-2.8" width="0.127" layer="21"/>
<wire x1="2.7" y1="-2.8" x2="-2.7" y2="-2.8" width="0.127" layer="21"/>
<wire x1="-2.7" y1="-2.8" x2="-2.7" y2="2.8" width="0.127" layer="21"/>
<wire x1="-2.6" y1="0.5" x2="2.6" y2="0.5" width="0" layer="41"/>
<wire x1="2.6" y1="0.5" x2="2.6" y2="-0.5" width="0" layer="41"/>
<wire x1="2.6" y1="-0.5" x2="-2.6" y2="-0.5" width="0" layer="41"/>
<wire x1="-2.6" y1="-0.5" x2="-2.6" y2="0.5" width="0" layer="41"/>
<smd name="1" x="0" y="1.6" dx="5" dy="2" layer="1" roundness="20"/>
<smd name="2" x="0" y="-1.6" dx="5" dy="2" layer="1" roundness="20"/>
<text x="-2.7" y="3.1" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-2.5" y="0.35" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-2.5" y="-0.4" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="2.5" y="0.4" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="2.5" y="-0.35" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
</package>
<package name="VLS3015">
<description>TDK&lt;br&gt;
VLS3010 - (3.00mm x 3.00mm x 1.00mm)&lt;br&gt;
VLS3012 - (3.00mm x 3.00mm x 1.20mm)&lt;br&gt;
VLS3015 - (3.00mm x 3.00mm x 1.50mm)&lt;br&gt;</description>
<wire x1="-1.7" y1="1.8" x2="1.7" y2="1.8" width="0.127" layer="21"/>
<wire x1="1.7" y1="1.8" x2="1.7" y2="-1.8" width="0.127" layer="21"/>
<wire x1="1.7" y1="-1.8" x2="-1.7" y2="-1.8" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-1.8" x2="-1.7" y2="1.8" width="0.127" layer="21"/>
<wire x1="-1.6" y1="0.5" x2="1.6" y2="0.5" width="0" layer="41"/>
<wire x1="1.6" y1="0.5" x2="1.6" y2="0.2" width="0" layer="41"/>
<wire x1="1.6" y1="-0.2" x2="1.6" y2="-0.5" width="0" layer="41"/>
<wire x1="1.6" y1="-0.5" x2="-1.6" y2="-0.5" width="0" layer="41"/>
<wire x1="-1.6" y1="-0.5" x2="-1.6" y2="-0.2" width="0" layer="41"/>
<wire x1="-1.6" y1="0.2" x2="-1.6" y2="0.5" width="0" layer="41"/>
<wire x1="-0.6" y1="-0.05" x2="-0.2" y2="-0.05" width="0.127" layer="21" curve="-180"/>
<wire x1="-0.2" y1="-0.05" x2="0.2" y2="-0.05" width="0.127" layer="21" curve="-180"/>
<wire x1="0.2" y1="-0.05" x2="0.6" y2="-0.05" width="0.127" layer="21" curve="-180"/>
<wire x1="-0.6" y1="-0.05" x2="-0.9" y2="-0.05" width="0.127" layer="21"/>
<wire x1="0.6" y1="-0.05" x2="0.9" y2="-0.05" width="0.127" layer="21"/>
<wire x1="-0.9" y1="-0.05" x2="-0.9" y2="-0.35" width="0.127" layer="21"/>
<wire x1="0.9" y1="-0.05" x2="0.9" y2="0.35" width="0.127" layer="21"/>
<smd name="1" x="0" y="1.1" dx="3" dy="1" layer="1" roundness="20"/>
<smd name="2" x="0" y="-1.1" dx="3" dy="1" layer="1" roundness="20"/>
<text x="-1.7" y="2" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-1.5" y="0.35" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-1.5" y="-0.4" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="1.5" y="0.4" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="1.5" y="-0.35" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
</package>
<package name="VLS252015">
<description>TDK&lt;br&gt;
VLS252012 - (2.50mm x 2.00mm x 1.20mm)&lt;br&gt;
VLS252015 - (2.50mm x 2.00mm x 1.50mm)&lt;br&gt;</description>
<wire x1="-1.2" y1="1.6" x2="1.2" y2="1.6" width="0.127" layer="21"/>
<wire x1="1.2" y1="1.6" x2="1.2" y2="-1.6" width="0.127" layer="21"/>
<wire x1="1.2" y1="-1.6" x2="-1.2" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-1.2" y1="-1.6" x2="-1.2" y2="1.6" width="0.127" layer="21"/>
<wire x1="-0.9" y1="0.4" x2="0.9" y2="0.4" width="0" layer="41"/>
<wire x1="0.9" y1="0.4" x2="0.9" y2="-0.4" width="0" layer="41"/>
<wire x1="0.9" y1="-0.4" x2="-0.9" y2="-0.4" width="0" layer="41"/>
<wire x1="-0.9" y1="-0.4" x2="-0.9" y2="0.4" width="0" layer="41"/>
<wire x1="-0.6" y1="-0.05" x2="-0.2" y2="-0.05" width="0.127" layer="21" curve="-180"/>
<wire x1="-0.2" y1="-0.05" x2="0.2" y2="-0.05" width="0.127" layer="21" curve="-180"/>
<wire x1="0.2" y1="-0.05" x2="0.6" y2="-0.05" width="0.127" layer="21" curve="-180"/>
<wire x1="-0.6" y1="-0.05" x2="-0.9" y2="-0.05" width="0.127" layer="21"/>
<wire x1="0.6" y1="-0.05" x2="0.9" y2="-0.05" width="0.127" layer="21"/>
<wire x1="-0.9" y1="-0.05" x2="-0.9" y2="-0.35" width="0.127" layer="21"/>
<wire x1="0.9" y1="-0.05" x2="0.9" y2="0.35" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.95" dx="2" dy="0.9" layer="1" roundness="20"/>
<smd name="2" x="0" y="-0.95" dx="2" dy="0.9" layer="1" roundness="20"/>
<text x="-1.2" y="1.8" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-1" y="0.25" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-1" y="-0.3" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="1" y="0.3" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="1" y="-0.25" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
</package>
<package name="MSOP10">
<description>also Texas Instruments DGS&lt;p&gt;
also Linear MS</description>
<wire x1="-1.5" y1="1.4" x2="1.5" y2="1.4" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.4" x2="1.5" y2="-1.4" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.4" x2="-1.5" y2="-1.4" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.4" x2="-1.5" y2="1.4" width="0.127" layer="21"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="-1" width="0.127" layer="21" curve="180"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="-1" width="0.127" layer="21"/>
<smd name="1" x="-1" y="-2.1" dx="0.28" dy="1" layer="1"/>
<smd name="2" x="-0.5" y="-2.1" dx="0.28" dy="1" layer="1"/>
<smd name="3" x="0" y="-2.1" dx="0.28" dy="1" layer="1"/>
<smd name="4" x="0.5" y="-2.1" dx="0.28" dy="1" layer="1"/>
<smd name="5" x="1" y="-2.1" dx="0.28" dy="1" layer="1"/>
<smd name="6" x="1" y="2.1" dx="0.28" dy="1" layer="1" rot="R180"/>
<smd name="7" x="0.5" y="2.1" dx="0.28" dy="1" layer="1" rot="R180"/>
<smd name="8" x="0" y="2.1" dx="0.28" dy="1" layer="1" rot="R180"/>
<smd name="9" x="-0.5" y="2.1" dx="0.28" dy="1" layer="1" rot="R180"/>
<smd name="10" x="-1" y="2.1" dx="0.28" dy="1" layer="1" rot="R180"/>
<text x="-1.8" y="-1.4" size="0.8128" layer="25" font="vector" ratio="16" rot="R90">&gt;NAME</text>
<text x="-1.35" y="-1.3" size="0.05" layer="27" font="vector" ratio="0" rot="R90">&gt;VALUE</text>
<text x="-1.4" y="1.3" size="0.05" layer="27" font="vector" ratio="0" rot="R270">&gt;VALUE</text>
<text x="1.4" y="-1.3" size="0.05" layer="27" font="vector" ratio="0" rot="R90">&gt;VALUE</text>
<text x="1.35" y="1.3" size="0.05" layer="27" font="vector" ratio="0" rot="R270">&gt;VALUE</text>
<rectangle x1="-1.1244" y1="-2.4" x2="-0.8744" y2="-1.4" layer="51"/>
<rectangle x1="-0.6244" y1="-2.4" x2="-0.3744" y2="-1.4" layer="51"/>
<rectangle x1="-0.1244" y1="-2.4" x2="0.1256" y2="-1.4" layer="51"/>
<rectangle x1="0.3756" y1="-2.4" x2="0.6256" y2="-1.4" layer="51"/>
<rectangle x1="0.8756" y1="-2.4" x2="1.1256" y2="-1.4" layer="51"/>
<rectangle x1="0.8744" y1="1.4" x2="1.1244" y2="2.4" layer="51"/>
<rectangle x1="0.3744" y1="1.4" x2="0.6244" y2="2.4" layer="51"/>
<rectangle x1="-0.1256" y1="1.4" x2="0.1244" y2="2.4" layer="51"/>
<rectangle x1="-0.6256" y1="1.4" x2="-0.3756" y2="2.4" layer="51"/>
<rectangle x1="-1.1256" y1="1.4" x2="-0.8756" y2="2.4" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="FRAME-A4L-LOC">
<wire x1="0" y1="0" x2="50.8" y2="0" width="0.1016" layer="94"/>
<wire x1="50.8" y1="0" x2="98.425" y2="0" width="0.1016" layer="94"/>
<wire x1="98.425" y1="0" x2="146.05" y2="0" width="0.1016" layer="94"/>
<wire x1="146.05" y1="0" x2="193.675" y2="0" width="0.1016" layer="94"/>
<wire x1="193.675" y1="0" x2="241.3" y2="0" width="0.1016" layer="94"/>
<wire x1="260.35" y1="0" x2="260.35" y2="53.975" width="0.1016" layer="94"/>
<wire x1="260.35" y1="53.975" x2="260.35" y2="104.775" width="0.1016" layer="94"/>
<wire x1="260.35" y1="104.775" x2="260.35" y2="155.575" width="0.1016" layer="94"/>
<wire x1="260.35" y1="155.575" x2="260.35" y2="179.07" width="0.1016" layer="94"/>
<wire x1="0" y1="179.07" x2="0" y2="155.575" width="0.1016" layer="94"/>
<wire x1="0" y1="155.575" x2="0" y2="104.775" width="0.1016" layer="94"/>
<wire x1="0" y1="104.775" x2="0" y2="53.975" width="0.1016" layer="94"/>
<wire x1="0" y1="53.975" x2="0" y2="0" width="0.1016" layer="94"/>
<wire x1="3.175" y1="3.175" x2="50.8" y2="3.175" width="0.1016" layer="94"/>
<wire x1="50.8" y1="3.175" x2="98.425" y2="3.175" width="0.1016" layer="94"/>
<wire x1="257.175" y1="3.175" x2="257.175" y2="8.255" width="0.1016" layer="94"/>
<wire x1="257.175" y1="8.255" x2="257.175" y2="13.335" width="0.1016" layer="94"/>
<wire x1="257.175" y1="13.335" x2="257.175" y2="18.415" width="0.1016" layer="94"/>
<wire x1="257.175" y1="18.415" x2="257.175" y2="23.495" width="0.1016" layer="94"/>
<wire x1="257.175" y1="23.495" x2="257.175" y2="53.975" width="0.1016" layer="94"/>
<wire x1="257.175" y1="53.975" x2="257.175" y2="104.775" width="0.1016" layer="94"/>
<wire x1="257.175" y1="104.775" x2="257.175" y2="155.575" width="0.1016" layer="94"/>
<wire x1="257.175" y1="155.575" x2="257.175" y2="175.895" width="0.1016" layer="94"/>
<wire x1="193.675" y1="175.895" x2="146.05" y2="175.895" width="0.1016" layer="94"/>
<wire x1="146.05" y1="175.895" x2="98.425" y2="175.895" width="0.1016" layer="94"/>
<wire x1="98.425" y1="175.895" x2="50.8" y2="175.895" width="0.1016" layer="94"/>
<wire x1="50.8" y1="175.895" x2="3.175" y2="175.895" width="0.1016" layer="94"/>
<wire x1="3.175" y1="175.895" x2="3.175" y2="155.575" width="0.1016" layer="94"/>
<wire x1="3.175" y1="155.575" x2="3.175" y2="104.775" width="0.1016" layer="94"/>
<wire x1="3.175" y1="104.775" x2="3.175" y2="53.975" width="0.1016" layer="94"/>
<wire x1="3.175" y1="53.975" x2="3.175" y2="3.175" width="0.1016" layer="94"/>
<wire x1="193.675" y1="179.07" x2="146.05" y2="179.07" width="0.1016" layer="94"/>
<wire x1="146.05" y1="179.07" x2="98.425" y2="179.07" width="0.1016" layer="94"/>
<wire x1="98.425" y1="179.07" x2="50.8" y2="179.07" width="0.1016" layer="94"/>
<wire x1="50.8" y1="179.07" x2="0" y2="179.07" width="0.1016" layer="94"/>
<wire x1="193.675" y1="179.07" x2="193.675" y2="175.895" width="0.1016" layer="94"/>
<wire x1="193.675" y1="3.175" x2="193.675" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="104.775" x2="3.175" y2="104.775" width="0.1016" layer="94"/>
<wire x1="257.175" y1="155.575" x2="260.35" y2="155.575" width="0.1016" layer="94"/>
<wire x1="98.425" y1="175.895" x2="98.425" y2="179.07" width="0.1016" layer="94"/>
<wire x1="98.425" y1="3.175" x2="98.425" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="53.975" x2="3.175" y2="53.975" width="0.1016" layer="94"/>
<wire x1="257.175" y1="104.775" x2="260.35" y2="104.775" width="0.1016" layer="94"/>
<wire x1="0" y1="155.575" x2="3.175" y2="155.575" width="0.1016" layer="94"/>
<wire x1="50.8" y1="175.895" x2="50.8" y2="179.07" width="0.1016" layer="94"/>
<wire x1="257.175" y1="53.975" x2="260.35" y2="53.975" width="0.1016" layer="94"/>
<wire x1="146.05" y1="175.895" x2="146.05" y2="179.07" width="0.1016" layer="94"/>
<wire x1="241.3" y1="179.07" x2="241.3" y2="175.895" width="0.1016" layer="94"/>
<wire x1="241.3" y1="3.175" x2="241.3" y2="0" width="0.1016" layer="94"/>
<wire x1="146.05" y1="3.175" x2="146.05" y2="0" width="0.1016" layer="94"/>
<wire x1="50.8" y1="0" x2="50.8" y2="3.175" width="0.1016" layer="94"/>
<wire x1="161.925" y1="3.175" x2="161.925" y2="23.495" width="0.6096" layer="94"/>
<wire x1="161.925" y1="23.495" x2="215.9" y2="23.495" width="0.6096" layer="94"/>
<wire x1="215.9" y1="23.495" x2="257.175" y2="23.495" width="0.6096" layer="94"/>
<wire x1="247.015" y1="3.175" x2="247.015" y2="8.255" width="0.1016" layer="94"/>
<wire x1="247.015" y1="8.255" x2="257.175" y2="8.255" width="0.1016" layer="94"/>
<wire x1="247.015" y1="8.255" x2="215.9" y2="8.255" width="0.1016" layer="94"/>
<wire x1="215.9" y1="8.255" x2="215.9" y2="3.175" width="0.6096" layer="94"/>
<wire x1="215.9" y1="8.255" x2="215.9" y2="13.335" width="0.6096" layer="94"/>
<wire x1="215.9" y1="13.335" x2="257.175" y2="13.335" width="0.1016" layer="94"/>
<wire x1="215.9" y1="13.335" x2="215.9" y2="18.415" width="0.6096" layer="94"/>
<wire x1="215.9" y1="18.415" x2="257.175" y2="18.415" width="0.1016" layer="94"/>
<wire x1="215.9" y1="18.415" x2="215.9" y2="23.495" width="0.6096" layer="94"/>
<wire x1="257.175" y1="175.895" x2="193.675" y2="175.895" width="0.1016" layer="94"/>
<wire x1="260.35" y1="179.07" x2="193.675" y2="179.07" width="0.1016" layer="94"/>
<wire x1="98.425" y1="3.175" x2="193.548" y2="3.175" width="0.1016" layer="94"/>
<wire x1="193.929" y1="3.175" x2="241.3" y2="3.175" width="0.1016" layer="94"/>
<wire x1="241.427" y1="3.175" x2="257.175" y2="3.175" width="0.1016" layer="94"/>
<wire x1="241.427" y1="0" x2="260.35" y2="0" width="0.1016" layer="94"/>
<text x="24.384" y="0.254" size="2.54" layer="94" font="vector">A</text>
<text x="74.422" y="0.254" size="2.54" layer="94" font="vector">B</text>
<text x="121.158" y="0.254" size="2.54" layer="94" font="vector">C</text>
<text x="169.418" y="0.254" size="2.54" layer="94" font="vector">D</text>
<text x="216.916" y="0.254" size="2.54" layer="94" font="vector">E</text>
<text x="254.762" y="0.254" size="2.54" layer="94" font="vector">F</text>
<text x="258.064" y="28.702" size="2.54" layer="94" font="vector">1</text>
<text x="257.81" y="79.502" size="2.54" layer="94" font="vector">2</text>
<text x="257.81" y="130.302" size="2.54" layer="94" font="vector">3</text>
<text x="215.9" y="176.276" size="2.54" layer="94" font="vector">E</text>
<text x="168.148" y="176.276" size="2.54" layer="94" font="vector">D</text>
<text x="120.904" y="176.276" size="2.54" layer="94" font="vector">C</text>
<text x="72.898" y="176.276" size="2.54" layer="94" font="vector">B</text>
<text x="24.384" y="176.276" size="2.54" layer="94" font="vector">A</text>
<text x="0.762" y="130.302" size="2.54" layer="94" font="vector">3</text>
<text x="0.762" y="79.248" size="2.54" layer="94" font="vector">2</text>
<text x="1.016" y="26.67" size="2.54" layer="94" font="vector">1</text>
<text x="217.805" y="14.605" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="217.805" y="9.525" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="231.14" y="4.445" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="217.551" y="4.318" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="249.682" y="176.276" size="2.54" layer="94" font="vector">F</text>
<text x="257.556" y="168.148" size="2.54" layer="94" font="vector">4</text>
<text x="0.508" y="168.148" size="2.54" layer="94" font="vector">4</text>
</symbol>
<symbol name="R-EU@1">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<circle x="-3.2385" y="-0.4445" radius="0.0635" width="0" layer="94"/>
<text x="-2.54" y="1.524" size="1.27" layer="95" font="vector">&gt;NAME</text>
<text x="-2.54" y="-2.794" size="1.27" layer="96" font="vector">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="POWER-0.030=1/32W">
<text x="-1.9685" y="-0.4445" size="0.889" layer="94" font="vector">1/32W</text>
</symbol>
<symbol name="POWER-0.050=1/20W">
<text x="-1.9685" y="-0.4445" size="0.889" layer="94" font="vector">1/20W</text>
</symbol>
<symbol name="POWER-0.063=1/16W">
<text x="-1.9685" y="-0.4445" size="0.889" layer="94" font="vector">1/16W</text>
</symbol>
<symbol name="POWER-0.100=1/10W">
<text x="-1.9685" y="-0.4445" size="0.889" layer="94" font="vector">1/10W</text>
</symbol>
<symbol name="POWER-0.125=1/8W">
<text x="-1.524" y="-0.4445" size="0.889" layer="94" font="vector">1/8W</text>
</symbol>
<symbol name="POWER-0.167=1/6W">
<text x="-1.524" y="-0.4445" size="0.889" layer="94" font="vector">1/6W</text>
</symbol>
<symbol name="POWER-0.200=1/5W">
<text x="-1.524" y="-0.4445" size="0.889" layer="94" font="vector">1/5W</text>
</symbol>
<symbol name="POWER-0.250=1/4W">
<text x="-1.524" y="-0.4445" size="0.889" layer="94" font="vector">1/4W</text>
</symbol>
<symbol name="POWER-0.333=1/3W">
<text x="-1.524" y="-0.4445" size="0.889" layer="94" font="vector">1/3W</text>
</symbol>
<symbol name="POWER-0.500=1/2W">
<text x="-1.524" y="-0.4445" size="0.889" layer="94" font="vector">1/2W</text>
</symbol>
<symbol name="POWER-0.750=3/4W">
<text x="-1.524" y="-0.4445" size="0.889" layer="94" font="vector">3/4W</text>
</symbol>
<symbol name="POWER-1W">
<text x="-0.6985" y="-0.4445" size="0.889" layer="94" font="vector">1W</text>
</symbol>
<symbol name="POWER-2W">
<text x="-0.6985" y="-0.4445" size="0.889" layer="94" font="vector">2W</text>
</symbol>
<symbol name="POWER-3W">
<text x="-0.6985" y="-0.4445" size="0.889" layer="94" font="vector">3W</text>
</symbol>
<symbol name="POWER-4W">
<text x="-0.6985" y="-0.4445" size="0.889" layer="94" font="vector">4W</text>
</symbol>
<symbol name="POWER-5W">
<text x="-0.6985" y="-0.4445" size="0.889" layer="94" font="vector">5W</text>
</symbol>
<symbol name="POWER-10W">
<text x="-1.1429" y="-0.4445" size="0.889" layer="94" font="vector">10W</text>
</symbol>
<symbol name="POWER-15W">
<text x="-1.1429" y="-0.4445" size="0.889" layer="94" font="vector">15W</text>
</symbol>
<symbol name="POWER-20W">
<text x="-1.1429" y="-0.4445" size="0.889" layer="94" font="vector">20W</text>
</symbol>
<symbol name="POWER-25W">
<text x="-1.1429" y="-0.4445" size="0.889" layer="94" font="vector">25W</text>
</symbol>
<symbol name="POWER-35W">
<text x="-1.1429" y="-0.4445" size="0.889" layer="94" font="vector">35W</text>
</symbol>
<symbol name="POWER-40W">
<text x="-1.1429" y="-0.4445" size="0.889" layer="94" font="vector">40W</text>
</symbol>
<symbol name="POWER-100W">
<text x="-1.524" y="-0.4445" size="0.889" layer="94" font="vector">100W</text>
</symbol>
<symbol name="POWER-50W">
<text x="-1.1429" y="-0.4445" size="0.889" layer="94" font="vector">50W</text>
</symbol>
<symbol name="5V0">
<wire x1="0" y1="1.905" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.778" y="3.175" size="1.397" layer="96">&gt;VALUE</text>
<pin name="5V0" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="FT2232D">
<wire x1="-15.24" y1="40.64" x2="15.24" y2="40.64" width="0.254" layer="94"/>
<wire x1="15.24" y1="40.64" x2="15.24" y2="-40.64" width="0.254" layer="94"/>
<wire x1="15.24" y1="-40.64" x2="-15.24" y2="-40.64" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-40.64" x2="-15.24" y2="40.64" width="0.254" layer="94"/>
<wire x1="13.208" y1="41.91" x2="13.716" y2="41.402" width="0.254" layer="94"/>
<wire x1="13.716" y1="41.402" x2="14.732" y2="42.418" width="0.254" layer="94"/>
<text x="-15.24" y="-43.18" size="1.778" layer="96">&gt;VALUE</text>
<text x="-15.24" y="41.148" size="1.778" layer="95">&gt;NAME</text>
<pin name="3V3OUT" x="-17.78" y="17.78" length="short" direction="out"/>
<pin name="ACBUS0" x="17.78" y="15.24" length="short" rot="R180"/>
<pin name="ACBUS1" x="17.78" y="12.7" length="short" rot="R180"/>
<pin name="ACBUS2" x="17.78" y="10.16" length="short" rot="R180"/>
<pin name="ACBUS3" x="17.78" y="7.62" length="short" rot="R180"/>
<pin name="ADBUS0" x="17.78" y="38.1" length="short" rot="R180"/>
<pin name="ADBUS1" x="17.78" y="35.56" length="short" rot="R180"/>
<pin name="ADBUS2" x="17.78" y="33.02" length="short" rot="R180"/>
<pin name="ADBUS3" x="17.78" y="30.48" length="short" rot="R180"/>
<pin name="ADBUS4" x="17.78" y="27.94" length="short" rot="R180"/>
<pin name="ADBUS5" x="17.78" y="25.4" length="short" rot="R180"/>
<pin name="ADBUS6" x="17.78" y="22.86" length="short" rot="R180"/>
<pin name="ADBUS7" x="17.78" y="20.32" length="short" rot="R180"/>
<pin name="GND@ANALOG" x="-17.78" y="-27.94" length="short" direction="pwr"/>
<pin name="AVCC" x="-17.78" y="22.86" length="short" direction="in"/>
<pin name="BCBUS0" x="17.78" y="-22.86" length="short" rot="R180"/>
<pin name="BCBUS1" x="17.78" y="-25.4" length="short" rot="R180"/>
<pin name="BCBUS2" x="17.78" y="-27.94" length="short" rot="R180"/>
<pin name="BCBUS3" x="17.78" y="-30.48" length="short" rot="R180"/>
<pin name="BDBUS0" x="17.78" y="0" length="short" rot="R180"/>
<pin name="BDBUS1" x="17.78" y="-2.54" length="short" rot="R180"/>
<pin name="BDBUS2" x="17.78" y="-5.08" length="short" rot="R180"/>
<pin name="BDBUS3" x="17.78" y="-7.62" length="short" rot="R180"/>
<pin name="BDBUS4" x="17.78" y="-10.16" length="short" rot="R180"/>
<pin name="BDBUS5" x="17.78" y="-12.7" length="short" rot="R180"/>
<pin name="BDBUS6" x="17.78" y="-15.24" length="short" rot="R180"/>
<pin name="BDBUS7" x="17.78" y="-17.78" length="short" rot="R180"/>
<pin name="EECS" x="-17.78" y="-15.24" length="short"/>
<pin name="EEDATA" x="-17.78" y="-20.32" length="short"/>
<pin name="EESK" x="-17.78" y="-17.78" length="short" direction="out" function="clk"/>
<pin name="GND@9" x="-17.78" y="-30.48" length="short" direction="pwr"/>
<pin name="GND@18" x="-17.78" y="-33.02" length="short" direction="pwr"/>
<pin name="GND@25" x="-17.78" y="-35.56" length="short" direction="pwr"/>
<pin name="GND@34" x="-17.78" y="-38.1" length="short" direction="pwr"/>
<pin name="PWREN#" x="17.78" y="-38.1" length="short" direction="out" rot="R180"/>
<pin name="RESET#" x="-17.78" y="33.02" length="short" direction="in"/>
<pin name="RSTOUT#" x="-17.78" y="5.08" length="short" direction="out"/>
<pin name="SI/WUA" x="17.78" y="5.08" length="short" rot="R180"/>
<pin name="SI/WUB" x="17.78" y="-33.02" length="short" rot="R180"/>
<pin name="TEST" x="-17.78" y="-25.4" length="short" direction="in"/>
<pin name="USBDM" x="-17.78" y="12.7" length="short"/>
<pin name="USBDP" x="-17.78" y="10.16" length="short"/>
<pin name="VCC@3" x="-17.78" y="38.1" length="short" direction="in"/>
<pin name="VCC@42" x="-17.78" y="35.56" length="short" direction="in"/>
<pin name="VCCIOA" x="-17.78" y="30.48" length="short" direction="in"/>
<pin name="VCCIOB" x="-17.78" y="27.94" length="short" direction="in"/>
<pin name="XTIN" x="-17.78" y="-2.54" length="short" direction="in"/>
<pin name="XTOUT" x="-17.78" y="-7.62" length="short" direction="out"/>
</symbol>
<symbol name="SIGNAL-UNIDIRECTIONAL">
<polygon width="0.0254" layer="95">
<vertex x="-1.016" y="0.762"/>
<vertex x="-1.016" y="-0.762"/>
<vertex x="1.016" y="0"/>
</polygon>
</symbol>
<symbol name="DIGITAL-EEPROM-MICROWIRE-6P">
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<text x="-7.62" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-7.62" y="8.128" size="1.778" layer="95">&gt;NAME</text>
<pin name="CS" x="10.16" y="5.08" length="short" direction="in" rot="R180"/>
<pin name="DI" x="10.16" y="0" length="short" direction="in" rot="R180"/>
<pin name="DO" x="10.16" y="-2.54" length="short" direction="out" rot="R180"/>
<pin name="GND" x="-10.16" y="-2.54" length="short" direction="pwr"/>
<pin name="CLK" x="10.16" y="2.54" length="short" direction="in" function="clk" rot="R180"/>
<pin name="VCC" x="-10.16" y="5.08" length="short" direction="in"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.159" y="-2.032" size="1.397" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="3V3">
<wire x1="0" y1="1.905" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.778" y="3.175" size="1.397" layer="96">&gt;VALUE</text>
<pin name="3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="C-EU">
<wire x1="-2.54" y1="0" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="0.762" y2="0" width="0.1524" layer="94"/>
<circle x="-1.4605" y="-0.4445" radius="0.0635" width="0" layer="94"/>
<text x="-2.54" y="2.032" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.27" layer="96">&gt;VALUE</text>
<rectangle x1="-0.762" y1="-1.524" x2="-0.254" y2="1.524" layer="94"/>
<rectangle x1="0.254" y1="-1.524" x2="0.762" y2="1.524" layer="94"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="L-EU">
<wire x1="2.54" y1="0" x2="0.8467" y2="0" width="0.254" layer="94" curve="180"/>
<wire x1="-2.54" y1="0" x2="-0.8467" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-0.8467" y1="0" x2="0.8467" y2="0" width="0.254" layer="94" curve="-180"/>
<circle x="-3.302" y="-0.4445" radius="0.0635" width="0" layer="94"/>
<text x="-2.54" y="1.27" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-1.778" size="1.27" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="R-EU-SMALL">
<wire x1="-2.54" y1="0" x2="-1.778" y2="0" width="0.1524" layer="94"/>
<wire x1="1.778" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-0.762" x2="1.778" y2="0" width="0.254" layer="94"/>
<wire x1="1.778" y1="0" x2="1.778" y2="0.762" width="0.254" layer="94"/>
<wire x1="1.778" y1="0.762" x2="-1.778" y2="0.762" width="0.254" layer="94"/>
<wire x1="-1.778" y1="0.762" x2="-1.778" y2="0" width="0.254" layer="94"/>
<wire x1="-1.778" y1="0" x2="-1.778" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-0.762" x2="1.778" y2="-0.762" width="0.254" layer="94"/>
<circle x="-2.2225" y="-0.4445" radius="0.0635" width="0" layer="94"/>
<text x="-1.778" y="1.27" size="1.016" layer="95">&gt;NAME</text>
<text x="-1.778" y="-2.286" size="1.016" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="1V2">
<wire x1="0" y1="1.905" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.778" y="3.175" size="1.397" layer="96">&gt;VALUE</text>
<pin name="1V2" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="LTC3419">
<wire x1="-10.16" y1="15.24" x2="10.16" y2="15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="15.24" x2="10.16" y2="-15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="-15.24" x2="-10.16" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-15.24" x2="-10.16" y2="15.24" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-6.35" x2="-12.7" y2="-6.35" width="0.1524" layer="250" curve="-270"/>
<wire x1="-12.7" y1="-6.35" x2="-12.446" y2="-5.334" width="0.1524" layer="250"/>
<wire x1="-12.7" y1="-6.35" x2="-11.938" y2="-5.588" width="0.1524" layer="250"/>
<wire x1="20.32" y1="0" x2="22.86" y2="0" width="0.1524" layer="250" curve="-270"/>
<wire x1="22.86" y1="0" x2="23.114" y2="1.016" width="0.1524" layer="250"/>
<wire x1="22.86" y1="0" x2="23.622" y2="0.762" width="0.1524" layer="250"/>
<wire x1="20.32" y1="-20.32" x2="22.86" y2="-20.32" width="0.1524" layer="250" curve="-270"/>
<wire x1="22.86" y1="-20.32" x2="23.114" y2="-19.304" width="0.1524" layer="250"/>
<wire x1="22.86" y1="-20.32" x2="23.622" y2="-19.558" width="0.1524" layer="250"/>
<text x="-10.16" y="15.748" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<text x="7.62" y="8.89" size="1.27" layer="250" rot="MR0">(0.6V)</text>
<text x="7.62" y="-11.43" size="1.27" layer="250" rot="MR0">(0.6V)</text>
<pin name="GND@5" x="-12.7" y="-10.16" length="short" direction="pwr"/>
<pin name="GND@6" x="-12.7" y="-12.7" length="short" direction="pwr"/>
<pin name="MODE" x="-12.7" y="5.08" length="short" direction="in"/>
<pin name="RUN1" x="-12.7" y="12.7" length="short" direction="in"/>
<pin name="RUN2" x="-12.7" y="10.16" length="short" direction="in"/>
<pin name="SW1" x="12.7" y="12.7" length="short" direction="out" rot="R180"/>
<pin name="SW2" x="12.7" y="-7.62" length="short" direction="out" rot="R180"/>
<pin name="VFB1" x="12.7" y="7.62" length="short" direction="in" rot="R180"/>
<pin name="VFB2" x="12.7" y="-12.7" length="short" direction="in" rot="R180"/>
<pin name="VIN" x="-12.7" y="0" length="short" direction="in"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FRAME-A4L-LOC" prefix="FRAME">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="A" symbol="FRAME-A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="A" symbol="R-EU@1" x="0" y="0"/>
<gate name="G$1" symbol="POWER-0.030=1/32W" x="-10.16" y="22.86" addlevel="request"/>
<gate name="G$2" symbol="POWER-0.050=1/20W" x="-5.08" y="22.86" addlevel="request"/>
<gate name="G$3" symbol="POWER-0.063=1/16W" x="0" y="22.86" addlevel="request"/>
<gate name="G$4" symbol="POWER-0.100=1/10W" x="5.08" y="22.86" addlevel="request"/>
<gate name="G$5" symbol="POWER-0.125=1/8W" x="10.16" y="22.86" addlevel="request"/>
<gate name="G$6" symbol="POWER-0.167=1/6W" x="15.24" y="22.86" addlevel="request"/>
<gate name="G$7" symbol="POWER-0.200=1/5W" x="-10.16" y="17.78" addlevel="request"/>
<gate name="G$8" symbol="POWER-0.250=1/4W" x="-5.08" y="17.78" addlevel="request"/>
<gate name="G$9" symbol="POWER-0.333=1/3W" x="0" y="17.78" addlevel="request"/>
<gate name="G$10" symbol="POWER-0.500=1/2W" x="5.08" y="17.78" addlevel="request"/>
<gate name="G$11" symbol="POWER-0.750=3/4W" x="10.16" y="17.78" addlevel="request"/>
<gate name="G$12" symbol="POWER-1W" x="15.24" y="17.78" addlevel="request"/>
<gate name="G$13" symbol="POWER-2W" x="-10.16" y="12.7" addlevel="request"/>
<gate name="G$14" symbol="POWER-3W" x="-5.08" y="12.7" addlevel="request"/>
<gate name="G$15" symbol="POWER-4W" x="0" y="12.7" addlevel="request"/>
<gate name="G$16" symbol="POWER-5W" x="5.08" y="12.7" addlevel="request"/>
<gate name="G$17" symbol="POWER-10W" x="10.16" y="12.7" addlevel="request"/>
<gate name="G$18" symbol="POWER-15W" x="15.24" y="12.7" addlevel="request"/>
<gate name="G$19" symbol="POWER-20W" x="-10.16" y="7.62" addlevel="request"/>
<gate name="G$20" symbol="POWER-25W" x="-5.08" y="7.62" addlevel="request"/>
<gate name="G$21" symbol="POWER-35W" x="0" y="7.62" addlevel="request"/>
<gate name="G$22" symbol="POWER-40W" x="5.08" y="7.62" addlevel="request"/>
<gate name="G$23" symbol="POWER-100W" x="15.24" y="7.62" addlevel="request"/>
<gate name="G$24" symbol="POWER-50W" x="10.16" y="7.62" addlevel="request"/>
</gates>
<devices>
<device name="0201" package="0201@1">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402@1">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0504" package="0504">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603@1">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805@1">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="01005" package="01005">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206@1">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210@1">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1806" package="1806@1">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="1812@1">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1825" package="1825">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="2010">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="2512@1">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3616" package="3616">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4022" package="4022">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402S" package="0402S@1">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="5V0" prefix="5V0_PWR">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="5V0" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FT2232D" prefix="U">
<gates>
<gate name="X" symbol="FT2232D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LQFP48">
<connects>
<connect gate="X" pin="3V3OUT" pad="6"/>
<connect gate="X" pin="ACBUS0" pad="15"/>
<connect gate="X" pin="ACBUS1" pad="13"/>
<connect gate="X" pin="ACBUS2" pad="12"/>
<connect gate="X" pin="ACBUS3" pad="11"/>
<connect gate="X" pin="ADBUS0" pad="24"/>
<connect gate="X" pin="ADBUS1" pad="23"/>
<connect gate="X" pin="ADBUS2" pad="22"/>
<connect gate="X" pin="ADBUS3" pad="21"/>
<connect gate="X" pin="ADBUS4" pad="20"/>
<connect gate="X" pin="ADBUS5" pad="19"/>
<connect gate="X" pin="ADBUS6" pad="17"/>
<connect gate="X" pin="ADBUS7" pad="16"/>
<connect gate="X" pin="AVCC" pad="46"/>
<connect gate="X" pin="BCBUS0" pad="30"/>
<connect gate="X" pin="BCBUS1" pad="29"/>
<connect gate="X" pin="BCBUS2" pad="28"/>
<connect gate="X" pin="BCBUS3" pad="27"/>
<connect gate="X" pin="BDBUS0" pad="40"/>
<connect gate="X" pin="BDBUS1" pad="39"/>
<connect gate="X" pin="BDBUS2" pad="38"/>
<connect gate="X" pin="BDBUS3" pad="37"/>
<connect gate="X" pin="BDBUS4" pad="36"/>
<connect gate="X" pin="BDBUS5" pad="35"/>
<connect gate="X" pin="BDBUS6" pad="33"/>
<connect gate="X" pin="BDBUS7" pad="32"/>
<connect gate="X" pin="EECS" pad="48"/>
<connect gate="X" pin="EEDATA" pad="2"/>
<connect gate="X" pin="EESK" pad="1"/>
<connect gate="X" pin="GND@18" pad="18"/>
<connect gate="X" pin="GND@25" pad="25"/>
<connect gate="X" pin="GND@34" pad="34"/>
<connect gate="X" pin="GND@9" pad="9"/>
<connect gate="X" pin="GND@ANALOG" pad="45"/>
<connect gate="X" pin="PWREN#" pad="41"/>
<connect gate="X" pin="RESET#" pad="4"/>
<connect gate="X" pin="RSTOUT#" pad="5"/>
<connect gate="X" pin="SI/WUA" pad="10"/>
<connect gate="X" pin="SI/WUB" pad="26"/>
<connect gate="X" pin="TEST" pad="47"/>
<connect gate="X" pin="USBDM" pad="8"/>
<connect gate="X" pin="USBDP" pad="7"/>
<connect gate="X" pin="VCC@3" pad="3"/>
<connect gate="X" pin="VCC@42" pad="42"/>
<connect gate="X" pin="VCCIOA" pad="14"/>
<connect gate="X" pin="VCCIOB" pad="31"/>
<connect gate="X" pin="XTIN" pad="43"/>
<connect gate="X" pin="XTOUT" pad="44"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SIGNAL-UNIDIRECTIONAL" prefix="DIR">
<gates>
<gate name="1" symbol="SIGNAL-UNIDIRECTIONAL" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIGITAL-EEPROM-MICROWIRE-6P" prefix="U" uservalue="yes">
<gates>
<gate name="1" symbol="DIGITAL-EEPROM-MICROWIRE-6P" x="0" y="0"/>
</gates>
<devices>
<device name="_SOT23" package="SOT23-6">
<connects>
<connect gate="1" pin="CLK" pad="4"/>
<connect gate="1" pin="CS" pad="5"/>
<connect gate="1" pin="DI" pad="3"/>
<connect gate="1" pin="DO" pad="1"/>
<connect gate="1" pin="GND" pad="2"/>
<connect gate="1" pin="VCC" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND_PWR">
<gates>
<gate name="A" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3V3" prefix="3V3_PWR">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C" prefix="C" uservalue="yes">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="A" symbol="C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="0201" package="0201">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402I" package="0402I">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402S" package="0402S">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0504" package="0504">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603I" package="0603I">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805I" package="0805I">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="01005" package="01005">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206I" package="1206I">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1806" package="1806">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="1812">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1825" package="1825">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="2010">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="2512">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3616" package="3616">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4022" package="4022">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4527" package="4527">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="L" prefix="L" uservalue="yes">
<description>&lt;B&gt;INDUCTOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="A" symbol="L-EU" x="0" y="0"/>
</gates>
<devices>
<device name="0201" package="0201">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402I" package="0402I">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0504" package="0504">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603I" package="0603I">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805I" package="0805I">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="01005" package="01005">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206I" package="1206I">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1806" package="1806">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="1812">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1825" package="1825">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="2010">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="2512">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3616" package="3616">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4022" package="4022">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_CDEP134" package="CDEP134">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_CDRH5D" package="CDRH5D">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_DR73/74" package="DR73/74">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_IHLP1616" package="IHLP1616">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_IHLP2020" package="IHLP2020">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_IHLP2525" package="IHLP2525">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_IHLP4040" package="IHLP4040">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_IHLP5050" package="IHLP5050">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SD53" package="SD53">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_VLCF4020" package="VLCF4020">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_VLCF5020" package="VLCF5020">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_VLS3015" package="VLS3015">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_VLS252015" package="VLS252015">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R-SMALL" prefix="R" uservalue="yes">
<gates>
<gate name="1" symbol="R-EU-SMALL" x="0" y="0"/>
</gates>
<devices>
<device name="0201" package="0201">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402I" package="0402I">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402S" package="0402S">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603I" package="0603I">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805I" package="0805I">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206I" package="1206I">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1V2" prefix="1V2_PWR">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="1V2" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LTC3419" prefix="U">
<description>Dual Monolithic 600mA Synchronous Step-Down Regulator</description>
<gates>
<gate name="A" symbol="LTC3419" x="0" y="0"/>
</gates>
<devices>
<device name="MS" package="MSOP10">
<connects>
<connect gate="A" pin="GND@5" pad="5"/>
<connect gate="A" pin="GND@6" pad="6"/>
<connect gate="A" pin="MODE" pad="3"/>
<connect gate="A" pin="RUN1" pad="2"/>
<connect gate="A" pin="RUN2" pad="9"/>
<connect gate="A" pin="SW1" pad="4"/>
<connect gate="A" pin="SW2" pad="8"/>
<connect gate="A" pin="VFB1" pad="1"/>
<connect gate="A" pin="VFB2" pad="10"/>
<connect gate="A" pin="VIN" pad="7"/>
</connects>
<technologies>
<technology name=""/>
<technology name="E"/>
<technology name="I"/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="crystal">
<packages>
<package name="HC49/S">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-3.048" y1="-2.159" x2="3.048" y2="-2.159" width="0.4064" layer="21"/>
<wire x1="-3.048" y1="2.159" x2="3.048" y2="2.159" width="0.4064" layer="21"/>
<wire x1="-3.048" y1="-1.651" x2="3.048" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="3.048" y1="1.651" x2="-3.048" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="0.254" y2="0.762" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.762" x2="0.254" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.762" x2="-0.254" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.762" x2="-0.254" y2="0.762" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0.762" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.762" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.27" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="2.159" x2="-3.048" y2="-2.159" width="0.4064" layer="21" curve="180"/>
<wire x1="3.048" y1="-2.159" x2="3.048" y2="2.159" width="0.4064" layer="21" curve="180"/>
<wire x1="-3.048" y1="1.651" x2="-3.048" y2="-1.651" width="0.1524" layer="21" curve="180"/>
<wire x1="3.048" y1="-1.651" x2="3.048" y2="1.651" width="0.1524" layer="21" curve="180"/>
<pad name="1" x="-2.413" y="0" drill="0.8128"/>
<pad name="2" x="2.413" y="0" drill="0.8128"/>
<text x="-5.08" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-3.937" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.445" y1="-2.54" x2="4.445" y2="2.54" layer="43"/>
<rectangle x1="-5.08" y1="-1.905" x2="-4.445" y2="1.905" layer="43"/>
<rectangle x1="-5.715" y1="-1.27" x2="-5.08" y2="1.27" layer="43"/>
<rectangle x1="4.445" y1="-1.905" x2="5.08" y2="1.905" layer="43"/>
<rectangle x1="5.08" y1="-1.27" x2="5.715" y2="1.27" layer="43"/>
</package>
<package name="HC49GW">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-5.08" y1="-6.35" x2="5.08" y2="-6.35" width="0.8128" layer="21"/>
<wire x1="4.445" y1="6.731" x2="1.016" y2="6.731" width="0.1524" layer="21"/>
<wire x1="1.016" y1="6.731" x2="-1.016" y2="6.731" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="6.731" x2="-4.445" y2="6.731" width="0.1524" layer="21"/>
<wire x1="4.445" y1="6.731" x2="5.08" y2="6.096" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="6.096" x2="-4.445" y2="6.731" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.3302" y1="5.08" x2="-0.3302" y2="2.54" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="2.54" x2="0.3048" y2="2.54" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="2.54" x2="0.3048" y2="5.08" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="5.08" x2="-0.3302" y2="5.08" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="5.08" x2="0.9398" y2="3.81" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="5.08" x2="-0.9398" y2="3.81" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="3.81" x2="2.032" y2="3.81" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="3.81" x2="0.9398" y2="2.54" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="3.81" x2="-2.032" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="3.81" x2="-0.9398" y2="2.54" width="0.3048" layer="21"/>
<wire x1="-2.413" y1="-6.604" x2="-2.413" y2="-8.255" width="0.6096" layer="51"/>
<wire x1="2.413" y1="-6.477" x2="2.413" y2="-8.382" width="0.6096" layer="51"/>
<wire x1="5.08" y1="-6.35" x2="5.08" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="6.096" x2="-5.08" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="0" y1="8.382" x2="0" y2="6.985" width="0.6096" layer="51"/>
<smd name="1" x="-2.413" y="-8.001" dx="1.27" dy="2.54" layer="1"/>
<smd name="2" x="2.413" y="-8.001" dx="1.27" dy="2.54" layer="1"/>
<smd name="3" x="0" y="8.001" dx="1.27" dy="2.794" layer="1"/>
<text x="-5.588" y="-5.08" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-5.715" y1="-8.255" x2="5.715" y2="8.255" layer="43"/>
</package>
<package name="HC49TL-H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="5.334" y1="-5.715" x2="-5.461" y2="-5.715" width="0.8128" layer="21"/>
<wire x1="4.445" y1="7.366" x2="1.143" y2="7.366" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="7.366" x2="-4.445" y2="7.366" width="0.1524" layer="21"/>
<wire x1="4.445" y1="7.366" x2="5.08" y2="6.731" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="6.731" x2="-4.445" y2="7.366" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.3302" y1="5.715" x2="-0.3302" y2="3.175" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="3.175" x2="0.3048" y2="3.175" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="3.175" x2="0.3048" y2="5.715" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="5.715" x2="-0.3302" y2="5.715" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="5.715" x2="0.9398" y2="4.445" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="5.715" x2="-0.9398" y2="4.445" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="4.445" x2="2.032" y2="4.445" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="4.445" x2="0.9398" y2="3.175" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="4.445" x2="-2.032" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="4.445" x2="-0.9398" y2="3.175" width="0.3048" layer="21"/>
<wire x1="-2.413" y1="-5.842" x2="-2.413" y2="-7.62" width="0.6096" layer="51"/>
<wire x1="2.413" y1="-5.842" x2="2.413" y2="-7.62" width="0.6096" layer="51"/>
<wire x1="5.08" y1="-5.715" x2="5.08" y2="6.731" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="6.731" x2="-5.08" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="1.143" y1="7.366" x2="-1.143" y2="7.366" width="0.1524" layer="51"/>
<wire x1="0" y1="7.874" x2="0" y2="7.62" width="0.6096" layer="51"/>
<pad name="1" x="-2.413" y="-7.62" drill="0.8128"/>
<pad name="2" x="2.413" y="-7.62" drill="0.8128"/>
<pad name="3" x="0" y="7.874" drill="0.8128"/>
<text x="-5.461" y="-4.445" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-2.54" y="-4.699" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.3048" y1="7.366" x2="0.3048" y2="7.5692" layer="51"/>
<rectangle x1="-6.35" y1="-6.985" x2="6.35" y2="-4.445" layer="43"/>
<rectangle x1="-5.715" y1="-4.445" x2="5.715" y2="8.255" layer="43"/>
</package>
<package name="HC49U-H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-5.08" y1="-3.175" x2="5.08" y2="-3.175" width="0.8128" layer="21"/>
<wire x1="4.445" y1="9.906" x2="-4.445" y2="9.906" width="0.1524" layer="21"/>
<wire x1="4.445" y1="9.906" x2="5.08" y2="9.271" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="9.271" x2="-4.445" y2="9.906" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.3302" y1="8.255" x2="-0.3302" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="5.715" x2="0.3048" y2="5.715" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="5.715" x2="0.3048" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="8.255" x2="-0.3302" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="8.255" x2="0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="8.255" x2="-0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="1.905" y2="6.985" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-1.905" y2="6.985" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-2.413" y1="-3.302" x2="-2.413" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="2.413" y1="-3.302" x2="2.413" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="5.08" y1="-3.175" x2="5.08" y2="9.271" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="9.271" x2="-5.08" y2="-3.175" width="0.1524" layer="21"/>
<pad name="1" x="-2.413" y="-5.08" drill="0.8128"/>
<pad name="2" x="2.413" y="-5.08" drill="0.8128"/>
<text x="-5.461" y="-1.397" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-2.54" y="-2.032" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-6.35" y1="-4.445" x2="6.35" y2="-1.905" layer="43"/>
<rectangle x1="-5.715" y1="-1.905" x2="5.715" y2="10.795" layer="43"/>
</package>
<package name="HC49U-LM">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-5.08" y1="-3.175" x2="5.08" y2="-3.175" width="0.8128" layer="21"/>
<wire x1="4.445" y1="9.906" x2="-4.445" y2="9.906" width="0.1524" layer="21"/>
<wire x1="4.445" y1="9.906" x2="5.08" y2="9.271" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="9.271" x2="-4.445" y2="9.906" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.572" y1="3.81" x2="4.572" y2="3.81" width="0.6096" layer="21"/>
<wire x1="-0.3302" y1="8.255" x2="-0.3302" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="5.715" x2="0.3048" y2="5.715" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="5.715" x2="0.3048" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="8.255" x2="-0.3302" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="8.255" x2="0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="8.255" x2="-0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="2.54" y2="6.985" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-2.54" y2="6.985" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-2.413" y1="-3.302" x2="-2.413" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="2.413" y1="-3.302" x2="2.413" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.048" x2="5.08" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="5.08" y1="4.572" x2="5.08" y2="9.271" width="0.1524" layer="21"/>
<wire x1="4.572" y1="3.81" x2="5.08" y2="3.81" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.715" y2="3.81" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.08" y2="3.048" width="0.1524" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.08" y2="4.572" width="0.1524" layer="51"/>
<wire x1="-5.08" y1="3.175" x2="-5.08" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="9.271" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="3.81" x2="-5.715" y2="3.81" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="3.175" width="0.1524" layer="51"/>
<pad name="1" x="-2.413" y="-5.08" drill="0.8128"/>
<pad name="2" x="2.413" y="-5.08" drill="0.8128"/>
<pad name="M" x="-5.715" y="3.81" drill="0.8128"/>
<pad name="M1" x="5.715" y="3.81" drill="0.8128"/>
<text x="-5.08" y="10.414" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.572" y="0" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.715" y1="-5.08" x2="5.715" y2="10.795" layer="43"/>
</package>
<package name="HC49U-V">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-2.921" y1="-2.286" x2="2.921" y2="-2.286" width="0.4064" layer="21"/>
<wire x1="-2.921" y1="2.286" x2="2.921" y2="2.286" width="0.4064" layer="21"/>
<wire x1="-2.921" y1="-1.778" x2="2.921" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.778" x2="-2.921" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.778" x2="2.921" y2="-1.778" width="0.1524" layer="21" curve="-180"/>
<wire x1="2.921" y1="2.286" x2="2.921" y2="-2.286" width="0.4064" layer="21" curve="-180"/>
<wire x1="-2.921" y1="2.286" x2="-2.921" y2="-2.286" width="0.4064" layer="21" curve="180"/>
<wire x1="-2.921" y1="1.778" x2="-2.921" y2="-1.778" width="0.1524" layer="21" curve="180"/>
<wire x1="-0.254" y1="0.889" x2="0.254" y2="0.889" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.889" x2="0.254" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.889" x2="-0.254" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.889" x2="-0.254" y2="0.889" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0.889" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.889" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.27" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-2.413" y="0" drill="0.8128"/>
<pad name="2" x="2.413" y="0" drill="0.8128"/>
<text x="-5.08" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.191" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.81" y1="-2.794" x2="3.81" y2="2.794" layer="43"/>
<rectangle x1="-4.318" y1="-2.54" x2="-3.81" y2="2.54" layer="43"/>
<rectangle x1="-4.826" y1="-2.286" x2="-4.318" y2="2.286" layer="43"/>
<rectangle x1="-5.334" y1="-1.778" x2="-4.826" y2="1.778" layer="43"/>
<rectangle x1="-5.588" y1="-1.27" x2="-5.334" y2="1.016" layer="43"/>
<rectangle x1="3.81" y1="-2.54" x2="4.318" y2="2.54" layer="43"/>
<rectangle x1="4.318" y1="-2.286" x2="4.826" y2="2.286" layer="43"/>
<rectangle x1="4.826" y1="-1.778" x2="5.334" y2="1.778" layer="43"/>
<rectangle x1="5.334" y1="-1.016" x2="5.588" y2="1.016" layer="43"/>
</package>
<package name="HC49U70">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-3.048" y1="-2.54" x2="3.048" y2="-2.54" width="0.4064" layer="21"/>
<wire x1="-3.048" y1="2.54" x2="3.048" y2="2.54" width="0.4064" layer="21"/>
<wire x1="-3.048" y1="-2.032" x2="3.048" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-0.3302" y1="1.016" x2="-0.3302" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-0.3302" y1="-1.016" x2="0.3048" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="-1.016" x2="0.3048" y2="1.016" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="1.016" x2="-0.3302" y2="1.016" width="0.1524" layer="21"/>
<wire x1="0.6858" y1="1.016" x2="0.6858" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.6858" y1="1.016" x2="-0.6858" y2="0" width="0.1524" layer="21"/>
<wire x1="0.6858" y1="0" x2="1.397" y2="0" width="0.1524" layer="21"/>
<wire x1="0.6858" y1="0" x2="0.6858" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-0.6858" y1="0" x2="-1.397" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.6858" y1="0" x2="-0.6858" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="-2.54" width="0.4064" layer="21" curve="180"/>
<wire x1="3.048" y1="2.54" x2="3.048" y2="-2.54" width="0.4064" layer="21" curve="-180"/>
<wire x1="-3.048" y1="-2.032" x2="-3.048" y2="2.032" width="0.1524" layer="21" curve="-180"/>
<wire x1="3.048" y1="2.032" x2="3.048" y2="-2.032" width="0.1524" layer="21" curve="-180"/>
<wire x1="3.048" y1="2.032" x2="-3.048" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-2.413" y="0" drill="0.8128"/>
<pad name="2" x="2.413" y="0" drill="0.8128"/>
<text x="-5.08" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.445" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.572" y1="-2.794" x2="-4.064" y2="2.794" layer="43"/>
<rectangle x1="-5.08" y1="-2.54" x2="-4.572" y2="2.54" layer="43"/>
<rectangle x1="-5.588" y1="-2.032" x2="-5.08" y2="2.032" layer="43"/>
<rectangle x1="-5.842" y1="-1.27" x2="-5.588" y2="1.27" layer="43"/>
<rectangle x1="-4.064" y1="-3.048" x2="4.064" y2="3.048" layer="43"/>
<rectangle x1="4.064" y1="-2.794" x2="4.572" y2="2.794" layer="43"/>
<rectangle x1="4.572" y1="-2.54" x2="5.08" y2="2.54" layer="43"/>
<rectangle x1="5.08" y1="-2.032" x2="5.588" y2="2.032" layer="43"/>
<rectangle x1="5.588" y1="-1.27" x2="5.842" y2="1.27" layer="43"/>
</package>
<package name="HC49UP">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-5.1091" y1="1.143" x2="-3.429" y2="2.0321" width="0.0508" layer="21" curve="-55.770993"/>
<wire x1="-5.715" y1="1.143" x2="-5.715" y2="2.159" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.032" x2="5.1091" y2="1.143" width="0.0508" layer="21" curve="-55.772485"/>
<wire x1="5.715" y1="1.143" x2="5.715" y2="2.159" width="0.1524" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="-3.429" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="3.429" y1="-2.032" x2="-3.429" y2="-2.032" width="0.0508" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="3.429" y2="1.27" width="0.0508" layer="21"/>
<wire x1="5.461" y1="-2.413" x2="-5.461" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-0.381" x2="6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="6.477" y1="-0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="5.461" y1="-2.413" x2="5.715" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.715" y1="-1.143" x2="5.715" y2="1.143" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-2.159" x2="5.715" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="3.9826" y2="-1.143" width="0.0508" layer="21" curve="25.842828"/>
<wire x1="3.429" y1="1.27" x2="3.9826" y2="1.143" width="0.0508" layer="21" curve="-25.842828"/>
<wire x1="3.429" y1="-2.032" x2="5.109" y2="-1.1429" width="0.0508" layer="21" curve="55.771157"/>
<wire x1="3.9826" y1="-1.143" x2="3.9826" y2="1.143" width="0.0508" layer="51" curve="128.314524"/>
<wire x1="5.1091" y1="-1.143" x2="5.1091" y2="1.143" width="0.0508" layer="51" curve="68.456213"/>
<wire x1="-5.1091" y1="-1.143" x2="-3.429" y2="-2.032" width="0.0508" layer="21" curve="55.772485"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.9826" y2="1.143" width="0.0508" layer="51" curve="-128.314524"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.429" y2="-1.27" width="0.0508" layer="21" curve="25.842828"/>
<wire x1="-3.9826" y1="1.143" x2="-3.429" y2="1.27" width="0.0508" layer="21" curve="-25.842828"/>
<wire x1="-6.477" y1="-0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.1091" y1="-1.143" x2="-5.1091" y2="1.143" width="0.0508" layer="51" curve="-68.456213"/>
<wire x1="-5.715" y1="-1.143" x2="-5.715" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-0.381" x2="-5.715" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="0.381" x2="-5.715" y2="1.143" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-2.159" x2="-5.715" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.159" x2="-5.461" y2="-2.413" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.715" y1="-0.381" x2="-6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-3.429" y1="2.032" x2="3.429" y2="2.032" width="0.0508" layer="21"/>
<wire x1="5.461" y1="2.413" x2="-5.461" y2="2.413" width="0.1524" layer="21"/>
<wire x1="5.461" y1="2.413" x2="5.715" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.715" y1="2.159" x2="-5.461" y2="2.413" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.254" y1="0.635" x2="-0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.635" x2="0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.635" x2="0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.635" x2="-0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.016" y2="0" width="0.0508" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.016" y2="0" width="0.0508" layer="21"/>
<smd name="1" x="-4.826" y="0" dx="5.334" dy="1.9304" layer="1"/>
<smd name="2" x="4.826" y="0" dx="5.334" dy="1.9304" layer="1"/>
<text x="-5.715" y="2.794" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.715" y="-4.191" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.604" y1="-3.048" x2="6.604" y2="3.048" layer="43"/>
</package>
<package name="HC13U-H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-9.906" y1="-3.048" x2="-9.271" y2="-3.048" width="1.27" layer="21"/>
<wire x1="-9.271" y1="-3.048" x2="9.271" y2="-3.048" width="1.27" layer="21"/>
<wire x1="9.271" y1="-3.048" x2="9.906" y2="-3.048" width="1.27" layer="21"/>
<wire x1="8.636" y1="33.401" x2="-8.636" y2="33.401" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="32.766" x2="-8.636" y2="33.401" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.636" y1="33.401" x2="9.271" y2="32.766" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.017" y1="15.24" x2="9.017" y2="15.24" width="0.6096" layer="21"/>
<wire x1="-0.3302" y1="21.59" x2="-0.3302" y2="19.05" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="19.05" x2="0.3048" y2="19.05" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="19.05" x2="0.3048" y2="21.59" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="21.59" x2="-0.3302" y2="21.59" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="21.59" x2="0.9398" y2="20.32" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="21.59" x2="-0.9398" y2="20.32" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="20.32" x2="1.905" y2="20.32" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="20.32" x2="0.9398" y2="19.05" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="20.32" x2="-1.905" y2="20.32" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="20.32" x2="-0.9398" y2="19.05" width="0.3048" layer="21"/>
<wire x1="9.144" y1="15.24" x2="10.16" y2="15.24" width="0.6096" layer="51"/>
<wire x1="-10.16" y1="15.24" x2="-9.144" y2="15.24" width="0.6096" layer="51"/>
<wire x1="-6.223" y1="-3.175" x2="-6.223" y2="-5.08" width="0.8128" layer="51"/>
<wire x1="6.223" y1="-3.175" x2="6.223" y2="-5.08" width="0.8128" layer="51"/>
<wire x1="9.271" y1="15.748" x2="9.271" y2="32.766" width="0.1524" layer="21"/>
<wire x1="9.271" y1="14.732" x2="9.271" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="9.271" y1="15.748" x2="9.271" y2="14.732" width="0.1524" layer="51"/>
<wire x1="-9.271" y1="14.732" x2="-9.271" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="15.748" x2="-9.271" y2="32.766" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="15.748" x2="-9.271" y2="14.732" width="0.1524" layer="51"/>
<pad name="1" x="-6.223" y="-5.08" drill="1.016"/>
<pad name="2" x="6.223" y="-5.08" drill="1.016"/>
<pad name="M" x="-10.16" y="15.24" drill="0.8128"/>
<pad name="M1" x="10.16" y="15.24" drill="0.8128"/>
<text x="-10.16" y="0" size="1.778" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-5.08" y="-1.27" size="1.778" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-5.08" x2="10.795" y2="34.925" layer="43"/>
</package>
<package name="HC18U-H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="5.461" y1="-3.175" x2="5.08" y2="-3.175" width="0.8128" layer="21"/>
<wire x1="5.08" y1="-3.175" x2="-5.08" y2="-3.175" width="0.8128" layer="21"/>
<wire x1="-5.08" y1="-3.175" x2="-5.461" y2="-3.175" width="0.8128" layer="21"/>
<wire x1="4.445" y1="10.16" x2="-4.445" y2="10.16" width="0.1524" layer="21"/>
<wire x1="4.445" y1="10.16" x2="5.08" y2="9.525" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="9.525" x2="-4.445" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.572" y1="3.81" x2="4.572" y2="3.81" width="0.6096" layer="21"/>
<wire x1="-0.3302" y1="8.255" x2="-0.3302" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="5.715" x2="0.3048" y2="5.715" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="5.715" x2="0.3048" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="8.255" x2="-0.3302" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="8.255" x2="0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="8.255" x2="-0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="2.54" y2="6.985" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-2.54" y2="6.985" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="-3.302" x2="-2.54" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="2.54" y1="-3.302" x2="2.54" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.048" x2="5.08" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="5.08" y1="4.572" x2="5.08" y2="9.525" width="0.1524" layer="21"/>
<wire x1="4.572" y1="3.81" x2="5.08" y2="3.81" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.715" y2="3.81" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.08" y2="3.048" width="0.1524" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.08" y2="4.572" width="0.1524" layer="51"/>
<wire x1="-5.08" y1="3.175" x2="-5.08" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="9.525" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="3.81" x2="-5.715" y2="3.81" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="3.175" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="-5.08" drill="0.8128"/>
<pad name="2" x="2.54" y="-5.08" drill="0.8128"/>
<pad name="M" x="-5.715" y="3.81" drill="0.8128"/>
<pad name="M1" x="5.715" y="3.81" drill="0.8128"/>
<text x="-5.08" y="10.668" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-0.889" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.35" y1="-5.08" x2="6.35" y2="10.795" layer="43"/>
</package>
<package name="HC18U-V">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="1.905" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="4.445" y2="2.54" width="0.4064" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="4.445" y2="-2.54" width="0.4064" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="5.08" y2="-1.905" width="0.4064" layer="21" curve="90"/>
<wire x1="4.445" y1="2.54" x2="5.08" y2="1.905" width="0.4064" layer="21" curve="-90"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.4064" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.4064" layer="21" curve="90"/>
<wire x1="-4.318" y1="-1.905" x2="4.318" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.905" x2="4.445" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.778" x2="4.445" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.318" y1="1.905" x2="4.445" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.318" y1="1.905" x2="-4.318" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.778" x2="-4.318" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.778" x2="-4.445" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="-1.905" x2="-4.445" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-0.3302" y1="1.27" x2="-0.3302" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="-1.27" x2="0.3048" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="-1.27" x2="0.3048" y2="1.27" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="1.27" x2="-0.3302" y2="1.27" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="1.27" x2="0.9398" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="1.27" x2="-0.9398" y2="0" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="0" x2="0.9398" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="0" x2="-0.9398" y2="-1.27" width="0.3048" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128"/>
<pad name="2" x="2.54" y="0" drill="0.8128"/>
<text x="-5.0546" y="3.2766" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.6228" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.715" y1="-3.175" x2="5.715" y2="3.175" layer="43"/>
</package>
<package name="HC33U-H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-9.906" y1="-3.048" x2="-9.271" y2="-3.048" width="1.27" layer="21"/>
<wire x1="-9.271" y1="-3.048" x2="9.271" y2="-3.048" width="1.27" layer="21"/>
<wire x1="9.271" y1="-3.048" x2="9.906" y2="-3.048" width="1.27" layer="21"/>
<wire x1="8.636" y1="16.51" x2="-8.636" y2="16.51" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="15.875" x2="-8.636" y2="16.51" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.636" y1="16.51" x2="9.271" y2="15.875" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.017" y1="7.62" x2="9.017" y2="7.62" width="0.6096" layer="21"/>
<wire x1="-0.3302" y1="13.97" x2="-0.3302" y2="11.43" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="11.43" x2="0.3048" y2="11.43" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="11.43" x2="0.3048" y2="13.97" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="13.97" x2="-0.3302" y2="13.97" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="13.97" x2="0.9398" y2="12.7" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="13.97" x2="-0.9398" y2="12.7" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="12.7" x2="1.905" y2="12.7" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="12.7" x2="0.9398" y2="11.43" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="12.7" x2="-1.905" y2="12.7" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="12.7" x2="-0.9398" y2="11.43" width="0.3048" layer="21"/>
<wire x1="9.144" y1="7.62" x2="10.16" y2="7.62" width="0.6096" layer="51"/>
<wire x1="-10.16" y1="7.62" x2="-9.144" y2="7.62" width="0.6096" layer="51"/>
<wire x1="-6.223" y1="-3.175" x2="-6.223" y2="-5.08" width="0.8128" layer="51"/>
<wire x1="6.223" y1="-3.175" x2="6.223" y2="-5.08" width="0.8128" layer="51"/>
<wire x1="9.271" y1="8.128" x2="9.271" y2="15.875" width="0.1524" layer="21"/>
<wire x1="9.271" y1="7.112" x2="9.271" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="9.271" y1="8.128" x2="9.271" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-9.271" y1="7.112" x2="-9.271" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="8.128" x2="-9.271" y2="15.875" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="8.128" x2="-9.271" y2="7.112" width="0.1524" layer="51"/>
<pad name="1" x="-6.223" y="-5.08" drill="1.016"/>
<pad name="2" x="6.223" y="-5.08" drill="1.016"/>
<pad name="M" x="-10.16" y="7.62" drill="0.8128"/>
<pad name="M1" x="10.16" y="7.62" drill="0.8128"/>
<text x="-7.62" y="17.272" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.35" y="2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.62" y1="-3.175" x2="-6.985" y2="16.51" layer="21"/>
<rectangle x1="6.985" y1="-3.175" x2="7.62" y2="16.51" layer="21"/>
<rectangle x1="-10.795" y1="-5.715" x2="10.795" y2="17.145" layer="43"/>
</package>
<package name="HC33U-V">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-0.3302" y1="2.54" x2="-0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="0" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="2.54" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="2.54" x2="-0.3302" y2="2.54" width="0.3048" layer="21"/>
<wire x1="0.9652" y1="2.54" x2="0.9652" y2="1.27" width="0.3048" layer="21"/>
<wire x1="-0.9652" y1="2.54" x2="-0.9652" y2="1.27" width="0.3048" layer="21"/>
<wire x1="0.9652" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.9652" y1="1.27" x2="0.9652" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.9652" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.9652" y1="1.27" x2="-0.9652" y2="0" width="0.3048" layer="21"/>
<wire x1="-5.207" y1="4.064" x2="5.207" y2="4.064" width="0.254" layer="21"/>
<wire x1="-5.207" y1="-4.064" x2="5.207" y2="-4.064" width="0.254" layer="21"/>
<wire x1="-5.207" y1="-3.683" x2="5.207" y2="-3.683" width="0.0508" layer="21"/>
<wire x1="-5.207" y1="3.683" x2="5.207" y2="3.683" width="0.0508" layer="21"/>
<wire x1="-5.207" y1="-3.683" x2="-5.207" y2="3.683" width="0.0508" layer="21" curve="-180"/>
<wire x1="5.207" y1="3.683" x2="5.207" y2="-3.683" width="0.0508" layer="21" curve="-180"/>
<wire x1="5.207" y1="4.064" x2="5.207" y2="-4.064" width="0.254" layer="21" curve="-180"/>
<wire x1="-5.207" y1="4.064" x2="-5.207" y2="-4.064" width="0.254" layer="21" curve="180"/>
<pad name="1" x="-6.223" y="0" drill="1.016"/>
<pad name="2" x="6.223" y="0" drill="1.016"/>
<text x="-5.08" y="4.826" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.255" y1="-3.81" x2="-6.985" y2="3.81" layer="43"/>
<rectangle x1="-8.89" y1="-3.175" x2="-8.255" y2="3.175" layer="43"/>
<rectangle x1="-9.525" y1="-2.54" x2="-8.89" y2="2.54" layer="43"/>
<rectangle x1="-6.985" y1="-4.445" x2="6.985" y2="4.445" layer="43"/>
<rectangle x1="6.985" y1="-3.81" x2="8.255" y2="3.81" layer="43"/>
<rectangle x1="8.255" y1="-3.175" x2="8.89" y2="3.175" layer="43"/>
<rectangle x1="8.89" y1="-2.54" x2="9.525" y2="2.54" layer="43"/>
</package>
<package name="SM49">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="3.429" y1="-2.032" x2="5.109" y2="-1.1429" width="0.0508" layer="21" curve="55.771157"/>
<wire x1="5.715" y1="-1.143" x2="5.715" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-5.1091" y1="-1.143" x2="-3.429" y2="-2.032" width="0.0508" layer="21" curve="55.772485"/>
<wire x1="-5.715" y1="-1.143" x2="-5.715" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="3.429" y2="1.27" width="0.0508" layer="21"/>
<wire x1="-3.429" y1="2.032" x2="3.429" y2="2.032" width="0.0508" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="-3.429" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="-5.461" y1="2.413" x2="5.461" y2="2.413" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-0.381" x2="-6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="0.381" x2="-6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="2.159" x2="-5.461" y2="2.413" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.715" y1="1.143" x2="-5.715" y2="-1.143" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="2.159" x2="-5.715" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-3.9826" y1="1.143" x2="-3.429" y2="1.27" width="0.0508" layer="21" curve="-25.842828"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.429" y2="-1.27" width="0.0508" layer="21" curve="25.842828"/>
<wire x1="-5.1091" y1="1.143" x2="-3.429" y2="2.0321" width="0.0508" layer="21" curve="-55.770993"/>
<wire x1="-3.9826" y1="1.143" x2="-3.9826" y2="-1.143" width="0.0508" layer="51" curve="128.314524"/>
<wire x1="-5.1091" y1="1.143" x2="-5.1091" y2="-1.143" width="0.0508" layer="51" curve="68.456213"/>
<wire x1="3.429" y1="2.032" x2="5.1091" y2="1.143" width="0.0508" layer="21" curve="-55.772485"/>
<wire x1="3.9826" y1="1.143" x2="3.9826" y2="-1.143" width="0.0508" layer="51" curve="-128.314524"/>
<wire x1="3.429" y1="1.27" x2="3.9826" y2="1.143" width="0.0508" layer="21" curve="-25.842828"/>
<wire x1="3.429" y1="-1.27" x2="3.9826" y2="-1.143" width="0.0508" layer="21" curve="25.842828"/>
<wire x1="6.477" y1="0.381" x2="6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="5.1091" y1="1.143" x2="5.1091" y2="-1.143" width="0.0508" layer="51" curve="-68.456213"/>
<wire x1="5.715" y1="1.143" x2="5.715" y2="0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="0.381" x2="5.715" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-0.381" x2="5.715" y2="-1.143" width="0.1524" layer="51"/>
<wire x1="5.715" y1="2.159" x2="5.715" y2="1.143" width="0.1524" layer="21"/>
<wire x1="5.461" y1="2.413" x2="5.715" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="5.715" y1="0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-0.381" x2="6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="3.429" y1="-2.032" x2="-3.429" y2="-2.032" width="0.0508" layer="21"/>
<wire x1="-5.461" y1="-2.413" x2="5.461" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.159" x2="-5.461" y2="-2.413" width="0.1524" layer="21" curve="90"/>
<wire x1="5.461" y1="-2.413" x2="5.715" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.254" y1="0.635" x2="-0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.635" x2="0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.635" x2="0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.635" x2="-0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.143" y2="0" width="0.0508" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.143" y2="0" width="0.0508" layer="21"/>
<smd name="1" x="-4.826" y="0" dx="5.08" dy="1.778" layer="1"/>
<smd name="2" x="4.826" y="0" dx="5.08" dy="1.778" layer="1"/>
<text x="-5.715" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.715" y="-4.064" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.604" y1="-2.54" x2="6.604" y2="2.794" layer="43"/>
</package>
<package name="TC26H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-0.889" y1="1.651" x2="0.889" y2="1.651" width="0.1524" layer="21"/>
<wire x1="0.762" y1="7.747" x2="1.016" y2="7.493" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.016" y1="7.493" x2="-0.762" y2="7.747" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.762" y1="7.747" x2="0.762" y2="7.747" width="0.1524" layer="21"/>
<wire x1="0.889" y1="1.651" x2="0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="1.016" y1="2.032" x2="1.016" y2="7.493" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.651" x2="-0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="2.032" x2="-0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="2.032" x2="-1.016" y2="7.493" width="0.1524" layer="21"/>
<wire x1="0.508" y1="0.762" x2="0.508" y2="1.143" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0.762" x2="-0.508" y2="1.27" width="0.4064" layer="21"/>
<wire x1="0.635" y1="0.635" x2="1.27" y2="0" width="0.4064" layer="51"/>
<wire x1="-0.635" y1="0.635" x2="-1.27" y2="0" width="0.4064" layer="51"/>
<wire x1="-0.508" y1="4.953" x2="-0.508" y2="4.572" width="0.1524" layer="21"/>
<wire x1="0.508" y1="4.572" x2="-0.508" y2="4.572" width="0.1524" layer="21"/>
<wire x1="0.508" y1="4.572" x2="0.508" y2="4.953" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="4.953" x2="0.508" y2="4.953" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="5.334" x2="0" y2="5.334" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="4.191" x2="0" y2="4.191" width="0.1524" layer="21"/>
<wire x1="0" y1="4.191" x2="0" y2="3.683" width="0.1524" layer="21"/>
<wire x1="0" y1="4.191" x2="0.508" y2="4.191" width="0.1524" layer="21"/>
<wire x1="0" y1="5.334" x2="0" y2="5.842" width="0.1524" layer="21"/>
<wire x1="0" y1="5.334" x2="0.508" y2="5.334" width="0.1524" layer="21"/>
<wire x1="1.016" y1="2.032" x2="0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="0.889" y1="2.032" x2="-0.889" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128"/>
<pad name="2" x="1.27" y="0" drill="0.8128"/>
<text x="-1.397" y="2.032" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="2.667" y="2.032" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="0.3048" y1="1.016" x2="0.7112" y2="1.6002" layer="21"/>
<rectangle x1="-0.7112" y1="1.016" x2="-0.3048" y2="1.6002" layer="21"/>
<rectangle x1="-1.778" y1="0.762" x2="1.778" y2="8.382" layer="43"/>
</package>
<package name="TC26V">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-0.127" y1="-0.508" x2="0.127" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="0.127" y1="0.508" x2="0.127" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="0.127" y1="0.508" x2="-0.127" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-0.127" y1="-0.508" x2="-0.127" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-0.381" y1="-0.508" x2="-0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-0.508" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.381" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.381" y1="0" x2="-0.381" y2="0.508" width="0.1524" layer="51"/>
<wire x1="0" y1="1.016" x2="0.7184" y2="0.7184" width="0.1524" layer="21" curve="-44.999323"/>
<wire x1="-0.7184" y1="0.7184" x2="0" y2="1.016" width="0.1524" layer="21" curve="-44.999323"/>
<wire x1="-0.7184" y1="-0.7184" x2="0" y2="-1.016" width="0.1524" layer="21" curve="44.999323"/>
<wire x1="0" y1="-1.016" x2="0.7184" y2="-0.7184" width="0.1524" layer="21" curve="44.999323"/>
<circle x="0" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128"/>
<pad name="2" x="1.27" y="0" drill="0.8128"/>
<text x="-2.032" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="TC38H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-1.397" y1="1.651" x2="1.397" y2="1.651" width="0.1524" layer="21"/>
<wire x1="1.27" y1="9.906" x2="1.524" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.524" y1="9.652" x2="-1.27" y2="9.906" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="9.906" x2="1.27" y2="9.906" width="0.1524" layer="21"/>
<wire x1="1.397" y1="1.651" x2="1.397" y2="2.032" width="0.1524" layer="21"/>
<wire x1="1.524" y1="2.032" x2="1.397" y2="2.032" width="0.1524" layer="21"/>
<wire x1="1.524" y1="2.032" x2="1.524" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="1.651" x2="-1.397" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.032" x2="-1.397" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.032" x2="-1.524" y2="9.652" width="0.1524" layer="21"/>
<wire x1="1.397" y1="2.032" x2="-1.397" y2="2.032" width="0.1524" layer="21"/>
<wire x1="0.5588" y1="0.7112" x2="0.508" y2="0.762" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.762" x2="0.508" y2="1.143" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0.762" x2="-0.508" y2="1.016" width="0.4064" layer="21"/>
<wire x1="-0.5588" y1="0.7112" x2="-0.508" y2="0.762" width="0.4064" layer="21"/>
<wire x1="0.635" y1="0.635" x2="1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="0.635" x2="-1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="5.588" x2="-0.762" y2="5.207" width="0.1524" layer="21"/>
<wire x1="0.762" y1="5.207" x2="-0.762" y2="5.207" width="0.1524" layer="21"/>
<wire x1="0.762" y1="5.207" x2="0.762" y2="5.588" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="5.588" x2="0.762" y2="5.588" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="5.969" x2="0" y2="5.969" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="4.826" x2="0" y2="4.826" width="0.1524" layer="21"/>
<wire x1="0" y1="4.826" x2="0" y2="4.318" width="0.1524" layer="21"/>
<wire x1="0" y1="4.826" x2="0.762" y2="4.826" width="0.1524" layer="21"/>
<wire x1="0" y1="5.969" x2="0" y2="6.477" width="0.1524" layer="21"/>
<wire x1="0" y1="5.969" x2="0.762" y2="5.969" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128"/>
<pad name="2" x="1.27" y="0" drill="0.8128"/>
<text x="-1.905" y="2.032" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="3.175" y="2.032" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="0.3048" y1="1.016" x2="0.7112" y2="1.6002" layer="21"/>
<rectangle x1="-0.7112" y1="1.016" x2="-0.3048" y2="1.6002" layer="21"/>
<rectangle x1="-1.778" y1="1.016" x2="1.778" y2="10.414" layer="43"/>
</package>
<package name="86SMX">
<description>&lt;b&gt;CRYSTAL RESONATOR&lt;/b&gt;</description>
<wire x1="-3.81" y1="1.905" x2="2.413" y2="1.905" width="0.0508" layer="21"/>
<wire x1="-3.81" y1="2.286" x2="2.413" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="2.413" y2="-1.905" width="0.0508" layer="21"/>
<wire x1="-3.81" y1="-2.286" x2="2.413" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.286" x2="-6.604" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.334" y1="1.905" x2="-5.334" y2="1.016" width="0.0508" layer="51"/>
<wire x1="-5.334" y1="-1.905" x2="-3.81" y2="-1.905" width="0.0508" layer="51"/>
<wire x1="-6.35" y1="-2.286" x2="-5.969" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="-2.286" x2="-4.191" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-4.191" y1="-2.286" x2="-3.81" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="-2.54" x2="-4.191" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="-2.2098" x2="-6.604" y2="-2.286" width="0.0508" layer="21"/>
<wire x1="-6.604" y1="-2.286" x2="-6.35" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-5.969" y1="-2.54" x2="-5.969" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-5.334" y1="1.016" x2="-5.334" y2="-1.016" width="0.0508" layer="21"/>
<wire x1="-5.334" y1="-1.016" x2="-5.334" y2="-1.905" width="0.0508" layer="51"/>
<wire x1="-5.334" y1="-1.905" x2="-6.35" y2="-2.2098" width="0.0508" layer="51"/>
<wire x1="-4.191" y1="-2.54" x2="-4.191" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="2.54" x2="-4.191" y2="2.54" width="0.1524" layer="51"/>
<wire x1="-5.334" y1="1.905" x2="-3.81" y2="1.905" width="0.0508" layer="51"/>
<wire x1="-6.35" y1="2.286" x2="-5.969" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="2.286" x2="-4.191" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-4.191" y1="2.286" x2="-3.81" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-6.604" y1="2.286" x2="-6.35" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="2.2098" x2="-6.604" y2="2.286" width="0.0508" layer="21"/>
<wire x1="-5.969" y1="2.54" x2="-5.969" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-5.334" y1="1.905" x2="-6.35" y2="2.2098" width="0.0508" layer="51"/>
<wire x1="-4.191" y1="2.54" x2="-4.191" y2="2.286" width="0.1524" layer="51"/>
<wire x1="6.604" y1="2.286" x2="6.604" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-1.905" x2="6.223" y2="1.905" width="0.0508" layer="21"/>
<wire x1="6.223" y1="-1.905" x2="6.604" y2="-2.286" width="0.0508" layer="21"/>
<wire x1="6.223" y1="-2.286" x2="6.604" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.794" y1="-2.54" x2="5.842" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="2.794" y1="-2.286" x2="2.794" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="5.842" y1="-2.54" x2="5.842" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-2.286" x2="6.223" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.905" x2="6.223" y2="-1.905" width="0.0508" layer="51"/>
<wire x1="6.223" y1="1.905" x2="6.604" y2="2.286" width="0.0508" layer="21"/>
<wire x1="6.223" y1="2.286" x2="6.604" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.794" y1="2.54" x2="5.842" y2="2.54" width="0.1524" layer="51"/>
<wire x1="2.794" y1="2.286" x2="2.794" y2="2.54" width="0.1524" layer="51"/>
<wire x1="5.842" y1="2.54" x2="5.842" y2="2.286" width="0.1524" layer="51"/>
<wire x1="2.413" y1="1.905" x2="6.223" y2="1.905" width="0.0508" layer="51"/>
<wire x1="2.413" y1="2.286" x2="6.223" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.651" x2="-0.254" y2="0.381" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="0.381" x2="0.254" y2="0.381" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.381" x2="0.254" y2="1.651" width="0.1524" layer="21"/>
<wire x1="0.254" y1="1.651" x2="-0.254" y2="1.651" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.651" x2="0.635" y2="1.016" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.016" x2="0.635" y2="0.381" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.016" x2="1.016" y2="1.016" width="0.0508" layer="21"/>
<wire x1="-0.635" y1="1.651" x2="-0.635" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.016" x2="-0.635" y2="0.381" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.016" x2="-1.016" y2="1.016" width="0.0508" layer="21"/>
<smd name="2" x="4.318" y="-2.286" dx="3.556" dy="2.1844" layer="1"/>
<smd name="3" x="4.318" y="2.286" dx="3.556" dy="2.1844" layer="1"/>
<smd name="1" x="-5.08" y="-2.286" dx="2.286" dy="2.1844" layer="1"/>
<smd name="4" x="-5.08" y="2.286" dx="2.286" dy="2.1844" layer="1"/>
<text x="-3.683" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.683" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MM20SS">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-2.032" y1="-1.27" x2="2.032" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="-2.032" y1="-1.778" x2="2.032" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.032" y1="1.27" x2="-2.032" y2="1.27" width="0.0508" layer="21"/>
<wire x1="-2.032" y1="1.778" x2="2.032" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.778" x2="-4.064" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="0.381" x2="-2.921" y2="-0.381" width="0.0508" layer="21"/>
<wire x1="-4.064" y1="-1.778" x2="-3.556" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="-1.552" x2="-4.064" y2="-1.778" width="0.0508" layer="21"/>
<wire x1="-2.032" y1="1.27" x2="-2.921" y2="1.27" width="0.0508" layer="51"/>
<wire x1="-2.921" y1="1.27" x2="-2.921" y2="0.381" width="0.0508" layer="51"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="-1.905" x2="-3.048" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="-0.381" x2="-2.921" y2="-1.27" width="0.0508" layer="51"/>
<wire x1="-2.921" y1="-1.27" x2="-2.032" y2="-1.27" width="0.0508" layer="51"/>
<wire x1="-3.556" y1="-1.778" x2="-2.032" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="-1.27" x2="-3.556" y2="-1.552" width="0.0508" layer="51"/>
<wire x1="-4.064" y1="1.778" x2="-3.556" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="1.552" x2="-4.064" y2="1.778" width="0.0508" layer="21"/>
<wire x1="-2.921" y1="1.27" x2="-3.556" y2="1.552" width="0.0508" layer="51"/>
<wire x1="-3.048" y1="1.778" x2="-3.048" y2="1.905" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="1.778" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="1.905" x2="-2.54" y2="1.905" width="0.1524" layer="51"/>
<wire x1="-3.556" y1="1.778" x2="-2.032" y2="1.778" width="0.1524" layer="51"/>
<wire x1="4.064" y1="-1.778" x2="4.064" y2="1.778" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="4.064" y2="-1.778" width="0.0508" layer="21"/>
<wire x1="3.556" y1="-1.27" x2="3.81" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="3.556" y1="-1.778" x2="4.064" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.556" y2="1.27" width="0.0508" layer="21"/>
<wire x1="3.556" y1="1.27" x2="2.032" y2="1.27" width="0.0508" layer="51"/>
<wire x1="3.048" y1="-1.905" x2="3.048" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="2.54" y1="-1.778" x2="2.54" y2="-1.905" width="0.1524" layer="51"/>
<wire x1="2.54" y1="-1.905" x2="3.048" y2="-1.905" width="0.1524" layer="51"/>
<wire x1="2.032" y1="-1.27" x2="3.556" y2="-1.27" width="0.0508" layer="51"/>
<wire x1="2.032" y1="-1.778" x2="3.556" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="3.81" y1="1.27" x2="4.064" y2="1.778" width="0.0508" layer="21"/>
<wire x1="3.556" y1="1.778" x2="4.064" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="1.778" width="0.1524" layer="51"/>
<wire x1="3.048" y1="1.778" x2="3.048" y2="1.905" width="0.1524" layer="51"/>
<wire x1="2.54" y1="1.905" x2="3.048" y2="1.905" width="0.1524" layer="51"/>
<wire x1="2.032" y1="1.778" x2="3.556" y2="1.778" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="-0.254" x2="-0.508" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="-0.254" x2="-0.508" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.254" x2="-1.778" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.254" x2="-1.778" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.635" x2="-1.143" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.635" x2="-0.508" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.635" x2="-1.143" y2="1.016" width="0.0508" layer="21"/>
<wire x1="-1.778" y1="-0.635" x2="-1.143" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.635" x2="-0.508" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.635" x2="-1.143" y2="-1.016" width="0.0508" layer="21"/>
<circle x="3.048" y="0" radius="0.254" width="0.1524" layer="21"/>
<smd name="1" x="-2.794" y="-1.524" dx="1.27" dy="1.8796" layer="1"/>
<smd name="2" x="2.794" y="-1.524" dx="1.27" dy="1.8796" layer="1"/>
<smd name="3" x="2.794" y="1.524" dx="1.27" dy="1.8796" layer="1"/>
<smd name="4" x="-2.794" y="1.524" dx="1.27" dy="1.8796" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MM39SL">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-3.683" y1="-1.651" x2="3.683" y2="-1.651" width="0.0508" layer="21"/>
<wire x1="-3.683" y1="-2.286" x2="3.683" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.286" x2="3.683" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="1.651" x2="-3.683" y2="1.651" width="0.0508" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-6.223" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="1.651" x2="-4.826" y2="0.762" width="0.0508" layer="51"/>
<wire x1="-5.715" y1="-2.286" x2="-3.683" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-4.826" y1="-1.651" x2="-3.683" y2="-1.651" width="0.0508" layer="51"/>
<wire x1="-5.715" y1="-2.055" x2="-6.223" y2="-2.286" width="0.0508" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-5.715" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="0.762" x2="-4.826" y2="-0.762" width="0.0508" layer="21"/>
<wire x1="-4.826" y1="-0.762" x2="-4.826" y2="-1.651" width="0.0508" layer="51"/>
<wire x1="-4.826" y1="-1.651" x2="-5.715" y2="-2.055" width="0.0508" layer="51"/>
<wire x1="-5.715" y1="2.286" x2="-3.683" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-3.683" y1="1.651" x2="-4.826" y2="1.651" width="0.0508" layer="51"/>
<wire x1="-6.223" y1="2.286" x2="-5.715" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.055" x2="-6.223" y2="2.286" width="0.0508" layer="21"/>
<wire x1="-4.826" y1="1.651" x2="-5.715" y2="2.055" width="0.0508" layer="51"/>
<wire x1="6.223" y1="-2.286" x2="6.223" y2="2.286" width="0.1524" layer="21"/>
<wire x1="5.842" y1="-1.651" x2="5.842" y2="1.651" width="0.0508" layer="21"/>
<wire x1="5.842" y1="-1.651" x2="6.223" y2="-2.286" width="0.0508" layer="21"/>
<wire x1="5.715" y1="-2.286" x2="6.223" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.286" x2="5.715" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-1.651" x2="5.842" y2="-1.651" width="0.0508" layer="21"/>
<wire x1="3.683" y1="-1.651" x2="5.715" y2="-1.651" width="0.0508" layer="51"/>
<wire x1="5.842" y1="1.651" x2="6.223" y2="2.286" width="0.0508" layer="21"/>
<wire x1="5.842" y1="1.651" x2="5.715" y2="1.651" width="0.0508" layer="21"/>
<wire x1="5.715" y1="2.286" x2="6.223" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="2.286" x2="5.715" y2="2.286" width="0.1524" layer="51"/>
<wire x1="5.715" y1="1.651" x2="3.683" y2="1.651" width="0.0508" layer="51"/>
<wire x1="-3.81" y1="-0.254" x2="-2.54" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.254" x2="-2.54" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.254" x2="-3.81" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.254" x2="-3.81" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.175" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0.635" x2="-3.175" y2="1.016" width="0.1016" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-0.635" x2="-3.175" y2="-1.016" width="0.1016" layer="21"/>
<circle x="5.08" y="0" radius="0.254" width="0.1524" layer="21"/>
<smd name="1" x="-4.699" y="-1.778" dx="1.778" dy="1.778" layer="1"/>
<smd name="2" x="4.699" y="-1.778" dx="1.778" dy="1.778" layer="1"/>
<smd name="3" x="4.699" y="1.778" dx="1.778" dy="1.778" layer="1"/>
<smd name="4" x="-4.699" y="1.778" dx="1.778" dy="1.778" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="CTS406">
<description>&lt;b&gt;Model 406 6.0x3.5mm Low Cost Surface Mount Crystal&lt;/b&gt;&lt;p&gt;
Source: 008-0260-0_E.pdf</description>
<wire x1="-2.475" y1="1.65" x2="-1.05" y2="1.65" width="0.2032" layer="51"/>
<wire x1="-1.05" y1="1.65" x2="1.05" y2="1.65" width="0.2032" layer="21"/>
<wire x1="1.05" y1="1.65" x2="2.475" y2="1.65" width="0.2032" layer="51"/>
<wire x1="2.9" y1="1.225" x2="2.9" y2="0.3" width="0.2032" layer="51"/>
<wire x1="2.9" y1="0.3" x2="2.9" y2="-0.3" width="0.2032" layer="21"/>
<wire x1="2.9" y1="-0.3" x2="2.9" y2="-1.225" width="0.2032" layer="51"/>
<wire x1="2.475" y1="-1.65" x2="1.05" y2="-1.65" width="0.2032" layer="51"/>
<wire x1="1.05" y1="-1.65" x2="-1.05" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="-1.05" y1="-1.65" x2="-2.475" y2="-1.65" width="0.2032" layer="51"/>
<wire x1="-2.9" y1="-1.225" x2="-2.9" y2="-0.3" width="0.2032" layer="51"/>
<wire x1="-2.9" y1="-0.3" x2="-2.9" y2="0.3" width="0.2032" layer="21"/>
<wire x1="-2.9" y1="0.3" x2="-2.9" y2="1.225" width="0.2032" layer="51"/>
<wire x1="-2.9" y1="1.225" x2="-2.475" y2="1.65" width="0.2032" layer="51" curve="89.516721"/>
<wire x1="2.475" y1="1.65" x2="2.9" y2="1.225" width="0.2032" layer="51" curve="89.516721"/>
<wire x1="2.9" y1="-1.225" x2="2.475" y2="-1.65" width="0.2032" layer="51" curve="89.516721"/>
<wire x1="-2.475" y1="-1.65" x2="-2.9" y2="-1.225" width="0.2032" layer="51" curve="89.516721"/>
<circle x="-2.05" y="-0.2" radius="0.182" width="0" layer="21"/>
<smd name="1" x="-2.2" y="-1.2" dx="1.9" dy="1.4" layer="1"/>
<smd name="2" x="2.2" y="-1.2" dx="1.9" dy="1.4" layer="1"/>
<smd name="3" x="2.2" y="1.2" dx="1.9" dy="1.4" layer="1"/>
<smd name="4" x="-2.2" y="1.2" dx="1.9" dy="1.4" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="Q">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="2.54" y="1.016" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.159" y="-1.143" size="0.8636" layer="93">1</text>
<text x="1.524" y="-1.143" size="0.8636" layer="93">2</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CRYSTAL" prefix="Q" uservalue="yes">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="Q" x="0" y="0"/>
</gates>
<devices>
<device name="HC49S" package="HC49/S">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="HC49GW" package="HC49GW">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="HC49TL-H" package="HC49TL-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="HC49U-H" package="HC49U-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="HC49U-LM" package="HC49U-LM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="HC49U-V" package="HC49U-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="HC49U70" package="HC49U70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="HC49UP" package="HC49UP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="HC13U-H" package="HC13U-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="HC18U-H" package="HC18U-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="HC18U-V" package="HC18U-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="HC33U-H" package="HC33U-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="HC33U-V" package="HC33U-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SM49" package="SM49">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TC26H" package="TC26H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TC26V" package="TC26V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TC38H" package="TC38H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="68SMX" package="86SMX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MM20SS" package="MM20SS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MM39SL" package="MM39SL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CTS406" package="CTS406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="jumper">
<packages>
<package name="JP2">
<description>&lt;b&gt;JUMPER&lt;/b&gt;</description>
<wire x1="-3.81" y1="-1.016" x2="-3.81" y2="1.016" width="0.1524" layer="21"/>
<wire x1="3.556" y1="1.27" x2="1.524" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.016" x2="1.524" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.556" y1="-1.27" x2="1.524" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.016" x2="1.524" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.556" y1="-1.27" x2="3.81" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="3.556" y1="1.27" x2="3.81" y2="1.016" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.016" x2="3.81" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.016" y1="1.27" x2="1.27" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.016" y1="1.27" x2="-1.016" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.016" x2="-1.016" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="1.27" x2="-1.27" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.016" x2="-3.556" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="1.27" x2="-3.556" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.016" y1="-1.27" x2="1.27" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.016" x2="-1.016" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-1.27" x2="-1.27" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.016" x2="-3.556" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.016" y1="-1.27" x2="-1.016" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-1.27" x2="-3.556" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.9144" shape="long" rot="R90"/>
<pad name="2" x="0" y="0" drill="0.9144" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="0.9144" shape="long" rot="R90"/>
<text x="-3.556" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.667" size="0.9906" layer="21" ratio="12">1</text>
<text x="0" y="-2.667" size="0.9906" layer="21" ratio="12">2</text>
<text x="2.54" y="-2.667" size="0.9906" layer="21" ratio="12">3</text>
<text x="-3.556" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.8448" y1="-0.3048" x2="-2.2352" y2="0.3048" layer="51"/>
<rectangle x1="-0.3048" y1="-0.3048" x2="0.3048" y2="0.3048" layer="51"/>
<rectangle x1="2.2352" y1="-0.3048" x2="2.8448" y2="0.3048" layer="51"/>
</package>
<package name="JP1">
<description>&lt;b&gt;JUMPER&lt;/b&gt;</description>
<wire x1="-1.016" y1="0" x2="-1.27" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="0" x2="-1.27" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="1.016" y1="0" x2="1.27" y2="0.254" width="0.1524" layer="21"/>
<wire x1="1.016" y1="0" x2="1.27" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.254" x2="1.27" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="1.016" y1="-2.54" x2="1.27" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="1.27" y1="2.286" x2="1.016" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.27" y1="2.286" x2="1.27" y2="0.254" width="0.1524" layer="21"/>
<wire x1="1.016" y1="2.54" x2="-1.016" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.286" x2="-1.016" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.286" x2="-1.27" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.254" x2="-1.27" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-2.54" x2="-1.27" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-2.54" x2="1.016" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="0" y="-1.27" drill="0.9144" shape="long"/>
<pad name="2" x="0" y="1.27" drill="0.9144" shape="long"/>
<text x="-1.651" y="-2.54" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="2.921" y="-2.54" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.3048" y1="0.9652" x2="0.3048" y2="1.5748" layer="51"/>
<rectangle x1="-0.3048" y1="-1.5748" x2="0.3048" y2="-0.9652" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="JP3E">
<wire x1="2.54" y1="0" x2="2.54" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.27" width="0.4064" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.27" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="1.27" width="0.4064" layer="94"/>
<wire x1="-3.175" y1="0" x2="3.175" y2="0" width="0.4064" layer="94"/>
<wire x1="3.175" y1="0" x2="3.175" y2="0.635" width="0.4064" layer="94"/>
<wire x1="3.175" y1="0.635" x2="-3.175" y2="0.635" width="0.4064" layer="94"/>
<wire x1="-3.175" y1="0.635" x2="-3.175" y2="0" width="0.4064" layer="94"/>
<text x="-3.81" y="0" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="0" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="1" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="2" x="0" y="-2.54" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="3" x="2.54" y="-2.54" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="JP2E">
<wire x1="0" y1="0" x2="0" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.27" width="0.4064" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="1.27" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.27" width="0.4064" layer="94"/>
<wire x1="-0.635" y1="0" x2="3.175" y2="0" width="0.4064" layer="94"/>
<wire x1="3.175" y1="0" x2="3.175" y2="0.635" width="0.4064" layer="94"/>
<wire x1="3.175" y1="0.635" x2="-0.635" y2="0.635" width="0.4064" layer="94"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="0" width="0.4064" layer="94"/>
<text x="-1.27" y="0" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="0" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="1" x="0" y="-2.54" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="2" x="2.54" y="-2.54" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="JP2E" prefix="JP" uservalue="yes">
<description>&lt;b&gt;JUMPER&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="JP3E" x="0" y="0"/>
</gates>
<devices>
<device name="" package="JP2">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JP1E" prefix="JP" uservalue="yes">
<description>&lt;b&gt;JUMPER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="JP2E" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="JP1">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-cypressindustries">
<packages>
<package name="32005-201">
<description>&lt;b&gt;MINI USB-B R/A SMT W/ REAR&lt;/b&gt;&lt;p&gt;
Source: http://www.cypressindustries.com/pdf/32005-201.pdf</description>
<wire x1="-5.9182" y1="3.8416" x2="-3.6879" y2="3.8416" width="0.1016" layer="51"/>
<wire x1="-3.6879" y1="3.8416" x2="-3.6879" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="-3.6879" y1="4.8799" x2="-3.3245" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="-3.3245" y1="4.8799" x2="-3.3245" y2="4.4646" width="0.1016" layer="51"/>
<wire x1="-3.3245" y1="4.4646" x2="-2.7015" y2="4.4646" width="0.1016" layer="51"/>
<wire x1="-2.7015" y1="4.4646" x2="-2.7015" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="-2.7015" y1="4.8799" x2="-2.3093" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="-2.3093" y1="4.8799" x2="-2.3093" y2="3.8416" width="0.1016" layer="51"/>
<wire x1="-1.5825" y1="3.8416" x2="0.7266" y2="3.8416" width="0.1016" layer="21"/>
<wire x1="2.8032" y1="3.8416" x2="0.7266" y2="3.8416" width="0.1016" layer="51"/>
<wire x1="0.7266" y1="3.8416" x2="0.519" y2="4.0492" width="0.1016" layer="21" curve="-90"/>
<wire x1="0.519" y1="4.0492" x2="0.519" y2="4.205" width="0.1016" layer="21"/>
<wire x1="0.519" y1="4.205" x2="2.907" y2="4.205" width="0.1016" layer="51"/>
<wire x1="2.907" y1="4.205" x2="3.4781" y2="3.6339" width="0.1016" layer="51" curve="-90"/>
<wire x1="-5.9182" y1="-3.8415" x2="-5.9182" y2="-3.8414" width="0.1016" layer="21"/>
<wire x1="-5.9182" y1="-3.8414" x2="-5.9182" y2="3.8416" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="2.9591" x2="-4.5685" y2="2.7514" width="0.1016" layer="21"/>
<wire x1="-4.5685" y1="2.7514" x2="-4.828" y2="2.5438" width="0.1016" layer="21" curve="68.629849"/>
<wire x1="-4.828" y1="2.5438" x2="-4.828" y2="1.9727" width="0.1016" layer="21" curve="34.099487"/>
<wire x1="-4.828" y1="1.9727" x2="-4.5685" y2="1.7651" width="0.1016" layer="21" curve="68.629849"/>
<wire x1="-4.5685" y1="1.7651" x2="-1.8171" y2="1.5055" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="1.5055" x2="-1.8171" y2="1.7132" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="1.7132" x2="-4.2051" y2="1.9727" width="0.1016" layer="21"/>
<wire x1="-4.2051" y1="1.9727" x2="-4.2051" y2="2.4919" width="0.1016" layer="21"/>
<wire x1="-4.2051" y1="2.4919" x2="-1.8171" y2="2.7514" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="2.7514" x2="-1.8171" y2="2.9591" width="0.1016" layer="21"/>
<wire x1="2.8032" y1="3.8416" x2="3.0627" y2="3.5821" width="0.1016" layer="51" curve="-90"/>
<wire x1="3.0627" y1="3.5821" x2="3.0627" y2="3.011" width="0.1016" layer="51"/>
<wire x1="3.0627" y1="3.011" x2="3.4261" y2="3.011" width="0.1016" layer="21"/>
<wire x1="1.713" y1="4.2569" x2="1.713" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="1.713" y1="4.8799" x2="2.1283" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="2.1283" y1="4.8799" x2="2.1283" y2="4.4646" width="0.1016" layer="51"/>
<wire x1="2.1283" y1="4.4646" x2="2.6474" y2="4.4646" width="0.1016" layer="51"/>
<wire x1="2.6474" y1="4.4646" x2="2.6474" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="2.6474" y1="4.8799" x2="3.0627" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="3.0627" y1="4.8799" x2="3.0627" y2="4.2569" width="0.1016" layer="51"/>
<wire x1="0.5709" y1="1.7651" x2="0.5709" y2="-1.765" width="0.1016" layer="21"/>
<wire x1="1.0381" y1="-1.8169" x2="1.0381" y2="1.817" width="0.1016" layer="21"/>
<wire x1="1.0381" y1="1.817" x2="0.8305" y2="2.0246" width="0.1016" layer="21" curve="90.055225"/>
<wire x1="0.8305" y1="2.0246" x2="0.8304" y2="2.0246" width="0.1016" layer="21"/>
<wire x1="0.8304" y1="2.0246" x2="0.5709" y2="1.7651" width="0.1016" layer="21" curve="89.955858"/>
<wire x1="1.5573" y1="-2.0246" x2="3.4261" y2="-2.0246" width="0.1016" layer="21"/>
<wire x1="3.0627" y1="-1.9726" x2="3.0627" y2="1.9727" width="0.1016" layer="51"/>
<wire x1="-4.5684" y1="1.2459" x2="-0.5192" y2="1.0383" width="0.1016" layer="21"/>
<wire x1="-0.5192" y1="1.0383" x2="-0.3116" y2="0.8306" width="0.1016" layer="21" curve="-83.771817"/>
<wire x1="-4.5685" y1="1.2459" x2="-4.7761" y2="1.0383" width="0.1016" layer="21" curve="90"/>
<wire x1="-4.7761" y1="1.0383" x2="-4.7761" y2="1.0382" width="0.1016" layer="21"/>
<wire x1="-4.7761" y1="1.0382" x2="-4.5685" y2="0.8306" width="0.1016" layer="21" curve="90"/>
<wire x1="-4.5685" y1="0.8306" x2="-1.1422" y2="0.623" width="0.1016" layer="21"/>
<wire x1="-5.9182" y1="-3.8414" x2="-3.6879" y2="-3.8414" width="0.1016" layer="51"/>
<wire x1="-3.6879" y1="-3.8414" x2="-3.6879" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="-3.6879" y1="-4.8797" x2="-3.3245" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="-3.3245" y1="-4.8797" x2="-3.3245" y2="-4.4644" width="0.1016" layer="51"/>
<wire x1="-3.3245" y1="-4.4644" x2="-2.7015" y2="-4.4644" width="0.1016" layer="51"/>
<wire x1="-2.7015" y1="-4.4644" x2="-2.7015" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="-2.7015" y1="-4.8797" x2="-2.3093" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="-2.3093" y1="-4.8797" x2="-2.3093" y2="-3.8414" width="0.1016" layer="51"/>
<wire x1="-2.3093" y1="-3.8414" x2="2.8032" y2="-3.8414" width="0.1016" layer="51"/>
<wire x1="0.7266" y1="-3.8414" x2="0.519" y2="-4.049" width="0.1016" layer="21" curve="90"/>
<wire x1="0.519" y1="-4.049" x2="0.519" y2="-4.2048" width="0.1016" layer="21"/>
<wire x1="0.519" y1="-4.2048" x2="2.907" y2="-4.2048" width="0.1016" layer="51"/>
<wire x1="2.907" y1="-4.2048" x2="3.4781" y2="-3.6337" width="0.1016" layer="51" curve="90.020069"/>
<wire x1="-1.8171" y1="-2.9589" x2="-4.5685" y2="-2.7512" width="0.1016" layer="21"/>
<wire x1="-4.5685" y1="-2.7512" x2="-4.828" y2="-2.5436" width="0.1016" layer="21" curve="-68.629849"/>
<wire x1="-4.828" y1="-2.5436" x2="-4.828" y2="-1.9725" width="0.1016" layer="21" curve="-34.099487"/>
<wire x1="-4.828" y1="-1.9725" x2="-4.5685" y2="-1.7649" width="0.1016" layer="21" curve="-68.629849"/>
<wire x1="-4.5685" y1="-1.7649" x2="-1.8171" y2="-1.5053" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="-1.5053" x2="-1.8171" y2="-1.713" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="-1.713" x2="-4.2051" y2="-1.9725" width="0.1016" layer="21"/>
<wire x1="-4.2051" y1="-1.9725" x2="-4.2051" y2="-2.4917" width="0.1016" layer="21"/>
<wire x1="-4.2051" y1="-2.4917" x2="-1.8171" y2="-2.7512" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="-2.7512" x2="-1.8171" y2="-2.9589" width="0.1016" layer="21"/>
<wire x1="2.8032" y1="-3.8414" x2="3.0627" y2="-3.5819" width="0.1016" layer="51" curve="90.044176"/>
<wire x1="3.0627" y1="-3.5819" x2="3.0627" y2="-3.0108" width="0.1016" layer="51"/>
<wire x1="3.0627" y1="-3.0108" x2="3.4261" y2="-3.0108" width="0.1016" layer="21"/>
<wire x1="1.713" y1="-4.2567" x2="1.713" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="1.713" y1="-4.8797" x2="2.1283" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="2.1283" y1="-4.8797" x2="2.1283" y2="-4.4644" width="0.1016" layer="51"/>
<wire x1="2.1283" y1="-4.4644" x2="2.6474" y2="-4.4644" width="0.1016" layer="51"/>
<wire x1="2.6474" y1="-4.4644" x2="2.6474" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="2.6474" y1="-4.8797" x2="3.0627" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="3.0627" y1="-4.8797" x2="3.0627" y2="-4.2567" width="0.1016" layer="51"/>
<wire x1="1.0381" y1="-1.8168" x2="0.8305" y2="-2.0244" width="0.1016" layer="21" curve="-90.055225"/>
<wire x1="0.8304" y1="-2.0244" x2="0.5709" y2="-1.7649" width="0.1016" layer="21" curve="-89.867677"/>
<wire x1="1.5573" y1="-1.9725" x2="1.5573" y2="2.0248" width="0.1016" layer="51"/>
<wire x1="1.5573" y1="2.0248" x2="3.4261" y2="2.0248" width="0.1016" layer="21"/>
<wire x1="-4.5684" y1="-1.2457" x2="-0.5192" y2="-1.0381" width="0.1016" layer="21"/>
<wire x1="-0.5192" y1="-1.0381" x2="-0.3116" y2="-0.8304" width="0.1016" layer="21" curve="83.722654"/>
<wire x1="-0.3116" y1="-0.8304" x2="-0.3116" y2="0.8307" width="0.1016" layer="21"/>
<wire x1="-4.5685" y1="-1.2457" x2="-4.7761" y2="-1.0381" width="0.1016" layer="21" curve="-90"/>
<wire x1="-4.7761" y1="-1.038" x2="-4.5685" y2="-0.8304" width="0.1016" layer="21" curve="-90"/>
<wire x1="-4.5685" y1="-0.8304" x2="-1.1422" y2="-0.6228" width="0.1016" layer="21"/>
<wire x1="-1.1422" y1="-0.6228" x2="-1.1422" y2="0.6232" width="0.1016" layer="21"/>
<wire x1="-1.5826" y1="-3.8414" x2="0.7267" y2="-3.8415" width="0.1016" layer="21"/>
<wire x1="-5.9182" y1="-3.8414" x2="-4.4146" y2="-3.8414" width="0.1016" layer="21"/>
<wire x1="-5.9182" y1="3.8416" x2="-4.4147" y2="3.8415" width="0.1016" layer="21"/>
<wire x1="-2.3093" y1="3.8416" x2="0.7265" y2="3.8415" width="0.1016" layer="51"/>
<wire x1="3.4781" y1="-2.0245" x2="3.4781" y2="-3.0109" width="0.1016" layer="21"/>
<wire x1="3.4781" y1="3.634" x2="3.478" y2="-3.0109" width="0.1016" layer="51"/>
<wire x1="3.4782" y1="3.011" x2="3.4782" y2="2.0246" width="0.1016" layer="21"/>
<smd name="M1" x="-3" y="-4.45" dx="2.5" dy="2" layer="1"/>
<smd name="M2" x="-3" y="4.45" dx="2.5" dy="2" layer="1"/>
<smd name="M4" x="2.9" y="-4.45" dx="3.3" dy="2" layer="1"/>
<smd name="M3" x="2.9" y="4.45" dx="3.3" dy="2" layer="1"/>
<smd name="1" x="3" y="1.6" dx="3.1" dy="0.5" layer="1"/>
<smd name="2" x="3" y="0.8" dx="3.1" dy="0.5" layer="1"/>
<smd name="3" x="3" y="0" dx="3.1" dy="0.5" layer="1"/>
<smd name="4" x="3" y="-0.8" dx="3.1" dy="0.5" layer="1"/>
<smd name="5" x="3" y="-1.6" dx="3.1" dy="0.5" layer="1"/>
<text x="-4.445" y="5.715" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.445" y="-6.985" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="2.2" drill="0.9"/>
<hole x="0" y="-2.2" drill="0.9"/>
</package>
<package name="32005-301">
<description>&lt;b&gt;MINI USB-B R/A SMT W/O REAR&lt;/b&gt;&lt;p&gt;
Source: http://www.cypressindustries.com/pdf/32005-301.pdf</description>
<wire x1="-5.9228" y1="3.8473" x2="3.1598" y2="3.8473" width="0.1016" layer="51"/>
<wire x1="2.9404" y1="3.7967" x2="2.9404" y2="2.5986" width="0.1016" layer="51"/>
<wire x1="2.9404" y1="2.5986" x2="1.8098" y2="2.5986" width="0.1016" layer="21"/>
<wire x1="1.8098" y1="3.7798" x2="1.8098" y2="-3.8473" width="0.1016" layer="51"/>
<wire x1="3.1597" y1="-3.8473" x2="-5.9228" y2="-3.8473" width="0.1016" layer="51"/>
<wire x1="-5.9228" y1="-3.8473" x2="-5.9228" y2="3.8473" width="0.1016" layer="21"/>
<wire x1="2.9573" y1="-3.8217" x2="2.9573" y2="-2.6998" width="0.1016" layer="51"/>
<wire x1="2.9573" y1="-2.6998" x2="1.8098" y2="-2.6998" width="0.1016" layer="21"/>
<wire x1="-5.9182" y1="3.8416" x2="-3.6879" y2="3.8416" width="0.1016" layer="51"/>
<wire x1="-3.6879" y1="3.8416" x2="-3.6879" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="-3.6879" y1="4.8799" x2="-3.3245" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="-3.3245" y1="4.8799" x2="-3.3245" y2="4.4646" width="0.1016" layer="51"/>
<wire x1="-3.3245" y1="4.4646" x2="-2.7015" y2="4.4646" width="0.1016" layer="51"/>
<wire x1="-2.7015" y1="4.4646" x2="-2.7015" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="-2.7015" y1="4.8799" x2="-2.3093" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="-2.3093" y1="4.8799" x2="-2.3093" y2="3.8416" width="0.1016" layer="51"/>
<wire x1="-5.9182" y1="-3.8415" x2="-5.9182" y2="-3.8414" width="0.1016" layer="21"/>
<wire x1="-5.9182" y1="-3.8414" x2="-5.9182" y2="3.8416" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="2.9591" x2="-4.5685" y2="2.7514" width="0.1016" layer="21"/>
<wire x1="-4.5685" y1="2.7514" x2="-4.828" y2="2.5438" width="0.1016" layer="21" curve="68.629849"/>
<wire x1="-4.828" y1="2.5438" x2="-4.828" y2="1.9727" width="0.1016" layer="21" curve="34.099487"/>
<wire x1="-4.828" y1="1.9727" x2="-4.5685" y2="1.7651" width="0.1016" layer="21" curve="68.629849"/>
<wire x1="-4.5685" y1="1.7651" x2="-1.8171" y2="1.5055" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="1.5055" x2="-1.8171" y2="1.7132" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="1.7132" x2="-4.2051" y2="1.9727" width="0.1016" layer="21"/>
<wire x1="-4.2051" y1="1.9727" x2="-4.2051" y2="2.4919" width="0.1016" layer="21"/>
<wire x1="-4.2051" y1="2.4919" x2="-1.8171" y2="2.7514" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="2.7514" x2="-1.8171" y2="2.9591" width="0.1016" layer="21"/>
<wire x1="1.713" y1="3.8856" x2="1.713" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="1.713" y1="4.8799" x2="2.1283" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="2.1283" y1="4.8799" x2="2.1283" y2="4.4646" width="0.1016" layer="51"/>
<wire x1="2.1283" y1="4.4646" x2="2.6474" y2="4.4646" width="0.1016" layer="51"/>
<wire x1="2.6474" y1="4.4646" x2="2.6474" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="2.6474" y1="4.8799" x2="3.1639" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="3.1639" y1="4.8799" x2="3.1639" y2="3.8519" width="0.1016" layer="51"/>
<wire x1="-4.5684" y1="1.2459" x2="-0.5192" y2="1.0383" width="0.1016" layer="21"/>
<wire x1="-0.5192" y1="1.0383" x2="-0.3116" y2="0.8306" width="0.1016" layer="21" curve="-83.771817"/>
<wire x1="-4.5685" y1="1.2459" x2="-4.7761" y2="1.0383" width="0.1016" layer="21" curve="90"/>
<wire x1="-4.7761" y1="1.0383" x2="-4.7761" y2="1.0382" width="0.1016" layer="21"/>
<wire x1="-4.7761" y1="1.0382" x2="-4.5685" y2="0.8306" width="0.1016" layer="21" curve="90"/>
<wire x1="-4.5685" y1="0.8306" x2="-1.1422" y2="0.623" width="0.1016" layer="21"/>
<wire x1="-5.9182" y1="-3.8414" x2="-3.6879" y2="-3.8414" width="0.1016" layer="51"/>
<wire x1="-3.6879" y1="-3.8414" x2="-3.6879" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="-3.6879" y1="-4.8797" x2="-3.3245" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="-3.3245" y1="-4.8797" x2="-3.3245" y2="-4.4644" width="0.1016" layer="51"/>
<wire x1="-3.3245" y1="-4.4644" x2="-2.7015" y2="-4.4644" width="0.1016" layer="51"/>
<wire x1="-2.7015" y1="-4.4644" x2="-2.7015" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="-2.7015" y1="-4.8797" x2="-2.3093" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="-2.3093" y1="-4.8797" x2="-2.3093" y2="-3.8414" width="0.1016" layer="51"/>
<wire x1="-1.8171" y1="-2.9589" x2="-4.5685" y2="-2.7512" width="0.1016" layer="21"/>
<wire x1="-4.5685" y1="-2.7512" x2="-4.828" y2="-2.5436" width="0.1016" layer="21" curve="-68.629849"/>
<wire x1="-4.828" y1="-2.5436" x2="-4.828" y2="-1.9725" width="0.1016" layer="21" curve="-34.099487"/>
<wire x1="-4.828" y1="-1.9725" x2="-4.5685" y2="-1.7649" width="0.1016" layer="21" curve="-68.629849"/>
<wire x1="-4.5685" y1="-1.7649" x2="-1.8171" y2="-1.5053" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="-1.5053" x2="-1.8171" y2="-1.713" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="-1.713" x2="-4.2051" y2="-1.9725" width="0.1016" layer="21"/>
<wire x1="-4.2051" y1="-1.9725" x2="-4.2051" y2="-2.4917" width="0.1016" layer="21"/>
<wire x1="-4.2051" y1="-2.4917" x2="-1.8171" y2="-2.7512" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="-2.7512" x2="-1.8171" y2="-2.9589" width="0.1016" layer="21"/>
<wire x1="1.713" y1="-3.8855" x2="1.713" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="1.713" y1="-4.8797" x2="2.1283" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="2.1283" y1="-4.8797" x2="2.1283" y2="-4.4644" width="0.1016" layer="51"/>
<wire x1="2.1283" y1="-4.4644" x2="2.6474" y2="-4.4644" width="0.1016" layer="51"/>
<wire x1="2.6474" y1="-4.4644" x2="2.6474" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="2.6474" y1="-4.8797" x2="3.1627" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="3.1627" y1="-4.8797" x2="3.1627" y2="-3.8518" width="0.1016" layer="51"/>
<wire x1="-4.5684" y1="-1.2457" x2="-0.5192" y2="-1.0381" width="0.1016" layer="21"/>
<wire x1="-0.5192" y1="-1.0381" x2="-0.3116" y2="-0.8304" width="0.1016" layer="21" curve="83.722654"/>
<wire x1="-0.3116" y1="-0.8304" x2="-0.3116" y2="0.8307" width="0.1016" layer="21"/>
<wire x1="-4.5685" y1="-1.2457" x2="-4.7761" y2="-1.0381" width="0.1016" layer="21" curve="-90"/>
<wire x1="-4.7761" y1="-1.038" x2="-4.5685" y2="-0.8304" width="0.1016" layer="21" curve="-90"/>
<wire x1="-4.5685" y1="-0.8304" x2="-1.1422" y2="-0.6228" width="0.1016" layer="21"/>
<wire x1="-1.1422" y1="-0.6228" x2="-1.1422" y2="0.6232" width="0.1016" layer="21"/>
<wire x1="-5.9182" y1="-3.8414" x2="-4.4146" y2="-3.8414" width="0.1016" layer="21"/>
<wire x1="-5.9182" y1="3.8416" x2="-4.4147" y2="3.8415" width="0.1016" layer="21"/>
<wire x1="1.0842" y1="-3.8472" x2="-1.6031" y2="-3.8472" width="0.1016" layer="21"/>
<wire x1="-1.5523" y1="3.8472" x2="0.9831" y2="3.8473" width="0.1016" layer="21"/>
<wire x1="2.9404" y1="3.3243" x2="2.9404" y2="2.5986" width="0.1016" layer="21"/>
<wire x1="1.8098" y1="2.5986" x2="1.8099" y2="3.3243" width="0.1016" layer="21"/>
<wire x1="1.8098" y1="-2.6999" x2="1.8098" y2="-3.3242" width="0.1016" layer="21"/>
<wire x1="2.9573" y1="-3.3324" x2="2.9573" y2="-2.6998" width="0.1016" layer="21"/>
<smd name="M1" x="-3" y="-4.45" dx="2.5" dy="2" layer="1"/>
<smd name="M2" x="-3" y="4.45" dx="2.5" dy="2" layer="1"/>
<smd name="M4" x="2.9" y="-4.45" dx="3.3" dy="2" layer="1"/>
<smd name="M3" x="2.9" y="4.45" dx="3.3" dy="2" layer="1"/>
<smd name="1" x="3" y="1.6" dx="3.1" dy="0.5" layer="1"/>
<smd name="2" x="3" y="0.8" dx="3.1" dy="0.5" layer="1"/>
<smd name="3" x="3" y="0" dx="3.1" dy="0.5" layer="1"/>
<smd name="4" x="3" y="-0.8" dx="3.1" dy="0.5" layer="1"/>
<smd name="5" x="3" y="-1.6" dx="3.1" dy="0.5" layer="1"/>
<text x="-4.445" y="5.715" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.445" y="-6.985" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="2.2" drill="0.9"/>
<hole x="0" y="-2.2" drill="0.9"/>
</package>
</packages>
<symbols>
<symbol name="MINI-USB-5">
<wire x1="-2.54" y1="6.35" x2="-2.54" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-6.35" x2="-1.27" y2="-7.62" width="0.254" layer="94" curve="90"/>
<wire x1="-1.27" y1="-7.62" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0" y1="-7.62" x2="1.016" y2="-8.128" width="0.254" layer="94" curve="-53.130102"/>
<wire x1="1.016" y1="-8.128" x2="2.54" y2="-8.89" width="0.254" layer="94" curve="53.130102"/>
<wire x1="2.54" y1="-8.89" x2="5.08" y2="-8.89" width="0.254" layer="94"/>
<wire x1="5.08" y1="-8.89" x2="6.35" y2="-7.62" width="0.254" layer="94" curve="90"/>
<wire x1="6.35" y1="-7.62" x2="6.35" y2="7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="6.35" x2="-1.27" y2="7.62" width="0.254" layer="94" curve="-90"/>
<wire x1="-1.27" y1="7.62" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="1.016" y2="8.128" width="0.254" layer="94" curve="53.130102"/>
<wire x1="1.016" y1="8.128" x2="2.54" y2="8.89" width="0.254" layer="94" curve="-53.130102"/>
<wire x1="2.54" y1="8.89" x2="5.08" y2="8.89" width="0.254" layer="94"/>
<wire x1="5.08" y1="8.89" x2="6.35" y2="7.62" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="1.27" y2="-6.35" width="0.254" layer="94"/>
<wire x1="1.27" y1="-6.35" x2="3.81" y2="-6.35" width="0.254" layer="94"/>
<wire x1="3.81" y1="-6.35" x2="3.81" y2="6.35" width="0.254" layer="94"/>
<wire x1="3.81" y1="6.35" x2="1.27" y2="6.35" width="0.254" layer="94"/>
<wire x1="1.27" y1="6.35" x2="0" y2="5.08" width="0.254" layer="94"/>
<text x="-2.54" y="11.43" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="10.16" y="-7.62" size="1.778" layer="96" font="vector" rot="R90">&gt;VALUE</text>
<pin name="1" x="-5.08" y="5.08" visible="pin" direction="pas"/>
<pin name="2" x="-5.08" y="2.54" visible="pin" direction="pas"/>
<pin name="3" x="-5.08" y="0" visible="pin" direction="pas"/>
<pin name="4" x="-5.08" y="-2.54" visible="pin" direction="pas"/>
<pin name="5" x="-5.08" y="-5.08" visible="pin" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MINI-USB-" prefix="X">
<description>&lt;b&gt;MINI USB-B Conector&lt;/b&gt;&lt;p&gt;
Source: www.cypressindustries.com</description>
<gates>
<gate name="G$1" symbol="MINI-USB-5" x="0" y="0"/>
</gates>
<devices>
<device name="32005-201" package="32005-201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="32005-301" package="32005-301">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="led">
<packages>
<package name="1206">
<description>&lt;b&gt;CHICAGO MINIATURE LAMP, INC.&lt;/b&gt;&lt;p&gt;
7022X Series SMT LEDs 1206 Package Size</description>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.5" x2="0.55" y2="-0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="-0.55" y1="-0.5" x2="-0.55" y2="0.5" width="0.1016" layer="51" curve="-84.547378"/>
<wire x1="-0.55" y1="0.5" x2="0.55" y2="0.5" width="0.1016" layer="21" curve="-95.452622"/>
<wire x1="0.55" y1="0.5" x2="0.55" y2="-0.5" width="0.1016" layer="51" curve="-84.547378"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<rectangle x1="0.45" y1="-0.7" x2="0.6" y2="-0.45" layer="21"/>
</package>
<package name="LED3MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="-1.524" y1="0" x2="-1.1708" y2="0.9756" width="0.1524" layer="51" curve="-39.80361"/>
<wire x1="-1.524" y1="0" x2="-1.1391" y2="-1.0125" width="0.1524" layer="51" curve="41.633208"/>
<wire x1="1.1571" y1="0.9918" x2="1.524" y2="0" width="0.1524" layer="51" curve="-40.601165"/>
<wire x1="1.1708" y1="-0.9756" x2="1.524" y2="0" width="0.1524" layer="51" curve="39.80361"/>
<wire x1="0" y1="1.524" x2="1.2401" y2="0.8858" width="0.1524" layer="21" curve="-54.461337"/>
<wire x1="-1.2192" y1="0.9144" x2="0" y2="1.524" width="0.1524" layer="21" curve="-53.130102"/>
<wire x1="0" y1="-1.524" x2="1.203" y2="-0.9356" width="0.1524" layer="21" curve="52.126876"/>
<wire x1="-1.203" y1="-0.9356" x2="0" y2="-1.524" width="0.1524" layer="21" curve="52.126876"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED5MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, round</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B152">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-2.9718" y1="-1.8542" x2="-2.9718" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="-0.254" x2="-2.9718" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="0.254" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="2.9718" y1="-1.8542" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="-1.8542" x2="-2.54" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.8542" x2="-2.1082" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="1.8542" x2="2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.54" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.8542" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="0.254" x2="-2.9718" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-1.1486" y1="0.8814" x2="0" y2="1.4478" width="0.1524" layer="21" curve="-52.498642"/>
<wire x1="0" y1="1.4478" x2="1.1476" y2="0.8827" width="0.1524" layer="21" curve="-52.433716"/>
<wire x1="-1.1351" y1="-0.8987" x2="0" y2="-1.4478" width="0.1524" layer="21" curve="51.629985"/>
<wire x1="0" y1="-1.4478" x2="1.1305" y2="-0.9044" width="0.1524" layer="21" curve="51.339172"/>
<wire x1="1.1281" y1="-0.9074" x2="1.4478" y2="0" width="0.1524" layer="51" curve="38.811177"/>
<wire x1="1.1401" y1="0.8923" x2="1.4478" y2="0" width="0.1524" layer="51" curve="-38.048073"/>
<wire x1="-1.4478" y1="0" x2="-1.1305" y2="-0.9044" width="0.1524" layer="51" curve="38.659064"/>
<wire x1="-1.4478" y1="0" x2="-1.1456" y2="0.8853" width="0.1524" layer="51" curve="-37.696376"/>
<wire x1="0" y1="1.7018" x2="1.4674" y2="0.8618" width="0.1524" layer="21" curve="-59.573488"/>
<wire x1="-1.4618" y1="0.8714" x2="0" y2="1.7018" width="0.1524" layer="21" curve="-59.200638"/>
<wire x1="0" y1="-1.7018" x2="1.4571" y2="-0.8793" width="0.1524" layer="21" curve="58.891781"/>
<wire x1="-1.4571" y1="-0.8793" x2="0" y2="-1.7018" width="0.1524" layer="21" curve="58.891781"/>
<wire x1="-1.7018" y1="0" x2="-1.4447" y2="0.8995" width="0.1524" layer="51" curve="-31.907626"/>
<wire x1="-1.7018" y1="0" x2="-1.4502" y2="-0.8905" width="0.1524" layer="51" curve="31.551992"/>
<wire x1="1.4521" y1="0.8874" x2="1.7018" y2="0" width="0.1524" layer="51" curve="-31.429586"/>
<wire x1="1.4459" y1="-0.8975" x2="1.7018" y2="0" width="0.1524" layer="51" curve="31.828757"/>
<wire x1="-2.1082" y1="1.8542" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<wire x1="2.9718" y1="1.8542" x2="2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.905" y="2.286" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.905" y="-3.556" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B153">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-5.5118" y1="-3.5052" x2="-5.5118" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="-0.254" x2="-5.5118" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="0.254" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="5.5118" y1="-3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="-3.5052" x2="-5.08" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="3.5052" x2="-4.6482" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="3.5052" x2="5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.08" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="3.5052" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="0.254" x2="-5.5118" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-4.6482" y1="3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="5.5118" y1="3.5052" x2="5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.254" layer="21"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.2129" y1="0.0539" x2="-0.0539" y2="2.2129" width="0.1524" layer="51" curve="-90.010616"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-4.191" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-5.08" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B155">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-1.27" y1="-3.048" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-3.048" x2="2.921" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="3.048" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-5.207" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.54" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.54" x2="-5.207" y2="-2.54" width="0.1524" layer="21" curve="180"/>
<wire x1="-6.985" y1="0.635" x2="-6.985" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="1.397" x2="-6.096" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="1.905" x2="-5.207" y2="-1.905" width="0.1524" layer="21"/>
<pad name="K" x="7.62" y="1.27" drill="0.8128" shape="long"/>
<pad name="A" x="7.62" y="-1.27" drill="0.8128" shape="long"/>
<text x="3.302" y="-2.794" size="1.016" layer="21" ratio="14">A+</text>
<text x="3.302" y="1.778" size="1.016" layer="21" ratio="14">K-</text>
<text x="11.684" y="-2.794" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="0.635" y="-4.445" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.921" y1="1.016" x2="6.731" y2="1.524" layer="21"/>
<rectangle x1="2.921" y1="-1.524" x2="6.731" y2="-1.016" layer="21"/>
<hole x="0" y="0" drill="0.8128"/>
</package>
<package name="Q62902-B156">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="0.0539" y1="-2.2129" x2="2.2129" y2="-0.0539" width="0.1524" layer="51" curve="90.005308"/>
<wire x1="2.54" y1="3.81" x2="3.81" y2="2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-3.302" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-3.81" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="-2.54" y2="-3.302" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-3.81" y="4.0894" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.7846" y="-5.3594" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.556" y="-3.302" size="1.016" layer="21" ratio="14">+</text>
<text x="2.794" y="-3.302" size="1.016" layer="21" ratio="14">-</text>
</package>
<package name="SFH480">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SFH482">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="U57X32">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
rectangle, 5.7 x 3.2 mm</description>
<wire x1="-3.175" y1="1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="2.54" y2="1.016" width="0.1524" layer="51"/>
<wire x1="2.286" y1="1.27" x2="2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="2.54" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="2.54" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-1.016" x2="2.54" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="1.27" x2="-1.778" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.254" y1="1.27" x2="0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.762" y1="1.27" x2="0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.778" y1="1.27" x2="1.778" y2="-1.27" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="3.683" y="0.254" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.683" y="-1.524" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="IRL80A">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
IR transmitter Siemens</description>
<wire x1="0.889" y1="2.286" x2="0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.889" y1="1.778" x2="0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="0.889" y1="0.762" x2="0.889" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-0.635" x2="0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-1.778" x2="0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-2.286" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="-0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.778" x2="-0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0.762" x2="-0.889" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-1.778" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="0.889" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="0.762" width="0.1524" layer="21" curve="-180"/>
<wire x1="-1.397" y1="0.254" x2="-1.397" y2="-0.254" width="0.0508" layer="21"/>
<wire x1="-1.143" y1="0.508" x2="-1.143" y2="-0.508" width="0.0508" layer="21"/>
<pad name="K" x="0" y="1.27" drill="0.8128" shape="octagon"/>
<pad name="A" x="0" y="-1.27" drill="0.8128" shape="octagon"/>
<text x="1.27" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.27" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="P-LCC-2">
<description>&lt;b&gt;TOPLED® High-optical Power LED (HOP)&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... ls_t675.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.8" x2="1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-1.8" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="C" x="0" y="-2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="2.54" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-2.25" x2="1.3" y2="-0.75" layer="31"/>
<rectangle x1="-1.3" y1="0.75" x2="1.3" y2="2.25" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.4" y1="0.65" x2="1.4" y2="2.35" layer="29"/>
<rectangle x1="-1.4" y1="-2.35" x2="1.4" y2="-0.65" layer="29"/>
</package>
<package name="OSRAM-MINI-TOP-LED">
<description>&lt;b&gt;BLUE LINETM Hyper Mini TOPLED® Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LB M676.pdf</description>
<wire x1="-0.6" y1="0.9" x2="-0.6" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.4" y1="-0.9" x2="0.6" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="-0.9" x2="0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="0.9" x2="-0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.95" x2="-0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="1.1" x2="0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="1.1" x2="0.45" y2="0.95" width="0.1016" layer="51"/>
<wire x1="-0.6" y1="-0.7" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-1.1" x2="0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="-1.1" x2="0.45" y2="-0.95" width="0.1016" layer="51"/>
<smd name="A" x="0" y="2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="1.905" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.175" size="1.27" layer="21">C</text>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.5" y1="0.6" x2="0.5" y2="1.4" layer="29"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-0.6" layer="29"/>
<rectangle x1="-0.15" y1="-0.6" x2="0.15" y2="-0.3" layer="51"/>
<rectangle x1="-0.45" y1="0.65" x2="0.45" y2="1.35" layer="31"/>
<rectangle x1="-0.45" y1="-1.35" x2="0.45" y2="-0.65" layer="31"/>
</package>
<package name="OSRAM-SIDELED">
<description>&lt;b&gt;Super SIDELED® High-Current LED&lt;/b&gt;&lt;p&gt;
LG A672, LP A672 &lt;br&gt;
Source: http://www.osram.convergy.de/ ... LG_LP_A672.pdf (2004.05.13)</description>
<wire x1="-1.85" y1="-2.05" x2="-1.85" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="-0.75" x2="-1.7" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="-0.75" x2="-1.7" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="0.75" x2="-1.85" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="0.75" x2="-1.85" y2="2.05" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="2.05" x2="0.9" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="-1.85" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="-2.05" x2="1.85" y2="-1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="-1.85" x2="1.85" y2="1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="1.85" x2="1.05" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.9" x2="-0.55" y2="0.9" width="0.1016" layer="51" curve="-167.319617"/>
<wire x1="-0.55" y1="-0.9" x2="0.85" y2="-1.2" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.55" y1="0.9" x2="0.85" y2="1.2" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="-2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="0.635" y="-3.175" size="1.27" layer="21" rot="R90">C</text>
<text x="0.635" y="2.54" size="1.27" layer="21" rot="R90">A</text>
<text x="-2.54" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.1" y1="-2.2" x2="2.1" y2="-0.4" layer="29"/>
<rectangle x1="-2.1" y1="0.4" x2="2.1" y2="2.2" layer="29"/>
<rectangle x1="-1.9" y1="-2.1" x2="1.9" y2="-0.6" layer="31"/>
<rectangle x1="-1.9" y1="0.6" x2="1.9" y2="2.1" layer="31"/>
<rectangle x1="-1.85" y1="-2.05" x2="-0.7" y2="-1" layer="51"/>
</package>
<package name="SMART-LED">
<description>&lt;b&gt;SmartLEDTM Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY L896.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.15" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="A" x="0" y="0.725" dx="0.35" dy="0.35" layer="1"/>
<smd name="B" x="0" y="-0.725" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-0.635" size="1.016" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-0.635" size="1.016" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
</package>
<package name="P-LCC-2-TOPLED-RG">
<description>&lt;b&gt;Hyper TOPLED® RG Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY T776.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="2.45" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-2.45" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="21"/>
<smd name="C" x="0" y="-3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="3.29" size="1.27" layer="21">A</text>
<text x="-0.635" y="-4.56" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-3" x2="1.3" y2="-1.5" layer="31"/>
<rectangle x1="-1.3" y1="1.5" x2="1.3" y2="3" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.15" y1="2.4" x2="1.15" y2="2.7" layer="51"/>
<rectangle x1="-1.15" y1="-2.7" x2="1.15" y2="-2.4" layer="51"/>
<rectangle x1="-1.5" y1="1.5" x2="1.5" y2="3.2" layer="29"/>
<rectangle x1="-1.5" y1="-3.2" x2="1.5" y2="-1.5" layer="29"/>
<hole x="0" y="0" drill="2.8"/>
</package>
<package name="MICRO-SIDELED">
<description>&lt;b&gt;Hyper Micro SIDELED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY Y876.pdf</description>
<wire x1="0.65" y1="1.1" x2="-0.1" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="1.1" x2="-0.35" y2="1" width="0.1016" layer="51"/>
<wire x1="-0.35" y1="1" x2="-0.35" y2="-0.9" width="0.1016" layer="21"/>
<wire x1="-0.35" y1="-0.9" x2="-0.1" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="-1.1" x2="0.65" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.65" y1="-1.1" x2="0.65" y2="1.1" width="0.1016" layer="21"/>
<wire x1="0.6" y1="0.9" x2="0.25" y2="0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="0.7" x2="0.25" y2="-0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="-0.7" x2="0.6" y2="-0.9" width="0.0508" layer="21"/>
<smd name="A" x="0" y="1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.4" y1="1.1" x2="0.4" y2="1.8" layer="29"/>
<rectangle x1="-0.4" y1="-1.8" x2="0.4" y2="-1.1" layer="29"/>
<rectangle x1="-0.35" y1="-1.75" x2="0.35" y2="-1.15" layer="31"/>
<rectangle x1="-0.35" y1="1.15" x2="0.35" y2="1.75" layer="31"/>
<rectangle x1="-0.125" y1="1.125" x2="0.125" y2="1.75" layer="51"/>
<rectangle x1="-0.125" y1="-1.75" x2="0.125" y2="-1.125" layer="51"/>
</package>
<package name="P-LCC-4">
<description>&lt;b&gt;Power TOPLED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LA_LY E67B.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="1.8" x2="-0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="1.8" x2="-0.5" y2="1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.65" x2="0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.8" x2="-0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="-1.8" x2="-0.5" y2="-1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.65" x2="0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.8" x2="1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1" y1="-1.8" x2="1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="A" x="-2" y="3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@3" x="2" y="3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@4" x="2" y="-3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="-2" y="-3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<text x="-3.81" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-1.905" y="-3.81" size="1.27" layer="21">C</text>
<text x="-1.905" y="2.54" size="1.27" layer="21">A</text>
<text x="1.27" y="2.54" size="1.27" layer="21">C</text>
<text x="1.27" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.15" y1="0.75" x2="-0.35" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="0.75" x2="1.15" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="-1.85" x2="1.15" y2="-0.75" layer="29"/>
<rectangle x1="-1.15" y1="-1.85" x2="-0.35" y2="-0.75" layer="29"/>
<rectangle x1="-1.1" y1="-1.8" x2="-0.4" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="-1.8" x2="1.1" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="0.8" x2="1.1" y2="1.8" layer="31"/>
<rectangle x1="-1.1" y1="0.8" x2="-0.4" y2="1.8" layer="31"/>
<rectangle x1="-0.2" y1="-0.2" x2="0.2" y2="0.2" layer="21"/>
</package>
<package name="CHIP-LED0603">
<description>&lt;b&gt;Hyper CHIPLED Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
LB Q993&lt;br&gt;
Source: http://www.osram.convergy.de/ ... Lb_q993.pdf</description>
<wire x1="-0.4" y1="0.45" x2="-0.4" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.45" x2="0.4" y2="-0.45" width="0.1016" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.635" y="-0.635" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-0.635" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.45" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="0.45" y2="-0.45" layer="51"/>
<rectangle x1="-0.45" y1="0" x2="-0.3" y2="0.3" layer="21"/>
<rectangle x1="0.3" y1="0" x2="0.45" y2="0.3" layer="21"/>
<rectangle x1="-0.15" y1="0" x2="0.15" y2="0.3" layer="21"/>
</package>
<package name="CHIP-LED0805">
<description>&lt;b&gt;Hyper CHIPLED Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
LB R99A&lt;br&gt;
Source: http://www.osram.convergy.de/ ... lb_r99a.pdf</description>
<wire x1="-0.625" y1="0.45" x2="-0.625" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="0.625" y1="0.45" x2="0.625" y2="-0.475" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.675" y1="0" x2="-0.525" y2="0.3" layer="21"/>
<rectangle x1="0.525" y1="0" x2="0.675" y2="0.3" layer="21"/>
<rectangle x1="-0.15" y1="0" x2="0.15" y2="0.3" layer="21"/>
<rectangle x1="-0.675" y1="0.45" x2="0.675" y2="1.05" layer="51"/>
<rectangle x1="-0.675" y1="-1.05" x2="0.675" y2="-0.45" layer="51"/>
</package>
<package name="MINI-TOPLED-SANTANA">
<description>&lt;b&gt;Mini TOPLED Santana®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG M470.pdf</description>
<wire x1="0.7" y1="-1" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.35" y1="-1" x2="-0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="-1" x2="-0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="1" x2="0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="0.7" y1="1" x2="0.7" y2="-0.65" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.45" y1="-0.7" x2="-0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="-0.7" x2="-0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="0.7" x2="0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="0.45" y1="0.7" x2="0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<smd name="C" x="0" y="-2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.55" y1="1.5" x2="0.55" y2="2.1" layer="29"/>
<rectangle x1="-0.55" y1="-2.1" x2="0.55" y2="-1.5" layer="29"/>
<rectangle x1="-0.5" y1="-2.05" x2="0.5" y2="-1.55" layer="31"/>
<rectangle x1="-0.5" y1="1.55" x2="0.5" y2="2.05" layer="31"/>
<rectangle x1="-0.2" y1="-0.4" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.5" y1="-2.1" x2="0.5" y2="-1.4" layer="51"/>
<rectangle x1="-0.5" y1="1.4" x2="0.5" y2="2.05" layer="51"/>
<rectangle x1="-0.5" y1="1" x2="0.5" y2="1.4" layer="21"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-1.05" layer="21"/>
<hole x="0" y="0" drill="2.7"/>
</package>
<package name="CHIPLED_0805">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_R971.pdf</description>
<wire x1="-0.35" y1="0.925" x2="0.35" y2="0.925" width="0.1016" layer="51" curve="162.394521"/>
<wire x1="-0.35" y1="-0.925" x2="0.35" y2="-0.925" width="0.1016" layer="51" curve="-162.394521"/>
<wire x1="0.575" y1="0.525" x2="0.575" y2="-0.525" width="0.1016" layer="51"/>
<wire x1="-0.575" y1="-0.5" x2="-0.575" y2="0.925" width="0.1016" layer="51"/>
<circle x="-0.45" y="0.85" radius="0.103" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="0.3" y1="0.5" x2="0.625" y2="1" layer="51"/>
<rectangle x1="-0.325" y1="0.5" x2="-0.175" y2="0.75" layer="51"/>
<rectangle x1="0.175" y1="0.5" x2="0.325" y2="0.75" layer="51"/>
<rectangle x1="-0.2" y1="0.5" x2="0.2" y2="0.675" layer="51"/>
<rectangle x1="0.3" y1="-1" x2="0.625" y2="-0.5" layer="51"/>
<rectangle x1="-0.625" y1="-1" x2="-0.3" y2="-0.5" layer="51"/>
<rectangle x1="0.175" y1="-0.75" x2="0.325" y2="-0.5" layer="51"/>
<rectangle x1="-0.325" y1="-0.75" x2="-0.175" y2="-0.5" layer="51"/>
<rectangle x1="-0.2" y1="-0.675" x2="0.2" y2="-0.5" layer="51"/>
<rectangle x1="-0.1" y1="0" x2="0.1" y2="0.2" layer="21"/>
<rectangle x1="-0.6" y1="0.5" x2="-0.3" y2="0.8" layer="51"/>
<rectangle x1="-0.625" y1="0.925" x2="-0.3" y2="1" layer="51"/>
</package>
<package name="CHIPLED_1206">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY N971.pdf</description>
<wire x1="-0.4" y1="1.6" x2="0.4" y2="1.6" width="0.1016" layer="51" curve="172.619069"/>
<wire x1="-0.8" y1="-0.95" x2="-0.8" y2="0.95" width="0.1016" layer="51"/>
<wire x1="0.8" y1="0.95" x2="0.8" y2="-0.95" width="0.1016" layer="51"/>
<circle x="-0.55" y="1.425" radius="0.1" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.75" dx="1.5" dy="1.5" layer="1"/>
<smd name="A" x="0" y="-1.75" dx="1.5" dy="1.5" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.85" y1="1.525" x2="-0.35" y2="1.65" layer="51"/>
<rectangle x1="-0.85" y1="1.225" x2="-0.625" y2="1.55" layer="51"/>
<rectangle x1="-0.45" y1="1.225" x2="-0.325" y2="1.45" layer="51"/>
<rectangle x1="-0.65" y1="1.225" x2="-0.225" y2="1.35" layer="51"/>
<rectangle x1="0.35" y1="1.3" x2="0.85" y2="1.65" layer="51"/>
<rectangle x1="0.25" y1="1.225" x2="0.85" y2="1.35" layer="51"/>
<rectangle x1="-0.85" y1="0.95" x2="0.85" y2="1.25" layer="51"/>
<rectangle x1="-0.85" y1="-1.65" x2="0.85" y2="-0.95" layer="51"/>
<rectangle x1="-0.85" y1="0.35" x2="-0.525" y2="0.775" layer="21"/>
<rectangle x1="0.525" y1="0.35" x2="0.85" y2="0.775" layer="21"/>
<rectangle x1="-0.175" y1="0" x2="0.175" y2="0.35" layer="21"/>
</package>
<package name="CHIPLED_0603">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY Q971.pdf</description>
<wire x1="-0.3" y1="0.8" x2="0.3" y2="0.8" width="0.1016" layer="51" curve="170.055574"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
</package>
<package name="CHIPLED-0603-TTW">
<description>&lt;b&gt;CHIPLED-0603&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.3" y1="0.8" x2="0.3" y2="0.8" width="0.1016" layer="51" curve="170.055574"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.4" y1="0.625" x2="0.4" y2="1.125" layer="29"/>
<rectangle x1="-0.4" y1="-1.125" x2="0.4" y2="-0.625" layer="29"/>
<rectangle x1="-0.175" y1="-0.675" x2="0.175" y2="-0.325" layer="29"/>
</package>
<package name="SMARTLED-TTW">
<description>&lt;b&gt;SmartLED TTW&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.15" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
<rectangle x1="-0.225" y1="0.3" x2="0.225" y2="0.975" layer="31"/>
<rectangle x1="-0.175" y1="-0.7" x2="0.175" y2="-0.325" layer="29" rot="R180"/>
<rectangle x1="-0.225" y1="-0.975" x2="0.225" y2="-0.3" layer="31" rot="R180"/>
</package>
<package name="LUMILED+">
<description>&lt;b&gt;Lumileds Lighting. LUXEON®&lt;/b&gt; with cool pad&lt;p&gt;
Source: K2.pdf</description>
<wire x1="-3.575" y1="2.3375" x2="-2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="3.575" x2="2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="2.3375" x2="3.575" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="-3.575" x2="-2.3375" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="-3.575" x2="-2.5" y2="-3.4125" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-3.4125" x2="-3.4125" y2="-2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="-3.4125" y1="-2.5" x2="-3.575" y2="-2.3375" width="0.2032" layer="21"/>
<wire x1="-3.575" y1="-2.3375" x2="-3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="2.3375" y1="3.575" x2="2.5" y2="3.4125" width="0.2032" layer="21"/>
<wire x1="2.5" y1="3.4125" x2="3.4125" y2="2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="3.4125" y1="2.5" x2="3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="-1.725" y1="2.225" x2="-1.0625" y2="2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<wire x1="1.725" y1="-2.225" x2="1.0625" y2="-2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<circle x="0" y="0" radius="2.725" width="0.2032" layer="51"/>
<smd name="1NC" x="-5.2" y="1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="2+" x="-5.2" y="-1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="3NC" x="5.2" y="-1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<smd name="4-" x="5.2" y="1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<text x="-3.175" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.975" y1="0.575" x2="-3.625" y2="1.6" layer="51"/>
<rectangle x1="-5.975" y1="-1.6" x2="-3.625" y2="-0.575" layer="51"/>
<rectangle x1="3.625" y1="-1.6" x2="5.975" y2="-0.575" layer="51" rot="R180"/>
<rectangle x1="3.625" y1="0.575" x2="5.975" y2="1.6" layer="51" rot="R180"/>
<polygon width="0.4064" layer="1">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="29">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="31">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
</package>
<package name="LUMILED">
<description>&lt;b&gt;Lumileds Lighting. LUXEON®&lt;/b&gt; without cool pad&lt;p&gt;
Source: K2.pdf</description>
<wire x1="-3.575" y1="2.3375" x2="-2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="3.575" x2="2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="2.3375" x2="3.575" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="-3.575" x2="-2.3375" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="-3.575" x2="-2.5" y2="-3.4125" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-3.4125" x2="-3.4125" y2="-2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="-3.4125" y1="-2.5" x2="-3.575" y2="-2.3375" width="0.2032" layer="21"/>
<wire x1="-3.575" y1="-2.3375" x2="-3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="2.3375" y1="3.575" x2="2.5" y2="3.4125" width="0.2032" layer="21"/>
<wire x1="2.5" y1="3.4125" x2="3.4125" y2="2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="3.4125" y1="2.5" x2="3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="-1.725" y1="2.225" x2="-1.0625" y2="2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<wire x1="1.725" y1="-2.225" x2="1.0625" y2="-2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<circle x="0" y="0" radius="2.725" width="0.2032" layer="51"/>
<smd name="1NC" x="-5.2" y="1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="2+" x="-5.2" y="-1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="3NC" x="5.2" y="-1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<smd name="4-" x="5.2" y="1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<text x="-3.175" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.975" y1="0.575" x2="-3.625" y2="1.6" layer="51"/>
<rectangle x1="-5.975" y1="-1.6" x2="-3.625" y2="-0.575" layer="51"/>
<rectangle x1="3.625" y1="-1.6" x2="5.975" y2="-0.575" layer="51" rot="R180"/>
<rectangle x1="3.625" y1="0.575" x2="5.975" y2="1.6" layer="51" rot="R180"/>
<polygon width="0.4064" layer="29">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="31">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
</package>
<package name="LED10MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
10 mm, round</description>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.254" layer="21" curve="-306.869898"/>
<wire x1="4.445" y1="0" x2="0" y2="-4.445" width="0.127" layer="21" curve="-90"/>
<wire x1="3.81" y1="0" x2="0" y2="-3.81" width="0.127" layer="21" curve="-90"/>
<wire x1="3.175" y1="0" x2="0" y2="-3.175" width="0.127" layer="21" curve="-90"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.127" layer="21" curve="-90"/>
<wire x1="-4.445" y1="0" x2="0" y2="4.445" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.81" y1="0" x2="0" y2="3.81" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.175" y1="0" x2="0" y2="3.175" width="0.127" layer="21" curve="-90"/>
<wire x1="-2.54" y1="0" x2="0" y2="2.54" width="0.127" layer="21" curve="-90"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="21"/>
<circle x="0" y="0" radius="5.08" width="0.127" layer="21"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="square"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="6.35" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="6.35" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="LD260">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, square, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="0" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="0" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="0" y1="1.27" x2="0.9917" y2="0.7934" width="0.1524" layer="21" curve="-51.33923"/>
<wire x1="-0.9917" y1="0.7934" x2="0" y2="1.27" width="0.1524" layer="21" curve="-51.33923"/>
<wire x1="0" y1="-1.27" x2="0.9917" y2="-0.7934" width="0.1524" layer="21" curve="51.33923"/>
<wire x1="-0.9917" y1="-0.7934" x2="0" y2="-1.27" width="0.1524" layer="21" curve="51.33923"/>
<wire x1="0.9558" y1="-0.8363" x2="1.27" y2="0" width="0.1524" layer="51" curve="41.185419"/>
<wire x1="0.9756" y1="0.813" x2="1.2699" y2="0" width="0.1524" layer="51" curve="-39.806332"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="-0.8265" width="0.1524" layer="51" curve="40.600331"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="0.8265" width="0.1524" layer="51" curve="-40.600331"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.635" x2="2.032" y2="0.635" layer="51"/>
<rectangle x1="1.905" y1="-0.635" x2="2.032" y2="0.635" layer="21"/>
</package>
<package name="LED2X5">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
2 x 5 mm, rectangle</description>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="0" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-0.254" x2="1.143" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="0.9398" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="0.9398" y1="-0.6096" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.651" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.4478" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="1.4478" y1="-0.6096" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-1.27" x2="2.413" y2="1.27" layer="21"/>
</package>
<package name="LSU260">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
1 mm, round, Siemens</description>
<wire x1="0" y1="-0.508" x2="-1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.508" x2="-1.143" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.508" x2="0" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="-0.254" x2="-1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.254" x2="-1.143" y2="0.508" width="0.1524" layer="51"/>
<wire x1="0.508" y1="-0.254" x2="1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="0.254" x2="0.508" y2="0.254" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-0.381" x2="0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.508" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="-0.381" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0.381" x2="0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.508" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0.381" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.254" x2="0.254" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.254" y1="0" x2="0" y2="0.254" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.381" y1="-0.381" x2="0.381" y2="0.381" width="0.1524" layer="21" curve="90"/>
<circle x="0" y="0" radius="0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="0.8382" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-1.8542" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.397" y1="-0.254" x2="-1.143" y2="0.254" layer="51"/>
<rectangle x1="0.508" y1="-0.254" x2="1.397" y2="0.254" layer="51"/>
</package>
<package name="LZR181">
<description>&lt;B&gt;LED BLOCK&lt;/B&gt;&lt;p&gt;
1 LED, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-0.8678" y1="0.7439" x2="0" y2="1.143" width="0.1524" layer="21" curve="-49.396139"/>
<wire x1="0" y1="1.143" x2="0.8678" y2="0.7439" width="0.1524" layer="21" curve="-49.396139"/>
<wire x1="-0.8678" y1="-0.7439" x2="0" y2="-1.143" width="0.1524" layer="21" curve="49.396139"/>
<wire x1="0" y1="-1.143" x2="0.8678" y2="-0.7439" width="0.1524" layer="21" curve="49.396139"/>
<wire x1="0.8678" y1="0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="-40.604135"/>
<wire x1="0.8678" y1="-0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="40.604135"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="0.7439" width="0.1524" layer="51" curve="-40.604135"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="-0.7439" width="0.1524" layer="51" curve="40.604135"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="1.27" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.889" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.27" y2="0.254" layer="51"/>
</package>
<package name="KA-3528ASYC">
<description>&lt;b&gt;SURFACE MOUNT LED LAMP&lt;/b&gt; 3.5x2.8mm&lt;p&gt;
Source: http://www.kingbright.com/manager/upload/pdf/KA-3528ASYC(Ver1189474662.1)</description>
<wire x1="-1.55" y1="1.35" x2="1.55" y2="1.35" width="0.1016" layer="21"/>
<wire x1="1.55" y1="1.35" x2="1.55" y2="-1.35" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-1.35" x2="-1.55" y2="-1.35" width="0.1016" layer="21"/>
<wire x1="-1.55" y1="-1.35" x2="-1.55" y2="1.35" width="0.1016" layer="51"/>
<wire x1="-0.65" y1="0.95" x2="0.65" y2="0.95" width="0.1016" layer="21" curve="-68.40813"/>
<wire x1="0.65" y1="-0.95" x2="-0.65" y2="-0.95" width="0.1016" layer="21" curve="-68.40813"/>
<circle x="0" y="0" radius="1.15" width="0.1016" layer="51"/>
<smd name="A" x="-1.55" y="0" dx="1.5" dy="2.2" layer="1"/>
<smd name="C" x="1.55" y="0" dx="1.5" dy="2.2" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.75" y1="0.6" x2="-1.6" y2="1.1" layer="51"/>
<rectangle x1="-1.75" y1="-1.1" x2="-1.6" y2="-0.6" layer="51"/>
<rectangle x1="1.6" y1="-1.1" x2="1.75" y2="-0.6" layer="51" rot="R180"/>
<rectangle x1="1.6" y1="0.6" x2="1.75" y2="1.1" layer="51" rot="R180"/>
<polygon width="0.1016" layer="51">
<vertex x="1.55" y="-1.35"/>
<vertex x="1.55" y="-0.625"/>
<vertex x="0.825" y="-1.35"/>
</polygon>
<polygon width="0.1016" layer="21">
<vertex x="1.55" y="-1.35"/>
<vertex x="1.55" y="-1.175"/>
<vertex x="1" y="-1.175"/>
<vertex x="0.825" y="-1.35"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="LED" uservalue="yes">
<description>&lt;b&gt;LED&lt;/b&gt;&lt;p&gt;
&lt;u&gt;OSRAM&lt;/u&gt;:&lt;br&gt;

- &lt;u&gt;CHIPLED&lt;/u&gt;&lt;br&gt;
LG R971, LG N971, LY N971, LG Q971, LY Q971, LO R971, LY R971
LH N974, LH R974&lt;br&gt;
LS Q976, LO Q976, LY Q976&lt;br&gt;
LO Q996&lt;br&gt;

- &lt;u&gt;Hyper CHIPLED&lt;/u&gt;&lt;br&gt;
LW Q18S&lt;br&gt;
LB Q993, LB Q99A, LB R99A&lt;br&gt;

- &lt;u&gt;SideLED&lt;/u&gt;&lt;br&gt;
LS A670, LO A670, LY A670, LG A670, LP A670&lt;br&gt;
LB A673, LV A673, LT A673, LW A673&lt;br&gt;
LH A674&lt;br&gt;
LY A675&lt;br&gt;
LS A676, LA A676, LO A676, LY A676, LW A676&lt;br&gt;
LS A679, LY A679, LG A679&lt;br&gt;

-  &lt;u&gt;Hyper Micro SIDELED®&lt;/u&gt;&lt;br&gt;
LS Y876, LA Y876, LO Y876, LY Y876&lt;br&gt;
LT Y87S&lt;br&gt;

- &lt;u&gt;SmartLED&lt;/u&gt;&lt;br&gt;
LW L88C, LW L88S&lt;br&gt;
LB L89C, LB L89S, LG L890&lt;br&gt;
LS L89K, LO L89K, LY L89K&lt;br&gt;
LS L896, LA L896, LO L896, LY L896&lt;br&gt;

- &lt;u&gt;TOPLED&lt;/u&gt;&lt;br&gt;
LS T670, LO T670, LY T670, LG T670, LP T670&lt;br&gt;
LSG T670, LSP T670, LSY T670, LOP T670, LYG T670&lt;br&gt;
LG T671, LOG T671, LSG T671&lt;br&gt;
LB T673, LV T673, LT T673, LW T673&lt;br&gt;
LH T674&lt;br&gt;
LS T676, LA T676, LO T676, LY T676, LB T676, LH T676, LSB T676, LW T676&lt;br&gt;
LB T67C, LV T67C, LT T67C, LS T67K, LO T67K, LY T67K, LW E67C&lt;br&gt;
LS E67B, LA E67B, LO E67B, LY E67B, LB E67C, LV E67C, LT E67C&lt;br&gt;
LW T67C&lt;br&gt;
LS T679, LY T679, LG T679&lt;br&gt;
LS T770, LO T770, LY T770, LG T770, LP T770&lt;br&gt;
LB T773, LV T773, LT T773, LW T773&lt;br&gt;
LH T774&lt;br&gt;
LS E675, LA E675, LY E675, LS T675&lt;br&gt;
LS T776, LA T776, LO T776, LY T776, LB T776&lt;br&gt;
LHGB T686&lt;br&gt;
LT T68C, LB T68C&lt;br&gt;

- &lt;u&gt;Hyper Mini TOPLED®&lt;/u&gt;&lt;br&gt;
LB M676&lt;br&gt;

- &lt;u&gt;Mini TOPLED Santana®&lt;/u&gt;&lt;br&gt;
LG M470&lt;br&gt;
LS M47K, LO M47K, LY M47K
&lt;p&gt;
Source: http://www.osram.convergy.de&lt;p&gt;

&lt;u&gt;LUXEON:&lt;/u&gt;&lt;br&gt;
- &lt;u&gt;LUMILED®&lt;/u&gt;&lt;br&gt;
LXK2-PW12-R00, LXK2-PW12-S00, LXK2-PW14-U00, LXK2-PW14-V00&lt;br&gt;
LXK2-PM12-R00, LXK2-PM12-S00, LXK2-PM14-U00&lt;br&gt;
LXK2-PE12-Q00, LXK2-PE12-R00, LXK2-PE12-S00, LXK2-PE14-T00, LXK2-PE14-U00&lt;br&gt;
LXK2-PB12-K00, LXK2-PB12-L00, LXK2-PB12-M00, LXK2-PB14-N00, LXK2-PB14-P00, LXK2-PB14-Q00&lt;br&gt;
LXK2-PR12-L00, LXK2-PR12-M00, LXK2-PR14-Q00, LXK2-PR14-R00&lt;br&gt;
LXK2-PD12-Q00, LXK2-PD12-R00, LXK2-PD12-S00&lt;br&gt;
LXK2-PH12-R00, LXK2-PH12-S00&lt;br&gt;
LXK2-PL12-P00, LXK2-PL12-Q00, LXK2-PL12-R00
&lt;p&gt;
Source: www.luxeon.com&lt;p&gt;

&lt;u&gt;KINGBRIGHT:&lt;/U&gt;&lt;p&gt;
KA-3528ASYC&lt;br&gt;
Source: www.kingbright.com</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="SMT1206" package="1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LD260" package="LD260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SQR2X5" package="LED2X5">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LSU260" package="LSU260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LZR181" package="LZR181">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B152" package="Q62902-B152">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B153" package="Q62902-B153">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B155" package="Q62902-B155">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B156" package="Q62902-B156">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SFH480" package="SFH480">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SFH482" package="SFH482">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SQR5.7X3.2" package="U57X32">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="IRL80A" package="IRL80A">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-2" package="P-LCC-2">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MINI-TOP" package="OSRAM-MINI-TOP-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIDELED" package="OSRAM-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMART-LED" package="SMART-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="B"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-2-BACK" package="P-LCC-2-TOPLED-RG">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MICRO-SIDELED" package="MICRO-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-4" package="P-LCC-4">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C@4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIP-LED0603" package="CHIP-LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIP-LED0805" package="CHIP-LED0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TOPLED-SANTANA" package="MINI-TOPLED-SANTANA">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_0805" package="CHIPLED_0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_1206" package="CHIPLED_1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_0603" package="CHIPLED_0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED-0603-TTW" package="CHIPLED-0603-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="SMARTLED-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-LUMILED+" package="LUMILED+">
<connects>
<connect gate="G$1" pin="A" pad="2+"/>
<connect gate="G$1" pin="C" pad="4-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-LUMILED" package="LUMILED">
<connects>
<connect gate="G$1" pin="A" pad="2+"/>
<connect gate="G$1" pin="C" pad="4-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="LED10MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KA-3528ASYC" package="KA-3528ASYC">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="xilinx_devices">
<packages>
<package name="TQ144">
<description>&lt;b&gt;TQ144 TQG144&lt;/b&gt;</description>
<wire x1="-9.625" y1="9.275" x2="-9.625" y2="-9.275" width="0.127" layer="51"/>
<wire x1="-9.625" y1="-9.275" x2="-9.275" y2="-9.625" width="0.127" layer="51"/>
<wire x1="-9.275" y1="-9.625" x2="9.275" y2="-9.625" width="0.127" layer="51"/>
<wire x1="9.275" y1="-9.625" x2="9.625" y2="-9.275" width="0.127" layer="51"/>
<wire x1="9.625" y1="-9.275" x2="9.625" y2="9.275" width="0.127" layer="51"/>
<wire x1="9.625" y1="9.275" x2="9.275" y2="9.625" width="0.127" layer="51"/>
<wire x1="9.275" y1="9.625" x2="-9.275" y2="9.625" width="0.127" layer="51"/>
<wire x1="-9.275" y1="9.625" x2="-9.625" y2="9.275" width="0.127" layer="51"/>
<wire x1="-10" y1="9.5" x2="-10" y2="-9.5" width="0.254" layer="21"/>
<wire x1="-10" y1="-9.5" x2="-9.5" y2="-10" width="0.254" layer="21"/>
<wire x1="-9.5" y1="-10" x2="9.5" y2="-10" width="0.254" layer="21"/>
<wire x1="9.5" y1="-10" x2="10" y2="-9.5" width="0.254" layer="21"/>
<wire x1="10" y1="-9.5" x2="10" y2="9.5" width="0.254" layer="21"/>
<wire x1="10" y1="9.5" x2="9.5" y2="10" width="0.254" layer="21"/>
<wire x1="9.5" y1="10" x2="-9.5" y2="10" width="0.254" layer="21"/>
<wire x1="-9.5" y1="10" x2="-10" y2="9.5" width="0.254" layer="21"/>
<circle x="-8" y="8" radius="0.5" width="0.127" layer="21"/>
<smd name="1" x="-11.1" y="8.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="2" x="-11.1" y="8.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="3" x="-11.1" y="7.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="4" x="-11.1" y="7.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="5" x="-11.1" y="6.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="6" x="-11.1" y="6.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="7" x="-11.1" y="5.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="8" x="-11.1" y="5.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="9" x="-11.1" y="4.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="10" x="-11.1" y="4.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="11" x="-11.1" y="3.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="12" x="-11.1" y="3.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="13" x="-11.1" y="2.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="14" x="-11.1" y="2.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="15" x="-11.1" y="1.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="16" x="-11.1" y="1.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="17" x="-11.1" y="0.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="18" x="-11.1" y="0.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="19" x="-11.1" y="-0.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="20" x="-11.1" y="-0.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="21" x="-11.1" y="-1.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="22" x="-11.1" y="-1.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="23" x="-11.1" y="-2.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="24" x="-11.1" y="-2.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="25" x="-11.1" y="-3.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="26" x="-11.1" y="-3.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="27" x="-11.1" y="-4.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="28" x="-11.1" y="-4.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="29" x="-11.1" y="-5.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="30" x="-11.1" y="-5.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="31" x="-11.1" y="-6.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="32" x="-11.1" y="-6.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="33" x="-11.1" y="-7.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="34" x="-11.1" y="-7.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="35" x="-11.1" y="-8.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="36" x="-11.1" y="-8.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="37" x="-8.75" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="38" x="-8.25" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="39" x="-7.75" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="40" x="-7.25" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="41" x="-6.75" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="42" x="-6.25" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="43" x="-5.75" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="44" x="-5.25" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="45" x="-4.75" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="46" x="-4.25" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="47" x="-3.75" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="48" x="-3.25" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="49" x="-2.75" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="50" x="-2.25" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="51" x="-1.75" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="52" x="-1.25" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="53" x="-0.75" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="54" x="-0.25" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="55" x="0.25" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="56" x="0.75" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="57" x="1.25" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="58" x="1.75" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="59" x="2.25" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="60" x="2.75" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="61" x="3.25" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="62" x="3.75" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="63" x="4.25" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="64" x="4.75" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="65" x="5.25" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="66" x="5.75" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="67" x="6.25" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="68" x="6.75" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="69" x="7.25" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="70" x="7.75" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="71" x="8.25" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="72" x="8.75" y="-11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="73" x="11.1" y="-8.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="74" x="11.1" y="-8.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="75" x="11.1" y="-7.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="76" x="11.1" y="-7.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="77" x="11.1" y="-6.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="78" x="11.1" y="-6.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="79" x="11.1" y="-5.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="80" x="11.1" y="-5.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="81" x="11.1" y="-4.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="82" x="11.1" y="-4.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="83" x="11.1" y="-3.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="84" x="11.1" y="-3.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="85" x="11.1" y="-2.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="86" x="11.1" y="-2.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="87" x="11.1" y="-1.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="88" x="11.1" y="-1.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="89" x="11.1" y="-0.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="90" x="11.1" y="-0.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="91" x="11.1" y="0.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="92" x="11.1" y="0.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="93" x="11.1" y="1.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="94" x="11.1" y="1.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="95" x="11.1" y="2.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="96" x="11.1" y="2.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="97" x="11.1" y="3.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="98" x="11.1" y="3.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="99" x="11.1" y="4.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="100" x="11.1" y="4.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="101" x="11.1" y="5.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="102" x="11.1" y="5.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="103" x="11.1" y="6.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="104" x="11.1" y="6.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="105" x="11.1" y="7.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="106" x="11.1" y="7.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="107" x="11.1" y="8.25" dx="1.6" dy="0.3" layer="1"/>
<smd name="108" x="11.1" y="8.75" dx="1.6" dy="0.3" layer="1"/>
<smd name="109" x="8.75" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="110" x="8.25" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="111" x="7.75" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="112" x="7.25" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="113" x="6.75" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="114" x="6.25" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="115" x="5.75" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="116" x="5.25" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="117" x="4.75" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="118" x="4.25" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="119" x="3.75" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="120" x="3.25" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="121" x="2.75" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="122" x="2.25" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="123" x="1.75" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="124" x="1.25" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="125" x="0.75" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="126" x="0.25" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="127" x="-0.25" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="128" x="-0.75" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="129" x="-1.25" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="130" x="-1.75" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="131" x="-2.25" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="132" x="-2.75" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="133" x="-3.25" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="134" x="-3.75" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="135" x="-4.25" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="136" x="-4.75" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="137" x="-5.25" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="138" x="-5.75" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="139" x="-6.25" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="140" x="-6.75" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="141" x="-7.25" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="142" x="-7.75" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="143" x="-8.25" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<smd name="144" x="-8.75" y="11.1" dx="1.6" dy="0.3" layer="1" rot="R90"/>
<text x="-2" y="1" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-2.5" y="-2" size="0.8128" layer="27" font="vector" ratio="16">&gt;VALUE</text>
<rectangle x1="-11" y1="8.62" x2="-10" y2="8.88" layer="51"/>
<rectangle x1="-11" y1="8.12" x2="-10" y2="8.38" layer="51"/>
<rectangle x1="-11" y1="7.62" x2="-10" y2="7.88" layer="51"/>
<rectangle x1="-11" y1="7.12" x2="-10" y2="7.38" layer="51"/>
<rectangle x1="-11" y1="6.62" x2="-10" y2="6.88" layer="51"/>
<rectangle x1="-11" y1="6.12" x2="-10" y2="6.38" layer="51"/>
<rectangle x1="-11" y1="5.62" x2="-10" y2="5.88" layer="51"/>
<rectangle x1="-11" y1="5.12" x2="-10" y2="5.38" layer="51"/>
<rectangle x1="-11" y1="4.62" x2="-10" y2="4.88" layer="51"/>
<rectangle x1="-11" y1="4.12" x2="-10" y2="4.38" layer="51"/>
<rectangle x1="-11" y1="3.62" x2="-10" y2="3.88" layer="51"/>
<rectangle x1="-11" y1="3.12" x2="-10" y2="3.38" layer="51"/>
<rectangle x1="-11" y1="2.62" x2="-10" y2="2.88" layer="51"/>
<rectangle x1="-11" y1="2.12" x2="-10" y2="2.38" layer="51"/>
<rectangle x1="-11" y1="1.62" x2="-10" y2="1.88" layer="51"/>
<rectangle x1="-11" y1="1.12" x2="-10" y2="1.38" layer="51"/>
<rectangle x1="-11" y1="0.62" x2="-10" y2="0.88" layer="51"/>
<rectangle x1="-11" y1="0.12" x2="-10" y2="0.38" layer="51"/>
<rectangle x1="-11" y1="-0.38" x2="-10" y2="-0.12" layer="51"/>
<rectangle x1="-11" y1="-0.88" x2="-10" y2="-0.62" layer="51"/>
<rectangle x1="-11" y1="-1.38" x2="-10" y2="-1.12" layer="51"/>
<rectangle x1="-11" y1="-1.88" x2="-10" y2="-1.62" layer="51"/>
<rectangle x1="-11" y1="-2.38" x2="-10" y2="-2.12" layer="51"/>
<rectangle x1="-11" y1="-2.88" x2="-10" y2="-2.62" layer="51"/>
<rectangle x1="-11" y1="-3.38" x2="-10" y2="-3.12" layer="51"/>
<rectangle x1="-11" y1="-3.88" x2="-10" y2="-3.62" layer="51"/>
<rectangle x1="-11" y1="-4.38" x2="-10" y2="-4.12" layer="51"/>
<rectangle x1="-11" y1="-4.88" x2="-10" y2="-4.62" layer="51"/>
<rectangle x1="-11" y1="-5.38" x2="-10" y2="-5.12" layer="51"/>
<rectangle x1="-11" y1="-5.88" x2="-10" y2="-5.62" layer="51"/>
<rectangle x1="-11" y1="-6.38" x2="-10" y2="-6.12" layer="51"/>
<rectangle x1="-11" y1="-6.88" x2="-10" y2="-6.62" layer="51"/>
<rectangle x1="-11" y1="-7.38" x2="-10" y2="-7.12" layer="51"/>
<rectangle x1="-11" y1="-7.88" x2="-10" y2="-7.62" layer="51"/>
<rectangle x1="-11" y1="-8.38" x2="-10" y2="-8.12" layer="51"/>
<rectangle x1="-11" y1="-8.88" x2="-10" y2="-8.62" layer="51"/>
<rectangle x1="-8.88" y1="-11" x2="-8.62" y2="-10" layer="51"/>
<rectangle x1="-8.38" y1="-11" x2="-8.12" y2="-10" layer="51"/>
<rectangle x1="-7.88" y1="-11" x2="-7.62" y2="-10" layer="51"/>
<rectangle x1="-7.38" y1="-11" x2="-7.12" y2="-10" layer="51"/>
<rectangle x1="-6.88" y1="-11" x2="-6.62" y2="-10" layer="51"/>
<rectangle x1="-6.38" y1="-11" x2="-6.12" y2="-10" layer="51"/>
<rectangle x1="-5.88" y1="-11" x2="-5.62" y2="-10" layer="51"/>
<rectangle x1="-5.38" y1="-11" x2="-5.12" y2="-10" layer="51"/>
<rectangle x1="-4.88" y1="-11" x2="-4.62" y2="-10" layer="51"/>
<rectangle x1="-4.38" y1="-11" x2="-4.12" y2="-10" layer="51"/>
<rectangle x1="-3.88" y1="-11" x2="-3.62" y2="-10" layer="51"/>
<rectangle x1="-3.38" y1="-11" x2="-3.12" y2="-10" layer="51"/>
<rectangle x1="-2.88" y1="-11" x2="-2.62" y2="-10" layer="51"/>
<rectangle x1="-2.38" y1="-11" x2="-2.12" y2="-10" layer="51"/>
<rectangle x1="-1.88" y1="-11" x2="-1.62" y2="-10" layer="51"/>
<rectangle x1="-1.38" y1="-11" x2="-1.12" y2="-10" layer="51"/>
<rectangle x1="-0.88" y1="-11" x2="-0.62" y2="-10" layer="51"/>
<rectangle x1="-0.38" y1="-11" x2="-0.12" y2="-10" layer="51"/>
<rectangle x1="0.12" y1="-11" x2="0.38" y2="-10" layer="51"/>
<rectangle x1="0.62" y1="-11" x2="0.88" y2="-10" layer="51"/>
<rectangle x1="1.12" y1="-11" x2="1.38" y2="-10" layer="51"/>
<rectangle x1="1.62" y1="-11" x2="1.88" y2="-10" layer="51"/>
<rectangle x1="2.12" y1="-11" x2="2.38" y2="-10" layer="51"/>
<rectangle x1="2.62" y1="-11" x2="2.88" y2="-10" layer="51"/>
<rectangle x1="3.12" y1="-11" x2="3.38" y2="-10" layer="51"/>
<rectangle x1="3.62" y1="-11" x2="3.88" y2="-10" layer="51"/>
<rectangle x1="4.12" y1="-11" x2="4.38" y2="-10" layer="51"/>
<rectangle x1="4.62" y1="-11" x2="4.88" y2="-10" layer="51"/>
<rectangle x1="5.12" y1="-11" x2="5.38" y2="-10" layer="51"/>
<rectangle x1="5.62" y1="-11" x2="5.88" y2="-10" layer="51"/>
<rectangle x1="6.12" y1="-11" x2="6.38" y2="-10" layer="51"/>
<rectangle x1="6.62" y1="-11" x2="6.88" y2="-10" layer="51"/>
<rectangle x1="7.12" y1="-11" x2="7.38" y2="-10" layer="51"/>
<rectangle x1="7.62" y1="-11" x2="7.88" y2="-10" layer="51"/>
<rectangle x1="8.12" y1="-11" x2="8.38" y2="-10" layer="51"/>
<rectangle x1="8.62" y1="-11" x2="8.88" y2="-10" layer="51"/>
<rectangle x1="10" y1="-8.88" x2="11" y2="-8.62" layer="51"/>
<rectangle x1="10" y1="-8.38" x2="11" y2="-8.12" layer="51"/>
<rectangle x1="10" y1="-7.88" x2="11" y2="-7.62" layer="51"/>
<rectangle x1="10" y1="-7.38" x2="11" y2="-7.12" layer="51"/>
<rectangle x1="10" y1="-6.88" x2="11" y2="-6.62" layer="51"/>
<rectangle x1="10" y1="-6.38" x2="11" y2="-6.12" layer="51"/>
<rectangle x1="10" y1="-5.88" x2="11" y2="-5.62" layer="51"/>
<rectangle x1="10" y1="-5.38" x2="11" y2="-5.12" layer="51"/>
<rectangle x1="10" y1="-4.88" x2="11" y2="-4.62" layer="51"/>
<rectangle x1="10" y1="-4.38" x2="11" y2="-4.12" layer="51"/>
<rectangle x1="10" y1="-3.88" x2="11" y2="-3.62" layer="51"/>
<rectangle x1="10" y1="-3.38" x2="11" y2="-3.12" layer="51"/>
<rectangle x1="10" y1="-2.88" x2="11" y2="-2.62" layer="51"/>
<rectangle x1="10" y1="-2.38" x2="11" y2="-2.12" layer="51"/>
<rectangle x1="10" y1="-1.88" x2="11" y2="-1.62" layer="51"/>
<rectangle x1="10" y1="-1.38" x2="11" y2="-1.12" layer="51"/>
<rectangle x1="10" y1="-0.88" x2="11" y2="-0.62" layer="51"/>
<rectangle x1="10" y1="-0.38" x2="11" y2="-0.12" layer="51"/>
<rectangle x1="10" y1="0.12" x2="11" y2="0.38" layer="51"/>
<rectangle x1="10" y1="0.62" x2="11" y2="0.88" layer="51"/>
<rectangle x1="10" y1="1.12" x2="11" y2="1.38" layer="51"/>
<rectangle x1="10" y1="1.62" x2="11" y2="1.88" layer="51"/>
<rectangle x1="10" y1="2.12" x2="11" y2="2.38" layer="51"/>
<rectangle x1="10" y1="2.62" x2="11" y2="2.88" layer="51"/>
<rectangle x1="10" y1="3.12" x2="11" y2="3.38" layer="51"/>
<rectangle x1="10" y1="3.62" x2="11" y2="3.88" layer="51"/>
<rectangle x1="10" y1="4.12" x2="11" y2="4.38" layer="51"/>
<rectangle x1="10" y1="4.62" x2="11" y2="4.88" layer="51"/>
<rectangle x1="10" y1="5.12" x2="11" y2="5.38" layer="51"/>
<rectangle x1="10" y1="5.62" x2="11" y2="5.88" layer="51"/>
<rectangle x1="10" y1="6.12" x2="11" y2="6.38" layer="51"/>
<rectangle x1="10" y1="6.62" x2="11" y2="6.88" layer="51"/>
<rectangle x1="10" y1="7.12" x2="11" y2="7.38" layer="51"/>
<rectangle x1="10" y1="7.62" x2="11" y2="7.88" layer="51"/>
<rectangle x1="10" y1="8.12" x2="11" y2="8.38" layer="51"/>
<rectangle x1="10" y1="8.62" x2="11" y2="8.88" layer="51"/>
<rectangle x1="8.62" y1="10" x2="8.88" y2="11" layer="51"/>
<rectangle x1="8.12" y1="10" x2="8.38" y2="11" layer="51"/>
<rectangle x1="7.62" y1="10" x2="7.88" y2="11" layer="51"/>
<rectangle x1="7.12" y1="10" x2="7.38" y2="11" layer="51"/>
<rectangle x1="6.62" y1="10" x2="6.88" y2="11" layer="51"/>
<rectangle x1="6.12" y1="10" x2="6.38" y2="11" layer="51"/>
<rectangle x1="5.62" y1="10" x2="5.88" y2="11" layer="51"/>
<rectangle x1="5.12" y1="10" x2="5.38" y2="11" layer="51"/>
<rectangle x1="4.62" y1="10" x2="4.88" y2="11" layer="51"/>
<rectangle x1="4.12" y1="10" x2="4.38" y2="11" layer="51"/>
<rectangle x1="3.62" y1="10" x2="3.88" y2="11" layer="51"/>
<rectangle x1="3.12" y1="10" x2="3.38" y2="11" layer="51"/>
<rectangle x1="2.62" y1="10" x2="2.88" y2="11" layer="51"/>
<rectangle x1="2.12" y1="10" x2="2.38" y2="11" layer="51"/>
<rectangle x1="1.62" y1="10" x2="1.88" y2="11" layer="51"/>
<rectangle x1="1.12" y1="10" x2="1.38" y2="11" layer="51"/>
<rectangle x1="0.62" y1="10" x2="0.88" y2="11" layer="51"/>
<rectangle x1="0.12" y1="10" x2="0.38" y2="11" layer="51"/>
<rectangle x1="-0.38" y1="10" x2="-0.12" y2="11" layer="51"/>
<rectangle x1="-0.88" y1="10" x2="-0.62" y2="11" layer="51"/>
<rectangle x1="-1.38" y1="10" x2="-1.12" y2="11" layer="51"/>
<rectangle x1="-1.88" y1="10" x2="-1.62" y2="11" layer="51"/>
<rectangle x1="-2.38" y1="10" x2="-2.12" y2="11" layer="51"/>
<rectangle x1="-2.88" y1="10" x2="-2.62" y2="11" layer="51"/>
<rectangle x1="-3.38" y1="10" x2="-3.12" y2="11" layer="51"/>
<rectangle x1="-3.88" y1="10" x2="-3.62" y2="11" layer="51"/>
<rectangle x1="-4.38" y1="10" x2="-4.12" y2="11" layer="51"/>
<rectangle x1="-4.88" y1="10" x2="-4.62" y2="11" layer="51"/>
<rectangle x1="-5.38" y1="10" x2="-5.12" y2="11" layer="51"/>
<rectangle x1="-5.88" y1="10" x2="-5.62" y2="11" layer="51"/>
<rectangle x1="-6.38" y1="10" x2="-6.12" y2="11" layer="51"/>
<rectangle x1="-6.88" y1="10" x2="-6.62" y2="11" layer="51"/>
<rectangle x1="-7.38" y1="10" x2="-7.12" y2="11" layer="51"/>
<rectangle x1="-7.88" y1="10" x2="-7.62" y2="11" layer="51"/>
<rectangle x1="-8.38" y1="10" x2="-8.12" y2="11" layer="51"/>
<rectangle x1="-8.88" y1="10" x2="-8.62" y2="11" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="6SLX9TQG144_0">
<wire x1="-10.16" y1="38.1" x2="0" y2="38.1" width="0.254" layer="94"/>
<wire x1="0" y1="38.1" x2="0" y2="-38.1" width="0.254" layer="94"/>
<wire x1="0" y1="-38.1" x2="-10.16" y2="-38.1" width="0.254" layer="94"/>
<text x="0" y="-40.64" size="1.778" layer="95" rot="MR0">&gt;NAME</text>
<text x="0" y="-43.18" size="1.778" layer="96" rot="MR0">&gt;VALUE</text>
<pin name="VCCO_0@0" x="5.08" y="35.56" length="middle" direction="in" rot="R180"/>
<pin name="VCCO_0@1" x="5.08" y="33.02" length="middle" direction="in" rot="R180"/>
<pin name="VCCO_0@2" x="5.08" y="30.48" length="middle" direction="in" rot="R180"/>
<pin name="IO_L1N_VREF_0" x="5.08" y="27.94" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L1P_HSWAPEN_0" x="5.08" y="25.4" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L2N_0" x="5.08" y="22.86" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L2P_0" x="5.08" y="20.32" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L3N_0" x="5.08" y="17.78" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L3P_0" x="5.08" y="15.24" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L4N_0" x="5.08" y="12.7" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L4P_0" x="5.08" y="10.16" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L34N_GCLK18_0" x="5.08" y="7.62" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L34P_GCLK19_0" x="5.08" y="5.08" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L35N_GCLK16_0" x="5.08" y="2.54" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L35P_GCLK17_0" x="5.08" y="0" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L36N_GCLK14_0" x="5.08" y="-2.54" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L36P_GCLK15_0" x="5.08" y="-5.08" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L37N_GCLK12_0" x="5.08" y="-7.62" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L37P_GCLK13_0" x="5.08" y="-10.16" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L62N_VREF_0" x="5.08" y="-12.7" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L62P_0" x="5.08" y="-15.24" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L63N_SCP6_0" x="5.08" y="-17.78" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L63P_SCP7_0" x="5.08" y="-20.32" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L64N_SCP4_0" x="5.08" y="-22.86" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L64P_SCP5_0" x="5.08" y="-25.4" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L65N_SCP2_0" x="5.08" y="-27.94" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L65P_SCP3_0" x="5.08" y="-30.48" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L66N_SCP0_0" x="5.08" y="-33.02" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L66P_SCP1_0" x="5.08" y="-35.56" length="middle" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="6SLX9TQG144_1">
<wire x1="-10.16" y1="35.56" x2="0" y2="35.56" width="0.254" layer="94"/>
<wire x1="0" y1="35.56" x2="0" y2="-35.56" width="0.254" layer="94"/>
<wire x1="0" y1="-35.56" x2="-10.16" y2="-35.56" width="0.254" layer="94"/>
<text x="0" y="-38.1" size="1.778" layer="95" rot="MR0">&gt;NAME</text>
<text x="0" y="-40.64" size="1.778" layer="96" rot="MR0">&gt;VALUE</text>
<pin name="VCCO_1@0" x="5.08" y="33.02" length="middle" direction="in" rot="R180"/>
<pin name="VCCO_1@1" x="5.08" y="30.48" length="middle" direction="in" rot="R180"/>
<pin name="VCCO_1@2" x="5.08" y="27.94" length="middle" direction="in" rot="R180"/>
<pin name="IO_L1N_VREF_1" x="5.08" y="25.4" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L1P_1" x="5.08" y="22.86" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L32N_1" x="5.08" y="20.32" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L32P_1" x="5.08" y="17.78" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L33N_1" x="5.08" y="15.24" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L33P_1" x="5.08" y="12.7" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L34N_1" x="5.08" y="10.16" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L34P_1" x="5.08" y="7.62" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L40N_GCLK10_1" x="5.08" y="5.08" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L40P_GCLK11_1" x="5.08" y="2.54" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L41N_GCLK8_1" x="5.08" y="0" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L41P_GCLK9_IRDY1_1" x="5.08" y="-2.54" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L42N_GCLK6_TRDY1_1" x="5.08" y="-5.08" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L42P_GCLK7_1" x="5.08" y="-7.62" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L43N_GCLK4_1" x="5.08" y="-10.16" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L43P_GCLK5_1" x="5.08" y="-12.7" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L45N_1" x="5.08" y="-15.24" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L45P_1" x="5.08" y="-17.78" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L46N_1" x="5.08" y="-20.32" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L46P_1" x="5.08" y="-22.86" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L47N_1" x="5.08" y="-25.4" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L47P_1" x="5.08" y="-27.94" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L74N_DOUT_BUSY_1" x="5.08" y="-30.48" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L74P_AWAKE_1" x="5.08" y="-33.02" length="middle" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="6SLX9TQG144_2">
<wire x1="-10.16" y1="40.64" x2="0" y2="40.64" width="0.254" layer="94"/>
<wire x1="0" y1="40.64" x2="0" y2="-40.64" width="0.254" layer="94"/>
<wire x1="0" y1="-40.64" x2="-10.16" y2="-40.64" width="0.254" layer="94"/>
<text x="0" y="-43.18" size="1.778" layer="95" rot="MR0">&gt;NAME</text>
<text x="0" y="-45.72" size="1.778" layer="96" rot="MR0">&gt;VALUE</text>
<pin name="VCCO_2@0" x="5.08" y="38.1" length="middle" direction="in" rot="R180"/>
<pin name="VCCO_2@1" x="5.08" y="35.56" length="middle" direction="in" rot="R180"/>
<pin name="CMPCS_B_2" x="5.08" y="33.02" length="middle" rot="R180"/>
<pin name="DONE_2" x="5.08" y="30.48" length="middle" rot="R180"/>
<pin name="IO_L1N_M0_CMPMISO_2" x="5.08" y="27.94" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L1P_CCLK_2" x="5.08" y="25.4" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L2N_CMPMOSI_2" x="5.08" y="22.86" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L2P_CMPCLK_2" x="5.08" y="20.32" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L3N_MOSI_CSI_B_MISO0_2" x="5.08" y="17.78" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L3P_D0_DIN_MISO_MISO1_2" x="5.08" y="15.24" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L12N_D2_MISO3_2" x="5.08" y="12.7" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L12P_D1_MISO2_2" x="5.08" y="10.16" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L13N_D10_2" x="5.08" y="7.62" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L13P_M1_2" x="5.08" y="5.08" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L14N_D12_2" x="5.08" y="2.54" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L14P_D11_2" x="5.08" y="0" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L30N_GCLK0_USERCCLK_2" x="5.08" y="-2.54" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L30P_GCLK1_D13_2" x="5.08" y="-5.08" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L31N_GCLK30_D15_2" x="5.08" y="-7.62" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L31P_GCLK31_D14_2" x="5.08" y="-10.16" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L48N_RDWR_B_VREF_2" x="5.08" y="-12.7" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L48P_D7_2" x="5.08" y="-15.24" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L49N_D4_2" x="5.08" y="-17.78" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L49P_D3_2" x="5.08" y="-20.32" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L62N_D6_2" x="5.08" y="-22.86" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L62P_D5_2" x="5.08" y="-25.4" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L64N_D9_2" x="5.08" y="-27.94" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L64P_D8_2" x="5.08" y="-30.48" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L65N_CSO_B_2" x="5.08" y="-33.02" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L65P_INIT_B_2" x="5.08" y="-35.56" length="middle" swaplevel="1" rot="R180"/>
<pin name="PROGRAM_B_2" x="5.08" y="-38.1" length="middle" direction="in" rot="R180"/>
</symbol>
<symbol name="6SLX9TQG144_3">
<wire x1="-10.16" y1="38.1" x2="0" y2="38.1" width="0.254" layer="94"/>
<wire x1="0" y1="38.1" x2="0" y2="-38.1" width="0.254" layer="94"/>
<wire x1="0" y1="-38.1" x2="-10.16" y2="-38.1" width="0.254" layer="94"/>
<text x="0" y="-40.64" size="1.778" layer="95" rot="MR0">&gt;NAME</text>
<text x="0" y="-43.18" size="1.778" layer="96" rot="MR0">&gt;VALUE</text>
<pin name="VCCO_3@0" x="5.08" y="35.56" length="middle" direction="in" rot="R180"/>
<pin name="VCCO_3@1" x="5.08" y="33.02" length="middle" direction="in" rot="R180"/>
<pin name="VCCO_3@2" x="5.08" y="30.48" length="middle" direction="in" rot="R180"/>
<pin name="IO_L1N_VREF_3" x="5.08" y="27.94" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L1P_3" x="5.08" y="25.4" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L2N_3" x="5.08" y="22.86" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L2P_3" x="5.08" y="20.32" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L36N_3" x="5.08" y="17.78" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L36P_3" x="5.08" y="15.24" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L37N_3" x="5.08" y="12.7" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L37P_3" x="5.08" y="10.16" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L41N_GCLK26_3" x="5.08" y="7.62" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L41P_GCLK27_3" x="5.08" y="5.08" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L42N_GCLK24_3" x="5.08" y="2.54" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L42P_GCLK25_TRDY2_3" x="5.08" y="0" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L43N_GCLK22_IRDY2_3" x="5.08" y="-2.54" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L43P_GCLK23_3" x="5.08" y="-5.08" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L44N_GCLK20_3" x="5.08" y="-7.62" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L44P_GCLK21_3" x="5.08" y="-10.16" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L49N_3" x="5.08" y="-12.7" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L49P_3" x="5.08" y="-15.24" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L50N_3" x="5.08" y="-17.78" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L50P_3" x="5.08" y="-20.32" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L51N_3" x="5.08" y="-22.86" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L51P_3" x="5.08" y="-25.4" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L52N_3" x="5.08" y="-27.94" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L52P_3" x="5.08" y="-30.48" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L83N_VREF_3" x="5.08" y="-33.02" length="middle" swaplevel="1" rot="R180"/>
<pin name="IO_L83P_3" x="5.08" y="-35.56" length="middle" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="6SLX9TQG144_GND">
<wire x1="-10.16" y1="17.78" x2="0" y2="17.78" width="0.254" layer="94"/>
<wire x1="0" y1="17.78" x2="0" y2="-17.78" width="0.254" layer="94"/>
<wire x1="0" y1="-17.78" x2="-10.16" y2="-17.78" width="0.254" layer="94"/>
<text x="0" y="-20.32" size="1.778" layer="95" rot="MR0">&gt;NAME</text>
<text x="0" y="-22.86" size="1.778" layer="96" rot="MR0">&gt;VALUE</text>
<pin name="GND@0" x="5.08" y="15.24" length="middle" direction="pwr" rot="R180"/>
<pin name="GND@1" x="5.08" y="12.7" length="middle" direction="pwr" rot="R180"/>
<pin name="GND@2" x="5.08" y="10.16" length="middle" direction="pwr" rot="R180"/>
<pin name="GND@3" x="5.08" y="7.62" length="middle" direction="pwr" rot="R180"/>
<pin name="GND@4" x="5.08" y="5.08" length="middle" direction="pwr" rot="R180"/>
<pin name="GND@5" x="5.08" y="2.54" length="middle" direction="pwr" rot="R180"/>
<pin name="GND@6" x="5.08" y="0" length="middle" direction="pwr" rot="R180"/>
<pin name="GND@7" x="5.08" y="-2.54" length="middle" direction="pwr" rot="R180"/>
<pin name="GND@8" x="5.08" y="-5.08" length="middle" direction="pwr" rot="R180"/>
<pin name="GND@9" x="5.08" y="-7.62" length="middle" direction="pwr" rot="R180"/>
<pin name="GND@10" x="5.08" y="-10.16" length="middle" direction="pwr" rot="R180"/>
<pin name="GND@11" x="5.08" y="-12.7" length="middle" direction="pwr" rot="R180"/>
<pin name="GND@12" x="5.08" y="-15.24" length="middle" direction="pwr" rot="R180"/>
</symbol>
<symbol name="6SLX9TQG144_NA">
<wire x1="-10.16" y1="7.62" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0" y1="-7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<text x="0" y="-10.16" size="1.778" layer="95" rot="MR0">&gt;NAME</text>
<text x="0" y="-12.7" size="1.778" layer="96" rot="MR0">&gt;VALUE</text>
<pin name="SUSPEND" x="5.08" y="5.08" length="middle" direction="in" rot="R180"/>
<pin name="TCK" x="5.08" y="2.54" length="middle" direction="in" rot="R180"/>
<pin name="TDI" x="5.08" y="0" length="middle" direction="in" rot="R180"/>
<pin name="TDO" x="5.08" y="-2.54" length="middle" direction="out" rot="R180"/>
<pin name="TMS" x="5.08" y="-5.08" length="middle" direction="in" rot="R180"/>
</symbol>
<symbol name="6SLX9TQG144_VCCAUX">
<wire x1="-10.16" y1="7.62" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0" y1="-7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<text x="0" y="-10.16" size="1.778" layer="95" rot="MR0">&gt;NAME</text>
<text x="0" y="-12.7" size="1.778" layer="96" rot="MR0">&gt;VALUE</text>
<pin name="VCCAUX@0" x="5.08" y="5.08" length="middle" direction="in" rot="R180"/>
<pin name="VCCAUX@1" x="5.08" y="2.54" length="middle" direction="in" rot="R180"/>
<pin name="VCCAUX@2" x="5.08" y="0" length="middle" direction="in" rot="R180"/>
<pin name="VCCAUX@3" x="5.08" y="-2.54" length="middle" direction="in" rot="R180"/>
<pin name="VCCAUX@4" x="5.08" y="-5.08" length="middle" direction="in" rot="R180"/>
</symbol>
<symbol name="6SLX9TQG144_VCCINT">
<wire x1="-10.16" y1="7.62" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0" y1="-7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<text x="0" y="-10.16" size="1.778" layer="95" rot="MR0">&gt;NAME</text>
<text x="0" y="-12.7" size="1.778" layer="96" rot="MR0">&gt;VALUE</text>
<pin name="VCCINT@0" x="5.08" y="5.08" length="middle" direction="in" rot="R180"/>
<pin name="VCCINT@1" x="5.08" y="2.54" length="middle" direction="in" rot="R180"/>
<pin name="VCCINT@2" x="5.08" y="0" length="middle" direction="in" rot="R180"/>
<pin name="VCCINT@3" x="5.08" y="-2.54" length="middle" direction="in" rot="R180"/>
<pin name="VCCINT@4" x="5.08" y="-5.08" length="middle" direction="in" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="6SLX9TQG144" prefix="U">
<description>Xilinx FPGA: 6SLX9TQG144</description>
<gates>
<gate name="B0" symbol="6SLX9TQG144_0" x="0" y="0"/>
<gate name="B1" symbol="6SLX9TQG144_1" x="45.72" y="0"/>
<gate name="B2" symbol="6SLX9TQG144_2" x="91.44" y="0"/>
<gate name="B3" symbol="6SLX9TQG144_3" x="137.16" y="0"/>
<gate name="BGND" symbol="6SLX9TQG144_GND" x="182.88" y="0"/>
<gate name="BNA" symbol="6SLX9TQG144_NA" x="228.6" y="0"/>
<gate name="BVCCAUX" symbol="6SLX9TQG144_VCCAUX" x="274.32" y="0"/>
<gate name="BVCCINT" symbol="6SLX9TQG144_VCCINT" x="320.04" y="0"/>
</gates>
<devices>
<device name="" package="TQ144">
<connects>
<connect gate="B0" pin="IO_L1N_VREF_0" pad="143"/>
<connect gate="B0" pin="IO_L1P_HSWAPEN_0" pad="144"/>
<connect gate="B0" pin="IO_L2N_0" pad="141"/>
<connect gate="B0" pin="IO_L2P_0" pad="142"/>
<connect gate="B0" pin="IO_L34N_GCLK18_0" pad="133"/>
<connect gate="B0" pin="IO_L34P_GCLK19_0" pad="134"/>
<connect gate="B0" pin="IO_L35N_GCLK16_0" pad="131"/>
<connect gate="B0" pin="IO_L35P_GCLK17_0" pad="132"/>
<connect gate="B0" pin="IO_L36N_GCLK14_0" pad="126"/>
<connect gate="B0" pin="IO_L36P_GCLK15_0" pad="127"/>
<connect gate="B0" pin="IO_L37N_GCLK12_0" pad="123"/>
<connect gate="B0" pin="IO_L37P_GCLK13_0" pad="124"/>
<connect gate="B0" pin="IO_L3N_0" pad="139"/>
<connect gate="B0" pin="IO_L3P_0" pad="140"/>
<connect gate="B0" pin="IO_L4N_0" pad="137"/>
<connect gate="B0" pin="IO_L4P_0" pad="138"/>
<connect gate="B0" pin="IO_L62N_VREF_0" pad="120"/>
<connect gate="B0" pin="IO_L62P_0" pad="121"/>
<connect gate="B0" pin="IO_L63N_SCP6_0" pad="118"/>
<connect gate="B0" pin="IO_L63P_SCP7_0" pad="119"/>
<connect gate="B0" pin="IO_L64N_SCP4_0" pad="116"/>
<connect gate="B0" pin="IO_L64P_SCP5_0" pad="117"/>
<connect gate="B0" pin="IO_L65N_SCP2_0" pad="114"/>
<connect gate="B0" pin="IO_L65P_SCP3_0" pad="115"/>
<connect gate="B0" pin="IO_L66N_SCP0_0" pad="111"/>
<connect gate="B0" pin="IO_L66P_SCP1_0" pad="112"/>
<connect gate="B0" pin="VCCO_0@0" pad="122"/>
<connect gate="B0" pin="VCCO_0@1" pad="125"/>
<connect gate="B0" pin="VCCO_0@2" pad="135"/>
<connect gate="B1" pin="IO_L1N_VREF_1" pad="104"/>
<connect gate="B1" pin="IO_L1P_1" pad="105"/>
<connect gate="B1" pin="IO_L32N_1" pad="101"/>
<connect gate="B1" pin="IO_L32P_1" pad="102"/>
<connect gate="B1" pin="IO_L33N_1" pad="99"/>
<connect gate="B1" pin="IO_L33P_1" pad="100"/>
<connect gate="B1" pin="IO_L34N_1" pad="97"/>
<connect gate="B1" pin="IO_L34P_1" pad="98"/>
<connect gate="B1" pin="IO_L40N_GCLK10_1" pad="94"/>
<connect gate="B1" pin="IO_L40P_GCLK11_1" pad="95"/>
<connect gate="B1" pin="IO_L41N_GCLK8_1" pad="92"/>
<connect gate="B1" pin="IO_L41P_GCLK9_IRDY1_1" pad="93"/>
<connect gate="B1" pin="IO_L42N_GCLK6_TRDY1_1" pad="87"/>
<connect gate="B1" pin="IO_L42P_GCLK7_1" pad="88"/>
<connect gate="B1" pin="IO_L43N_GCLK4_1" pad="84"/>
<connect gate="B1" pin="IO_L43P_GCLK5_1" pad="85"/>
<connect gate="B1" pin="IO_L45N_1" pad="82"/>
<connect gate="B1" pin="IO_L45P_1" pad="83"/>
<connect gate="B1" pin="IO_L46N_1" pad="80"/>
<connect gate="B1" pin="IO_L46P_1" pad="81"/>
<connect gate="B1" pin="IO_L47N_1" pad="78"/>
<connect gate="B1" pin="IO_L47P_1" pad="79"/>
<connect gate="B1" pin="IO_L74N_DOUT_BUSY_1" pad="74"/>
<connect gate="B1" pin="IO_L74P_AWAKE_1" pad="75"/>
<connect gate="B1" pin="VCCO_1@0" pad="103"/>
<connect gate="B1" pin="VCCO_1@1" pad="76"/>
<connect gate="B1" pin="VCCO_1@2" pad="86"/>
<connect gate="B2" pin="CMPCS_B_2" pad="72"/>
<connect gate="B2" pin="DONE_2" pad="71"/>
<connect gate="B2" pin="IO_L12N_D2_MISO3_2" pad="61"/>
<connect gate="B2" pin="IO_L12P_D1_MISO2_2" pad="62"/>
<connect gate="B2" pin="IO_L13N_D10_2" pad="59"/>
<connect gate="B2" pin="IO_L13P_M1_2" pad="60"/>
<connect gate="B2" pin="IO_L14N_D12_2" pad="57"/>
<connect gate="B2" pin="IO_L14P_D11_2" pad="58"/>
<connect gate="B2" pin="IO_L1N_M0_CMPMISO_2" pad="69"/>
<connect gate="B2" pin="IO_L1P_CCLK_2" pad="70"/>
<connect gate="B2" pin="IO_L2N_CMPMOSI_2" pad="66"/>
<connect gate="B2" pin="IO_L2P_CMPCLK_2" pad="67"/>
<connect gate="B2" pin="IO_L30N_GCLK0_USERCCLK_2" pad="55"/>
<connect gate="B2" pin="IO_L30P_GCLK1_D13_2" pad="56"/>
<connect gate="B2" pin="IO_L31N_GCLK30_D15_2" pad="50"/>
<connect gate="B2" pin="IO_L31P_GCLK31_D14_2" pad="51"/>
<connect gate="B2" pin="IO_L3N_MOSI_CSI_B_MISO0_2" pad="64"/>
<connect gate="B2" pin="IO_L3P_D0_DIN_MISO_MISO1_2" pad="65"/>
<connect gate="B2" pin="IO_L48N_RDWR_B_VREF_2" pad="47"/>
<connect gate="B2" pin="IO_L48P_D7_2" pad="48"/>
<connect gate="B2" pin="IO_L49N_D4_2" pad="45"/>
<connect gate="B2" pin="IO_L49P_D3_2" pad="46"/>
<connect gate="B2" pin="IO_L62N_D6_2" pad="43"/>
<connect gate="B2" pin="IO_L62P_D5_2" pad="44"/>
<connect gate="B2" pin="IO_L64N_D9_2" pad="40"/>
<connect gate="B2" pin="IO_L64P_D8_2" pad="41"/>
<connect gate="B2" pin="IO_L65N_CSO_B_2" pad="38"/>
<connect gate="B2" pin="IO_L65P_INIT_B_2" pad="39"/>
<connect gate="B2" pin="PROGRAM_B_2" pad="37"/>
<connect gate="B2" pin="VCCO_2@0" pad="42"/>
<connect gate="B2" pin="VCCO_2@1" pad="63"/>
<connect gate="B3" pin="IO_L1N_VREF_3" pad="34"/>
<connect gate="B3" pin="IO_L1P_3" pad="35"/>
<connect gate="B3" pin="IO_L2N_3" pad="32"/>
<connect gate="B3" pin="IO_L2P_3" pad="33"/>
<connect gate="B3" pin="IO_L36N_3" pad="29"/>
<connect gate="B3" pin="IO_L36P_3" pad="30"/>
<connect gate="B3" pin="IO_L37N_3" pad="26"/>
<connect gate="B3" pin="IO_L37P_3" pad="27"/>
<connect gate="B3" pin="IO_L41N_GCLK26_3" pad="23"/>
<connect gate="B3" pin="IO_L41P_GCLK27_3" pad="24"/>
<connect gate="B3" pin="IO_L42N_GCLK24_3" pad="21"/>
<connect gate="B3" pin="IO_L42P_GCLK25_TRDY2_3" pad="22"/>
<connect gate="B3" pin="IO_L43N_GCLK22_IRDY2_3" pad="16"/>
<connect gate="B3" pin="IO_L43P_GCLK23_3" pad="17"/>
<connect gate="B3" pin="IO_L44N_GCLK20_3" pad="14"/>
<connect gate="B3" pin="IO_L44P_GCLK21_3" pad="15"/>
<connect gate="B3" pin="IO_L49N_3" pad="11"/>
<connect gate="B3" pin="IO_L49P_3" pad="12"/>
<connect gate="B3" pin="IO_L50N_3" pad="9"/>
<connect gate="B3" pin="IO_L50P_3" pad="10"/>
<connect gate="B3" pin="IO_L51N_3" pad="7"/>
<connect gate="B3" pin="IO_L51P_3" pad="8"/>
<connect gate="B3" pin="IO_L52N_3" pad="5"/>
<connect gate="B3" pin="IO_L52P_3" pad="6"/>
<connect gate="B3" pin="IO_L83N_VREF_3" pad="1"/>
<connect gate="B3" pin="IO_L83P_3" pad="2"/>
<connect gate="B3" pin="VCCO_3@0" pad="18"/>
<connect gate="B3" pin="VCCO_3@1" pad="31"/>
<connect gate="B3" pin="VCCO_3@2" pad="4"/>
<connect gate="BGND" pin="GND@0" pad="108"/>
<connect gate="BGND" pin="GND@1" pad="113"/>
<connect gate="BGND" pin="GND@10" pad="77"/>
<connect gate="BGND" pin="GND@11" pad="91"/>
<connect gate="BGND" pin="GND@12" pad="96"/>
<connect gate="BGND" pin="GND@2" pad="13"/>
<connect gate="BGND" pin="GND@3" pad="130"/>
<connect gate="BGND" pin="GND@4" pad="136"/>
<connect gate="BGND" pin="GND@5" pad="25"/>
<connect gate="BGND" pin="GND@6" pad="3"/>
<connect gate="BGND" pin="GND@7" pad="49"/>
<connect gate="BGND" pin="GND@8" pad="54"/>
<connect gate="BGND" pin="GND@9" pad="68"/>
<connect gate="BNA" pin="SUSPEND" pad="73"/>
<connect gate="BNA" pin="TCK" pad="109"/>
<connect gate="BNA" pin="TDI" pad="110"/>
<connect gate="BNA" pin="TDO" pad="106"/>
<connect gate="BNA" pin="TMS" pad="107"/>
<connect gate="BVCCAUX" pin="VCCAUX@0" pad="129"/>
<connect gate="BVCCAUX" pin="VCCAUX@1" pad="20"/>
<connect gate="BVCCAUX" pin="VCCAUX@2" pad="36"/>
<connect gate="BVCCAUX" pin="VCCAUX@3" pad="53"/>
<connect gate="BVCCAUX" pin="VCCAUX@4" pad="90"/>
<connect gate="BVCCINT" pin="VCCINT@0" pad="128"/>
<connect gate="BVCCINT" pin="VCCINT@1" pad="19"/>
<connect gate="BVCCINT" pin="VCCINT@2" pad="28"/>
<connect gate="BVCCINT" pin="VCCINT@3" pad="52"/>
<connect gate="BVCCINT" pin="VCCINT@4" pad="89"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="GadgetFactory_all">
<packages>
</packages>
<symbols>
<symbol name="3V3">
<wire x1="0" y1="1.905" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.778" y="3.175" size="1.397" layer="96">&gt;VALUE</text>
<pin name="3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.286" y="-2.032" size="1.397" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="3V3" prefix="3V3_PWR">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<gates>
<gate name="A" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="GadgetFactory_Pub - Copy">
<packages>
<package name="STAND-OFF">
<description>&lt;b&gt;Stand Off&lt;/b&gt;&lt;p&gt;
This is the mechanical footprint for a #4 phillips button head screw. Use the keepout ring to avoid running the screw head into surrounding components. SKU : PRT-00447</description>
<wire x1="0" y1="1.8542" x2="0" y2="-1.8542" width="0.2032" layer="41" curve="-180"/>
<wire x1="0" y1="-1.8542" x2="0" y2="1.8542" width="0.2032" layer="41" curve="-180"/>
<wire x1="0" y1="-1.8542" x2="0" y2="1.8542" width="0.2032" layer="42" curve="180"/>
<wire x1="0" y1="-1.8542" x2="0" y2="1.8542" width="0.2032" layer="42" curve="-180"/>
<circle x="0" y="0" radius="2.794" width="0.127" layer="39"/>
<hole x="0" y="0" drill="3.302"/>
</package>
<package name="STAND-OFF-TIGHT">
<description>&lt;b&gt;Stand Off&lt;/b&gt;&lt;p&gt;
This is the mechanical footprint for a #4 phillips button head screw. Use the keepout ring to avoid running the screw head into surrounding components. SKU : PRT-00447</description>
<wire x1="0" y1="1.8542" x2="0" y2="-1.8542" width="0.2032" layer="41" curve="-180"/>
<wire x1="0" y1="-1.8542" x2="0" y2="1.8542" width="0.2032" layer="41" curve="-180"/>
<wire x1="0" y1="-1.8542" x2="0" y2="1.8542" width="0.2032" layer="42" curve="180"/>
<wire x1="0" y1="-1.8542" x2="0" y2="1.8542" width="0.2032" layer="42" curve="-180"/>
<circle x="0" y="0" radius="2.794" width="0.127" layer="39"/>
<hole x="0" y="0" drill="3.048"/>
</package>
</packages>
<symbols>
<symbol name="STAND-OFF">
<circle x="0" y="0" radius="1.27" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="STAND-OFF" prefix="JP">
<description>&lt;b&gt;Stand Off&lt;/b&gt;&lt;p&gt;
This is the mechanical footprint for a #4 phillips button head screw. Use the keepout ring to avoid running the screw head into surrounding components. SKU : PRT-00447</description>
<gates>
<gate name="G$1" symbol="STAND-OFF" x="0" y="0"/>
</gates>
<devices>
<device name="" package="STAND-OFF">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TIGHT" package="STAND-OFF-TIGHT">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="adafruit">
<packages>
<package name="EVQ-Q2">
<wire x1="-3.3" y1="3" x2="3.3" y2="3" width="0.127" layer="21"/>
<wire x1="3.3" y1="3" x2="3.3" y2="-3" width="0.127" layer="21"/>
<wire x1="3.3" y1="-3" x2="-3.3" y2="-3" width="0.127" layer="21"/>
<wire x1="-3.3" y1="-3" x2="-3.3" y2="3" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.5033" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1" width="0.127" layer="21"/>
<smd name="B" x="-3.4" y="2" dx="3.2" dy="1.2" layer="1"/>
<smd name="B'" x="3.4" y="2" dx="3.2" dy="1.2" layer="1"/>
<smd name="A'" x="3.4" y="-2" dx="3.2" dy="1.2" layer="1"/>
<smd name="A" x="-3.4" y="-2" dx="3.2" dy="1.2" layer="1"/>
<text x="-3" y="3.5" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-4.8" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="TS2">
<wire x1="0" y1="1.905" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-3.175" y2="1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.905" x2="-3.175" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-4.445" y2="0" width="0.254" layer="94"/>
<wire x1="-4.445" y1="0" x2="-4.445" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="0" x2="-3.175" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<circle x="0" y="-2.54" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="2.54" radius="0.127" width="0.4064" layer="94"/>
<text x="-6.35" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="3.175" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="P" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="S1" x="2.54" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="P1" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="EVQQ2">
<description>SMT 6mm switch, EVQQ2 series
&lt;p&gt;http://www.ladyada.net/library/eagle&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="TS2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="EVQ-Q2">
<connects>
<connect gate="G$1" pin="P" pad="A"/>
<connect gate="G$1" pin="P1" pad="A'"/>
<connect gate="G$1" pin="S" pad="B"/>
<connect gate="G$1" pin="S1" pad="B'"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Aesthetics">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find non-functional items- supply symbols, logos, notations, frame blocks, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="CREATIVE_COMMONS">
<text x="-20.32" y="5.08" size="1.778" layer="51">Released under the Creative Commons Attribution Share-Alike 4.0 License</text>
<text x="0" y="2.54" size="1.778" layer="51"> https://creativecommons.org/licenses/by-sa/4.0/</text>
<text x="11.43" y="0" size="1.778" layer="51">Designed by:</text>
</package>
</packages>
<symbols>
<symbol name="FR-A4L">
<rectangle x1="178.7652" y1="0" x2="179.3748" y2="20.32" layer="94"/>
<rectangle x1="225.7552" y1="-26.67" x2="226.3648" y2="67.31" layer="94" rot="R90"/>
<wire x1="225.29" y1="-0.1" x2="225.29" y2="5.08" width="0.1016" layer="94"/>
<wire x1="225.29" y1="5.08" x2="273.05" y2="5.08" width="0.1016" layer="94"/>
<wire x1="225.29" y1="5.08" x2="179.07" y2="5.08" width="0.1016" layer="94"/>
<wire x1="179.07" y1="10.16" x2="225.29" y2="10.16" width="0.1016" layer="94"/>
<wire x1="225.29" y1="10.16" x2="273.05" y2="10.16" width="0.1016" layer="94"/>
<wire x1="179.07" y1="15.24" x2="273.05" y2="15.24" width="0.1016" layer="94"/>
<wire x1="225.29" y1="5.08" x2="225.29" y2="10.16" width="0.1016" layer="94"/>
<wire x1="179.07" y1="19.05" x2="179.07" y2="20.32" width="0.6096" layer="94"/>
<wire x1="179.07" y1="20.32" x2="180.34" y2="20.32" width="0.6096" layer="94"/>
<text x="181.61" y="11.43" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="181.61" y="6.35" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="195.58" y="1.27" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="181.61" y="1.27" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="181.61" y="16.51" size="2.54" layer="94" font="vector">&gt;CNAME</text>
<text x="226.16" y="1.27" size="2.54" layer="94" font="vector">Rev:</text>
<text x="226.26" y="6.35" size="2.54" layer="94" font="vector">&gt;DESIGNER</text>
<text x="234.92" y="1.17" size="2.54" layer="94" font="vector">&gt;CREVISION</text>
<frame x1="-3.81" y1="-3.81" x2="276.86" y2="182.88" columns="8" rows="5" layer="94"/>
</symbol>
<symbol name="LETTER_L">
<wire x1="0" y1="185.42" x2="248.92" y2="185.42" width="0.4064" layer="94"/>
<wire x1="248.92" y1="185.42" x2="248.92" y2="0" width="0.4064" layer="94"/>
<wire x1="0" y1="185.42" x2="0" y2="0" width="0.4064" layer="94"/>
<wire x1="0" y1="0" x2="248.92" y2="0" width="0.4064" layer="94"/>
</symbol>
<symbol name="DOCFIELD">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.254" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.254" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.254" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.254" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.254" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.254" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.254" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.254" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.254" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.254" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.254" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.254" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.254" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94" font="vector">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94" font="vector">REV:</text>
<text x="1.524" y="17.78" size="2.54" layer="94" font="vector">TITLE:</text>
<text x="15.494" y="17.78" size="2.7432" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="2.54" y="31.75" size="1.9304" layer="94">Released under the Creative Commons</text>
<text x="2.54" y="27.94" size="1.9304" layer="94">Attribution Share-Alike 4.0 License</text>
<text x="2.54" y="24.13" size="1.9304" layer="94"> https://creativecommons.org/licenses/by-sa/4.0/</text>
<text x="1.27" y="11.43" size="2.54" layer="94">Design by:</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="FRAME-A4L" prefix="FRAME">
<description>&lt;b&gt;Schematic Frame-European Format&lt;/b&gt;
&lt;br&gt;&lt;br&gt;
Standard A4 size frame in Landscape</description>
<gates>
<gate name="G$1" symbol="FR-A4L" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FRAME-LETTER" prefix="FRAME">
<description>&lt;b&gt;Schematic Frame&lt;/b&gt;&lt;p&gt;
Standard 8.5x11 US Letter frame</description>
<gates>
<gate name="G$1" symbol="LETTER_L" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD" x="147.32" y="0" addlevel="must"/>
</gates>
<devices>
<device name="" package="CREATIVE_COMMONS">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pipistrello_v2">
<description>Generated from &lt;b&gt;pipistrello_v2.sch&lt;/b&gt;&lt;p&gt;
by exp-project-lbr.ulp</description>
<packages>
<package name="SAANLIMA_FCI-HDMI-TYPE-A-RECEPT">
<wire x1="7.5" y1="7.75" x2="7.5" y2="-8.25" width="0.127" layer="51"/>
<wire x1="0" y1="-7.75" x2="0" y2="7.25" width="0.127" layer="51"/>
<wire x1="0" y1="7.25" x2="8.5" y2="7.25" width="0.127" layer="51"/>
<wire x1="8.5" y1="7.25" x2="8.5" y2="-7.75" width="0.127" layer="51"/>
<wire x1="8.5" y1="-7.75" x2="0" y2="-7.75" width="0.127" layer="51"/>
<smd name="1" x="-0.95" y="4.5" dx="1.9" dy="0.3" layer="1"/>
<smd name="2" x="-0.95" y="4" dx="1.9" dy="0.3" layer="1"/>
<smd name="3" x="-0.95" y="3.5" dx="1.9" dy="0.3" layer="1"/>
<smd name="4" x="-0.95" y="3" dx="1.9" dy="0.3" layer="1"/>
<smd name="5" x="-0.95" y="2.5" dx="1.9" dy="0.3" layer="1"/>
<smd name="6" x="-0.95" y="2" dx="1.9" dy="0.3" layer="1"/>
<smd name="7" x="-0.95" y="1.5" dx="1.9" dy="0.3" layer="1"/>
<smd name="8" x="-0.95" y="1" dx="1.9" dy="0.3" layer="1"/>
<smd name="9" x="-0.95" y="0.5" dx="1.9" dy="0.3" layer="1" rot="R180"/>
<smd name="10" x="-0.95" y="0" dx="1.9" dy="0.3" layer="1"/>
<smd name="11" x="-0.95" y="-0.5" dx="1.9" dy="0.3" layer="1"/>
<smd name="12" x="-0.95" y="-1" dx="1.9" dy="0.3" layer="1"/>
<smd name="13" x="-0.95" y="-1.5" dx="1.9" dy="0.3" layer="1"/>
<smd name="14" x="-0.95" y="-2" dx="1.9" dy="0.3" layer="1"/>
<smd name="15" x="-0.95" y="-2.5" dx="1.9" dy="0.3" layer="1"/>
<smd name="16" x="-0.95" y="-3" dx="1.9" dy="0.3" layer="1"/>
<smd name="17" x="-0.95" y="-3.5" dx="1.9" dy="0.3" layer="1"/>
<smd name="18" x="-0.95" y="-4" dx="1.9" dy="0.3" layer="1"/>
<smd name="19" x="-0.95" y="-4.5" dx="1.9" dy="0.3" layer="1"/>
<pad name="SH1" x="4.9" y="7.6" drill="1.3"/>
<pad name="SH2" x="0" y="7" drill="1.3"/>
<pad name="SH3" x="0" y="-7.5" drill="1.3"/>
<pad name="SH4" x="4.9" y="-8.1" drill="1.3"/>
<text x="10.16" y="-2.54" size="1.27" layer="51" rot="R90">Edge</text>
<text x="-1.25" y="9.41" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-10.43" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="SAANLIMA_CTS742C083">
<description>&lt;b&gt;Chip Resistor Array&lt;/b&gt;&lt;p&gt;
Source: http://www.ctscorp.com/components/Datasheets/CTSChipArrayDs.pdf</description>
<wire x1="-1.55" y1="0.75" x2="-1.45" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.45" y1="0.75" x2="-1.3" y2="0.6" width="0.1016" layer="51"/>
<wire x1="-1.3" y1="0.6" x2="-1.1" y2="0.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="0.6" x2="-0.95" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-0.95" y1="0.75" x2="-0.65" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-0.15" y1="0.75" x2="0.15" y2="0.75" width="0.1016" layer="51"/>
<wire x1="0.65" y1="0.75" x2="0.95" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.45" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="21"/>
<wire x1="1.55" y1="-0.75" x2="1.45" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="0.95" y1="-0.75" x2="0.65" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="0.15" y1="-0.75" x2="-0.15" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.65" y1="-0.75" x2="-0.95" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.45" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="21"/>
<wire x1="-0.65" y1="0.75" x2="-0.5" y2="0.6" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="0.6" x2="-0.3" y2="0.6" width="0.1016" layer="51"/>
<wire x1="-0.3" y1="0.6" x2="-0.15" y2="0.75" width="0.1016" layer="51"/>
<wire x1="0.15" y1="0.75" x2="0.3" y2="0.6" width="0.1016" layer="51"/>
<wire x1="0.3" y1="0.6" x2="0.5" y2="0.6" width="0.1016" layer="51"/>
<wire x1="0.5" y1="0.6" x2="0.65" y2="0.75" width="0.1016" layer="51"/>
<wire x1="0.95" y1="0.75" x2="1.1" y2="0.6" width="0.1016" layer="51"/>
<wire x1="1.1" y1="0.6" x2="1.3" y2="0.6" width="0.1016" layer="51"/>
<wire x1="1.3" y1="0.6" x2="1.45" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.45" y1="-0.75" x2="1.3" y2="-0.6" width="0.1016" layer="51"/>
<wire x1="1.3" y1="-0.6" x2="1.1" y2="-0.6" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-0.6" x2="0.95" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="0.65" y1="-0.75" x2="0.5" y2="-0.6" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-0.6" x2="0.3" y2="-0.6" width="0.1016" layer="51"/>
<wire x1="0.3" y1="-0.6" x2="0.15" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.15" y1="-0.75" x2="-0.3" y2="-0.6" width="0.1016" layer="51"/>
<wire x1="-0.3" y1="-0.6" x2="-0.5" y2="-0.6" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="-0.6" x2="-0.65" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.95" y1="-0.75" x2="-1.1" y2="-0.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-0.6" x2="-1.3" y2="-0.6" width="0.1016" layer="51"/>
<wire x1="-1.3" y1="-0.6" x2="-1.45" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-0.45" x2="-1.55" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.45" x2="1.55" y2="0.45" width="0.1016" layer="51"/>
<rectangle x1="-1.5" y1="-1.3" x2="-0.9" y2="-0.3" layer="29"/>
<rectangle x1="-0.7" y1="-1.3" x2="-0.1" y2="-0.3" layer="29"/>
<rectangle x1="0.1" y1="-1.3" x2="0.7" y2="-0.3" layer="29"/>
<rectangle x1="0.9" y1="-1.3" x2="1.5" y2="-0.3" layer="29"/>
<rectangle x1="0.9" y1="0.3" x2="1.5" y2="1.3" layer="29" rot="R180"/>
<rectangle x1="0.1" y1="0.3" x2="0.7" y2="1.3" layer="29" rot="R180"/>
<rectangle x1="-0.7" y1="0.3" x2="-0.1" y2="1.3" layer="29" rot="R180"/>
<rectangle x1="-1.5" y1="0.3" x2="-0.9" y2="1.3" layer="29" rot="R180"/>
<smd name="1" x="-1.2" y="-0.8" dx="0.5" dy="0.9" layer="1" stop="no"/>
<smd name="2" x="-0.4" y="-0.8" dx="0.5" dy="0.9" layer="1" stop="no"/>
<smd name="3" x="0.4" y="-0.8" dx="0.5" dy="0.9" layer="1" stop="no"/>
<smd name="4" x="1.2" y="-0.8" dx="0.5" dy="0.9" layer="1" stop="no"/>
<smd name="5" x="1.2" y="0.8" dx="0.5" dy="0.9" layer="1" stop="no"/>
<smd name="6" x="0.4" y="0.8" dx="0.5" dy="0.9" layer="1" stop="no"/>
<smd name="7" x="-0.4" y="0.8" dx="0.5" dy="0.9" layer="1" stop="no"/>
<smd name="8" x="-1.2" y="0.8" dx="0.5" dy="0.9" layer="1" stop="no"/>
<text x="-1.778" y="1.524" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.778" y="-2.794" size="1.27" layer="27">&gt;VALUE</text>
<polygon width="0.1016" layer="51">
<vertex x="-0.6" y="-0.675"/>
<vertex x="-0.6" y="-0.45"/>
<vertex x="-0.2" y="-0.45"/>
<vertex x="-0.2" y="-0.675"/>
<vertex x="-0.3" y="-0.575"/>
<vertex x="-0.5" y="-0.575"/>
</polygon>
<polygon width="0.1016" layer="51">
<vertex x="-1.4" y="-0.675"/>
<vertex x="-1.4" y="-0.45"/>
<vertex x="-1" y="-0.45"/>
<vertex x="-1" y="-0.675"/>
<vertex x="-1.1" y="-0.575"/>
<vertex x="-1.3" y="-0.575"/>
</polygon>
<polygon width="0.1016" layer="51">
<vertex x="0.2" y="-0.675"/>
<vertex x="0.2" y="-0.45"/>
<vertex x="0.6" y="-0.45"/>
<vertex x="0.6" y="-0.675"/>
<vertex x="0.5" y="-0.575"/>
<vertex x="0.3" y="-0.575"/>
</polygon>
<polygon width="0.1016" layer="51">
<vertex x="1" y="-0.675"/>
<vertex x="1" y="-0.45"/>
<vertex x="1.4" y="-0.45"/>
<vertex x="1.4" y="-0.675"/>
<vertex x="1.3" y="-0.575"/>
<vertex x="1.1" y="-0.575"/>
</polygon>
<polygon width="0.1016" layer="51">
<vertex x="1.4" y="0.675"/>
<vertex x="1.4" y="0.45"/>
<vertex x="1" y="0.45"/>
<vertex x="1" y="0.675"/>
<vertex x="1.1" y="0.575"/>
<vertex x="1.3" y="0.575"/>
</polygon>
<polygon width="0.1016" layer="51">
<vertex x="0.6" y="0.675"/>
<vertex x="0.6" y="0.45"/>
<vertex x="0.2" y="0.45"/>
<vertex x="0.2" y="0.675"/>
<vertex x="0.3" y="0.575"/>
<vertex x="0.5" y="0.575"/>
</polygon>
<polygon width="0.1016" layer="51">
<vertex x="-0.2" y="0.675"/>
<vertex x="-0.2" y="0.45"/>
<vertex x="-0.6" y="0.45"/>
<vertex x="-0.6" y="0.675"/>
<vertex x="-0.5" y="0.575"/>
<vertex x="-0.3" y="0.575"/>
</polygon>
<polygon width="0.1016" layer="51">
<vertex x="-1" y="0.675"/>
<vertex x="-1" y="0.45"/>
<vertex x="-1.4" y="0.45"/>
<vertex x="-1.4" y="0.675"/>
<vertex x="-1.3" y="0.575"/>
<vertex x="-1.1" y="0.575"/>
</polygon>
</package>
<package name="SAANLIMA_PSOP-8">
<circle x="-0.66" y="-0.67" radius="0.1923" width="0.08" layer="21"/>
<wire x1="-1" y1="-1.03" x2="1" y2="-1.03" width="0.08" layer="21"/>
<wire x1="1" y1="-1.03" x2="1" y2="1.04" width="0.08" layer="21"/>
<wire x1="1" y1="1.04" x2="-1" y2="1.04" width="0.08" layer="21"/>
<wire x1="-1" y1="1.04" x2="-1" y2="-1.03" width="0.08" layer="21"/>
<smd name="1" x="-0.73" y="-1.52" dx="0.25" dy="0.8" layer="1"/>
<smd name="2" x="-0.23" y="-1.52" dx="0.25" dy="0.8" layer="1"/>
<smd name="3" x="0.27" y="-1.52" dx="0.25" dy="0.8" layer="1"/>
<smd name="4" x="0.77" y="-1.52" dx="0.25" dy="0.8" layer="1"/>
<smd name="5" x="0.77" y="1.53" dx="0.25" dy="0.8" layer="1"/>
<smd name="6" x="0.27" y="1.53" dx="0.25" dy="0.8" layer="1"/>
<smd name="7" x="-0.23" y="1.53" dx="0.25" dy="0.8" layer="1"/>
<smd name="8" x="-0.73" y="1.53" dx="0.25" dy="0.8" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="SAANLIMA_HDMI">
<wire x1="-10.16" y1="43.18" x2="-10.16" y2="-60.96" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-60.96" x2="12.7" y2="-60.96" width="0.254" layer="94"/>
<wire x1="12.7" y1="-60.96" x2="12.7" y2="43.18" width="0.254" layer="94"/>
<wire x1="12.7" y1="43.18" x2="-10.16" y2="43.18" width="0.254" layer="94"/>
<pin name="+5V" x="-15.24" y="-45.72" length="middle"/>
<pin name="CEC" x="-15.24" y="-20.32" length="middle"/>
<pin name="CLOCK+" x="-15.24" y="-5.08" length="middle"/>
<pin name="CLOCK-" x="-15.24" y="-15.24" length="middle"/>
<pin name="CLOCKSHIELD" x="-15.24" y="-10.16" length="middle"/>
<pin name="DATA0+" x="-15.24" y="10.16" length="middle"/>
<pin name="DATA0-" x="-15.24" y="0" length="middle"/>
<pin name="DATA0SHIELD" x="-15.24" y="5.08" length="middle"/>
<pin name="DATA1+" x="-15.24" y="25.4" length="middle"/>
<pin name="DATA1-" x="-15.24" y="15.24" length="middle"/>
<pin name="DATA1SHIELD" x="-15.24" y="20.32" length="middle"/>
<pin name="DATA2+" x="-15.24" y="40.64" length="middle"/>
<pin name="DATA2-" x="-15.24" y="30.48" length="middle"/>
<pin name="DATA2SHIELD" x="-15.24" y="35.56" length="middle"/>
<pin name="DDC/CEC-GRND" x="-15.24" y="-40.64" length="middle"/>
<pin name="DETECT" x="-15.24" y="-50.8" length="middle"/>
<pin name="RES" x="-15.24" y="-25.4" length="middle"/>
<pin name="SCL" x="-15.24" y="-30.48" length="middle"/>
<pin name="SDA" x="-15.24" y="-35.56" length="middle"/>
<pin name="SH1" x="-5.08" y="-66.04" length="middle" rot="R90"/>
<pin name="SH2" x="0" y="-66.04" length="middle" rot="R90"/>
<pin name="SH3" x="5.08" y="-66.04" length="middle" rot="R90"/>
<pin name="SH4" x="10.16" y="-66.04" length="middle" rot="R90"/>
<text x="-10.16" y="45.72" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="SAANLIMA_RN4">
<wire x1="-1.27" y1="1.778" x2="3.81" y2="1.778" width="0.254" layer="94"/>
<wire x1="3.81" y1="3.302" x2="-1.27" y2="3.302" width="0.254" layer="94"/>
<wire x1="-1.27" y1="3.302" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="1.778" width="0.254" layer="94"/>
<wire x1="3.81" y1="1.778" x2="3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="3.302" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="3.81" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-1.27" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0.762" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="3.81" y1="-0.762" x2="3.81" y2="0" width="0.254" layer="94"/>
<wire x1="3.81" y1="0" x2="3.81" y2="0.762" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-0.762" x2="3.81" y2="-0.762" width="0.254" layer="94"/>
<wire x1="3.81" y1="0.762" x2="-1.27" y2="0.762" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-3.302" x2="3.81" y2="-3.302" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-5.842" x2="3.81" y2="-5.842" width="0.254" layer="94"/>
<wire x1="3.81" y1="-1.778" x2="-1.27" y2="-1.778" width="0.254" layer="94"/>
<wire x1="3.81" y1="-4.318" x2="-1.27" y2="-4.318" width="0.254" layer="94"/>
<wire x1="3.81" y1="-3.302" x2="3.81" y2="-2.54" width="0.254" layer="94"/>
<wire x1="3.81" y1="-5.842" x2="3.81" y2="-5.08" width="0.254" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="-1.778" width="0.254" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="-4.318" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="-3.302" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-1.27" y2="-5.842" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-1.778" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-4.318" x2="-1.27" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="4.318" x2="-2.54" y2="4.318" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-6.858" x2="-2.54" y2="-6.858" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="4.318" x2="-2.54" y2="-6.858" width="0.4064" layer="94"/>
<wire x1="3.81" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="4.318" x2="5.08" y2="-6.858" width="0.4064" layer="94"/>
<pin name="1" x="-5.08" y="2.54" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="2"/>
<pin name="3" x="-5.08" y="-2.54" visible="pad" length="short" direction="pas" swaplevel="3"/>
<pin name="4" x="-5.08" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="4"/>
<pin name="5" x="7.62" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="4" rot="R180"/>
<pin name="6" x="7.62" y="-2.54" visible="pad" length="short" direction="pas" swaplevel="3" rot="R180"/>
<pin name="7" x="7.62" y="0" visible="pad" length="short" direction="pas" swaplevel="2" rot="R180"/>
<pin name="8" x="7.62" y="2.54" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-1.905" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-9.398" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="SAANLIMA_PCA9306">
<wire x1="-10.16" y1="-5.08" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="1.778" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="1.778" y2="0" width="0.254" layer="94"/>
<pin name="EN" x="15.24" y="7.62" length="middle" rot="R180"/>
<pin name="GND" x="-15.24" y="-2.54" length="middle"/>
<pin name="SCL1" x="-15.24" y="2.54" length="middle"/>
<pin name="SCL2" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="SDA1" x="-15.24" y="0" length="middle"/>
<pin name="SDA2" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="VREF1" x="-15.24" y="5.08" length="middle"/>
<pin name="VREF2" x="15.24" y="5.08" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SAANLIMA_HDMI-TYPE-A-RECEPTICAL">
<gates>
<gate name="G$1" symbol="SAANLIMA_HDMI" x="0" y="5.08"/>
</gates>
<devices>
<device name="FCI10029449" package="SAANLIMA_FCI-HDMI-TYPE-A-RECEPT">
<connects>
<connect gate="G$1" pin="+5V" pad="18"/>
<connect gate="G$1" pin="CEC" pad="13"/>
<connect gate="G$1" pin="CLOCK+" pad="10"/>
<connect gate="G$1" pin="CLOCK-" pad="12"/>
<connect gate="G$1" pin="CLOCKSHIELD" pad="11"/>
<connect gate="G$1" pin="DATA0+" pad="7"/>
<connect gate="G$1" pin="DATA0-" pad="9"/>
<connect gate="G$1" pin="DATA0SHIELD" pad="8"/>
<connect gate="G$1" pin="DATA1+" pad="4"/>
<connect gate="G$1" pin="DATA1-" pad="6"/>
<connect gate="G$1" pin="DATA1SHIELD" pad="5"/>
<connect gate="G$1" pin="DATA2+" pad="1"/>
<connect gate="G$1" pin="DATA2-" pad="3"/>
<connect gate="G$1" pin="DATA2SHIELD" pad="2"/>
<connect gate="G$1" pin="DDC/CEC-GRND" pad="17"/>
<connect gate="G$1" pin="DETECT" pad="19"/>
<connect gate="G$1" pin="RES" pad="14"/>
<connect gate="G$1" pin="SCL" pad="15"/>
<connect gate="G$1" pin="SDA" pad="16"/>
<connect gate="G$1" pin="SH1" pad="SH1"/>
<connect gate="G$1" pin="SH2" pad="SH2"/>
<connect gate="G$1" pin="SH3" pad="SH3"/>
<connect gate="G$1" pin="SH4" pad="SH4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SAANLIMA_CTS742C083" prefix="RN">
<description>&lt;b&gt;Chip Resistor Array&lt;/b&gt; 4 Single Resistor&lt;p&gt;
Source: http://www.ctscorp.com/components/Datasheets/CTSChipArrayDs.pdf</description>
<gates>
<gate name="G$1" symbol="SAANLIMA_RN4" x="0" y="0" swaplevel="4"/>
</gates>
<devices>
<device name="" package="SAANLIMA_CTS742C083">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SAANLIMA_PCA9306">
<gates>
<gate name="G$1" symbol="SAANLIMA_PCA9306" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SAANLIMA_PSOP-8">
<connects>
<connect gate="G$1" pin="EN" pad="8"/>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="SCL1" pad="3"/>
<connect gate="G$1" pin="SCL2" pad="6"/>
<connect gate="G$1" pin="SDA1" pad="4"/>
<connect gate="G$1" pin="SDA2" pad="5"/>
<connect gate="G$1" pin="VREF1" pad="2"/>
<connect gate="G$1" pin="VREF2" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="dan-supply">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.016" y1="-0.254" x2="0" y2="-0.254" width="0.254" layer="94"/>
<wire x1="0" y1="-0.254" x2="1.016" y2="-0.254" width="0.254" layer="94"/>
<wire x1="1.016" y1="-0.254" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.016" y2="-0.254" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-0.254" width="0.1524" layer="94"/>
<text x="-0.635" y="-1.651" size="1.27" layer="96" rot="R270">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="dan-transistor">
<packages>
<package name="SOT323">
<description>&lt;b&gt;SOT323&lt;/b&gt;
&lt;p&gt;From http://www.diodes.com/datasheets/ds31859.pdf&lt;/p&gt;</description>
<wire x1="-1.1" y1="0.675" x2="-1.1" y2="-0.675" width="0.127" layer="21"/>
<wire x1="-1.1" y1="-0.675" x2="1.1" y2="-0.675" width="0.127" layer="21"/>
<wire x1="1.1" y1="-0.675" x2="1.1" y2="0.675" width="0.127" layer="21"/>
<wire x1="1.1" y1="0.675" x2="-1.1" y2="0.675" width="0.127" layer="21"/>
<circle x="-0.65" y="-0.1625" radius="0.1625" width="0.127" layer="21"/>
<smd name="3" x="0" y="0.95" dx="0.7" dy="0.9" layer="1"/>
<smd name="1" x="-0.65" y="-0.95" dx="0.7" dy="0.9" layer="1" rot="R180"/>
<smd name="2" x="0.65" y="-0.95" dx="0.7" dy="0.9" layer="1" rot="R180"/>
<text x="-1.27" y="-1.27" size="1.016" layer="25" font="vector" ratio="20" rot="R90">&gt;NAME</text>
<rectangle x1="-0.2" y1="0.675" x2="0.2" y2="1.1" layer="51"/>
<rectangle x1="-0.85" y1="-1.1" x2="-0.45" y2="-0.675" layer="51" rot="R180"/>
<rectangle x1="0.45" y1="-1.1" x2="0.85" y2="-0.675" layer="51" rot="R180"/>
</package>
</packages>
<symbols>
<symbol name="MOSFET-N-GDS-D">
<wire x1="-2.54" y1="-2.54" x2="-1.2192" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="0.762" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0" y1="3.683" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.397" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.397" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-3.683" width="0.254" layer="94"/>
<wire x1="-1.143" y1="2.54" x2="-1.143" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="5.08" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="0.508" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0.508" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="5.588" y1="0.508" x2="5.08" y2="0.508" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0.508" x2="4.572" y2="0.508" width="0.1524" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.3592" width="0" layer="94"/>
<circle x="2.54" y="2.54" radius="0.3592" width="0" layer="94"/>
<text x="7.112" y="-4.064" size="1.27" layer="96" rot="R90">&gt;VALUE</text>
<text x="-1.778" y="-2.032" size="1.27" layer="95" rot="R90">&gt;NAME</text>
<pin name="S" x="2.54" y="-7.62" visible="off" length="middle" direction="pas" rot="R90"/>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="D1" x="2.54" y="7.62" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="5.08" y="0.508"/>
<vertex x="4.572" y="-0.254"/>
<vertex x="5.588" y="-0.254"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="0.254" y="0"/>
<vertex x="1.27" y="0.762"/>
<vertex x="1.27" y="-0.762"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="MOSFET-N-GDS-D" prefix="Q" uservalue="yes">
<gates>
<gate name="G$1" symbol="MOSFET-N-GDS-D" x="0" y="0"/>
</gates>
<devices>
<device name="-SOT323" package="SOT323">
<connects>
<connect gate="G$1" pin="D1" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="dan-rcl">
<packages>
<package name="0402">
<description>&lt;b&gt;0402&lt;/b&gt;
&lt;p&gt;From Panasonic ERJ-2RKF1000X Recommended Land Patterns: http://industrial.panasonic.com/www-data/pdf/AOA0000/AOA0000PE32.pdf&lt;/p&gt;
&lt;p&gt;Can fit one 8-mil (0.2032mm) trace between pads&lt;/p&gt;</description>
<smd name="2" x="0.6548" y="0" dx="0.7" dy="0.6" layer="1"/>
<smd name="1" x="-0.6548" y="0" dx="0.7" dy="0.6" layer="1"/>
<text x="1.27" y="-0.635" size="1.016" layer="25" font="vector" ratio="20">&gt;NAME</text>
<rectangle x1="-0.3048" y1="-0.25" x2="0.3048" y2="-0.0468" layer="21"/>
<rectangle x1="-0.525" y1="-0.275" x2="-0.15" y2="0.275" layer="51"/>
<rectangle x1="0.15" y1="-0.275" x2="0.525" y2="0.275" layer="51" rot="R180"/>
<rectangle x1="-0.3048" y1="0.0468" x2="0.3048" y2="0.25" layer="21" rot="R180"/>
</package>
<package name="0603">
<description>&lt;b&gt;0603&lt;/b&gt;
&lt;p&gt;From Panasonic ERJ-2RKF1000X Recommended Land Patterns: http://industrial.panasonic.com/www-data/pdf/AOA0000/AOA0000PE32.pdf&lt;/p&gt;
&lt;p&gt;Can fit two 6.25-mil (0.15875mm) traces between pads (on 0.00625 grid)&lt;/p&gt;</description>
<smd name="2" x="0.725" y="0" dx="0.55" dy="1" layer="1"/>
<smd name="1" x="-0.725" y="0" dx="0.55" dy="1" layer="1"/>
<text x="1.27" y="-0.635" size="1.016" layer="25" font="vector" ratio="20">&gt;NAME</text>
<rectangle x1="-0.45" y1="-0.4" x2="0.45" y2="-0.1968" layer="21"/>
<rectangle x1="-0.825" y1="-0.425" x2="-0.4" y2="0.425" layer="51"/>
<rectangle x1="0.4" y1="-0.425" x2="0.825" y2="0.425" layer="51" rot="R180"/>
<rectangle x1="-0.45" y1="0.1968" x2="0.45" y2="0.4" layer="21" rot="R180"/>
</package>
<package name="0805">
<description>&lt;b&gt;0805&lt;/b&gt;
&lt;p&gt;From Panasonic ERJ-2RKF1000X Recommended Land Patterns: http://industrial.panasonic.com/www-data/pdf/AOA0000/AOA0000PE32.pdf&lt;/p&gt;
&lt;p&gt;Can fit three 6.25-mil (0.15875mm) traces between pads (on 0.00625 grid)&lt;/p&gt;</description>
<smd name="2" x="1.1" y="0" dx="0.9" dy="1.4" layer="1"/>
<smd name="1" x="-1.1" y="0" dx="0.9" dy="1.4" layer="1"/>
<text x="1.905" y="-0.635" size="1.016" layer="25" font="vector" ratio="20">&gt;NAME</text>
<rectangle x1="-0.65" y1="-0.625" x2="0.65" y2="-0.4218" layer="21"/>
<rectangle x1="-1.025" y1="-0.65" x2="-0.475" y2="0.65" layer="51"/>
<rectangle x1="0.475" y1="-0.65" x2="1.025" y2="0.65" layer="51" rot="R180"/>
<rectangle x1="-0.65" y1="0.4218" x2="0.65" y2="0.625" layer="21" rot="R180"/>
</package>
<package name="1206">
<description>&lt;b&gt;1206&lt;/b&gt;
&lt;p&gt;From Panasonic ERJ-2RKF1000X Recommended Land Patterns: http://industrial.panasonic.com/www-data/pdf/AOA0000/AOA0000PE32.pdf&lt;/p&gt;
&lt;p&gt;Can fit six 6.25-mil (0.15875mm) traces between pads (on 0.00625 grid)&lt;/p&gt;</description>
<smd name="1" x="-1.65" y="0" dx="1.1" dy="1.8" layer="1"/>
<smd name="2" x="1.65" y="0" dx="1.1" dy="1.8" layer="1" rot="R180"/>
<text x="3.175" y="-0.635" size="1.016" layer="25" font="vector" ratio="20">&gt;NAME</text>
<rectangle x1="-1.1" y1="-0.8" x2="1.1" y2="-0.5968" layer="21"/>
<rectangle x1="-1.625" y1="-0.825" x2="-1" y2="0.825" layer="51"/>
<rectangle x1="1" y1="-0.825" x2="1.625" y2="0.825" layer="51" rot="R180"/>
<rectangle x1="-1.1" y1="0.5968" x2="1.1" y2="0.8" layer="21" rot="R180"/>
</package>
<package name="1206H">
<description>&lt;b&gt;High-power 1206&lt;/b&gt;
&lt;p&gt;From Panasonic ERJ-2RKF1000X Recommended Land Patterns: http://industrial.panasonic.com/www-data/pdf/AOA0000/AOA0000PE32.pdf&lt;/p&gt;
&lt;p&gt;Can fit three 6.25-mil (0.15875mm) trace between pads (on 0.00625 grid)&lt;/p&gt;</description>
<smd name="1" x="-1.55" y="0" dx="1.9" dy="1.8" layer="1"/>
<smd name="2" x="1.55" y="0" dx="1.9" dy="1.8" layer="1" rot="R180"/>
<text x="3.175" y="-0.635" size="1.016" layer="25" font="vector" ratio="20">&gt;NAME</text>
<rectangle x1="-1.625" y1="-0.825" x2="-1" y2="0.825" layer="51"/>
<rectangle x1="1" y1="-0.825" x2="1.625" y2="0.825" layer="51" rot="R180"/>
<rectangle x1="-0.6" y1="0.5968" x2="0.6" y2="0.8" layer="21" rot="R180"/>
<rectangle x1="-0.6" y1="-0.8" x2="0.6" y2="-0.5968" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="R">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="2.794" y="0.4826" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.794" y="0.508" size="1.27" layer="96" rot="MR0">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="C">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="0.381" y="0.254" size="1.27" layer="95" rot="MR90">&gt;NAME</text>
<text x="0.381" y="-2.794" size="1.27" layer="96" rot="R270">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206H" package="1206H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rt_esd_diodes">
<packages>
<package name="SLP2510P8">
<smd name="10" x="-1" y="0.4375" dx="0.2" dy="0.675" layer="1"/>
<smd name="9" x="-0.5" y="0.4375" dx="0.2" dy="0.675" layer="1"/>
<smd name="7" x="0.5" y="0.4375" dx="0.2" dy="0.675" layer="1"/>
<smd name="6" x="1" y="0.4375" dx="0.2" dy="0.675" layer="1"/>
<smd name="1" x="-1" y="-0.4375" dx="0.2" dy="0.675" layer="1"/>
<smd name="2" x="-0.5" y="-0.4375" dx="0.2" dy="0.675" layer="1"/>
<smd name="4" x="0.5" y="-0.4375" dx="0.2" dy="0.675" layer="1"/>
<smd name="5" x="1" y="-0.4375" dx="0.2" dy="0.675" layer="1"/>
<smd name="3" x="0" y="0" dx="0.4" dy="1.55" layer="1"/>
<wire x1="1.3" y1="1" x2="-1.3" y2="1" width="0.127" layer="21"/>
<wire x1="-1.3" y1="1" x2="-1.3" y2="-1" width="0.127" layer="21"/>
<wire x1="-1.3" y1="-1" x2="1.3" y2="-1" width="0.127" layer="21"/>
<wire x1="1.3" y1="-1" x2="1.3" y2="1" width="0.127" layer="21"/>
<circle x="-1.3" y="-1.2" radius="0.1" width="0" layer="21"/>
<text x="-1.5032" y="-1.076" size="0.381" layer="21" rot="R90">&gt;NAME</text>
</package>
<package name="RSA5M">
<description>ESD protection device</description>
<smd name="1" x="0" y="1.525" dx="1.2" dy="0.85" layer="1"/>
<smd name="2" x="0" y="-1.525" dx="1.2" dy="0.85" layer="1"/>
<wire x1="0.8" y1="1.6" x2="1" y2="1.6" width="0.127" layer="21"/>
<wire x1="1" y1="1.6" x2="1" y2="0.6" width="0.127" layer="21"/>
<wire x1="1" y1="0.6" x2="1" y2="-1.6" width="0.127" layer="21"/>
<wire x1="1" y1="-1.6" x2="0.8" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-0.8" y1="1.6" x2="-1" y2="1.6" width="0.127" layer="21"/>
<wire x1="-1" y1="1.6" x2="-1" y2="0.6" width="0.127" layer="21"/>
<wire x1="-1" y1="0.6" x2="-1" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-0.8" y1="-1.6" x2="-1" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-1" y1="0.6" x2="1" y2="0.6" width="0.127" layer="21"/>
<text x="1.4" y="1.6" size="0.7" layer="21" rot="R270">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="RCLAMP0524P">
<description>Diode for ESD protection. Suitable for HDMI connectors</description>
<pin name="IO1A" x="-12.7" y="7.62" length="middle"/>
<pin name="IO2A" x="-12.7" y="5.08" length="middle"/>
<pin name="GND3" x="-12.7" y="0" length="middle"/>
<pin name="IO3A" x="-12.7" y="-5.08" length="middle"/>
<pin name="IO4A" x="-12.7" y="-7.62" length="middle"/>
<pin name="IO4B" x="15.24" y="-7.62" length="middle" rot="R180"/>
<pin name="IO3B" x="15.24" y="-5.08" length="middle" rot="R180"/>
<pin name="IO2B" x="15.24" y="5.08" length="middle" rot="R180"/>
<pin name="IO1B" x="15.24" y="7.62" length="middle" rot="R180"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<text x="-7.62" y="12.7" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="RSA5M">
<wire x1="-5.08" y1="0" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="0" width="0.254" layer="94"/>
<pin name="1" x="-7.62" y="0" length="short"/>
<pin name="2" x="5.08" y="0" length="short" rot="R180"/>
<text x="-5.08" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RCLAMP0524P">
<gates>
<gate name="G$1" symbol="RCLAMP0524P" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SLP2510P8">
<connects>
<connect gate="G$1" pin="GND3" pad="3"/>
<connect gate="G$1" pin="IO1A" pad="1"/>
<connect gate="G$1" pin="IO1B" pad="10"/>
<connect gate="G$1" pin="IO2A" pad="2"/>
<connect gate="G$1" pin="IO2B" pad="9"/>
<connect gate="G$1" pin="IO3A" pad="4"/>
<connect gate="G$1" pin="IO3B" pad="7"/>
<connect gate="G$1" pin="IO4A" pad="5"/>
<connect gate="G$1" pin="IO4B" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RSA5M" prefix="D" uservalue="yes">
<description>ESD protection device</description>
<gates>
<gate name="G$1" symbol="RSA5M" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RSA5M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="dan-translate">
<packages>
<package name="SOT23-5">
<description>&lt;b&gt;SOT23-5&lt;/b&gt;
&lt;p&gt;From http://www.ti.com/lit/gpn/sn74lvc1t45&lt;/p&gt;</description>
<wire x1="-1.5" y1="0.8" x2="-1.5" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-0.8" x2="1.5" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-0.8" x2="1.5" y2="0.8" width="0.2032" layer="21"/>
<wire x1="1.5" y1="0.8" x2="-1.5" y2="0.8" width="0.2032" layer="21"/>
<circle x="-0.95" y="-0.2" radius="0.2" width="0.2032" layer="21"/>
<smd name="1" x="-0.95" y="-1.25" dx="0.6" dy="1" layer="1" rot="R180"/>
<smd name="2" x="0" y="-1.25" dx="0.6" dy="1" layer="1" rot="R180"/>
<smd name="3" x="0.95" y="-1.25" dx="0.6" dy="1" layer="1" rot="R180"/>
<smd name="4" x="0.95" y="1.25" dx="0.6" dy="1" layer="1"/>
<smd name="5" x="-0.95" y="1.25" dx="0.6" dy="1" layer="1"/>
<text x="-1.77" y="-1.065" size="1.016" layer="25" font="vector" ratio="20" rot="R90">&gt;NAME</text>
<rectangle x1="-1.19" y1="-1.5" x2="-0.71" y2="-0.8" layer="51" rot="R180"/>
<rectangle x1="0.71" y1="-1.5" x2="1.19" y2="-0.8" layer="51" rot="R180"/>
<rectangle x1="-0.24" y1="-1.5" x2="0.24" y2="-0.8" layer="51" rot="R180"/>
<rectangle x1="-1.19" y1="0.8" x2="-0.71" y2="1.5" layer="51"/>
<rectangle x1="0.71" y1="0.8" x2="1.19" y2="1.5" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="SN74AHC1G126">
<wire x1="-0.762" y1="-1.27" x2="-0.508" y2="-1.27" width="0.127" layer="94"/>
<wire x1="-0.508" y1="-1.27" x2="0.762" y2="0" width="0.127" layer="94"/>
<wire x1="0.762" y1="0" x2="0" y2="0.762" width="0.127" layer="94"/>
<wire x1="0" y1="0.762" x2="-0.508" y2="1.27" width="0.127" layer="94"/>
<wire x1="-0.508" y1="1.27" x2="-0.762" y2="1.27" width="0.127" layer="94"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="0" width="0.127" layer="94"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.27" width="0.127" layer="94"/>
<wire x1="-0.762" y1="0" x2="-3.048" y2="0" width="0.127" layer="94"/>
<wire x1="0.762" y1="0" x2="2.032" y2="0" width="0.127" layer="94"/>
<wire x1="2.032" y1="0" x2="2.032" y2="-2.54" width="0.127" layer="94"/>
<wire x1="2.032" y1="-2.54" x2="3.81" y2="-2.54" width="0.127" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.302" y2="-3.048" width="0.127" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.302" y2="-2.032" width="0.127" layer="94"/>
<wire x1="-1.778" y1="2.54" x2="0" y2="2.54" width="0.127" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0.762" width="0.127" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<text x="-7.62" y="7.874" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="5.588" size="1.778" layer="96">&gt;VALUE</text>
<pin name="OE" x="-12.7" y="2.54" length="middle" direction="in"/>
<pin name="A" x="-12.7" y="0" length="middle" direction="in"/>
<pin name="GND" x="-12.7" y="-2.54" length="middle" direction="pas"/>
<pin name="VCC" x="12.7" y="2.54" length="middle" direction="pas" rot="R180"/>
<pin name="Y" x="12.7" y="-2.54" length="middle" direction="out" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SN74AHC1G126" prefix="IC">
<description>&lt;b&gt;SN74AHC1G126&lt;/b&gt;
&lt;p&gt;1-bit bus buffer gate with 3-state output&lt;/p&gt;
&lt;p&gt;2V to 5.5V&lt;/p&gt;
&lt;p&gt;Max 6ns t_pd at 5V&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="SN74AHC1G126" x="0" y="0"/>
</gates>
<devices>
<device name="DBV" package="SOT23-5">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="OE" pad="1"/>
<connect gate="G$1" pin="VCC" pad="5"/>
<connect gate="G$1" pin="Y" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rt_translate">
<packages>
<package name="SC70-5">
<description>&lt;b&gt;SMT SC70-5&lt;/b&gt;&lt;p&gt;
SOT353 - Philips Semiconductors&lt;br&gt;
Source: http://www.semiconductors.philips.com/acrobat_download/datasheets/74HC_HCT1G66_3.pdf</description>
<wire x1="1" y1="0.55" x2="-1" y2="0.55" width="0.127" layer="51"/>
<wire x1="-1" y1="0.55" x2="-1" y2="-0.55" width="0.127" layer="21"/>
<wire x1="-1" y1="-0.55" x2="1" y2="-0.55" width="0.127" layer="51"/>
<wire x1="1" y1="-0.55" x2="1" y2="0.55" width="0.127" layer="21"/>
<smd name="1" x="-0.65" y="-0.85" dx="0.4" dy="0.7" layer="1"/>
<smd name="2" x="0" y="-0.85" dx="0.4" dy="0.7" layer="1"/>
<smd name="3" x="0.65" y="-0.85" dx="0.4" dy="0.7" layer="1"/>
<smd name="4" x="0.65" y="0.85" dx="0.4" dy="0.7" layer="1"/>
<smd name="5" x="-0.65" y="0.85" dx="0.4" dy="0.7" layer="1"/>
<text x="-2.54" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.125" y1="-1.05" x2="0.125" y2="-0.6" layer="51"/>
<rectangle x1="-0.775" y1="-1.05" x2="-0.525" y2="-0.6" layer="51"/>
<rectangle x1="0.525" y1="-1.05" x2="0.775" y2="-0.6" layer="51"/>
<rectangle x1="-0.775" y1="0.6" x2="-0.525" y2="1.05" layer="51"/>
<rectangle x1="0.525" y1="0.6" x2="0.775" y2="1.05" layer="51"/>
</package>
<package name="SOT23-5">
<description>&lt;b&gt;SOT23-5&lt;/b&gt;
&lt;p&gt;From http://www.ti.com/lit/gpn/sn74lvc1t45&lt;/p&gt;</description>
<wire x1="-1.5" y1="0.8" x2="-1.5" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-0.8" x2="1.5" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-0.8" x2="1.5" y2="0.8" width="0.2032" layer="21"/>
<wire x1="1.5" y1="0.8" x2="-1.5" y2="0.8" width="0.2032" layer="21"/>
<circle x="-0.95" y="-0.2" radius="0.2" width="0.2032" layer="21"/>
<smd name="1" x="-0.95" y="-1.25" dx="0.6" dy="1" layer="1" rot="R180"/>
<smd name="2" x="0" y="-1.25" dx="0.6" dy="1" layer="1" rot="R180"/>
<smd name="3" x="0.95" y="-1.25" dx="0.6" dy="1" layer="1" rot="R180"/>
<smd name="4" x="0.95" y="1.25" dx="0.6" dy="1" layer="1"/>
<smd name="5" x="-0.95" y="1.25" dx="0.6" dy="1" layer="1"/>
<text x="-1.77" y="-1.065" size="1.016" layer="25" font="vector" ratio="20" rot="R90">&gt;NAME</text>
<rectangle x1="-1.19" y1="-1.5" x2="-0.71" y2="-0.8" layer="51" rot="R180"/>
<rectangle x1="0.71" y1="-1.5" x2="1.19" y2="-0.8" layer="51" rot="R180"/>
<rectangle x1="-0.24" y1="-1.5" x2="0.24" y2="-0.8" layer="51" rot="R180"/>
<rectangle x1="-1.19" y1="0.8" x2="-0.71" y2="1.5" layer="51"/>
<rectangle x1="0.71" y1="0.8" x2="1.19" y2="1.5" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="74HCT1G04GW">
<wire x1="-0.762" y1="-1.27" x2="-0.508" y2="-1.27" width="0.127" layer="94"/>
<wire x1="-0.508" y1="-1.27" x2="0.762" y2="0" width="0.127" layer="94"/>
<wire x1="0.762" y1="0" x2="0" y2="0.762" width="0.127" layer="94"/>
<wire x1="0" y1="0.762" x2="-0.508" y2="1.27" width="0.127" layer="94"/>
<wire x1="-0.508" y1="1.27" x2="-0.762" y2="1.27" width="0.127" layer="94"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="0" width="0.127" layer="94"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.27" width="0.127" layer="94"/>
<wire x1="-0.762" y1="0" x2="-3.048" y2="0" width="0.127" layer="94"/>
<wire x1="0.762" y1="0" x2="2.032" y2="0" width="0.127" layer="94"/>
<wire x1="2.032" y1="0" x2="2.032" y2="-2.54" width="0.127" layer="94"/>
<wire x1="2.032" y1="-2.54" x2="3.81" y2="-2.54" width="0.127" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.302" y2="-3.048" width="0.127" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.302" y2="-2.032" width="0.127" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<text x="-7.62" y="7.874" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="5.588" size="1.778" layer="96">&gt;VALUE</text>
<pin name="NC" x="-12.7" y="2.54" length="middle" direction="in"/>
<pin name="A" x="-12.7" y="0" length="middle" direction="in"/>
<pin name="GND" x="-12.7" y="-2.54" length="middle" direction="pas"/>
<pin name="VCC" x="12.7" y="2.54" length="middle" direction="pas" rot="R180"/>
<pin name="Y" x="12.7" y="-2.54" length="middle" direction="out" rot="R180"/>
</symbol>
<symbol name="SN74AHC1G126">
<wire x1="-0.762" y1="-1.27" x2="-0.508" y2="-1.27" width="0.127" layer="94"/>
<wire x1="-0.508" y1="-1.27" x2="0.762" y2="0" width="0.127" layer="94"/>
<wire x1="0.762" y1="0" x2="0" y2="0.762" width="0.127" layer="94"/>
<wire x1="0" y1="0.762" x2="-0.508" y2="1.27" width="0.127" layer="94"/>
<wire x1="-0.508" y1="1.27" x2="-0.762" y2="1.27" width="0.127" layer="94"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="0" width="0.127" layer="94"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.27" width="0.127" layer="94"/>
<wire x1="-0.762" y1="0" x2="-3.048" y2="0" width="0.127" layer="94"/>
<wire x1="0.762" y1="0" x2="2.032" y2="0" width="0.127" layer="94"/>
<wire x1="2.032" y1="0" x2="2.032" y2="-2.54" width="0.127" layer="94"/>
<wire x1="2.032" y1="-2.54" x2="3.81" y2="-2.54" width="0.127" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.302" y2="-3.048" width="0.127" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.302" y2="-2.032" width="0.127" layer="94"/>
<wire x1="-1.778" y1="2.54" x2="0" y2="2.54" width="0.127" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0.762" width="0.127" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<text x="-7.62" y="7.874" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="5.588" size="1.778" layer="96">&gt;VALUE</text>
<pin name="OE" x="-12.7" y="2.54" length="middle" direction="in"/>
<pin name="A" x="-12.7" y="0" length="middle" direction="in"/>
<pin name="GND" x="-12.7" y="-2.54" length="middle" direction="pas"/>
<pin name="VCC" x="12.7" y="2.54" length="middle" direction="pas" rot="R180"/>
<pin name="Y" x="12.7" y="-2.54" length="middle" direction="out" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="74HC1G04GW" prefix="U">
<description>74HC1G04GW, IC Inverter

http://www.digikey.com/product-detail/en/74HCT1G04GW,125/568-7743-1-ND/2753887</description>
<gates>
<gate name="G$1" symbol="74HCT1G04GW" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SC70-5">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="NC" pad="1"/>
<connect gate="G$1" pin="VCC" pad="5"/>
<connect gate="G$1" pin="Y" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SN74AHC1G126" prefix="IC">
<description>&lt;b&gt;SN74AHC1G126&lt;/b&gt;
&lt;p&gt;1-bit bus buffer gate with 3-state output&lt;/p&gt;
&lt;p&gt;2V to 5.5V&lt;/p&gt;
&lt;p&gt;Max 6ns t_pd at 5V&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="SN74AHC1G126" x="0" y="0"/>
</gates>
<devices>
<device name="DBV" package="SOT23-5">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="OE" pad="1"/>
<connect gate="G$1" pin="VCC" pad="5"/>
<connect gate="G$1" pin="Y" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="FCI_By_element14_Batch_1">
<description>Developed by element14 :&lt;br&gt;
element14 CAD Library consolidation.ulp
at 25/07/2012 10:14:29</description>
<packages>
<package name="10106813-04X112LF">
<hole x="-6" y="1.9" drill="0.625"/>
<hole x="6" y="1.9" drill="0.625"/>
<smd name="NC1" x="-6.98" y="0" dx="1.475" dy="2.15" layer="1"/>
<smd name="NC2" x="6.98" y="0" dx="1.475" dy="2.15" layer="1"/>
<smd name="PAD2" x="-4.75" y="2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD4" x="-4.25" y="2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD6" x="-3.75" y="2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD8" x="-3.25" y="2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD10" x="-2.75" y="2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD12" x="-2.25" y="2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD14" x="-1.75" y="2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD16" x="-1.25" y="2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD18" x="-0.75" y="2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD20" x="-0.25" y="2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD40" x="4.75" y="2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD38" x="4.25" y="2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD36" x="3.75" y="2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD34" x="3.25" y="2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD32" x="2.75" y="2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD30" x="2.25" y="2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD28" x="1.75" y="2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD26" x="1.25" y="2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD24" x="0.75" y="2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD22" x="0.25" y="2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD1" x="-4.75" y="-2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD3" x="-4.25" y="-2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD5" x="-3.75" y="-2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD7" x="-3.25" y="-2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD9" x="-2.75" y="-2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD11" x="-2.25" y="-2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD13" x="-1.75" y="-2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD15" x="-1.25" y="-2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD17" x="-0.75" y="-2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD19" x="-0.25" y="-2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD39" x="4.75" y="-2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD37" x="4.25" y="-2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD35" x="3.75" y="-2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD33" x="3.25" y="-2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD31" x="2.75" y="-2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD29" x="2.25" y="-2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD27" x="1.75" y="-2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD25" x="1.25" y="-2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD23" x="0.75" y="-2.55" dx="0.25" dy="1.187" layer="1"/>
<smd name="PAD21" x="0.25" y="-2.55" dx="0.25" dy="1.187" layer="1"/>
<wire x1="-5.2" y1="2.5" x2="-7.4" y2="2.5" width="0.127" layer="21"/>
<wire x1="-7.4" y1="2.5" x2="-7.4" y2="1.3" width="0.127" layer="21"/>
<wire x1="-7.4" y1="-1.3" x2="-7.4" y2="-1.9" width="0.127" layer="21"/>
<wire x1="-7.4" y1="-1.9" x2="-6.7" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-6.7" y1="-2.6" x2="-5.2" y2="-2.6" width="0.127" layer="21"/>
<wire x1="5.3" y1="-2.6" x2="7.4" y2="-2.6" width="0.127" layer="21"/>
<wire x1="7.4" y1="-2.6" x2="7.4" y2="-1.4" width="0.127" layer="21"/>
<wire x1="7.4" y1="1.3" x2="7.4" y2="2.5" width="0.127" layer="21"/>
<wire x1="7.4" y1="2.5" x2="5.3" y2="2.5" width="0.127" layer="21"/>
<text x="-8.2" y="-3.5" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="-5.2" y="-1.6" size="1.016" layer="21">1</text>
<text x="-5.2" y="0.3" size="1.016" layer="21">2</text>
</package>
</packages>
<symbols>
<symbol name="10106813-041112LF">
<wire x1="-7.62" y1="27.94" x2="-7.62" y2="-33.02" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-33.02" x2="7.62" y2="-33.02" width="0.254" layer="94"/>
<wire x1="7.62" y1="-33.02" x2="7.62" y2="27.94" width="0.254" layer="94"/>
<wire x1="7.62" y1="27.94" x2="-7.62" y2="27.94" width="0.254" layer="94"/>
<pin name="P1" x="-12.7" y="25.4" length="middle"/>
<pin name="P3" x="-12.7" y="22.86" length="middle"/>
<pin name="P5" x="-12.7" y="20.32" length="middle"/>
<pin name="P7" x="-12.7" y="17.78" length="middle"/>
<pin name="P9" x="-12.7" y="15.24" length="middle"/>
<pin name="P11" x="-12.7" y="12.7" length="middle"/>
<pin name="P13" x="-12.7" y="10.16" length="middle"/>
<pin name="P15" x="-12.7" y="7.62" length="middle"/>
<pin name="P17" x="-12.7" y="5.08" length="middle"/>
<pin name="P19" x="-12.7" y="2.54" length="middle"/>
<pin name="P21" x="-12.7" y="-2.54" length="middle"/>
<pin name="P23" x="-12.7" y="-5.08" length="middle"/>
<pin name="P25" x="-12.7" y="-7.62" length="middle"/>
<pin name="P27" x="-12.7" y="-10.16" length="middle"/>
<pin name="P29" x="-12.7" y="-12.7" length="middle"/>
<pin name="P31" x="-12.7" y="-15.24" length="middle"/>
<pin name="P33" x="-12.7" y="-17.78" length="middle"/>
<pin name="P35" x="-12.7" y="-20.32" length="middle"/>
<pin name="P37" x="-12.7" y="-22.86" length="middle"/>
<pin name="P39" x="-12.7" y="-25.4" length="middle"/>
<pin name="P40" x="12.7" y="-25.4" length="middle" rot="R180"/>
<pin name="P38" x="12.7" y="-22.86" length="middle" rot="R180"/>
<pin name="P36" x="12.7" y="-20.32" length="middle" rot="R180"/>
<pin name="P34" x="12.7" y="-17.78" length="middle" rot="R180"/>
<pin name="P32" x="12.7" y="-15.24" length="middle" rot="R180"/>
<pin name="P30" x="12.7" y="-12.7" length="middle" rot="R180"/>
<pin name="P28" x="12.7" y="-10.16" length="middle" rot="R180"/>
<pin name="P26" x="12.7" y="-7.62" length="middle" rot="R180"/>
<pin name="P24" x="12.7" y="-5.08" length="middle" rot="R180"/>
<pin name="P22" x="12.7" y="-2.54" length="middle" rot="R180"/>
<pin name="P20" x="12.7" y="2.54" length="middle" rot="R180"/>
<pin name="P18" x="12.7" y="5.08" length="middle" rot="R180"/>
<pin name="P16" x="12.7" y="7.62" length="middle" rot="R180"/>
<pin name="P14" x="12.7" y="10.16" length="middle" rot="R180"/>
<pin name="P12" x="12.7" y="12.7" length="middle" rot="R180"/>
<pin name="P10" x="12.7" y="15.24" length="middle" rot="R180"/>
<pin name="P8" x="12.7" y="17.78" length="middle" rot="R180"/>
<pin name="P6" x="12.7" y="20.32" length="middle" rot="R180"/>
<pin name="P4" x="12.7" y="22.86" length="middle" rot="R180"/>
<pin name="P2" x="12.7" y="25.4" length="middle" rot="R180"/>
<text x="-7.62" y="30.48" size="1.778" layer="95">&gt;NAME</text>
<pin name="NC2" x="12.7" y="-30.48" length="middle" rot="R180"/>
<pin name="NC1" x="-12.7" y="-30.48" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MEZZO-STACK-0.5MM" prefix="J">
<description>http://www.digikey.com/product-detail/en/10106813-041112LF/609-4601-1-ND/2708839</description>
<gates>
<gate name="G$1" symbol="10106813-041112LF" x="0" y="0"/>
</gates>
<devices>
<device name="" package="10106813-04X112LF">
<connects>
<connect gate="G$1" pin="NC1" pad="NC1"/>
<connect gate="G$1" pin="NC2" pad="NC2"/>
<connect gate="G$1" pin="P1" pad="PAD1"/>
<connect gate="G$1" pin="P10" pad="PAD10"/>
<connect gate="G$1" pin="P11" pad="PAD11"/>
<connect gate="G$1" pin="P12" pad="PAD12"/>
<connect gate="G$1" pin="P13" pad="PAD13"/>
<connect gate="G$1" pin="P14" pad="PAD14"/>
<connect gate="G$1" pin="P15" pad="PAD15"/>
<connect gate="G$1" pin="P16" pad="PAD16"/>
<connect gate="G$1" pin="P17" pad="PAD17"/>
<connect gate="G$1" pin="P18" pad="PAD18"/>
<connect gate="G$1" pin="P19" pad="PAD19"/>
<connect gate="G$1" pin="P2" pad="PAD2"/>
<connect gate="G$1" pin="P20" pad="PAD20"/>
<connect gate="G$1" pin="P21" pad="PAD21"/>
<connect gate="G$1" pin="P22" pad="PAD22"/>
<connect gate="G$1" pin="P23" pad="PAD23"/>
<connect gate="G$1" pin="P24" pad="PAD24"/>
<connect gate="G$1" pin="P25" pad="PAD25"/>
<connect gate="G$1" pin="P26" pad="PAD26"/>
<connect gate="G$1" pin="P27" pad="PAD27"/>
<connect gate="G$1" pin="P28" pad="PAD28"/>
<connect gate="G$1" pin="P29" pad="PAD29"/>
<connect gate="G$1" pin="P3" pad="PAD3"/>
<connect gate="G$1" pin="P30" pad="PAD30"/>
<connect gate="G$1" pin="P31" pad="PAD31"/>
<connect gate="G$1" pin="P32" pad="PAD32"/>
<connect gate="G$1" pin="P33" pad="PAD33"/>
<connect gate="G$1" pin="P34" pad="PAD34"/>
<connect gate="G$1" pin="P35" pad="PAD35"/>
<connect gate="G$1" pin="P36" pad="PAD36"/>
<connect gate="G$1" pin="P37" pad="PAD37"/>
<connect gate="G$1" pin="P38" pad="PAD38"/>
<connect gate="G$1" pin="P39" pad="PAD39"/>
<connect gate="G$1" pin="P4" pad="PAD4"/>
<connect gate="G$1" pin="P40" pad="PAD40"/>
<connect gate="G$1" pin="P5" pad="PAD5"/>
<connect gate="G$1" pin="P6" pad="PAD6"/>
<connect gate="G$1" pin="P7" pad="PAD7"/>
<connect gate="G$1" pin="P8" pad="PAD8"/>
<connect gate="G$1" pin="P9" pad="PAD9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.1778" drill="0.381">
<clearance class="0" value="0.1778"/>
</class>
<class number="1" name="Power" width="0.8128" drill="0.381">
<clearance class="1" value="0.1778"/>
</class>
</classes>
<parts>
<part name="C24" library="GadgetFactory" deviceset="C" device="0603" value=".1uF"/>
<part name="C29" library="GadgetFactory" deviceset="C" device="0603" value=".1uF"/>
<part name="C19" library="GadgetFactory" deviceset="C" device="0603" value=".1uF"/>
<part name="C18" library="GadgetFactory" deviceset="C" device="0603" value=".1uF"/>
<part name="C21" library="GadgetFactory" deviceset="C" device="0603" value=".1uF"/>
<part name="C31" library="GadgetFactory" deviceset="C" device="0603" value=".1uF"/>
<part name="C26" library="GadgetFactory" deviceset="C" device="0603" value=".1uF"/>
<part name="C15" library="GadgetFactory" deviceset="C" device="0603" value=".1uF"/>
<part name="C16" library="GadgetFactory" deviceset="C" device="0603" value=".1uF"/>
<part name="C17" library="GadgetFactory" deviceset="C" device="0603" value=".1uF"/>
<part name="C25" library="GadgetFactory" deviceset="C" device="0603" value=".1uF"/>
<part name="C30" library="GadgetFactory" deviceset="C" device="0603" value=".1uF"/>
<part name="C28" library="GadgetFactory" deviceset="C" device="0603" value=".1uF"/>
<part name="C22" library="GadgetFactory" deviceset="C" device="0603" value=".1uF"/>
<part name="C23" library="GadgetFactory" deviceset="C" device="0603" value=".1uF"/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="OSC" library="MondoCPLDCore-XC95_Rev1.5" deviceset="OSC1" device="" value="32Mhz"/>
<part name="C27" library="GadgetFactory" deviceset="C" device="0603" value=".1uF"/>
<part name="C3" library="Bogdan_Commons" deviceset="C" device="0805" value="0.1uF"/>
<part name="C12" library="Bogdan_Commons" deviceset="C" device="0805" value="27pF"/>
<part name="C6" library="Bogdan_Commons" deviceset="C" device="0805" value="27pF"/>
<part name="C7" library="Bogdan_Commons" deviceset="C" device="0805" value="0.1uF"/>
<part name="C5" library="Bogdan_Commons" deviceset="C" device="0805" value="0.1uF"/>
<part name="GND5" library="Bogdan_Commons" deviceset="GND" device=""/>
<part name="GND6" library="Bogdan_Commons" deviceset="GND" device=""/>
<part name="GND7" library="Bogdan_Commons" deviceset="GND" device=""/>
<part name="GND11" library="Bogdan_Commons" deviceset="GND" device=""/>
<part name="GND12" library="Bogdan_Commons" deviceset="GND" device=""/>
<part name="GND8" library="Bogdan_Commons" deviceset="GND" device=""/>
<part name="GND9" library="Bogdan_Commons" deviceset="GND" device=""/>
<part name="GND13" library="Bogdan_Commons" deviceset="GND" device=""/>
<part name="C9" library="Bogdan_Commons" deviceset="C" device="0805" value="0.1uF"/>
<part name="C4" library="Bogdan_Commons" deviceset="C" device="0805" value="0.1uF"/>
<part name="R1" library="Bogdan_Commons" deviceset="R" device="0805" value="10K"/>
<part name="R4" library="Bogdan_Commons" deviceset="R" device="0805" value="2K2"/>
<part name="R3" library="Bogdan_Commons" deviceset="R" device="0805" value="1K5"/>
<part name="R7" library="Bogdan_Commons" deviceset="R" device="0805" value="470R"/>
<part name="5V0_PWR1" library="Bogdan_Commons" deviceset="5V0" device=""/>
<part name="5V0_PWR2" library="Bogdan_Commons" deviceset="5V0" device=""/>
<part name="5V0_PWR3" library="Bogdan_Commons" deviceset="5V0" device=""/>
<part name="GND15" library="Bogdan_Commons" deviceset="GND" device=""/>
<part name="U1" library="Bogdan_Commons" deviceset="DIGITAL-EEPROM-MICROWIRE-6P" device="_SOT23" value="93C46B"/>
<part name="R6" library="Bogdan_Commons" deviceset="R" device="0805" value="27R"/>
<part name="R2" library="Bogdan_Commons" deviceset="R" device="0805" value="27R"/>
<part name="C11" library="Bogdan_Commons" deviceset="C" device="0805" value="0.1uF"/>
<part name="GND16" library="Bogdan_Commons" deviceset="GND" device=""/>
<part name="U3" library="Bogdan_Commons" deviceset="FT2232D" device=""/>
<part name="X1" library="crystal" deviceset="CRYSTAL" device="HC49S" value="6MHz"/>
<part name="DIR1" library="Bogdan_Commons" deviceset="SIGNAL-UNIDIRECTIONAL" device=""/>
<part name="DIR2" library="Bogdan_Commons" deviceset="SIGNAL-UNIDIRECTIONAL" device=""/>
<part name="DIR3" library="Bogdan_Commons" deviceset="SIGNAL-UNIDIRECTIONAL" device=""/>
<part name="DIR4" library="Bogdan_Commons" deviceset="SIGNAL-UNIDIRECTIONAL" device=""/>
<part name="DIR9" library="Bogdan_Commons" deviceset="SIGNAL-UNIDIRECTIONAL" device=""/>
<part name="DIR10" library="Bogdan_Commons" deviceset="SIGNAL-UNIDIRECTIONAL" device=""/>
<part name="USB" library="con-cypressindustries" deviceset="MINI-USB-" device="32005-201"/>
<part name="R8" library="GadgetFactory" deviceset="R" device="0805" value="68"/>
<part name="R11" library="GadgetFactory" deviceset="R" device="0805" value="68"/>
<part name="R10" library="GadgetFactory" deviceset="R" device="0805" value="68"/>
<part name="R13" library="GadgetFactory" deviceset="R" device="0805" value="220"/>
<part name="R12" library="GadgetFactory" deviceset="R" device="0805" value="220"/>
<part name="RX" library="led" deviceset="LED" device="CHIP-LED0805" value="Green"/>
<part name="TX" library="led" deviceset="LED" device="CHIP-LED0805" value="Green"/>
<part name="PWRSELECT" library="jumper" deviceset="JP2E" device=""/>
<part name="FRAME4" library="Bogdan_Commons" deviceset="FRAME-A4L-LOC" device=""/>
<part name="U7" library="GadgetFactory" deviceset="FLASH-SPI-25XX" device="SMD1"/>
<part name="U8" library="xilinx_devices" deviceset="6SLX9TQG144" device=""/>
<part name="GND_PWR1" library="Bogdan_Commons" deviceset="GND" device=""/>
<part name="GND_PWR2" library="Bogdan_Commons" deviceset="GND" device=""/>
<part name="3V3_PWR1" library="Bogdan_Commons" deviceset="3V3" device=""/>
<part name="GND_PWR3" library="Bogdan_Commons" deviceset="GND" device=""/>
<part name="GND_PWR4" library="Bogdan_Commons" deviceset="GND" device=""/>
<part name="3V3_PWR2" library="Bogdan_Commons" deviceset="3V3" device=""/>
<part name="1V2_PWR1" library="GadgetFactory" deviceset="1V2" device=""/>
<part name="3V3_PWR4" library="Bogdan_Commons" deviceset="3V3" device=""/>
<part name="3V3_PWR5" library="Bogdan_Commons" deviceset="3V3" device=""/>
<part name="3V3_PWR6" library="Bogdan_Commons" deviceset="3V3" device=""/>
<part name="3V3_PWR7" library="Bogdan_Commons" deviceset="3V3" device=""/>
<part name="R33" library="GadgetFactory" deviceset="R" device="0805" value="47K"/>
<part name="3V3_PWR3" library="GadgetFactory_all" deviceset="3V3" device=""/>
<part name="GND1" library="GadgetFactory_all" deviceset="GND" device=""/>
<part name="JP5" library="GadgetFactory_Pub - Copy" deviceset="STAND-OFF" device=""/>
<part name="JP2" library="GadgetFactory_Pub - Copy" deviceset="STAND-OFF" device=""/>
<part name="JP3" library="GadgetFactory_Pub - Copy" deviceset="STAND-OFF" device=""/>
<part name="SW1" library="adafruit" deviceset="EVQQ2" device=""/>
<part name="R37" library="GadgetFactory" deviceset="R" device="0805" value="390"/>
<part name="LED1" library="led" deviceset="LED" device="CHIPLED_0805" value="Green"/>
<part name="V6" library="supply2" deviceset="GND" device=""/>
<part name="JP4" library="jumper" deviceset="JP1E" device=""/>
<part name="V9" library="supply2" deviceset="GND" device=""/>
<part name="U5" library="Bogdan_Commons" deviceset="LTC3419" device="MS" technology="E"/>
<part name="L1" library="Bogdan_Commons" deviceset="L" device="_VLS3015" value="3.3uH"/>
<part name="L2" library="Bogdan_Commons" deviceset="L" device="_VLS3015" value="3.3uH"/>
<part name="C50" library="Bogdan_Commons" deviceset="C" device="0805" value="22u/6V3"/>
<part name="GND_PWR5" library="Bogdan_Commons" deviceset="GND" device=""/>
<part name="C49" library="Bogdan_Commons" deviceset="C" device="0805" value="10u/10V"/>
<part name="GND_PWR7" library="Bogdan_Commons" deviceset="GND" device=""/>
<part name="C48" library="Bogdan_Commons" deviceset="C" device="0805" value="22u/6V3"/>
<part name="GND_PWR8" library="Bogdan_Commons" deviceset="GND" device=""/>
<part name="R14" library="Bogdan_Commons" deviceset="R-SMALL" device="0603" value="6K8 1%"/>
<part name="R15" library="Bogdan_Commons" deviceset="R-SMALL" device="0603" value="1K5 1%"/>
<part name="R16" library="Bogdan_Commons" deviceset="R-SMALL" device="0603" value="1K5 1%"/>
<part name="R17" library="Bogdan_Commons" deviceset="R-SMALL" device="0603" value="1K5 1%"/>
<part name="GND_PWR9" library="Bogdan_Commons" deviceset="GND" device=""/>
<part name="GND_PWR10" library="Bogdan_Commons" deviceset="GND" device=""/>
<part name="5V0_PWR4" library="Bogdan_Commons" deviceset="5V0" device=""/>
<part name="3V3_PWR8" library="Bogdan_Commons" deviceset="3V3" device=""/>
<part name="1V2_PWR2" library="Bogdan_Commons" deviceset="1V2" device=""/>
<part name="3V3_PWR28" library="Bogdan_Commons" deviceset="3V3" device=""/>
<part name="R5" library="GadgetFactory" deviceset="R" device="0805" value="1K8"/>
<part name="PWR" library="led" deviceset="LED" device="CHIP-LED0805" value="Green"/>
<part name="V4" library="supply2" deviceset="GND" device=""/>
<part name="V3" library="supply2" deviceset="GND" device=""/>
<part name="PWRIN" library="jumper" deviceset="JP1E" device=""/>
<part name="FRAME1" library="SparkFun-Aesthetics" deviceset="FRAME-A4L" device=""/>
<part name="U$1" library="pipistrello_v2" deviceset="SAANLIMA_HDMI-TYPE-A-RECEPTICAL" device="FCI10029449"/>
<part name="SUPPLY1" library="dan-supply" deviceset="GND" device=""/>
<part name="SUPPLY2" library="dan-supply" deviceset="GND" device=""/>
<part name="SUPPLY3" library="dan-supply" deviceset="GND" device=""/>
<part name="SUPPLY4" library="dan-supply" deviceset="GND" device=""/>
<part name="Q1" library="dan-transistor" deviceset="MOSFET-N-GDS-D" device="-SOT323" value="FDV301N"/>
<part name="R9" library="dan-rcl" deviceset="R" device="0603" value="1K"/>
<part name="RN1" library="pipistrello_v2" deviceset="SAANLIMA_CTS742C083" device="" value="51"/>
<part name="RN2" library="pipistrello_v2" deviceset="SAANLIMA_CTS742C083" device="" value="51"/>
<part name="R18" library="dan-rcl" deviceset="R" device="0603" value="1K"/>
<part name="R19" library="dan-rcl" deviceset="R" device="0603" value="1K"/>
<part name="U$2" library="pipistrello_v2" deviceset="SAANLIMA_PCA9306" device="" value="PCA9306"/>
<part name="C1" library="dan-rcl" deviceset="C" device="0603" value="1uF"/>
<part name="R20" library="dan-rcl" deviceset="R" device="0603" value="100K"/>
<part name="U$3" library="rt_esd_diodes" deviceset="RCLAMP0524P" device=""/>
<part name="SUPPLY5" library="dan-supply" deviceset="GND" device=""/>
<part name="3V3_PWR9" library="Bogdan_Commons" deviceset="3V3" device=""/>
<part name="R21" library="dan-rcl" deviceset="R" device="0603" value="1K"/>
<part name="R22" library="dan-rcl" deviceset="R" device="0603" value="1K"/>
<part name="U$4" library="rt_esd_diodes" deviceset="RCLAMP0524P" device=""/>
<part name="SUPPLY6" library="dan-supply" deviceset="GND" device=""/>
<part name="D1" library="rt_esd_diodes" deviceset="RSA5M" device="" value="RSA5M"/>
<part name="SUPPLY7" library="dan-supply" deviceset="GND" device=""/>
<part name="IC1" library="dan-translate" deviceset="SN74AHC1G126" device="DBV"/>
<part name="SUPPLY8" library="dan-supply" deviceset="GND" device=""/>
<part name="3V3_PWR10" library="Bogdan_Commons" deviceset="3V3" device=""/>
<part name="R24" library="dan-rcl" deviceset="R" device="0805" value="PRG21BB150MB1RK"/>
<part name="R25" library="dan-rcl" deviceset="R" device="0603" value="0"/>
<part name="R26" library="dan-rcl" deviceset="R" device="0603" value="0 (DNP)"/>
<part name="D2" library="rt_esd_diodes" deviceset="RSA5M" device=""/>
<part name="5V0_PWR5" library="Bogdan_Commons" deviceset="5V0" device=""/>
<part name="SUPPLY10" library="dan-supply" deviceset="GND" device=""/>
<part name="SUPPLY11" library="dan-supply" deviceset="GND" device=""/>
<part name="U$5" library="pipistrello_v2" deviceset="SAANLIMA_HDMI-TYPE-A-RECEPTICAL" device="FCI10029449"/>
<part name="SUPPLY12" library="dan-supply" deviceset="GND" device=""/>
<part name="SUPPLY13" library="dan-supply" deviceset="GND" device=""/>
<part name="U$6" library="rt_esd_diodes" deviceset="RCLAMP0524P" device=""/>
<part name="U$7" library="rt_esd_diodes" deviceset="RCLAMP0524P" device=""/>
<part name="FRAME5" library="SparkFun-Aesthetics" deviceset="FRAME-LETTER" device=""/>
<part name="SUPPLY14" library="dan-supply" deviceset="GND" device=""/>
<part name="SUPPLY15" library="dan-supply" deviceset="GND" device=""/>
<part name="3V3_PWR11" library="Bogdan_Commons" deviceset="3V3" device=""/>
<part name="5V0_PWR6" library="Bogdan_Commons" deviceset="5V0" device=""/>
<part name="U2" library="rt_translate" deviceset="74HC1G04GW" device="" value="74HCT1G04GW"/>
<part name="5V0_PWR7" library="Bogdan_Commons" deviceset="5V0" device=""/>
<part name="SUPPLY9" library="dan-supply" deviceset="GND" device=""/>
<part name="IC2" library="rt_translate" deviceset="SN74AHC1G126" device="DBV"/>
<part name="3V3_PWR12" library="Bogdan_Commons" deviceset="3V3" device=""/>
<part name="3V3_PWR13" library="Bogdan_Commons" deviceset="3V3" device=""/>
<part name="SUPPLY16" library="dan-supply" deviceset="GND" device=""/>
<part name="USBSECTION" library="SparkFun-Aesthetics" deviceset="FRAME-A4L" device=""/>
<part name="FRAME2" library="SparkFun-Aesthetics" deviceset="FRAME-A4L" device=""/>
<part name="SUPPLY17" library="dan-supply" deviceset="GND" device=""/>
<part name="FRAME3" library="SparkFun-Aesthetics" deviceset="FRAME-A4L" device=""/>
<part name="SUPPLY18" library="dan-supply" deviceset="GND" device=""/>
<part name="J1" library="FCI_By_element14_Batch_1" deviceset="MEZZO-STACK-0.5MM" device=""/>
<part name="SUPPLY19" library="dan-supply" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="-35.56" y="-45.72" size="1.778" layer="91">Power Requirements</text>
<text x="-35.56" y="-48.26" size="1.778" layer="91">VCCINT requires 1.2V</text>
<text x="-35.56" y="-50.8" size="1.778" layer="91">VCCAUX requires 3.3V</text>
<text x="-35.56" y="-53.34" size="1.778" layer="91">VCCO's are 3.3V (no longer selectable)</text>
<text x="88.9" y="-53.34" size="2.54" layer="97">Power Section</text>
<text x="68.58" y="88.9" size="1.778" layer="98">delays 1V2 start</text>
</plain>
<instances>
<instance part="C24" gate="A" x="-63.5" y="40.64" rot="R270"/>
<instance part="C29" gate="A" x="-58.42" y="40.64" rot="R270"/>
<instance part="C19" gate="A" x="-53.34" y="40.64" rot="R270"/>
<instance part="C18" gate="A" x="-48.26" y="40.64" rot="R270"/>
<instance part="C21" gate="A" x="-43.18" y="40.64" rot="R270"/>
<instance part="C31" gate="A" x="-38.1" y="40.64" rot="R270"/>
<instance part="C26" gate="A" x="-33.02" y="40.64" rot="R270"/>
<instance part="C15" gate="A" x="-27.94" y="40.64" rot="R270"/>
<instance part="C16" gate="A" x="-22.86" y="40.64" rot="R270"/>
<instance part="C17" gate="A" x="-17.78" y="40.64" rot="R270"/>
<instance part="C25" gate="A" x="-10.16" y="40.64" rot="R270"/>
<instance part="C30" gate="A" x="5.08" y="40.64" rot="R270"/>
<instance part="C28" gate="A" x="12.7" y="40.64" rot="R270"/>
<instance part="C22" gate="A" x="20.32" y="40.64" rot="R270"/>
<instance part="C23" gate="A" x="27.94" y="40.64" rot="R270"/>
<instance part="GND2" gate="1" x="-43.18" y="30.48"/>
<instance part="C27" gate="A" x="35.56" y="40.64" rot="R270"/>
<instance part="FRAME4" gate="A" x="-129.54" y="-73.66"/>
<instance part="U8" gate="BGND" x="-50.8" y="-35.56"/>
<instance part="U8" gate="BVCCAUX" x="-22.86" y="-25.4"/>
<instance part="U8" gate="BVCCINT" x="5.08" y="-25.4"/>
<instance part="GND_PWR4" gate="A" x="-45.72" y="-58.42"/>
<instance part="3V3_PWR2" gate="A" x="-17.78" y="-12.7"/>
<instance part="1V2_PWR1" gate="A" x="10.16" y="-12.7"/>
<instance part="JP5" gate="G$1" x="27.94" y="-60.96"/>
<instance part="JP2" gate="G$1" x="27.94" y="-66.04"/>
<instance part="JP3" gate="G$1" x="27.94" y="-55.88"/>
<instance part="U5" gate="A" x="81.28" y="68.58"/>
<instance part="L1" gate="A" x="101.6" y="81.28"/>
<instance part="L2" gate="A" x="101.6" y="60.96"/>
<instance part="C50" gate="A" x="109.22" y="53.34" rot="R90"/>
<instance part="GND_PWR5" gate="A" x="109.22" y="45.72"/>
<instance part="C49" gate="A" x="58.42" y="63.5" rot="R90"/>
<instance part="GND_PWR7" gate="A" x="66.04" y="50.8"/>
<instance part="C48" gate="A" x="109.22" y="73.66" rot="R90"/>
<instance part="GND_PWR8" gate="A" x="109.22" y="66.04"/>
<instance part="R14" gate="1" x="101.6" y="76.2"/>
<instance part="R15" gate="1" x="96.52" y="71.12" rot="R90"/>
<instance part="R16" gate="1" x="101.6" y="55.88"/>
<instance part="R17" gate="1" x="96.52" y="50.8" rot="R90"/>
<instance part="GND_PWR9" gate="A" x="96.52" y="66.04"/>
<instance part="GND_PWR10" gate="A" x="96.52" y="45.72"/>
<instance part="5V0_PWR4" gate="A" x="58.42" y="73.66"/>
<instance part="3V3_PWR8" gate="A" x="109.22" y="86.36"/>
<instance part="1V2_PWR2" gate="A" x="116.84" y="66.04"/>
<instance part="3V3_PWR28" gate="A" x="66.04" y="86.36"/>
<instance part="R5" gate="A" x="83.82" y="15.24" rot="R90"/>
<instance part="PWR" gate="G$1" x="83.82" y="7.62"/>
<instance part="V4" gate="GND" x="83.82" y="-2.54" smashed="yes">
<attribute name="VALUE" x="85.725" y="-4.445" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="V3" gate="GND" x="48.26" y="2.54" smashed="yes">
<attribute name="VALUE" x="50.165" y="0.635" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="PWRIN" gate="A" x="63.5" y="10.16" rot="R270"/>
</instances>
<busses>
<bus name="GND">
<segment>
<wire x1="-63.5" y1="33.02" x2="40.64" y2="33.02" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="1V2">
<segment>
<wire x1="-43.18" y1="45.72" x2="-27.94" y2="45.72" width="0.762" layer="92"/>
<label x="-40.64" y="48.26" size="1.778" layer="95"/>
</segment>
</bus>
<bus name="3V3">
<segment>
<wire x1="35.56" y1="45.72" x2="40.64" y2="45.72" width="0.762" layer="92"/>
<label x="40.64" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-63.5" y1="45.72" x2="-48.26" y2="45.72" width="0.762" layer="92"/>
<label x="-60.96" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-22.86" y1="45.72" x2="-17.78" y2="45.72" width="0.762" layer="92"/>
<label x="-25.4" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-10.16" y1="45.72" x2="-2.54" y2="45.72" width="0.762" layer="92"/>
<label x="-10.16" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="5.08" y1="45.72" x2="12.7" y2="45.72" width="0.762" layer="92"/>
<label x="5.08" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="20.32" y1="45.72" x2="27.94" y2="45.72" width="0.762" layer="92"/>
<label x="20.32" y="48.26" size="1.778" layer="95"/>
</segment>
</bus>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<wire x1="-63.5" y1="33.02" x2="-63.5" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C24" gate="A" pin="2"/>
</segment>
<segment>
<wire x1="-58.42" y1="33.02" x2="-58.42" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C29" gate="A" pin="2"/>
</segment>
<segment>
<wire x1="-53.34" y1="33.02" x2="-53.34" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C19" gate="A" pin="2"/>
</segment>
<segment>
<wire x1="-48.26" y1="33.02" x2="-48.26" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C18" gate="A" pin="2"/>
</segment>
<segment>
<wire x1="-43.18" y1="33.02" x2="-43.18" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C21" gate="A" pin="2"/>
<pinref part="GND2" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="-38.1" y1="33.02" x2="-38.1" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C31" gate="A" pin="2"/>
</segment>
<segment>
<wire x1="-33.02" y1="33.02" x2="-33.02" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C26" gate="A" pin="2"/>
</segment>
<segment>
<wire x1="-27.94" y1="33.02" x2="-27.94" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C15" gate="A" pin="2"/>
</segment>
<segment>
<wire x1="-22.86" y1="33.02" x2="-22.86" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C16" gate="A" pin="2"/>
</segment>
<segment>
<wire x1="-17.78" y1="33.02" x2="-17.78" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C17" gate="A" pin="2"/>
</segment>
<segment>
<wire x1="-10.16" y1="33.02" x2="-10.16" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C25" gate="A" pin="2"/>
</segment>
<segment>
<wire x1="5.08" y1="33.02" x2="5.08" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C30" gate="A" pin="2"/>
</segment>
<segment>
<wire x1="12.7" y1="33.02" x2="12.7" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C28" gate="A" pin="2"/>
</segment>
<segment>
<wire x1="20.32" y1="33.02" x2="20.32" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C22" gate="A" pin="2"/>
</segment>
<segment>
<wire x1="27.94" y1="33.02" x2="27.94" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C23" gate="A" pin="2"/>
</segment>
<segment>
<wire x1="35.56" y1="33.02" x2="35.56" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C27" gate="A" pin="2"/>
</segment>
<segment>
<wire x1="-45.72" y1="-20.32" x2="-45.72" y2="-22.86" width="0.2032" layer="91"/>
<wire x1="-45.72" y1="-22.86" x2="-45.72" y2="-25.4" width="0.2032" layer="91"/>
<wire x1="-45.72" y1="-25.4" x2="-45.72" y2="-27.94" width="0.2032" layer="91"/>
<wire x1="-45.72" y1="-27.94" x2="-45.72" y2="-30.48" width="0.2032" layer="91"/>
<wire x1="-45.72" y1="-30.48" x2="-45.72" y2="-33.02" width="0.2032" layer="91"/>
<wire x1="-45.72" y1="-33.02" x2="-45.72" y2="-35.56" width="0.2032" layer="91"/>
<wire x1="-45.72" y1="-35.56" x2="-45.72" y2="-38.1" width="0.2032" layer="91"/>
<wire x1="-45.72" y1="-38.1" x2="-45.72" y2="-40.64" width="0.2032" layer="91"/>
<wire x1="-45.72" y1="-40.64" x2="-45.72" y2="-43.18" width="0.2032" layer="91"/>
<wire x1="-45.72" y1="-43.18" x2="-45.72" y2="-45.72" width="0.2032" layer="91"/>
<wire x1="-45.72" y1="-45.72" x2="-45.72" y2="-48.26" width="0.2032" layer="91"/>
<wire x1="-45.72" y1="-48.26" x2="-45.72" y2="-50.8" width="0.2032" layer="91"/>
<wire x1="-45.72" y1="-50.8" x2="-45.72" y2="-55.88" width="0.2032" layer="91"/>
<junction x="-45.72" y="-50.8"/>
<junction x="-45.72" y="-48.26"/>
<junction x="-45.72" y="-45.72"/>
<junction x="-45.72" y="-43.18"/>
<junction x="-45.72" y="-40.64"/>
<junction x="-45.72" y="-38.1"/>
<junction x="-45.72" y="-35.56"/>
<junction x="-45.72" y="-33.02"/>
<junction x="-45.72" y="-30.48"/>
<junction x="-45.72" y="-27.94"/>
<junction x="-45.72" y="-25.4"/>
<junction x="-45.72" y="-22.86"/>
<junction x="-45.72" y="-20.32"/>
<pinref part="U8" gate="BGND" pin="GND@0"/>
<pinref part="GND_PWR4" gate="A" pin="GND"/>
<pinref part="U8" gate="BGND" pin="GND@12"/>
<pinref part="U8" gate="BGND" pin="GND@11"/>
<pinref part="U8" gate="BGND" pin="GND@10"/>
<pinref part="U8" gate="BGND" pin="GND@9"/>
<pinref part="U8" gate="BGND" pin="GND@8"/>
<pinref part="U8" gate="BGND" pin="GND@7"/>
<pinref part="U8" gate="BGND" pin="GND@6"/>
<pinref part="U8" gate="BGND" pin="GND@5"/>
<pinref part="U8" gate="BGND" pin="GND@4"/>
<pinref part="U8" gate="BGND" pin="GND@3"/>
<pinref part="U8" gate="BGND" pin="GND@2"/>
<pinref part="U8" gate="BGND" pin="GND@1"/>
</segment>
<segment>
<wire x1="109.22" y1="50.8" x2="109.22" y2="48.26" width="0.1524" layer="91"/>
<pinref part="C50" gate="A" pin="1"/>
<pinref part="GND_PWR5" gate="A" pin="GND"/>
</segment>
<segment>
<wire x1="68.58" y1="58.42" x2="66.04" y2="58.42" width="0.4064" layer="91"/>
<wire x1="66.04" y1="58.42" x2="58.42" y2="58.42" width="0.4064" layer="91"/>
<wire x1="58.42" y1="58.42" x2="58.42" y2="60.96" width="0.4064" layer="91"/>
<wire x1="68.58" y1="55.88" x2="66.04" y2="55.88" width="0.4064" layer="91"/>
<wire x1="66.04" y1="55.88" x2="66.04" y2="58.42" width="0.4064" layer="91"/>
<wire x1="66.04" y1="53.34" x2="66.04" y2="55.88" width="0.4064" layer="91"/>
<junction x="66.04" y="58.42"/>
<junction x="66.04" y="55.88"/>
<pinref part="U5" gate="A" pin="GND@5"/>
<pinref part="C49" gate="A" pin="1"/>
<pinref part="U5" gate="A" pin="GND@6"/>
<pinref part="GND_PWR7" gate="A" pin="GND"/>
</segment>
<segment>
<wire x1="109.22" y1="71.12" x2="109.22" y2="68.58" width="0.1524" layer="91"/>
<pinref part="C48" gate="A" pin="1"/>
<pinref part="GND_PWR8" gate="A" pin="GND"/>
</segment>
<segment>
<pinref part="R15" gate="1" pin="1"/>
<pinref part="GND_PWR9" gate="A" pin="GND"/>
</segment>
<segment>
<pinref part="R17" gate="1" pin="1"/>
<pinref part="GND_PWR10" gate="A" pin="GND"/>
</segment>
<segment>
<wire x1="83.82" y1="0" x2="83.82" y2="2.54" width="0.1524" layer="91"/>
<pinref part="PWR" gate="G$1" pin="C"/>
<pinref part="V4" gate="GND" pin="GND"/>
</segment>
<segment>
<wire x1="40.64" y1="5.08" x2="48.26" y2="5.08" width="0.2032" layer="91"/>
<wire x1="40.64" y1="7.62" x2="48.26" y2="7.62" width="0.2032" layer="91"/>
<wire x1="48.26" y1="7.62" x2="48.26" y2="5.08" width="0.2032" layer="91"/>
<wire x1="48.26" y1="7.62" x2="60.96" y2="7.62" width="0.2032" layer="91"/>
<junction x="48.26" y="7.62"/>
<junction x="48.26" y="5.08"/>
<pinref part="V3" gate="GND" pin="GND"/>
<pinref part="PWRIN" gate="A" pin="2"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<wire x1="35.56" y1="43.18" x2="35.56" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C27" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="-63.5" y1="45.72" x2="-63.5" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C24" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="-58.42" y1="45.72" x2="-58.42" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C29" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="-53.34" y1="45.72" x2="-53.34" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C19" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="-48.26" y1="45.72" x2="-48.26" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C18" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="-17.78" y1="-30.48" x2="-17.78" y2="-27.94" width="0.2032" layer="91"/>
<wire x1="-17.78" y1="-27.94" x2="-17.78" y2="-25.4" width="0.2032" layer="91"/>
<wire x1="-17.78" y1="-25.4" x2="-17.78" y2="-22.86" width="0.2032" layer="91"/>
<wire x1="-17.78" y1="-22.86" x2="-17.78" y2="-20.32" width="0.2032" layer="91"/>
<wire x1="-17.78" y1="-20.32" x2="-17.78" y2="-15.24" width="0.2032" layer="91"/>
<junction x="-17.78" y="-20.32"/>
<junction x="-17.78" y="-22.86"/>
<junction x="-17.78" y="-25.4"/>
<junction x="-17.78" y="-27.94"/>
<junction x="-17.78" y="-30.48"/>
<pinref part="U8" gate="BVCCAUX" pin="VCCAUX@4"/>
<pinref part="3V3_PWR2" gate="A" pin="3V3"/>
<pinref part="U8" gate="BVCCAUX" pin="VCCAUX@0"/>
<pinref part="U8" gate="BVCCAUX" pin="VCCAUX@1"/>
<pinref part="U8" gate="BVCCAUX" pin="VCCAUX@2"/>
<pinref part="U8" gate="BVCCAUX" pin="VCCAUX@3"/>
</segment>
<segment>
<wire x1="-22.86" y1="45.72" x2="-22.86" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C16" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="-17.78" y1="45.72" x2="-17.78" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C17" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="5.08" y1="45.72" x2="5.08" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C30" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="12.7" y1="45.72" x2="12.7" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C28" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="20.32" y1="45.72" x2="20.32" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C22" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="27.94" y1="45.72" x2="27.94" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C23" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="-10.16" y1="45.72" x2="-10.16" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C25" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="109.22" y1="76.2" x2="109.22" y2="81.28" width="0.1524" layer="91"/>
<wire x1="109.22" y1="81.28" x2="106.68" y2="81.28" width="0.4064" layer="91"/>
<wire x1="109.22" y1="81.28" x2="104.14" y2="76.2" width="0.1524" layer="91"/>
<wire x1="109.22" y1="83.82" x2="109.22" y2="81.28" width="0.4064" layer="91"/>
<junction x="109.22" y="81.28"/>
<pinref part="L1" gate="A" pin="2"/>
<pinref part="C48" gate="A" pin="2"/>
<pinref part="R14" gate="1" pin="2"/>
<pinref part="3V3_PWR8" gate="A" pin="3V3"/>
</segment>
<segment>
<wire x1="68.58" y1="78.74" x2="66.04" y2="78.74" width="0.1524" layer="91"/>
<wire x1="66.04" y1="78.74" x2="66.04" y2="83.82" width="0.1524" layer="91"/>
<pinref part="U5" gate="A" pin="RUN2"/>
<pinref part="3V3_PWR28" gate="A" pin="3V3"/>
</segment>
</net>
<net name="1V2" class="0">
<segment>
<wire x1="-43.18" y1="45.72" x2="-43.18" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C21" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="-38.1" y1="45.72" x2="-38.1" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C31" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="-27.94" y1="45.72" x2="-27.94" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C15" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="-33.02" y1="45.72" x2="-33.02" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C26" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="10.16" y1="-30.48" x2="10.16" y2="-27.94" width="0.2032" layer="91"/>
<wire x1="10.16" y1="-27.94" x2="10.16" y2="-25.4" width="0.2032" layer="91"/>
<wire x1="10.16" y1="-25.4" x2="10.16" y2="-22.86" width="0.2032" layer="91"/>
<wire x1="10.16" y1="-22.86" x2="10.16" y2="-20.32" width="0.2032" layer="91"/>
<wire x1="10.16" y1="-20.32" x2="10.16" y2="-15.24" width="0.2032" layer="91"/>
<junction x="10.16" y="-30.48"/>
<junction x="10.16" y="-27.94"/>
<junction x="10.16" y="-25.4"/>
<junction x="10.16" y="-22.86"/>
<junction x="10.16" y="-20.32"/>
<pinref part="U8" gate="BVCCINT" pin="VCCINT@4"/>
<pinref part="1V2_PWR1" gate="A" pin="1V2"/>
<pinref part="U8" gate="BVCCINT" pin="VCCINT@3"/>
<pinref part="U8" gate="BVCCINT" pin="VCCINT@2"/>
<pinref part="U8" gate="BVCCINT" pin="VCCINT@1"/>
<pinref part="U8" gate="BVCCINT" pin="VCCINT@0"/>
</segment>
<segment>
<wire x1="106.68" y1="60.96" x2="109.22" y2="60.96" width="0.4064" layer="91"/>
<wire x1="109.22" y1="60.96" x2="109.22" y2="55.88" width="0.1524" layer="91"/>
<wire x1="109.22" y1="60.96" x2="104.14" y2="55.88" width="0.1524" layer="91"/>
<wire x1="109.22" y1="60.96" x2="116.84" y2="60.96" width="0.4064" layer="91"/>
<wire x1="116.84" y1="60.96" x2="116.84" y2="63.5" width="0.4064" layer="91"/>
<junction x="109.22" y="60.96"/>
<pinref part="L2" gate="A" pin="2"/>
<pinref part="C50" gate="A" pin="2"/>
<pinref part="R16" gate="1" pin="2"/>
<pinref part="1V2_PWR2" gate="A" pin="1V2"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<wire x1="68.58" y1="68.58" x2="63.5" y2="68.58" width="0.4064" layer="91"/>
<wire x1="63.5" y1="68.58" x2="63.5" y2="73.66" width="0.1524" layer="91"/>
<wire x1="63.5" y1="73.66" x2="63.5" y2="81.28" width="0.1524" layer="91"/>
<wire x1="63.5" y1="81.28" x2="68.58" y2="81.28" width="0.1524" layer="91"/>
<wire x1="68.58" y1="73.66" x2="63.5" y2="73.66" width="0.1524" layer="91"/>
<wire x1="63.5" y1="68.58" x2="58.42" y2="68.58" width="0.4064" layer="91"/>
<wire x1="58.42" y1="68.58" x2="58.42" y2="66.04" width="0.4064" layer="91"/>
<wire x1="58.42" y1="71.12" x2="58.42" y2="68.58" width="0.4064" layer="91"/>
<junction x="63.5" y="73.66"/>
<junction x="63.5" y="68.58"/>
<junction x="58.42" y="68.58"/>
<pinref part="U5" gate="A" pin="VIN"/>
<pinref part="U5" gate="A" pin="RUN1"/>
<pinref part="U5" gate="A" pin="MODE"/>
<pinref part="C49" gate="A" pin="2"/>
<pinref part="5V0_PWR4" gate="A" pin="5V0"/>
</segment>
<segment>
<wire x1="83.82" y1="20.32" x2="83.82" y2="22.86" width="0.2032" layer="91"/>
<label x="83.82" y="22.86" size="1.778" layer="95" rot="R90"/>
<pinref part="R5" gate="A" pin="2"/>
</segment>
</net>
<net name="VI" class="0">
<segment>
<wire x1="40.64" y1="10.16" x2="60.96" y2="10.16" width="0.2032" layer="91"/>
<label x="43.18" y="10.16" size="1.778" layer="95"/>
<pinref part="PWRIN" gate="A" pin="1"/>
</segment>
</net>
<net name="SW_3V3" class="0">
<segment>
<wire x1="96.52" y1="81.28" x2="93.98" y2="81.28" width="0.4064" layer="91"/>
<pinref part="U5" gate="A" pin="SW1"/>
<pinref part="L1" gate="A" pin="1"/>
</segment>
</net>
<net name="SW_1V2" class="0">
<segment>
<wire x1="93.98" y1="60.96" x2="96.52" y2="60.96" width="0.4064" layer="91"/>
<pinref part="L2" gate="A" pin="1"/>
<pinref part="U5" gate="A" pin="SW2"/>
</segment>
</net>
<net name="FB_1V2" class="0">
<segment>
<wire x1="99.06" y1="55.88" x2="96.52" y2="55.88" width="0.1524" layer="91"/>
<wire x1="96.52" y1="55.88" x2="93.98" y2="55.88" width="0.1524" layer="91"/>
<wire x1="96.52" y1="53.34" x2="96.52" y2="55.88" width="0.1524" layer="91"/>
<junction x="96.52" y="55.88"/>
<pinref part="R16" gate="1" pin="1"/>
<pinref part="U5" gate="A" pin="VFB2"/>
<pinref part="R17" gate="1" pin="2"/>
</segment>
</net>
<net name="FB_3V3" class="0">
<segment>
<wire x1="96.52" y1="76.2" x2="93.98" y2="76.2" width="0.1524" layer="91"/>
<wire x1="96.52" y1="73.66" x2="96.52" y2="76.2" width="0.1524" layer="91"/>
<wire x1="96.52" y1="76.2" x2="99.06" y2="76.2" width="0.1524" layer="91"/>
<junction x="96.52" y="76.2"/>
<pinref part="U5" gate="A" pin="VFB1"/>
<pinref part="R15" gate="1" pin="2"/>
<pinref part="R14" gate="1" pin="1"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="R5" gate="A" pin="1"/>
<pinref part="PWR" gate="G$1" pin="A"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="-134.62" y="-93.98"/>
<instance part="U$1" gate="G$1" x="121.92" y="15.24"/>
<instance part="SUPPLY1" gate="GND" x="-111.76" y="40.64" rot="R270"/>
<instance part="SUPPLY2" gate="GND" x="12.7" y="-86.36"/>
<instance part="SUPPLY3" gate="GND" x="-60.96" y="-71.12"/>
<instance part="SUPPLY4" gate="GND" x="96.52" y="-60.96"/>
<instance part="Q1" gate="G$1" x="10.16" y="-73.66"/>
<instance part="R9" gate="G$1" x="104.14" y="-45.72" rot="R270"/>
<instance part="RN1" gate="G$1" x="71.12" y="68.58" rot="R90"/>
<instance part="RN2" gate="G$1" x="48.26" y="68.58" rot="R90"/>
<instance part="R18" gate="G$1" x="-55.88" y="-33.02" rot="R90"/>
<instance part="R19" gate="G$1" x="-50.8" y="-33.02" rot="R90"/>
<instance part="U$2" gate="G$1" x="-83.82" y="-45.72"/>
<instance part="C1" gate="G$1" x="-60.96" y="-63.5" rot="R180"/>
<instance part="R20" gate="G$1" x="-50.8" y="-58.42"/>
<instance part="U$3" gate="G$1" x="-86.36" y="40.64"/>
<instance part="SUPPLY5" gate="GND" x="-101.6" y="-55.88"/>
<instance part="3V3_PWR9" gate="A" x="-121.92" y="-33.02"/>
<instance part="R21" gate="G$1" x="-104.14" y="-27.94" rot="R90"/>
<instance part="R22" gate="G$1" x="-109.22" y="-27.94" rot="R90"/>
<instance part="U$4" gate="G$1" x="-86.36" y="68.58"/>
<instance part="SUPPLY6" gate="GND" x="-111.76" y="68.58" rot="R270"/>
<instance part="D1" gate="G$1" x="33.02" y="-76.2" rot="R270"/>
<instance part="SUPPLY7" gate="GND" x="33.02" y="-86.36"/>
<instance part="IC1" gate="G$1" x="17.78" y="-33.02" rot="MR0"/>
<instance part="SUPPLY8" gate="GND" x="35.56" y="-40.64"/>
<instance part="3V3_PWR10" gate="A" x="-5.08" y="-25.4"/>
<instance part="SUPPLY15" gate="GND" x="124.46" y="-63.5"/>
<instance part="3V3_PWR11" gate="A" x="35.56" y="-25.4"/>
<instance part="5V0_PWR6" gate="A" x="-53.34" y="-17.78"/>
<instance part="U2" gate="G$1" x="-86.36" y="7.62"/>
<instance part="5V0_PWR7" gate="A" x="-68.58" y="15.24"/>
<instance part="SUPPLY9" gate="GND" x="-101.6" y="0"/>
<instance part="SUPPLY17" gate="GND" x="-119.38" y="-78.74"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DATA2SHIELD"/>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
<wire x1="106.68" y1="50.8" x2="96.52" y2="50.8" width="0.1524" layer="91"/>
<wire x1="96.52" y1="50.8" x2="96.52" y2="35.56" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="DATA1SHIELD"/>
<wire x1="96.52" y1="35.56" x2="96.52" y2="20.32" width="0.1524" layer="91"/>
<wire x1="96.52" y1="20.32" x2="96.52" y2="5.08" width="0.1524" layer="91"/>
<wire x1="96.52" y1="5.08" x2="96.52" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="96.52" y1="-25.4" x2="96.52" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="106.68" y1="35.56" x2="96.52" y2="35.56" width="0.1524" layer="91"/>
<junction x="96.52" y="35.56"/>
<pinref part="U$1" gate="G$1" pin="DATA0SHIELD"/>
<wire x1="106.68" y1="20.32" x2="96.52" y2="20.32" width="0.1524" layer="91"/>
<junction x="96.52" y="20.32"/>
<pinref part="U$1" gate="G$1" pin="CLOCKSHIELD"/>
<wire x1="106.68" y1="5.08" x2="96.52" y2="5.08" width="0.1524" layer="91"/>
<junction x="96.52" y="5.08"/>
<pinref part="U$1" gate="G$1" pin="DDC/CEC-GRND"/>
<wire x1="106.68" y1="-25.4" x2="96.52" y2="-25.4" width="0.1524" layer="91"/>
<junction x="96.52" y="-25.4"/>
</segment>
<segment>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="-60.96" y1="-68.58" x2="-60.96" y2="-66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="GND3"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
<wire x1="-99.06" y1="40.64" x2="-109.22" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="GND"/>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
<wire x1="-99.06" y1="-48.26" x2="-101.6" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="-101.6" y1="-48.26" x2="-101.6" y2="-53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="GND3"/>
<pinref part="SUPPLY6" gate="GND" pin="GND"/>
<wire x1="-99.06" y1="68.58" x2="-109.22" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY7" gate="GND" pin="GND"/>
<pinref part="D1" gate="G$1" pin="2"/>
<wire x1="33.02" y1="-83.82" x2="33.02" y2="-81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q1" gate="G$1" pin="S"/>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
<wire x1="12.7" y1="-81.28" x2="12.7" y2="-83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GND"/>
<wire x1="30.48" y1="-35.56" x2="35.56" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="SUPPLY8" gate="GND" pin="GND"/>
<wire x1="35.56" y1="-35.56" x2="35.56" y2="-38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="SH4"/>
<wire x1="132.08" y1="-50.8" x2="132.08" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-55.88" x2="127" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="SUPPLY15" gate="GND" pin="GND"/>
<wire x1="127" y1="-55.88" x2="124.46" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="124.46" y1="-55.88" x2="124.46" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="SH3"/>
<wire x1="127" y1="-50.8" x2="127" y2="-55.88" width="0.1524" layer="91"/>
<junction x="127" y="-55.88"/>
<pinref part="U$1" gate="G$1" pin="SH1"/>
<wire x1="116.84" y1="-50.8" x2="116.84" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="116.84" y1="-55.88" x2="121.92" y2="-55.88" width="0.1524" layer="91"/>
<junction x="124.46" y="-55.88"/>
<pinref part="U$1" gate="G$1" pin="SH2"/>
<wire x1="121.92" y1="-55.88" x2="124.46" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="121.92" y1="-50.8" x2="121.92" y2="-55.88" width="0.1524" layer="91"/>
<junction x="121.92" y="-55.88"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GND"/>
<wire x1="-99.06" y1="5.08" x2="-101.6" y2="5.08" width="0.1524" layer="91"/>
<wire x1="-101.6" y1="5.08" x2="-101.6" y2="2.54" width="0.1524" layer="91"/>
<pinref part="SUPPLY9" gate="GND" pin="GND"/>
</segment>
</net>
<net name="HDMI_RX_5V" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="+5V"/>
<wire x1="106.68" y1="-30.48" x2="86.36" y2="-30.48" width="0.1524" layer="91"/>
<label x="78.74" y="-33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="104.14" y1="-50.8" x2="104.14" y2="-55.88" width="0.1524" layer="91"/>
<label x="99.06" y="-58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="1"/>
<wire x1="33.02" y1="-68.58" x2="33.02" y2="-63.5" width="0.1524" layer="91"/>
<label x="27.94" y="-60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="HDMI_RX_HP" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DETECT"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="106.68" y1="-35.56" x2="104.14" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-35.56" x2="104.14" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-35.56" x2="86.36" y2="-35.56" width="0.1524" layer="91"/>
<junction x="104.14" y="-35.56"/>
<label x="78.74" y="-38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q1" gate="G$1" pin="D1"/>
<wire x1="12.7" y1="-66.04" x2="12.7" y2="-63.5" width="0.1524" layer="91"/>
<label x="5.08" y="-60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="A"/>
<wire x1="30.48" y1="-33.02" x2="45.72" y2="-33.02" width="0.1524" layer="91"/>
<label x="38.1" y="-35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<pinref part="RN1" gate="G$1" pin="5"/>
<wire x1="76.2" y1="76.2" x2="76.2" y2="81.28" width="0.1524" layer="91"/>
<wire x1="76.2" y1="81.28" x2="73.66" y2="81.28" width="0.1524" layer="91"/>
<pinref part="RN1" gate="G$1" pin="6"/>
<wire x1="73.66" y1="81.28" x2="71.12" y2="81.28" width="0.1524" layer="91"/>
<wire x1="71.12" y1="81.28" x2="68.58" y2="81.28" width="0.1524" layer="91"/>
<wire x1="68.58" y1="81.28" x2="53.34" y2="81.28" width="0.1524" layer="91"/>
<wire x1="53.34" y1="81.28" x2="50.8" y2="81.28" width="0.1524" layer="91"/>
<wire x1="50.8" y1="81.28" x2="48.26" y2="81.28" width="0.1524" layer="91"/>
<wire x1="48.26" y1="81.28" x2="45.72" y2="81.28" width="0.1524" layer="91"/>
<wire x1="45.72" y1="81.28" x2="27.94" y2="81.28" width="0.1524" layer="91"/>
<wire x1="73.66" y1="76.2" x2="73.66" y2="81.28" width="0.1524" layer="91"/>
<junction x="73.66" y="81.28"/>
<pinref part="RN1" gate="G$1" pin="7"/>
<wire x1="71.12" y1="76.2" x2="71.12" y2="81.28" width="0.1524" layer="91"/>
<junction x="71.12" y="81.28"/>
<pinref part="RN1" gate="G$1" pin="8"/>
<wire x1="68.58" y1="76.2" x2="68.58" y2="81.28" width="0.1524" layer="91"/>
<junction x="68.58" y="81.28"/>
<pinref part="RN2" gate="G$1" pin="5"/>
<wire x1="53.34" y1="76.2" x2="53.34" y2="81.28" width="0.1524" layer="91"/>
<junction x="53.34" y="81.28"/>
<pinref part="RN2" gate="G$1" pin="6"/>
<wire x1="50.8" y1="76.2" x2="50.8" y2="81.28" width="0.1524" layer="91"/>
<junction x="50.8" y="81.28"/>
<pinref part="RN2" gate="G$1" pin="7"/>
<wire x1="48.26" y1="76.2" x2="48.26" y2="81.28" width="0.1524" layer="91"/>
<junction x="48.26" y="81.28"/>
<pinref part="RN2" gate="G$1" pin="8"/>
<wire x1="45.72" y1="76.2" x2="45.72" y2="81.28" width="0.1524" layer="91"/>
<junction x="45.72" y="81.28"/>
<label x="27.94" y="78.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="VREF1"/>
<pinref part="3V3_PWR9" gate="A" pin="3V3"/>
<wire x1="-99.06" y1="-40.64" x2="-114.3" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="-114.3" y1="-40.64" x2="-121.92" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="-121.92" y1="-40.64" x2="-121.92" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-104.14" y1="-20.32" x2="-109.22" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="-20.32" x2="-114.3" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="-114.3" y1="-20.32" x2="-114.3" y2="-40.64" width="0.1524" layer="91"/>
<junction x="-114.3" y="-40.64"/>
<pinref part="R22" gate="G$1" pin="2"/>
<wire x1="-109.22" y1="-22.86" x2="-109.22" y2="-20.32" width="0.1524" layer="91"/>
<junction x="-109.22" y="-20.32"/>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="-104.14" y1="-22.86" x2="-104.14" y2="-20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="VCC"/>
<wire x1="5.08" y1="-30.48" x2="-5.08" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="3V3_PWR10" gate="A" pin="3V3"/>
<wire x1="-5.08" y1="-30.48" x2="-5.08" y2="-27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="OE"/>
<wire x1="30.48" y1="-30.48" x2="35.56" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="35.56" y1="-30.48" x2="35.56" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="3V3_PWR11" gate="A" pin="3V3"/>
</segment>
</net>
<net name="RX_TMDS2_P" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DATA2+"/>
<wire x1="106.68" y1="55.88" x2="53.34" y2="55.88" width="0.1524" layer="91"/>
<pinref part="RN2" gate="G$1" pin="4"/>
<wire x1="53.34" y1="55.88" x2="25.4" y2="55.88" width="0.1524" layer="91"/>
<wire x1="53.34" y1="63.5" x2="53.34" y2="55.88" width="0.1524" layer="91"/>
<junction x="53.34" y="55.88"/>
<label x="27.94" y="53.34" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="IO4B"/>
<wire x1="-71.12" y1="60.96" x2="-66.04" y2="60.96" width="0.1524" layer="91"/>
<label x="-66.04" y="60.96" size="1.778" layer="95"/>
<label x="-66.04" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="IO4A"/>
<wire x1="-99.06" y1="60.96" x2="-101.6" y2="60.96" width="0.1524" layer="91"/>
<label x="-119.38" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="RX_TMDS2_N" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DATA2-"/>
<wire x1="106.68" y1="45.72" x2="50.8" y2="45.72" width="0.1524" layer="91"/>
<pinref part="RN2" gate="G$1" pin="3"/>
<wire x1="50.8" y1="45.72" x2="25.4" y2="45.72" width="0.1524" layer="91"/>
<wire x1="50.8" y1="63.5" x2="50.8" y2="45.72" width="0.1524" layer="91"/>
<junction x="50.8" y="45.72"/>
<label x="27.94" y="48.26" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="IO3B"/>
<wire x1="-71.12" y1="63.5" x2="-66.04" y2="63.5" width="0.1524" layer="91"/>
<label x="-66.04" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="IO3A"/>
<wire x1="-99.06" y1="63.5" x2="-101.6" y2="63.5" width="0.1524" layer="91"/>
<label x="-119.38" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="RX_TMDS1_P" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DATA1+"/>
<wire x1="106.68" y1="40.64" x2="48.26" y2="40.64" width="0.1524" layer="91"/>
<pinref part="RN2" gate="G$1" pin="2"/>
<wire x1="48.26" y1="40.64" x2="25.4" y2="40.64" width="0.1524" layer="91"/>
<wire x1="48.26" y1="63.5" x2="48.26" y2="40.64" width="0.1524" layer="91"/>
<junction x="48.26" y="40.64"/>
<label x="27.94" y="38.1" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="IO2B"/>
<wire x1="-71.12" y1="73.66" x2="-66.04" y2="73.66" width="0.1524" layer="91"/>
<label x="-66.04" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="IO2A"/>
<wire x1="-99.06" y1="73.66" x2="-101.6" y2="73.66" width="0.1524" layer="91"/>
<label x="-119.38" y="73.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="RX_TMDS1_N" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DATA1-"/>
<wire x1="106.68" y1="30.48" x2="45.72" y2="30.48" width="0.1524" layer="91"/>
<pinref part="RN2" gate="G$1" pin="1"/>
<wire x1="45.72" y1="30.48" x2="25.4" y2="30.48" width="0.1524" layer="91"/>
<wire x1="45.72" y1="63.5" x2="45.72" y2="30.48" width="0.1524" layer="91"/>
<junction x="45.72" y="30.48"/>
<label x="27.94" y="33.02" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="IO1B"/>
<wire x1="-71.12" y1="76.2" x2="-66.04" y2="76.2" width="0.1524" layer="91"/>
<label x="-66.04" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="IO1A"/>
<wire x1="-99.06" y1="76.2" x2="-101.6" y2="76.2" width="0.1524" layer="91"/>
<label x="-119.38" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="RX_TMDS0_P" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DATA0+"/>
<wire x1="106.68" y1="25.4" x2="76.2" y2="25.4" width="0.1524" layer="91"/>
<pinref part="RN1" gate="G$1" pin="4"/>
<wire x1="76.2" y1="25.4" x2="25.4" y2="25.4" width="0.1524" layer="91"/>
<wire x1="76.2" y1="63.5" x2="76.2" y2="25.4" width="0.1524" layer="91"/>
<junction x="76.2" y="25.4"/>
<label x="27.94" y="22.86" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="IO4B"/>
<wire x1="-71.12" y1="33.02" x2="-66.04" y2="33.02" width="0.1524" layer="91"/>
<label x="-66.04" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="IO4A"/>
<wire x1="-99.06" y1="33.02" x2="-101.6" y2="33.02" width="0.1524" layer="91"/>
<label x="-119.38" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="RX_TMDS0_N" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DATA0-"/>
<wire x1="106.68" y1="15.24" x2="73.66" y2="15.24" width="0.1524" layer="91"/>
<pinref part="RN1" gate="G$1" pin="3"/>
<wire x1="73.66" y1="15.24" x2="25.4" y2="15.24" width="0.1524" layer="91"/>
<wire x1="73.66" y1="63.5" x2="73.66" y2="15.24" width="0.1524" layer="91"/>
<junction x="73.66" y="15.24"/>
<label x="27.94" y="17.78" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="IO3B"/>
<wire x1="-71.12" y1="35.56" x2="-66.04" y2="35.56" width="0.1524" layer="91"/>
<label x="-66.04" y="35.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="IO3A"/>
<wire x1="-99.06" y1="35.56" x2="-101.6" y2="35.56" width="0.1524" layer="91"/>
<label x="-119.38" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="RX_TMDS3_P" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="CLOCK+"/>
<wire x1="106.68" y1="10.16" x2="71.12" y2="10.16" width="0.1524" layer="91"/>
<pinref part="RN1" gate="G$1" pin="2"/>
<wire x1="71.12" y1="10.16" x2="25.4" y2="10.16" width="0.1524" layer="91"/>
<wire x1="71.12" y1="63.5" x2="71.12" y2="10.16" width="0.1524" layer="91"/>
<junction x="71.12" y="10.16"/>
<label x="27.94" y="7.62" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="IO2B"/>
<wire x1="-71.12" y1="45.72" x2="-66.04" y2="45.72" width="0.1524" layer="91"/>
<label x="-66.04" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="IO2A"/>
<wire x1="-99.06" y1="45.72" x2="-101.6" y2="45.72" width="0.1524" layer="91"/>
<label x="-119.38" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="RX_TMDS3_N" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="CLOCK-"/>
<wire x1="106.68" y1="0" x2="68.58" y2="0" width="0.1524" layer="91"/>
<pinref part="RN1" gate="G$1" pin="1"/>
<wire x1="68.58" y1="0" x2="25.4" y2="0" width="0.1524" layer="91"/>
<wire x1="68.58" y1="63.5" x2="68.58" y2="0" width="0.1524" layer="91"/>
<junction x="68.58" y="0"/>
<label x="27.94" y="2.54" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="IO1B"/>
<wire x1="-71.12" y1="48.26" x2="-66.04" y2="48.26" width="0.1524" layer="91"/>
<label x="-66.04" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="IO1A"/>
<wire x1="-99.06" y1="48.26" x2="-101.6" y2="48.26" width="0.1524" layer="91"/>
<label x="-119.38" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="CEC" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="CEC"/>
<wire x1="106.68" y1="-5.08" x2="25.4" y2="-5.08" width="0.1524" layer="91"/>
<label x="22.86" y="-5.08" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDC_SCL" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="SCL"/>
<wire x1="106.68" y1="-15.24" x2="86.36" y2="-15.24" width="0.1524" layer="91"/>
<label x="78.74" y="-17.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="SCL2"/>
<wire x1="-45.72" y1="-43.18" x2="-55.88" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="-55.88" y1="-43.18" x2="-68.58" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="-38.1" x2="-55.88" y2="-43.18" width="0.1524" layer="91"/>
<junction x="-55.88" y="-43.18"/>
<label x="-43.18" y="-43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDC_SDA" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="SDA"/>
<wire x1="106.68" y1="-20.32" x2="86.36" y2="-20.32" width="0.1524" layer="91"/>
<label x="78.74" y="-22.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="SDA2"/>
<wire x1="-45.72" y1="-45.72" x2="-50.8" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="-50.8" y1="-45.72" x2="-68.58" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="-38.1" x2="-50.8" y2="-45.72" width="0.1524" layer="91"/>
<junction x="-50.8" y="-45.72"/>
<label x="-43.18" y="-45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R20" gate="G$1" pin="1"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="-55.88" y1="-58.42" x2="-60.96" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="-58.42" x2="-60.96" y2="-40.64" width="0.1524" layer="91"/>
<junction x="-60.96" y="-58.42"/>
<pinref part="U$2" gate="G$1" pin="VREF2"/>
<wire x1="-60.96" y1="-40.64" x2="-68.58" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="-40.64" x2="-60.96" y2="-38.1" width="0.1524" layer="91"/>
<junction x="-60.96" y="-40.64"/>
<pinref part="U$2" gate="G$1" pin="EN"/>
<wire x1="-60.96" y1="-38.1" x2="-68.58" y2="-38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="FPGA_SCL" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="SCL1"/>
<label x="-129.54" y="-43.18" size="1.778" layer="95"/>
<wire x1="-99.06" y1="-43.18" x2="-109.22" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="-109.22" y1="-43.18" x2="-114.3" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="-43.18" x2="-109.22" y2="-33.02" width="0.1524" layer="91"/>
<junction x="-109.22" y="-43.18"/>
</segment>
</net>
<net name="FPGA_SDA" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="SDA1"/>
<label x="-129.54" y="-45.72" size="1.778" layer="95"/>
<wire x1="-99.06" y1="-45.72" x2="-104.14" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="-104.14" y1="-45.72" x2="-114.3" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="-104.14" y1="-33.02" x2="-104.14" y2="-45.72" width="0.1524" layer="91"/>
<junction x="-104.14" y="-45.72"/>
</segment>
</net>
<net name="FPGA_HP_CNTL" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="G"/>
<wire x1="5.08" y1="-76.2" x2="2.54" y2="-76.2" width="0.1524" layer="91"/>
<label x="2.54" y="-86.36" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="FPGA_RX_HP" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="Y"/>
<wire x1="5.08" y1="-35.56" x2="2.54" y2="-35.56" width="0.1524" layer="91"/>
<label x="-7.62" y="-38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="5V0" class="0">
<segment>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="-50.8" y1="-27.94" x2="-50.8" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="-25.4" x2="-53.34" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="-53.34" y1="-25.4" x2="-55.88" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="-25.4" x2="-55.88" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="-25.4" x2="-53.34" y2="-20.32" width="0.1524" layer="91"/>
<junction x="-53.34" y="-25.4"/>
<pinref part="5V0_PWR6" gate="A" pin="5V0"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="VCC"/>
<wire x1="-73.66" y1="10.16" x2="-68.58" y2="10.16" width="0.1524" layer="91"/>
<pinref part="5V0_PWR7" gate="A" pin="5V0"/>
<wire x1="-68.58" y1="10.16" x2="-68.58" y2="12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="EN_I2C" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="Y"/>
<wire x1="-73.66" y1="5.08" x2="-71.12" y2="5.08" width="0.1524" layer="91"/>
<label x="-71.12" y="5.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="-45.72" y1="-58.42" x2="-43.18" y2="-58.42" width="0.1524" layer="91"/>
<label x="-43.18" y="-58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="FPGA_EN_I2C" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="A"/>
<wire x1="-99.06" y1="7.62" x2="-101.6" y2="7.62" width="0.1524" layer="91"/>
<label x="-101.6" y="7.62" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
</plain>
<instances>
<instance part="R24" gate="G$1" x="-33.02" y="-66.04" rot="R180"/>
<instance part="R25" gate="G$1" x="-50.8" y="-63.5" rot="R180"/>
<instance part="R26" gate="G$1" x="-50.8" y="-71.12" rot="R180"/>
<instance part="D2" gate="G$1" x="-7.62" y="-76.2" rot="R270"/>
<instance part="5V0_PWR5" gate="A" x="-60.96" y="-55.88"/>
<instance part="SUPPLY10" gate="GND" x="-7.62" y="-86.36"/>
<instance part="SUPPLY11" gate="GND" x="-78.74" y="-5.08" rot="R270"/>
<instance part="U$5" gate="G$1" x="76.2" y="22.86"/>
<instance part="SUPPLY12" gate="GND" x="55.88" y="-48.26"/>
<instance part="SUPPLY13" gate="GND" x="78.74" y="-53.34"/>
<instance part="U$6" gate="G$1" x="-58.42" y="40.64"/>
<instance part="U$7" gate="G$1" x="-58.42" y="-5.08"/>
<instance part="FRAME5" gate="G$1" x="-114.3" y="-96.52"/>
<instance part="FRAME5" gate="G$2" x="33.02" y="-96.52"/>
<instance part="SUPPLY14" gate="GND" x="-78.74" y="40.64" rot="R270"/>
<instance part="IC2" gate="G$1" x="-25.4" y="-35.56"/>
<instance part="3V3_PWR12" gate="A" x="-40.64" y="-27.94"/>
<instance part="3V3_PWR13" gate="A" x="-5.08" y="-27.94"/>
<instance part="SUPPLY16" gate="GND" x="-40.64" y="-43.18"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$8" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="2"/>
<wire x1="-55.88" y1="-63.5" x2="-60.96" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="-63.5" x2="-60.96" y2="-60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="HDMI_RX_5V" class="0">
<segment>
<pinref part="R26" gate="G$1" pin="2"/>
<wire x1="-55.88" y1="-71.12" x2="-66.04" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="-66.04" y1="-71.12" x2="-66.04" y2="-68.58" width="0.1524" layer="91"/>
<label x="-71.12" y="-68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<wire x1="-60.96" y1="-58.42" x2="-60.96" y2="-60.96" width="0.4064" layer="91"/>
<pinref part="5V0_PWR5" gate="A" pin="5V0"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="1"/>
<wire x1="-45.72" y1="-63.5" x2="-38.1" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="R24" gate="G$1" pin="2"/>
<wire x1="-38.1" y1="-63.5" x2="-38.1" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="-66.04" x2="-38.1" y2="-71.12" width="0.1524" layer="91"/>
<junction x="-38.1" y="-66.04"/>
<pinref part="R26" gate="G$1" pin="1"/>
<wire x1="-38.1" y1="-71.12" x2="-45.72" y2="-71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="2"/>
<pinref part="SUPPLY10" gate="GND" pin="GND"/>
<wire x1="-7.62" y1="-81.28" x2="-7.62" y2="-83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="SH4"/>
<wire x1="86.36" y1="-43.18" x2="86.36" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-48.26" x2="81.28" y2="-48.26" width="0.1524" layer="91"/>
<pinref part="SUPPLY13" gate="GND" pin="GND"/>
<wire x1="81.28" y1="-48.26" x2="78.74" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-48.26" x2="78.74" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="U$5" gate="G$1" pin="SH3"/>
<wire x1="81.28" y1="-43.18" x2="81.28" y2="-48.26" width="0.1524" layer="91"/>
<junction x="81.28" y="-48.26"/>
<pinref part="U$5" gate="G$1" pin="SH2"/>
<wire x1="76.2" y1="-43.18" x2="76.2" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-48.26" x2="78.74" y2="-48.26" width="0.1524" layer="91"/>
<junction x="78.74" y="-48.26"/>
<pinref part="U$5" gate="G$1" pin="SH1"/>
<wire x1="71.12" y1="-43.18" x2="71.12" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-48.26" x2="76.2" y2="-48.26" width="0.1524" layer="91"/>
<junction x="76.2" y="-48.26"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="DATA2SHIELD"/>
<pinref part="SUPPLY12" gate="GND" pin="GND"/>
<wire x1="60.96" y1="58.42" x2="55.88" y2="58.42" width="0.1524" layer="91"/>
<wire x1="55.88" y1="58.42" x2="55.88" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U$5" gate="G$1" pin="DATA1SHIELD"/>
<wire x1="55.88" y1="43.18" x2="55.88" y2="27.94" width="0.1524" layer="91"/>
<wire x1="55.88" y1="27.94" x2="55.88" y2="12.7" width="0.1524" layer="91"/>
<wire x1="55.88" y1="12.7" x2="55.88" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-17.78" x2="55.88" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="60.96" y1="43.18" x2="55.88" y2="43.18" width="0.1524" layer="91"/>
<junction x="55.88" y="43.18"/>
<pinref part="U$5" gate="G$1" pin="DATA0SHIELD"/>
<wire x1="60.96" y1="27.94" x2="55.88" y2="27.94" width="0.1524" layer="91"/>
<junction x="55.88" y="27.94"/>
<pinref part="U$5" gate="G$1" pin="CLOCKSHIELD"/>
<wire x1="60.96" y1="12.7" x2="55.88" y2="12.7" width="0.1524" layer="91"/>
<junction x="55.88" y="12.7"/>
<pinref part="U$5" gate="G$1" pin="DDC/CEC-GRND"/>
<wire x1="60.96" y1="-17.78" x2="55.88" y2="-17.78" width="0.1524" layer="91"/>
<junction x="55.88" y="-17.78"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="GND3"/>
<pinref part="SUPPLY14" gate="GND" pin="GND"/>
<wire x1="-71.12" y1="40.64" x2="-76.2" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$7" gate="G$1" pin="GND3"/>
<pinref part="SUPPLY11" gate="GND" pin="GND"/>
<wire x1="-71.12" y1="-5.08" x2="-76.2" y2="-5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="GND"/>
<wire x1="-38.1" y1="-38.1" x2="-40.64" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="-38.1" x2="-40.64" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="SUPPLY16" gate="GND" pin="GND"/>
</segment>
</net>
<net name="TX_TMDS2_P" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="DATA2+"/>
<wire x1="60.96" y1="63.5" x2="38.1" y2="63.5" width="0.1524" layer="91"/>
<label x="33.02" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$7" gate="G$1" pin="IO4B"/>
<wire x1="-43.18" y1="-12.7" x2="-38.1" y2="-12.7" width="0.1524" layer="91"/>
<label x="-38.1" y="-12.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$7" gate="G$1" pin="IO4A"/>
<wire x1="-71.12" y1="-12.7" x2="-76.2" y2="-12.7" width="0.1524" layer="91"/>
<label x="-93.98" y="-12.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="TX_TMDS2_N" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="DATA2-"/>
<wire x1="60.96" y1="53.34" x2="38.1" y2="53.34" width="0.1524" layer="91"/>
<label x="33.02" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$7" gate="G$1" pin="IO3B"/>
<wire x1="-43.18" y1="-10.16" x2="-38.1" y2="-10.16" width="0.1524" layer="91"/>
<label x="-38.1" y="-10.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$7" gate="G$1" pin="IO3A"/>
<wire x1="-71.12" y1="-10.16" x2="-76.2" y2="-10.16" width="0.1524" layer="91"/>
<label x="-93.98" y="-10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="TX_TMDS1_P" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="DATA1+"/>
<wire x1="60.96" y1="48.26" x2="38.1" y2="48.26" width="0.1524" layer="91"/>
<label x="33.02" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$7" gate="G$1" pin="IO2B"/>
<wire x1="-43.18" y1="0" x2="-38.1" y2="0" width="0.1524" layer="91"/>
<label x="-38.1" y="0" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$7" gate="G$1" pin="IO2A"/>
<wire x1="-71.12" y1="0" x2="-76.2" y2="0" width="0.1524" layer="91"/>
<label x="-93.98" y="0" size="1.778" layer="95"/>
</segment>
</net>
<net name="TX_TMDS1_N" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="DATA1-"/>
<wire x1="60.96" y1="38.1" x2="38.1" y2="38.1" width="0.1524" layer="91"/>
<label x="33.02" y="35.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$7" gate="G$1" pin="IO1B"/>
<wire x1="-43.18" y1="2.54" x2="-38.1" y2="2.54" width="0.1524" layer="91"/>
<label x="-38.1" y="2.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$7" gate="G$1" pin="IO1A"/>
<wire x1="-71.12" y1="2.54" x2="-76.2" y2="2.54" width="0.1524" layer="91"/>
<label x="-93.98" y="2.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="TX_TMDS0_P" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="DATA0+"/>
<wire x1="60.96" y1="33.02" x2="38.1" y2="33.02" width="0.1524" layer="91"/>
<label x="33.02" y="30.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="IO4B"/>
<wire x1="-43.18" y1="33.02" x2="-38.1" y2="33.02" width="0.1524" layer="91"/>
<label x="-38.1" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="IO4A"/>
<wire x1="-71.12" y1="33.02" x2="-76.2" y2="33.02" width="0.1524" layer="91"/>
<label x="-93.98" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="TX_TMDS0_N" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="DATA0-"/>
<wire x1="60.96" y1="22.86" x2="38.1" y2="22.86" width="0.1524" layer="91"/>
<label x="33.02" y="20.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="IO3B"/>
<wire x1="-43.18" y1="35.56" x2="-38.1" y2="35.56" width="0.1524" layer="91"/>
<label x="-38.1" y="35.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="IO3A"/>
<wire x1="-71.12" y1="35.56" x2="-76.2" y2="35.56" width="0.1524" layer="91"/>
<label x="-93.98" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="TX_TMDS3_P" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="CLOCK+"/>
<wire x1="60.96" y1="17.78" x2="38.1" y2="17.78" width="0.1524" layer="91"/>
<label x="33.02" y="15.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="IO2B"/>
<wire x1="-43.18" y1="45.72" x2="-38.1" y2="45.72" width="0.1524" layer="91"/>
<label x="-38.1" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="IO2A"/>
<wire x1="-71.12" y1="45.72" x2="-76.2" y2="45.72" width="0.1524" layer="91"/>
<label x="-93.98" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="TX_TMDS3_N" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="CLOCK-"/>
<wire x1="60.96" y1="7.62" x2="38.1" y2="7.62" width="0.1524" layer="91"/>
<label x="33.02" y="5.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="IO1B"/>
<wire x1="-43.18" y1="48.26" x2="-38.1" y2="48.26" width="0.1524" layer="91"/>
<label x="-38.1" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="IO1A"/>
<wire x1="-71.12" y1="48.26" x2="-76.2" y2="48.26" width="0.1524" layer="91"/>
<label x="-93.98" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="CEC" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="CEC"/>
<wire x1="60.96" y1="2.54" x2="38.1" y2="2.54" width="0.1524" layer="91"/>
<label x="33.02" y="0" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDC_SCL" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="SCL"/>
<wire x1="60.96" y1="-7.62" x2="38.1" y2="-7.62" width="0.1524" layer="91"/>
<label x="33.02" y="-10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDC_SDA" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="SDA"/>
<wire x1="60.96" y1="-12.7" x2="38.1" y2="-12.7" width="0.1524" layer="91"/>
<label x="33.02" y="-15.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="HDMI_TX_5V" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="+5V"/>
<wire x1="60.96" y1="-22.86" x2="38.1" y2="-22.86" width="0.1524" layer="91"/>
<label x="33.02" y="-25.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R24" gate="G$1" pin="1"/>
<wire x1="-27.94" y1="-66.04" x2="-7.62" y2="-66.04" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="-66.04" x2="-7.62" y2="-68.58" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="-66.04" x2="-7.62" y2="-60.96" width="0.1524" layer="91"/>
<junction x="-7.62" y="-66.04"/>
<label x="-15.24" y="-58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="HDMI_TX_HP" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="DETECT"/>
<wire x1="60.96" y1="-27.94" x2="38.1" y2="-27.94" width="0.1524" layer="91"/>
<label x="33.02" y="-30.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="A"/>
<wire x1="-38.1" y1="-35.56" x2="-40.64" y2="-35.56" width="0.1524" layer="91"/>
<label x="-55.88" y="-35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="VCC"/>
<wire x1="-12.7" y1="-33.02" x2="-5.08" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="-33.02" x2="-5.08" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="3V3_PWR13" gate="A" pin="3V3"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="OE"/>
<wire x1="-38.1" y1="-33.02" x2="-40.64" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="-33.02" x2="-40.64" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="3V3_PWR12" gate="A" pin="3V3"/>
</segment>
</net>
<net name="FPGA_TX_HP" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="Y"/>
<wire x1="-12.7" y1="-38.1" x2="-10.16" y2="-38.1" width="0.1524" layer="91"/>
<label x="-7.62" y="-38.1" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="-50.8" y="-30.48" size="1.778" layer="91">93LC46B</text>
</plain>
<instances>
<instance part="C3" gate="A" x="-60.96" y="-20.32" rot="R90"/>
<instance part="C12" gate="A" x="-25.4" y="0"/>
<instance part="C6" gate="A" x="-25.4" y="-10.16"/>
<instance part="C7" gate="A" x="-20.32" y="22.86"/>
<instance part="C5" gate="A" x="-12.7" y="17.78"/>
<instance part="GND5" gate="A" x="-27.94" y="17.78" rot="R270"/>
<instance part="GND6" gate="A" x="-27.94" y="22.86" rot="R270"/>
<instance part="GND7" gate="A" x="-10.16" y="-43.18"/>
<instance part="GND11" gate="A" x="-35.56" y="-5.08" rot="R270"/>
<instance part="GND12" gate="A" x="-22.86" y="30.48"/>
<instance part="GND8" gate="A" x="-60.96" y="-27.94"/>
<instance part="GND9" gate="A" x="-60.96" y="-2.54"/>
<instance part="GND13" gate="A" x="-48.26" y="-2.54"/>
<instance part="C9" gate="A" x="-48.26" y="5.08" rot="R90"/>
<instance part="C4" gate="A" x="-22.86" y="38.1" rot="R90"/>
<instance part="R1" gate="A" x="-38.1" y="-35.56"/>
<instance part="R4" gate="A" x="-25.4" y="-27.94" rot="R90"/>
<instance part="R3" gate="A" x="-15.24" y="5.08"/>
<instance part="R7" gate="A" x="-15.24" y="33.02" rot="R90"/>
<instance part="5V0_PWR1" gate="A" x="-38.1" y="48.26"/>
<instance part="5V0_PWR2" gate="A" x="-60.96" y="-10.16"/>
<instance part="5V0_PWR3" gate="A" x="-48.26" y="-35.56" rot="R90"/>
<instance part="GND15" gate="A" x="-55.88" y="-27.94"/>
<instance part="U1" gate="1" x="-43.18" y="-20.32"/>
<instance part="R6" gate="A" x="-38.1" y="15.24"/>
<instance part="R2" gate="A" x="-38.1" y="7.62"/>
<instance part="C11" gate="A" x="-30.48" y="38.1" rot="R90"/>
<instance part="GND16" gate="A" x="-30.48" y="30.48"/>
<instance part="U3" gate="X" x="10.16" y="0"/>
<instance part="X1" gate="G$1" x="-15.24" y="-5.08" rot="R90"/>
<instance part="DIR1" gate="1" x="48.26" y="45.72"/>
<instance part="DIR2" gate="1" x="48.26" y="38.1"/>
<instance part="DIR3" gate="1" x="48.26" y="30.48" rot="R180"/>
<instance part="DIR4" gate="1" x="48.26" y="22.86"/>
<instance part="DIR9" gate="1" x="48.26" y="15.24"/>
<instance part="DIR10" gate="1" x="48.26" y="7.62" rot="R180"/>
<instance part="USB" gate="G$1" x="-66.04" y="10.16" rot="MR0"/>
<instance part="R8" gate="A" x="55.88" y="22.86"/>
<instance part="R11" gate="A" x="55.88" y="45.72"/>
<instance part="R10" gate="A" x="55.88" y="38.1"/>
<instance part="R13" gate="A" x="48.26" y="-30.48"/>
<instance part="R12" gate="A" x="48.26" y="-20.32"/>
<instance part="RX" gate="G$1" x="58.42" y="-20.32" rot="R270"/>
<instance part="TX" gate="G$1" x="58.42" y="-30.48" rot="R270"/>
<instance part="PWRSELECT" gate="1" x="-68.58" y="40.64" rot="R90"/>
<instance part="USBSECTION" gate="G$1" x="-132.08" y="-101.6"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<wire x1="-15.24" y1="17.78" x2="-25.4" y2="17.78" width="0.1524" layer="91"/>
<pinref part="C5" gate="A" pin="1"/>
<pinref part="GND5" gate="A" pin="GND"/>
</segment>
<segment>
<wire x1="-22.86" y1="22.86" x2="-25.4" y2="22.86" width="0.1524" layer="91"/>
<pinref part="C7" gate="A" pin="1"/>
<pinref part="GND6" gate="A" pin="GND"/>
</segment>
<segment>
<wire x1="-7.62" y1="-33.02" x2="-10.16" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="-35.56" x2="-10.16" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-35.56" x2="-10.16" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="-30.48" x2="-10.16" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-30.48" x2="-10.16" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="-38.1" x2="-10.16" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-38.1" x2="-10.16" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="-27.94" x2="-10.16" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-27.94" x2="-10.16" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="-25.4" x2="-10.16" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-25.4" x2="-10.16" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-40.64" x2="-10.16" y2="-38.1" width="0.1524" layer="91"/>
<junction x="-10.16" y="-30.48"/>
<junction x="-10.16" y="-33.02"/>
<junction x="-10.16" y="-35.56"/>
<junction x="-10.16" y="-27.94"/>
<junction x="-10.16" y="-38.1"/>
<pinref part="GND7" gate="A" pin="GND"/>
<pinref part="U3" gate="X" pin="GND@ANALOG"/>
<pinref part="U3" gate="X" pin="GND@9"/>
<pinref part="U3" gate="X" pin="GND@18"/>
<pinref part="U3" gate="X" pin="GND@25"/>
<pinref part="U3" gate="X" pin="GND@34"/>
<pinref part="U3" gate="X" pin="TEST"/>
</segment>
<segment>
<wire x1="-27.94" y1="-10.16" x2="-30.48" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="-10.16" x2="-30.48" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="-5.08" x2="-33.02" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="0" x2="-30.48" y2="0" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="-5.08" x2="-30.48" y2="0" width="0.1524" layer="91"/>
<junction x="-30.48" y="-5.08"/>
<pinref part="C6" gate="A" pin="1"/>
<pinref part="GND11" gate="A" pin="GND"/>
<pinref part="C12" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="-22.86" y1="35.56" x2="-22.86" y2="33.02" width="0.1524" layer="91"/>
<pinref part="GND12" gate="A" pin="GND"/>
<pinref part="C4" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="-60.96" y1="0" x2="-60.96" y2="2.54" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="2.54" x2="-60.96" y2="5.08" width="0.2032" layer="91"/>
<pinref part="GND9" gate="A" pin="GND"/>
<pinref part="USB" gate="G$1" pin="5"/>
</segment>
<segment>
<wire x1="-48.26" y1="0" x2="-48.26" y2="2.54" width="0.1524" layer="91"/>
<pinref part="GND13" gate="A" pin="GND"/>
<pinref part="C9" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="-53.34" y1="-22.86" x2="-55.88" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="-25.4" x2="-55.88" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="GND15" gate="A" pin="GND"/>
<pinref part="U1" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="-60.96" y1="-22.86" x2="-60.96" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="C3" gate="A" pin="1"/>
<pinref part="GND8" gate="A" pin="GND"/>
</segment>
<segment>
<wire x1="-30.48" y1="35.56" x2="-30.48" y2="33.02" width="0.1524" layer="91"/>
<pinref part="C11" gate="A" pin="1"/>
<pinref part="GND16" gate="A" pin="GND"/>
</segment>
</net>
<net name="TCK" class="0">
<segment>
<wire x1="60.96" y1="45.72" x2="68.58" y2="45.72" width="0.2032" layer="91"/>
<label x="66.04" y="45.72" size="1.778" layer="95"/>
<pinref part="R11" gate="A" pin="2"/>
</segment>
</net>
<net name="TDO" class="0">
<segment>
<wire x1="45.72" y1="33.02" x2="45.72" y2="30.48" width="0.1524" layer="91"/>
<wire x1="45.72" y1="30.48" x2="50.8" y2="30.48" width="0.1524" layer="91"/>
<wire x1="27.94" y1="33.02" x2="45.72" y2="33.02" width="0.1524" layer="91"/>
<label x="30.48" y="33.02" size="1.778" layer="95"/>
<pinref part="U3" gate="X" pin="ADBUS2"/>
</segment>
</net>
<net name="TDI" class="0">
<segment>
<wire x1="60.96" y1="38.1" x2="68.58" y2="38.1" width="0.2032" layer="91"/>
<label x="66.04" y="38.1" size="1.778" layer="95"/>
<pinref part="R10" gate="A" pin="2"/>
</segment>
</net>
<net name="TMS" class="0">
<segment>
<wire x1="60.96" y1="22.86" x2="68.58" y2="22.86" width="0.2032" layer="91"/>
<label x="66.04" y="22.86" size="1.778" layer="95"/>
<pinref part="R8" gate="A" pin="2"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<wire x1="-7.62" y1="27.94" x2="-10.16" y2="27.94" width="0.1524" layer="91"/>
<label x="-10.16" y="27.94" size="1.778" layer="95"/>
<pinref part="U3" gate="X" pin="VCCIOB"/>
</segment>
<segment>
<wire x1="-7.62" y1="30.48" x2="-10.16" y2="30.48" width="0.1524" layer="91"/>
<label x="-10.16" y="30.48" size="1.778" layer="95"/>
<pinref part="U3" gate="X" pin="VCCIOA"/>
</segment>
<segment>
<wire x1="27.94" y1="-33.02" x2="38.1" y2="-33.02" width="0.2032" layer="91"/>
<label x="30.48" y="-33.02" size="1.778" layer="95"/>
<pinref part="U3" gate="X" pin="SI/WUB"/>
</segment>
<segment>
<wire x1="27.94" y1="5.08" x2="38.1" y2="5.08" width="0.2032" layer="91"/>
<label x="30.48" y="5.08" size="1.778" layer="95"/>
<pinref part="U3" gate="X" pin="SI/WUA"/>
</segment>
<segment>
<wire x1="60.96" y1="-20.32" x2="68.58" y2="-20.32" width="0.2032" layer="91"/>
<label x="63.5" y="-20.32" size="1.778" layer="95"/>
<pinref part="RX" gate="G$1" pin="A"/>
</segment>
<segment>
<wire x1="60.96" y1="-30.48" x2="68.58" y2="-30.48" width="0.2032" layer="91"/>
<label x="63.5" y="-30.48" size="1.778" layer="95"/>
<pinref part="TX" gate="G$1" pin="A"/>
</segment>
</net>
<net name="USB_RXD" class="0">
<segment>
<wire x1="27.94" y1="-2.54" x2="45.72" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="45.72" y1="-2.54" x2="45.72" y2="7.62" width="0.1524" layer="91"/>
<wire x1="45.72" y1="7.62" x2="68.58" y2="7.62" width="0.1524" layer="91"/>
<label x="58.42" y="7.62" size="1.778" layer="95"/>
<pinref part="U3" gate="X" pin="BDBUS1"/>
</segment>
</net>
<net name="USB_TXD" class="0">
<segment>
<wire x1="27.94" y1="0" x2="43.18" y2="0" width="0.1524" layer="91"/>
<wire x1="43.18" y1="0" x2="43.18" y2="15.24" width="0.1524" layer="91"/>
<wire x1="43.18" y1="15.24" x2="68.58" y2="15.24" width="0.1524" layer="91"/>
<label x="58.42" y="15.24" size="1.778" layer="95"/>
<pinref part="U3" gate="X" pin="BDBUS0"/>
</segment>
</net>
<net name="N$15_A" class="0">
<segment>
<wire x1="-10.16" y1="5.08" x2="-7.62" y2="5.08" width="0.1524" layer="91"/>
<pinref part="R3" gate="A" pin="2"/>
<pinref part="U3" gate="X" pin="RSTOUT#"/>
</segment>
</net>
<net name="FILTERED_VCC_A" class="0">
<segment>
<wire x1="-15.24" y1="27.94" x2="-15.24" y2="22.86" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="22.86" x2="-7.62" y2="22.86" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="22.86" x2="-15.24" y2="22.86" width="0.1524" layer="91"/>
<junction x="-15.24" y="22.86"/>
<pinref part="C7" gate="A" pin="2"/>
<pinref part="R7" gate="A" pin="1"/>
<pinref part="U3" gate="X" pin="AVCC"/>
</segment>
</net>
<net name="EECS_A" class="0">
<segment>
<wire x1="-33.02" y1="-15.24" x2="-7.62" y2="-15.24" width="0.1524" layer="91"/>
<label x="-20.32" y="-15.24" size="1.778" layer="95"/>
<pinref part="U1" gate="1" pin="CS"/>
<pinref part="U3" gate="X" pin="EECS"/>
</segment>
</net>
<net name="EESK" class="0">
<segment>
<wire x1="-33.02" y1="-17.78" x2="-7.62" y2="-17.78" width="0.1524" layer="91"/>
<label x="-20.32" y="-17.78" size="1.778" layer="95"/>
<pinref part="U1" gate="1" pin="CLK"/>
<pinref part="U3" gate="X" pin="EESK"/>
</segment>
</net>
<net name="EEDATA_A" class="0">
<segment>
<wire x1="-33.02" y1="-20.32" x2="-25.4" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="-20.32" x2="-7.62" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="-22.86" x2="-25.4" y2="-20.32" width="0.1524" layer="91"/>
<junction x="-25.4" y="-20.32"/>
<label x="-20.32" y="-20.32" size="1.778" layer="95"/>
<pinref part="R4" gate="A" pin="2"/>
<pinref part="U1" gate="1" pin="DI"/>
<pinref part="U3" gate="X" pin="EEDATA"/>
</segment>
</net>
<net name="VBUS_A" class="0">
<segment>
<wire x1="-48.26" y1="7.62" x2="-48.26" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="15.24" x2="-48.26" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="15.24" x2="-48.26" y2="22.86" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="33.02" x2="-48.26" y2="22.86" width="0.2032" layer="91"/>
<wire x1="-48.26" y1="35.56" x2="-48.26" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="15.24" x2="-55.88" y2="15.24" width="0.2032" layer="91"/>
<wire x1="-66.04" y1="38.1" x2="-48.26" y2="38.1" width="0.2032" layer="91"/>
<wire x1="-48.26" y1="38.1" x2="-48.26" y2="35.56" width="0.2032" layer="91"/>
<junction x="-48.26" y="15.24"/>
<pinref part="C9" gate="A" pin="2"/>
<pinref part="USB" gate="G$1" pin="1"/>
<pinref part="PWRSELECT" gate="1" pin="1"/>
</segment>
</net>
<net name="3V3OUT_A" class="0">
<segment>
<wire x1="-10.16" y1="17.78" x2="-7.62" y2="17.78" width="0.1524" layer="91"/>
<pinref part="C5" gate="A" pin="2"/>
<pinref part="U3" gate="X" pin="3V3OUT"/>
</segment>
</net>
<net name="XTIN_A" class="0">
<segment>
<wire x1="-10.16" y1="0" x2="-10.16" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-2.54" x2="-7.62" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="0" x2="-15.24" y2="0" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="0" x2="-10.16" y2="0" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="-2.54" x2="-15.24" y2="0" width="0.1524" layer="91"/>
<junction x="-15.24" y="0"/>
<pinref part="C12" gate="A" pin="2"/>
<pinref part="U3" gate="X" pin="XTIN"/>
<pinref part="X1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="XTOUT_A" class="0">
<segment>
<wire x1="-10.16" y1="-10.16" x2="-10.16" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-7.62" x2="-7.62" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="-10.16" x2="-15.24" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="-10.16" x2="-10.16" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="-7.62" x2="-15.24" y2="-10.16" width="0.1524" layer="91"/>
<junction x="-15.24" y="-10.16"/>
<pinref part="C6" gate="A" pin="2"/>
<pinref part="U3" gate="X" pin="XTOUT"/>
<pinref part="X1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DO_A" class="0">
<segment>
<wire x1="-33.02" y1="-22.86" x2="-30.48" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="-22.86" x2="-30.48" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="-35.56" x2="-25.4" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="-35.56" x2="-25.4" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="-35.56" x2="-30.48" y2="-35.56" width="0.1524" layer="91"/>
<junction x="-30.48" y="-35.56"/>
<pinref part="R4" gate="A" pin="1"/>
<pinref part="R1" gate="A" pin="2"/>
<pinref part="U1" gate="1" pin="DO"/>
</segment>
</net>
<net name="D-" class="0">
<segment>
<wire x1="-45.72" y1="12.7" x2="-55.88" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="15.24" x2="-45.72" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="12.7" x2="-55.88" y2="12.7" width="0.2032" layer="91"/>
<pinref part="R6" gate="A" pin="1"/>
<pinref part="USB" gate="G$1" pin="2"/>
</segment>
</net>
<net name="D+" class="0">
<segment>
<wire x1="-45.72" y1="10.16" x2="-55.88" y2="10.16" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="10.16" x2="-43.18" y2="7.62" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="10.16" x2="-55.88" y2="10.16" width="0.2032" layer="91"/>
<pinref part="R2" gate="A" pin="1"/>
<pinref part="USB" gate="G$1" pin="3"/>
</segment>
</net>
<net name="USBDM_A" class="0">
<segment>
<wire x1="-30.48" y1="12.7" x2="-7.62" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="15.24" x2="-30.48" y2="12.7" width="0.1524" layer="91"/>
<pinref part="R6" gate="A" pin="2"/>
<pinref part="U3" gate="X" pin="USBDM"/>
</segment>
</net>
<net name="USBDP_A" class="0">
<segment>
<wire x1="-22.86" y1="10.16" x2="-7.62" y2="10.16" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="5.08" x2="-22.86" y2="5.08" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="5.08" x2="-22.86" y2="10.16" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="10.16" x2="-22.86" y2="10.16" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="10.16" x2="-33.02" y2="7.62" width="0.1524" layer="91"/>
<junction x="-22.86" y="10.16"/>
<pinref part="R3" gate="A" pin="1"/>
<pinref part="R2" gate="A" pin="2"/>
<pinref part="U3" gate="X" pin="USBDP"/>
</segment>
</net>
<net name="USB_TCK" class="0">
<segment>
<wire x1="50.8" y1="45.72" x2="43.18" y2="45.72" width="0.1524" layer="91"/>
<wire x1="43.18" y1="45.72" x2="43.18" y2="38.1" width="0.1524" layer="91"/>
<wire x1="43.18" y1="38.1" x2="27.94" y2="38.1" width="0.1524" layer="91"/>
<label x="30.48" y="38.1" size="1.778" layer="95"/>
<pinref part="U3" gate="X" pin="ADBUS0"/>
<pinref part="R11" gate="A" pin="1"/>
</segment>
</net>
<net name="USB_TDI" class="0">
<segment>
<wire x1="45.72" y1="35.56" x2="45.72" y2="38.1" width="0.1524" layer="91"/>
<wire x1="45.72" y1="38.1" x2="50.8" y2="38.1" width="0.1524" layer="91"/>
<wire x1="27.94" y1="35.56" x2="45.72" y2="35.56" width="0.1524" layer="91"/>
<label x="30.48" y="35.56" size="1.778" layer="95"/>
<pinref part="U3" gate="X" pin="ADBUS1"/>
<pinref part="R10" gate="A" pin="1"/>
</segment>
</net>
<net name="USB_TMS" class="0">
<segment>
<wire x1="50.8" y1="22.86" x2="43.18" y2="22.86" width="0.1524" layer="91"/>
<wire x1="43.18" y1="22.86" x2="43.18" y2="30.48" width="0.1524" layer="91"/>
<wire x1="43.18" y1="30.48" x2="27.94" y2="30.48" width="0.1524" layer="91"/>
<label x="30.48" y="30.48" size="1.778" layer="95"/>
<pinref part="U3" gate="X" pin="ADBUS3"/>
<pinref part="R8" gate="A" pin="1"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="R12" gate="A" pin="2"/>
<pinref part="RX" gate="G$1" pin="C"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="R13" gate="A" pin="2"/>
<pinref part="TX" gate="G$1" pin="C"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<wire x1="27.94" y1="-30.48" x2="43.18" y2="-30.48" width="0.2032" layer="91"/>
<pinref part="U3" gate="X" pin="BCBUS3"/>
<pinref part="R13" gate="A" pin="1"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<wire x1="27.94" y1="-27.94" x2="35.56" y2="-27.94" width="0.2032" layer="91"/>
<wire x1="35.56" y1="-27.94" x2="35.56" y2="-20.32" width="0.2032" layer="91"/>
<wire x1="35.56" y1="-20.32" x2="43.18" y2="-20.32" width="0.2032" layer="91"/>
<pinref part="U3" gate="X" pin="BCBUS2"/>
<pinref part="R12" gate="A" pin="1"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<wire x1="-66.04" y1="40.64" x2="-55.88" y2="40.64" width="0.2032" layer="91"/>
<label x="-66.04" y="40.64" size="1.778" layer="95"/>
<pinref part="PWRSELECT" gate="1" pin="2"/>
</segment>
<segment>
<wire x1="-60.96" y1="-12.7" x2="-60.96" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="-15.24" x2="-53.34" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="-17.78" x2="-60.96" y2="-15.24" width="0.1524" layer="91"/>
<junction x="-60.96" y="-15.24"/>
<pinref part="C3" gate="A" pin="2"/>
<pinref part="5V0_PWR2" gate="A" pin="5V0"/>
<pinref part="U1" gate="1" pin="VCC"/>
</segment>
<segment>
<wire x1="-43.18" y1="-35.56" x2="-45.72" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="R1" gate="A" pin="1"/>
<pinref part="5V0_PWR3" gate="A" pin="5V0"/>
</segment>
<segment>
<wire x1="-7.62" y1="33.02" x2="-10.16" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="35.56" x2="-10.16" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="35.56" x2="-10.16" y2="35.56" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="38.1" x2="-10.16" y2="35.56" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="38.1" x2="-10.16" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="40.64" x2="-22.86" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="45.72" x2="-38.1" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="43.18" x2="-30.48" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="40.64" x2="-30.48" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="43.18" x2="-22.86" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="43.18" x2="-15.24" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="43.18" x2="-15.24" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="43.18" x2="-10.16" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="43.18" x2="-10.16" y2="38.1" width="0.1524" layer="91"/>
<junction x="-10.16" y="35.56"/>
<junction x="-10.16" y="38.1"/>
<junction x="-30.48" y="43.18"/>
<junction x="-22.86" y="43.18"/>
<junction x="-15.24" y="43.18"/>
<pinref part="U3" gate="X" pin="RESET#"/>
<pinref part="U3" gate="X" pin="VCC@42"/>
<pinref part="U3" gate="X" pin="VCC@3"/>
<pinref part="R7" gate="A" pin="2"/>
<pinref part="C4" gate="A" pin="2"/>
<pinref part="5V0_PWR1" gate="A" pin="5V0"/>
<pinref part="C11" gate="A" pin="2"/>
</segment>
</net>
<net name="VI" class="0">
<segment>
<wire x1="-66.04" y1="43.18" x2="-55.88" y2="43.18" width="0.2032" layer="91"/>
<label x="-66.04" y="43.18" size="1.778" layer="95"/>
<pinref part="PWRSELECT" gate="1" pin="3"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<wire x1="68.58" y1="20.32" x2="68.58" y2="-22.86" width="0.2032" layer="97"/>
<wire x1="68.58" y1="-22.86" x2="132.08" y2="-22.86" width="0.2032" layer="97"/>
<wire x1="132.08" y1="-22.86" x2="132.08" y2="20.32" width="0.2032" layer="97"/>
<wire x1="132.08" y1="20.32" x2="68.58" y2="20.32" width="0.2032" layer="97"/>
<text x="71.12" y="-12.7" size="1.778" layer="91">JTAG Configuration</text>
<text x="71.12" y="-15.24" size="1.778" layer="91">HSWAP low pulls up all pins during configuration</text>
<text x="71.12" y="-17.78" size="1.778" layer="91">M0 high, M1 low puts FPGA in Master Serial / SPI mode</text>
<text x="71.12" y="-20.32" size="1.778" layer="91">PROG_B asserted causes a JTAG reconfiguration</text>
<text x="88.9" y="17.78" size="1.778" layer="91">JTAG</text>
<text x="71.12" y="-45.72" size="1.778" layer="97" rot="MR0">REBOOT</text>
<text x="-132.08" y="-83.82" size="1.016" layer="91">This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike License.</text>
<text x="-132.08" y="-86.36" size="1.016" layer="91">To view a copy of this license, visit http://creativecommons.org/ ; or, (b) send a letter to Creative Commons, 171 2nd Street, Suite 300, San Francisco, California, 94105, USA.</text>
</plain>
<instances>
<instance part="FRAME2" gate="G$1" x="-134.62" y="-88.9"/>
<instance part="OSC" gate="G$1" x="93.98" y="76.2"/>
<instance part="U7" gate="G$1" x="93.98" y="55.88"/>
<instance part="U8" gate="B0" x="-99.06" y="43.18" rot="MR180"/>
<instance part="U8" gate="B1" x="15.24" y="45.72" rot="MR0"/>
<instance part="U8" gate="B2" x="15.24" y="-38.1" rot="MR0"/>
<instance part="U8" gate="B3" x="-99.06" y="-40.64" rot="MR180"/>
<instance part="U8" gate="BNA" x="114.3" y="2.54" rot="R180"/>
<instance part="GND_PWR1" gate="A" x="-91.44" y="17.78" rot="MR270"/>
<instance part="GND_PWR2" gate="A" x="7.62" y="-33.02" rot="MR90"/>
<instance part="3V3_PWR1" gate="A" x="-5.08" y="-7.62" rot="MR0"/>
<instance part="GND_PWR3" gate="A" x="109.22" y="-5.08"/>
<instance part="3V3_PWR4" gate="A" x="-88.9" y="-73.66" rot="MR0"/>
<instance part="3V3_PWR5" gate="A" x="7.62" y="2.54" rot="MR0"/>
<instance part="3V3_PWR6" gate="A" x="5.08" y="78.74" rot="MR0"/>
<instance part="3V3_PWR7" gate="A" x="-88.9" y="10.16" rot="MR0"/>
<instance part="R33" gate="A" x="88.9" y="-43.18" smashed="yes" rot="MR90">
<attribute name="VALUE" x="85.598" y="-46.99" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="3V3_PWR3" gate="A" x="88.9" y="-35.56" rot="MR0"/>
<instance part="GND1" gate="A" x="73.66" y="-50.8" rot="MR0"/>
<instance part="SW1" gate="G$1" x="78.74" y="-45.72" rot="MR270"/>
<instance part="R37" gate="A" x="116.84" y="-36.83" smashed="yes" rot="R90">
<attribute name="VALUE" x="120.142" y="-40.64" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="LED1" gate="G$1" x="116.84" y="-44.45"/>
<instance part="V6" gate="GND" x="116.84" y="-53.34" smashed="yes">
<attribute name="VALUE" x="118.745" y="-55.245" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="JP4" gate="A" x="104.14" y="-48.26" rot="R270"/>
<instance part="V9" gate="GND" x="101.6" y="-53.34" smashed="yes">
<attribute name="VALUE" x="103.505" y="-55.245" size="1.778" layer="96" rot="R180"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<wire x1="104.14" y1="73.66" x2="114.3" y2="73.66" width="0.1524" layer="91"/>
<label x="114.3" y="73.66" size="1.778" layer="95" rot="MR0"/>
<pinref part="OSC" gate="G$1" pin="GND"/>
</segment>
<segment>
<wire x1="66.04" y1="50.8" x2="78.74" y2="50.8" width="0.1524" layer="91"/>
<label x="66.04" y="50.8" size="1.778" layer="95"/>
<pinref part="U7" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="U8" gate="B0" pin="IO_L1P_HSWAPEN_0"/>
<pinref part="GND_PWR1" gate="A" pin="GND"/>
</segment>
<segment>
<pinref part="U8" gate="B2" pin="IO_L13P_M1_2"/>
<pinref part="GND_PWR2" gate="A" pin="GND"/>
</segment>
<segment>
<pinref part="U8" gate="BNA" pin="SUSPEND"/>
<pinref part="GND_PWR3" gate="A" pin="GND"/>
</segment>
<segment>
<wire x1="83.82" y1="7.62" x2="93.98" y2="7.62" width="0.2032" layer="91"/>
<label x="93.98" y="7.62" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="73.66" y1="-48.26" x2="73.66" y2="-45.72" width="0.2032" layer="91"/>
<junction x="73.66" y="-48.26"/>
<junction x="73.66" y="-45.72"/>
<pinref part="SW1" gate="G$1" pin="S1"/>
<pinref part="GND1" gate="A" pin="GND"/>
<pinref part="SW1" gate="G$1" pin="S"/>
</segment>
<segment>
<wire x1="116.84" y1="-50.8" x2="116.84" y2="-49.53" width="0.1524" layer="91"/>
<pinref part="LED1" gate="G$1" pin="C"/>
<pinref part="V6" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="JP4" gate="A" pin="2"/>
<pinref part="V9" gate="GND" pin="GND"/>
</segment>
</net>
<net name="TCK" class="0">
<segment>
<wire x1="109.22" y1="0" x2="101.6" y2="0" width="0.2032" layer="91"/>
<label x="106.68" y="0" size="1.778" layer="95" rot="R180"/>
<pinref part="U8" gate="BNA" pin="TCK"/>
</segment>
<segment>
<wire x1="83.82" y1="5.08" x2="93.98" y2="5.08" width="0.2032" layer="91"/>
<label x="93.98" y="5.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="TDO" class="0">
<segment>
<wire x1="109.22" y1="5.08" x2="101.6" y2="5.08" width="0.2032" layer="91"/>
<label x="106.68" y="5.08" size="1.778" layer="95" rot="R180"/>
<pinref part="U8" gate="BNA" pin="TDO"/>
</segment>
<segment>
<wire x1="83.82" y1="2.54" x2="93.98" y2="2.54" width="0.2032" layer="91"/>
<label x="93.98" y="2.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="TDI" class="0">
<segment>
<wire x1="109.22" y1="2.54" x2="101.6" y2="2.54" width="0.2032" layer="91"/>
<label x="106.68" y="2.54" size="1.778" layer="95" rot="R180"/>
<pinref part="U8" gate="BNA" pin="TDI"/>
</segment>
<segment>
<wire x1="83.82" y1="0" x2="93.98" y2="0" width="0.2032" layer="91"/>
<label x="93.98" y="0" size="1.778" layer="95"/>
</segment>
</net>
<net name="TMS" class="0">
<segment>
<wire x1="109.22" y1="7.62" x2="101.6" y2="7.62" width="0.2032" layer="91"/>
<label x="106.68" y="7.62" size="1.778" layer="95" rot="R180"/>
<pinref part="U8" gate="BNA" pin="TMS"/>
</segment>
<segment>
<wire x1="83.82" y1="-2.54" x2="93.98" y2="-2.54" width="0.2032" layer="91"/>
<label x="93.98" y="-2.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<wire x1="83.82" y1="78.74" x2="76.2" y2="78.74" width="0.1524" layer="91"/>
<label x="76.2" y="78.74" size="1.778" layer="95"/>
<pinref part="OSC" gate="G$1" pin="VCC"/>
</segment>
<segment>
<wire x1="83.82" y1="73.66" x2="76.2" y2="73.66" width="0.1524" layer="91"/>
<label x="76.2" y="73.66" size="1.778" layer="95"/>
<pinref part="OSC" gate="G$1" pin="OE"/>
</segment>
<segment>
<wire x1="121.92" y1="58.42" x2="109.22" y2="58.42" width="0.1524" layer="91"/>
<label x="121.92" y="58.42" size="1.778" layer="95" rot="MR0"/>
<pinref part="U7" gate="G$1" pin="VCC"/>
</segment>
<segment>
<wire x1="78.74" y1="53.34" x2="66.04" y2="53.34" width="0.1524" layer="91"/>
<label x="66.04" y="53.34" size="1.778" layer="95"/>
<pinref part="U7" gate="G$1" pin="WP"/>
</segment>
<segment>
<wire x1="121.92" y1="55.88" x2="109.22" y2="55.88" width="0.1524" layer="91"/>
<label x="121.92" y="55.88" size="1.778" layer="95" rot="MR0"/>
<pinref part="U7" gate="G$1" pin="HOLD"/>
</segment>
<segment>
<wire x1="-5.08" y1="-10.16" x2="10.16" y2="-10.16" width="0.1524" layer="91"/>
<label x="-5.08" y="-10.16" size="1.778" layer="95"/>
<pinref part="U8" gate="B2" pin="IO_L1N_M0_CMPMISO_2"/>
<pinref part="3V3_PWR1" gate="A" pin="3V3"/>
</segment>
<segment>
<wire x1="-93.98" y1="-71.12" x2="-93.98" y2="-73.66" width="0.2032" layer="91"/>
<wire x1="-93.98" y1="-73.66" x2="-93.98" y2="-76.2" width="0.2032" layer="91"/>
<wire x1="-93.98" y1="-76.2" x2="-88.9" y2="-76.2" width="0.2032" layer="91"/>
<junction x="-93.98" y="-71.12"/>
<junction x="-93.98" y="-73.66"/>
<junction x="-93.98" y="-76.2"/>
<pinref part="U8" gate="B3" pin="VCCO_3@2"/>
<pinref part="3V3_PWR4" gate="A" pin="3V3"/>
<pinref part="U8" gate="B3" pin="VCCO_3@1"/>
<pinref part="U8" gate="B3" pin="VCCO_3@0"/>
</segment>
<segment>
<wire x1="10.16" y1="73.66" x2="10.16" y2="76.2" width="0.2032" layer="91"/>
<wire x1="10.16" y1="78.74" x2="10.16" y2="76.2" width="0.2032" layer="91"/>
<wire x1="10.16" y1="76.2" x2="5.08" y2="76.2" width="0.2032" layer="91"/>
<junction x="10.16" y="78.74"/>
<junction x="10.16" y="76.2"/>
<junction x="10.16" y="73.66"/>
<pinref part="U8" gate="B1" pin="VCCO_1@2"/>
<pinref part="U8" gate="B1" pin="VCCO_1@0"/>
<pinref part="U8" gate="B1" pin="VCCO_1@1"/>
<pinref part="3V3_PWR6" gate="A" pin="3V3"/>
</segment>
<segment>
<wire x1="-93.98" y1="12.7" x2="-93.98" y2="10.16" width="0.2032" layer="91"/>
<wire x1="-93.98" y1="10.16" x2="-93.98" y2="7.62" width="0.2032" layer="91"/>
<wire x1="-93.98" y1="7.62" x2="-88.9" y2="7.62" width="0.2032" layer="91"/>
<junction x="-93.98" y="12.7"/>
<junction x="-93.98" y="10.16"/>
<junction x="-93.98" y="7.62"/>
<pinref part="U8" gate="B0" pin="VCCO_0@2"/>
<pinref part="U8" gate="B0" pin="VCCO_0@1"/>
<pinref part="U8" gate="B0" pin="VCCO_0@0"/>
<pinref part="3V3_PWR7" gate="A" pin="3V3"/>
</segment>
<segment>
<wire x1="83.82" y1="10.16" x2="93.98" y2="10.16" width="0.2032" layer="91"/>
<label x="93.98" y="10.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R33" gate="A" pin="2"/>
<pinref part="3V3_PWR3" gate="A" pin="3V3"/>
</segment>
<segment>
<wire x1="10.16" y1="-2.54" x2="7.62" y2="-2.54" width="0.2032" layer="91"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="0" width="0.2032" layer="91"/>
<wire x1="7.62" y1="0" x2="10.16" y2="0" width="0.1524" layer="91"/>
<junction x="7.62" y="0"/>
<pinref part="U8" gate="B2" pin="VCCO_2@0"/>
<pinref part="U8" gate="B2" pin="VCCO_2@1"/>
<pinref part="3V3_PWR5" gate="A" pin="3V3"/>
</segment>
</net>
<net name="OSC_IN" class="0">
<segment>
<wire x1="104.14" y1="78.74" x2="114.3" y2="78.74" width="0.1524" layer="91"/>
<label x="114.3" y="78.74" size="1.778" layer="95" rot="MR0"/>
<pinref part="OSC" gate="G$1" pin="OUT"/>
</segment>
<segment>
<wire x1="-5.08" y1="50.8" x2="10.16" y2="50.8" width="0.1524" layer="91"/>
<label x="10.16" y="50.8" size="1.778" layer="95" rot="MR0"/>
<pinref part="U8" gate="B1" pin="IO_L40N_GCLK10_1"/>
</segment>
</net>
<net name="USB_RXD" class="0">
<segment>
<wire x1="-5.08" y1="68.58" x2="10.16" y2="68.58" width="0.1524" layer="91"/>
<label x="7.62" y="68.58" size="1.778" layer="95" rot="MR0"/>
<pinref part="U8" gate="B1" pin="IO_L1P_1"/>
</segment>
</net>
<net name="USB_TXD" class="0">
<segment>
<wire x1="-5.08" y1="66.04" x2="10.16" y2="66.04" width="0.1524" layer="91"/>
<label x="7.62" y="66.04" size="1.778" layer="95" rot="MR0"/>
<pinref part="U8" gate="B1" pin="IO_L32N_1"/>
</segment>
</net>
<net name="FLASH_SI" class="0">
<segment>
<wire x1="121.92" y1="50.8" x2="109.22" y2="50.8" width="0.1524" layer="91"/>
<label x="121.92" y="50.8" size="1.778" layer="95" rot="MR0"/>
<pinref part="U7" gate="G$1" pin="MOSI"/>
</segment>
<segment>
<wire x1="-5.08" y1="-20.32" x2="10.16" y2="-20.32" width="0.1524" layer="91"/>
<label x="-5.08" y="-20.32" size="1.778" layer="95"/>
<pinref part="U8" gate="B2" pin="IO_L3N_MOSI_CSI_B_MISO0_2"/>
</segment>
</net>
<net name="FLASH_SCK" class="0">
<segment>
<wire x1="121.92" y1="53.34" x2="109.22" y2="53.34" width="0.1524" layer="91"/>
<label x="121.92" y="53.34" size="1.778" layer="95" rot="MR0"/>
<pinref part="U7" gate="G$1" pin="SCK"/>
</segment>
<segment>
<wire x1="-5.08" y1="-12.7" x2="10.16" y2="-12.7" width="0.1524" layer="91"/>
<label x="-5.08" y="-12.7" size="1.778" layer="95"/>
<pinref part="U8" gate="B2" pin="IO_L1P_CCLK_2"/>
</segment>
</net>
<net name="FLASH_CS" class="0">
<segment>
<wire x1="78.74" y1="58.42" x2="68.58" y2="58.42" width="0.1524" layer="91"/>
<label x="66.04" y="58.42" size="1.778" layer="95"/>
<pinref part="U7" gate="G$1" pin="CS"/>
</segment>
<segment>
<wire x1="10.16" y1="-71.12" x2="-5.08" y2="-71.12" width="0.1524" layer="91"/>
<label x="-5.08" y="-71.12" size="1.778" layer="95"/>
<pinref part="U8" gate="B2" pin="IO_L65N_CSO_B_2"/>
</segment>
</net>
<net name="FLASH_SO" class="0">
<segment>
<wire x1="66.04" y1="55.88" x2="78.74" y2="55.88" width="0.1524" layer="91"/>
<label x="66.04" y="55.88" size="1.778" layer="95"/>
<pinref part="U7" gate="G$1" pin="MISO"/>
</segment>
<segment>
<wire x1="-5.08" y1="-22.86" x2="10.16" y2="-22.86" width="0.1524" layer="91"/>
<label x="-5.08" y="-22.86" size="1.778" layer="95"/>
<pinref part="U8" gate="B2" pin="IO_L3P_D0_DIN_MISO_MISO1_2"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="R37" gate="A" pin="1"/>
<pinref part="LED1" gate="G$1" pin="A"/>
</segment>
</net>
<net name="LED1" class="0">
<segment>
<wire x1="116.84" y1="-31.75" x2="116.84" y2="-29.21" width="0.1524" layer="91"/>
<label x="115.57" y="-31.75" size="1.778" layer="95" rot="R90"/>
<pinref part="R37" gate="A" pin="2"/>
</segment>
<segment>
<wire x1="-83.82" y1="78.74" x2="-93.98" y2="78.74" width="0.1524" layer="91"/>
<label x="-83.82" y="78.74" size="1.778" layer="95" rot="MR0"/>
<pinref part="U8" gate="B0" pin="IO_L66P_SCP1_0"/>
</segment>
</net>
<net name="REBOOT" class="0">
<segment>
<wire x1="101.6" y1="-48.26" x2="88.9" y2="-48.26" width="0.2032" layer="91"/>
<wire x1="88.9" y1="-48.26" x2="83.82" y2="-48.26" width="0.2032" layer="91"/>
<wire x1="83.82" y1="-48.26" x2="83.82" y2="-45.72" width="0.2032" layer="91"/>
<junction x="83.82" y="-45.72"/>
<junction x="88.9" y="-48.26"/>
<junction x="83.82" y="-48.26"/>
<label x="91.44" y="-48.26" size="1.778" layer="95"/>
<pinref part="SW1" gate="G$1" pin="P"/>
<pinref part="R33" gate="A" pin="1"/>
<pinref part="SW1" gate="G$1" pin="P1"/>
<pinref part="JP4" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="10.16" y1="-76.2" x2="-5.08" y2="-76.2" width="0.2032" layer="91"/>
<label x="-5.08" y="-76.2" size="1.778" layer="95"/>
<pinref part="U8" gate="B2" pin="PROGRAM_B_2"/>
</segment>
</net>
<net name="TX_TMDS3_N" class="0">
<segment>
<pinref part="U8" gate="B2" pin="IO_L31N_GCLK30_D15_2"/>
<wire x1="10.16" y1="-45.72" x2="5.08" y2="-45.72" width="0.1524" layer="91"/>
<label x="2.54" y="-45.72" size="1.778" layer="95" ratio="17" rot="R180"/>
</segment>
</net>
<net name="TX_TMDS3_P" class="0">
<segment>
<pinref part="U8" gate="B2" pin="IO_L31P_GCLK31_D14_2"/>
<wire x1="10.16" y1="-48.26" x2="5.08" y2="-48.26" width="0.1524" layer="91"/>
<label x="2.54" y="-48.26" size="1.778" layer="95" ratio="17" rot="R180"/>
</segment>
</net>
<net name="TX_TMDS0_N" class="0">
<segment>
<pinref part="U8" gate="B2" pin="IO_L30N_GCLK0_USERCCLK_2"/>
<wire x1="10.16" y1="-40.64" x2="5.08" y2="-40.64" width="0.1524" layer="91"/>
<label x="2.54" y="-40.64" size="1.778" layer="95" ratio="17" rot="R180"/>
</segment>
</net>
<net name="TX_TMDS0_P" class="0">
<segment>
<pinref part="U8" gate="B2" pin="IO_L30P_GCLK1_D13_2"/>
<wire x1="10.16" y1="-43.18" x2="5.08" y2="-43.18" width="0.1524" layer="91"/>
<label x="2.54" y="-43.18" size="1.778" layer="95" ratio="17" rot="R180"/>
</segment>
</net>
<net name="TX_TMDS1_N" class="0">
<segment>
<pinref part="U8" gate="B2" pin="IO_L14N_D12_2"/>
<wire x1="10.16" y1="-35.56" x2="5.08" y2="-35.56" width="0.1524" layer="91"/>
<label x="2.54" y="-35.56" size="1.778" layer="95" ratio="17" rot="R180"/>
</segment>
</net>
<net name="TX_TMDS1_P" class="0">
<segment>
<pinref part="U8" gate="B2" pin="IO_L14P_D11_2"/>
<wire x1="10.16" y1="-38.1" x2="5.08" y2="-38.1" width="0.1524" layer="91"/>
<label x="2.54" y="-38.1" size="1.778" layer="95" ratio="17" rot="R180"/>
</segment>
</net>
<net name="TX_TMDS2_N" class="0">
<segment>
<pinref part="U8" gate="B2" pin="IO_L12N_D2_MISO3_2"/>
<wire x1="10.16" y1="-25.4" x2="5.08" y2="-25.4" width="0.1524" layer="91"/>
<label x="2.54" y="-25.4" size="1.778" layer="95" ratio="17" rot="R180"/>
</segment>
</net>
<net name="TX_TMDS2_P" class="0">
<segment>
<pinref part="U8" gate="B2" pin="IO_L12P_D1_MISO2_2"/>
<wire x1="10.16" y1="-27.94" x2="5.08" y2="-27.94" width="0.1524" layer="91"/>
<label x="2.54" y="-27.94" size="1.778" layer="95" ratio="17" rot="R180"/>
</segment>
</net>
<net name="FPGA_TX_HP" class="0">
<segment>
<pinref part="U8" gate="B0" pin="IO_L66N_SCP0_0"/>
<wire x1="-93.98" y1="76.2" x2="-88.9" y2="76.2" width="0.1524" layer="91"/>
<label x="-88.9" y="76.2" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="FPGA_RX_HP" class="0">
<segment>
<pinref part="U8" gate="B0" pin="IO_L65P_SCP3_0"/>
<wire x1="-93.98" y1="73.66" x2="-88.9" y2="73.66" width="0.1524" layer="91"/>
<label x="-88.9" y="73.66" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="FPGA_EN_I2C" class="0">
<segment>
<pinref part="U8" gate="B0" pin="IO_L65N_SCP2_0"/>
<wire x1="-93.98" y1="71.12" x2="-88.9" y2="71.12" width="0.1524" layer="91"/>
<label x="-88.9" y="71.12" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="RX_TMDS2_P" class="0">
<segment>
<pinref part="U8" gate="B0" pin="IO_L34P_GCLK19_0"/>
<wire x1="-93.98" y1="38.1" x2="-88.9" y2="38.1" width="0.1524" layer="91"/>
<label x="-88.9" y="38.1" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="RX_TMDS2_N" class="0">
<segment>
<pinref part="U8" gate="B0" pin="IO_L34N_GCLK18_0"/>
<wire x1="-93.98" y1="35.56" x2="-88.9" y2="35.56" width="0.1524" layer="91"/>
<label x="-88.9" y="35.56" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="RX_TMDS1_P" class="0">
<segment>
<pinref part="U8" gate="B0" pin="IO_L35P_GCLK17_0"/>
<wire x1="-93.98" y1="43.18" x2="-88.9" y2="43.18" width="0.1524" layer="91"/>
<label x="-88.9" y="43.18" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="RX_TMDS1_N" class="0">
<segment>
<pinref part="U8" gate="B0" pin="IO_L35N_GCLK16_0"/>
<wire x1="-93.98" y1="40.64" x2="-88.9" y2="40.64" width="0.1524" layer="91"/>
<label x="-88.9" y="40.64" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="RX_TMDS0_P" class="0">
<segment>
<pinref part="U8" gate="B0" pin="IO_L36P_GCLK15_0"/>
<wire x1="-93.98" y1="48.26" x2="-88.9" y2="48.26" width="0.1524" layer="91"/>
<label x="-88.9" y="48.26" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="RX_TMDS0_N" class="0">
<segment>
<pinref part="U8" gate="B0" pin="IO_L36N_GCLK14_0"/>
<wire x1="-93.98" y1="45.72" x2="-88.9" y2="45.72" width="0.1524" layer="91"/>
<label x="-88.9" y="45.72" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="RX_TMDS3_P" class="0">
<segment>
<pinref part="U8" gate="B0" pin="IO_L37P_GCLK13_0"/>
<wire x1="-93.98" y1="53.34" x2="-88.9" y2="53.34" width="0.1524" layer="91"/>
<label x="-88.9" y="53.34" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="RX_TMDS3_N" class="0">
<segment>
<pinref part="U8" gate="B0" pin="IO_L37N_GCLK12_0"/>
<wire x1="-93.98" y1="50.8" x2="-88.9" y2="50.8" width="0.1524" layer="91"/>
<label x="-88.9" y="50.8" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="FPGA_SDA" class="0">
<segment>
<pinref part="U8" gate="B3" pin="IO_L37N_3"/>
<wire x1="-93.98" y1="-53.34" x2="-91.44" y2="-53.34" width="0.1524" layer="91"/>
<label x="-91.44" y="-53.34" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="FPGA_SCL" class="0">
<segment>
<pinref part="U8" gate="B3" pin="IO_L36P_3"/>
<wire x1="-93.98" y1="-55.88" x2="-91.44" y2="-55.88" width="0.1524" layer="91"/>
<label x="-91.44" y="-55.88" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN1" class="0">
<segment>
<pinref part="U8" gate="B3" pin="IO_L83P_3"/>
<wire x1="-93.98" y1="-5.08" x2="-91.44" y2="-5.08" width="0.1524" layer="91"/>
<label x="-91.44" y="-5.08" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN2" class="0">
<segment>
<pinref part="U8" gate="B3" pin="IO_L52P_3"/>
<wire x1="-93.98" y1="-10.16" x2="-91.44" y2="-10.16" width="0.1524" layer="91"/>
<label x="-91.44" y="-10.16" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN3" class="0">
<segment>
<pinref part="U8" gate="B3" pin="IO_L52N_3"/>
<wire x1="-93.98" y1="-12.7" x2="-91.44" y2="-12.7" width="0.1524" layer="91"/>
<label x="-91.44" y="-12.7" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN4" class="0">
<segment>
<pinref part="U8" gate="B3" pin="IO_L51P_3"/>
<wire x1="-93.98" y1="-15.24" x2="-91.44" y2="-15.24" width="0.1524" layer="91"/>
<label x="-91.44" y="-15.24" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN5" class="0">
<segment>
<pinref part="U8" gate="B3" pin="IO_L51N_3"/>
<wire x1="-93.98" y1="-17.78" x2="-91.44" y2="-17.78" width="0.1524" layer="91"/>
<label x="-91.44" y="-17.78" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN6" class="0">
<segment>
<pinref part="U8" gate="B3" pin="IO_L50P_3"/>
<wire x1="-93.98" y1="-20.32" x2="-91.44" y2="-20.32" width="0.1524" layer="91"/>
<label x="-91.44" y="-20.32" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN7" class="0">
<segment>
<pinref part="U8" gate="B3" pin="IO_L50N_3"/>
<wire x1="-93.98" y1="-22.86" x2="-91.44" y2="-22.86" width="0.1524" layer="91"/>
<label x="-91.44" y="-22.86" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN8" class="0">
<segment>
<pinref part="U8" gate="B3" pin="IO_L49P_3"/>
<wire x1="-93.98" y1="-25.4" x2="-91.44" y2="-25.4" width="0.1524" layer="91"/>
<label x="-91.44" y="-25.4" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN9" class="0">
<segment>
<pinref part="U8" gate="B3" pin="IO_L49N_3"/>
<wire x1="-93.98" y1="-27.94" x2="-91.44" y2="-27.94" width="0.1524" layer="91"/>
<label x="-91.44" y="-27.94" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN10" class="0">
<segment>
<pinref part="U8" gate="B3" pin="IO_L44P_GCLK21_3"/>
<wire x1="-93.98" y1="-30.48" x2="-91.44" y2="-30.48" width="0.1524" layer="91"/>
<label x="-91.44" y="-30.48" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN11" class="0">
<segment>
<pinref part="U8" gate="B3" pin="IO_L44N_GCLK20_3"/>
<wire x1="-93.98" y1="-33.02" x2="-91.44" y2="-33.02" width="0.1524" layer="91"/>
<label x="-91.44" y="-33.02" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN12" class="0">
<segment>
<pinref part="U8" gate="B3" pin="IO_L43P_GCLK23_3"/>
<wire x1="-93.98" y1="-35.56" x2="-91.44" y2="-35.56" width="0.1524" layer="91"/>
<label x="-91.44" y="-35.56" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN13" class="0">
<segment>
<pinref part="U8" gate="B3" pin="IO_L43N_GCLK22_IRDY2_3"/>
<wire x1="-93.98" y1="-38.1" x2="-91.44" y2="-38.1" width="0.1524" layer="91"/>
<label x="-91.44" y="-38.1" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN14" class="0">
<segment>
<pinref part="U8" gate="B3" pin="IO_L42P_GCLK25_TRDY2_3"/>
<wire x1="-93.98" y1="-40.64" x2="-91.44" y2="-40.64" width="0.1524" layer="91"/>
<label x="-91.44" y="-40.64" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN15" class="0">
<segment>
<pinref part="U8" gate="B3" pin="IO_L42N_GCLK24_3"/>
<wire x1="-93.98" y1="-43.18" x2="-91.44" y2="-43.18" width="0.1524" layer="91"/>
<label x="-91.44" y="-43.18" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN16" class="0">
<segment>
<pinref part="U8" gate="B3" pin="IO_L41P_GCLK27_3"/>
<wire x1="-93.98" y1="-45.72" x2="-91.44" y2="-45.72" width="0.1524" layer="91"/>
<label x="-91.44" y="-45.72" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN17" class="0">
<segment>
<pinref part="U8" gate="B3" pin="IO_L41N_GCLK26_3"/>
<wire x1="-93.98" y1="-48.26" x2="-91.44" y2="-48.26" width="0.1524" layer="91"/>
<label x="-91.44" y="-48.26" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
</plain>
<instances>
<instance part="FRAME3" gate="G$1" x="-134.62" y="-88.9"/>
<instance part="SUPPLY18" gate="GND" x="-71.12" y="-38.1"/>
<instance part="J1" gate="G$1" x="-50.8" y="5.08"/>
<instance part="SUPPLY19" gate="GND" x="-30.48" y="-38.1"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P5"/>
<wire x1="-63.5" y1="25.4" x2="-71.12" y2="25.4" width="0.1524" layer="91"/>
<pinref part="SUPPLY18" gate="GND" pin="GND"/>
<wire x1="-71.12" y1="25.4" x2="-71.12" y2="17.78" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="P11"/>
<wire x1="-71.12" y1="17.78" x2="-71.12" y2="10.16" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="10.16" x2="-71.12" y2="0" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="0" x2="-71.12" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="-10.16" x2="-71.12" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="-17.78" x2="-71.12" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="17.78" x2="-71.12" y2="17.78" width="0.1524" layer="91"/>
<junction x="-71.12" y="17.78"/>
<pinref part="J1" gate="G$1" pin="P17"/>
<wire x1="-63.5" y1="10.16" x2="-71.12" y2="10.16" width="0.1524" layer="91"/>
<junction x="-71.12" y="10.16"/>
<pinref part="J1" gate="G$1" pin="P23"/>
<wire x1="-63.5" y1="0" x2="-71.12" y2="0" width="0.1524" layer="91"/>
<junction x="-71.12" y="0"/>
<pinref part="J1" gate="G$1" pin="P31"/>
<wire x1="-63.5" y1="-10.16" x2="-71.12" y2="-10.16" width="0.1524" layer="91"/>
<junction x="-71.12" y="-10.16"/>
<pinref part="J1" gate="G$1" pin="P37"/>
<wire x1="-63.5" y1="-17.78" x2="-71.12" y2="-17.78" width="0.1524" layer="91"/>
<junction x="-71.12" y="-17.78"/>
</segment>
<segment>
<pinref part="SUPPLY19" gate="GND" pin="GND"/>
<wire x1="-30.48" y1="-35.56" x2="-30.48" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="P38"/>
<wire x1="-30.48" y1="-17.78" x2="-38.1" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="-17.78" x2="-30.48" y2="-10.16" width="0.1524" layer="91"/>
<junction x="-30.48" y="-17.78"/>
<pinref part="J1" gate="G$1" pin="P6"/>
<wire x1="-30.48" y1="-10.16" x2="-30.48" y2="0" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="0" x2="-30.48" y2="10.16" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="10.16" x2="-30.48" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="17.78" x2="-30.48" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="25.4" x2="-38.1" y2="25.4" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="P12"/>
<wire x1="-38.1" y1="17.78" x2="-30.48" y2="17.78" width="0.1524" layer="91"/>
<junction x="-30.48" y="17.78"/>
<pinref part="J1" gate="G$1" pin="P18"/>
<wire x1="-38.1" y1="10.16" x2="-30.48" y2="10.16" width="0.1524" layer="91"/>
<junction x="-30.48" y="10.16"/>
<pinref part="J1" gate="G$1" pin="P24"/>
<wire x1="-38.1" y1="0" x2="-30.48" y2="0" width="0.1524" layer="91"/>
<junction x="-30.48" y="0"/>
<pinref part="J1" gate="G$1" pin="P32"/>
<wire x1="-38.1" y1="-10.16" x2="-30.48" y2="-10.16" width="0.1524" layer="91"/>
<junction x="-30.48" y="-10.16"/>
</segment>
</net>
<net name="CN13" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P35"/>
<wire x1="-63.5" y1="-15.24" x2="-78.74" y2="-15.24" width="0.1524" layer="91"/>
<label x="-78.74" y="-15.24" size="1.778" layer="95" ratio="17" rot="R180"/>
</segment>
</net>
<net name="CN12" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P33"/>
<wire x1="-63.5" y1="-12.7" x2="-78.74" y2="-12.7" width="0.1524" layer="91"/>
<label x="-78.74" y="-12.7" size="1.778" layer="95" ratio="17" rot="R180"/>
</segment>
</net>
<net name="CN14" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P39"/>
<wire x1="-63.5" y1="-20.32" x2="-78.74" y2="-20.32" width="0.1524" layer="91"/>
<label x="-78.74" y="-20.32" size="1.778" layer="95" ratio="17" rot="R180"/>
</segment>
</net>
<net name="CN11" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P29"/>
<wire x1="-63.5" y1="-7.62" x2="-78.74" y2="-7.62" width="0.1524" layer="91"/>
<label x="-78.74" y="-7.62" size="1.778" layer="95" ratio="17" rot="R180"/>
</segment>
</net>
<net name="CN10" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P27"/>
<wire x1="-63.5" y1="-5.08" x2="-78.74" y2="-5.08" width="0.1524" layer="91"/>
<label x="-78.74" y="-5.08" size="1.778" layer="95" ratio="17" rot="R180"/>
</segment>
</net>
<net name="CN9" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P25"/>
<wire x1="-63.5" y1="-2.54" x2="-78.74" y2="-2.54" width="0.1524" layer="91"/>
<label x="-78.74" y="-2.54" size="1.778" layer="95" ratio="17" rot="R180"/>
</segment>
</net>
<net name="CN8" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P21"/>
<wire x1="-63.5" y1="2.54" x2="-78.74" y2="2.54" width="0.1524" layer="91"/>
<label x="-78.74" y="2.54" size="1.778" layer="95" ratio="17" rot="R180"/>
</segment>
</net>
<net name="CN7" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P19"/>
<wire x1="-63.5" y1="7.62" x2="-78.74" y2="7.62" width="0.1524" layer="91"/>
<label x="-78.74" y="7.62" size="1.778" layer="95" ratio="17" rot="R180"/>
</segment>
</net>
<net name="CN6" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P15"/>
<wire x1="-63.5" y1="12.7" x2="-78.74" y2="12.7" width="0.1524" layer="91"/>
<label x="-78.74" y="12.7" size="1.778" layer="95" ratio="17" rot="R180"/>
</segment>
</net>
<net name="CN5" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P13"/>
<wire x1="-63.5" y1="15.24" x2="-78.74" y2="15.24" width="0.1524" layer="91"/>
<label x="-78.74" y="15.24" size="1.778" layer="95" ratio="17" rot="R180"/>
</segment>
</net>
<net name="CN4" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P9"/>
<wire x1="-63.5" y1="20.32" x2="-78.74" y2="20.32" width="0.1524" layer="91"/>
<label x="-78.74" y="20.32" size="1.778" layer="95" ratio="17" rot="R180"/>
</segment>
</net>
<net name="CN3" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P7"/>
<wire x1="-63.5" y1="22.86" x2="-78.74" y2="22.86" width="0.1524" layer="91"/>
<label x="-78.74" y="22.86" size="1.778" layer="95" ratio="17" rot="R180"/>
</segment>
</net>
<net name="CN2" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P3"/>
<wire x1="-63.5" y1="27.94" x2="-78.74" y2="27.94" width="0.1524" layer="91"/>
<label x="-78.74" y="27.94" size="1.778" layer="95" ratio="17" rot="R180"/>
</segment>
</net>
<net name="CN1" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P1"/>
<wire x1="-63.5" y1="30.48" x2="-78.74" y2="30.48" width="0.1524" layer="91"/>
<label x="-78.74" y="30.48" size="1.778" layer="95" ratio="17" rot="R180"/>
</segment>
</net>
<net name="CN15" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P2"/>
<wire x1="-38.1" y1="30.48" x2="-27.94" y2="30.48" width="0.1524" layer="91"/>
<label x="-27.94" y="30.48" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN16" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P4"/>
<wire x1="-38.1" y1="27.94" x2="-27.94" y2="27.94" width="0.1524" layer="91"/>
<label x="-27.94" y="27.94" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN17" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P8"/>
<wire x1="-38.1" y1="22.86" x2="-27.94" y2="22.86" width="0.1524" layer="91"/>
<label x="-27.94" y="22.86" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN18" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P10"/>
<wire x1="-38.1" y1="20.32" x2="-27.94" y2="20.32" width="0.1524" layer="91"/>
<label x="-27.94" y="20.32" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN19" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P14"/>
<wire x1="-38.1" y1="15.24" x2="-27.94" y2="15.24" width="0.1524" layer="91"/>
<label x="-27.94" y="15.24" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN20" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P16"/>
<wire x1="-38.1" y1="12.7" x2="-27.94" y2="12.7" width="0.1524" layer="91"/>
<label x="-27.94" y="12.7" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN21" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P20"/>
<wire x1="-38.1" y1="7.62" x2="-27.94" y2="7.62" width="0.1524" layer="91"/>
<label x="-27.94" y="7.62" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN22" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P22"/>
<wire x1="-38.1" y1="2.54" x2="-27.94" y2="2.54" width="0.1524" layer="91"/>
<label x="-27.94" y="2.54" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN23" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P26"/>
<wire x1="-38.1" y1="-2.54" x2="-27.94" y2="-2.54" width="0.1524" layer="91"/>
<label x="-27.94" y="-2.54" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN24" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P28"/>
<wire x1="-38.1" y1="-5.08" x2="-27.94" y2="-5.08" width="0.1524" layer="91"/>
<label x="-27.94" y="-5.08" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN25" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P30"/>
<wire x1="-38.1" y1="-7.62" x2="-27.94" y2="-7.62" width="0.1524" layer="91"/>
<label x="-27.94" y="-7.62" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN26" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P34"/>
<wire x1="-38.1" y1="-12.7" x2="-27.94" y2="-12.7" width="0.1524" layer="91"/>
<label x="-27.94" y="-12.7" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN27" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P36"/>
<wire x1="-38.1" y1="-15.24" x2="-27.94" y2="-15.24" width="0.1524" layer="91"/>
<label x="-27.94" y="-15.24" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
<net name="CN28" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="P40"/>
<wire x1="-38.1" y1="-20.32" x2="-27.94" y2="-20.32" width="0.1524" layer="91"/>
<label x="-27.94" y="-20.32" size="1.778" layer="95" ratio="17"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
